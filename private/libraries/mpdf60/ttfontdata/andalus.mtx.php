<?php
$name='Andalus';
$type='TTF';
$desc=array (
  'CapHeight' => 688,
  'XHeight' => 510,
  'FontBBox' => '[-126 -421 1101 1105]',
  'Flags' => 4,
  'Ascent' => 1105,
  'Descent' => -421,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 500,
);
$unitsPerEm=2048;
$up=-366;
$ut=49;
$strp=259;
$strs=50;
$ttffile='D:/xampp/xampp/htdocs/ims/application/private/libraries/mpdf60/ttfonts/andlso.ttf';
$TTCfontID='0';
$originalsize=158956;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='andalus';
$panose=' 0 0 2 2 6 3 5 4 5 2 3 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 1105, -421, 0
// usWinAscent/usWinDescent = 1105, -421
// hhea Ascent/Descent/LineGap = 1105, -421, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>