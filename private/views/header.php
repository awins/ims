<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>IMS | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="/application/public/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/application/public/bootstrap/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="/application/public/bootstrap/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="/application/public/bootstrap/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="/application/public/bootstrap/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="/application/public/bootstrap/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="/application/public/bootstrap/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="/application/public/bootstrap/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="/application/public/bootstrap/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"  />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php 
    if ( isset($css)){
        foreach ($css as $value) { ?>
            <link href="<?php echo $value ?>" rel="stylesheet" type="text/css" title="compact"  />
            <?php
        }
    } ?>


    <style type="text/css">
        body{
          font-family: 'cambria';
          font-size: 10pt;

        }
        .table-hover tr td {
            cursor: pointer;
        }

        .table{
            border-collapse: collapse !important;

        }
        .table-bordered th, .table-bordered td {
            border: 1px solid black !important;


        }

        .table-bordered.black tr th {
            border: none;
            background-color: black;
            color: white;
            padding: 0;
        }        
      
    
    </style>

    
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      
      <header class="main-header" >
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS</b> System</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="/application/public/bootstrap/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $this->user->full_name ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="/application/public/bootstrap/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $this->user->full_name ?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar" >
        <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
              <!-- Sidebar user panel -->
                <!-- <div class="user-panel">
                    <div class="pull-left image">
                      <img src="/application/public/bootstrap/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                      <p>Administrator</p>

                      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div> -->
              <!-- search form -->
              <!-- <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                  <input type="text" name="q" class="form-control" placeholder="Search..."/>
                  <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </form> -->
              <!-- /.search form -->
              <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    
                    <li>
                        <a href="/home"><i class="fa fa-book"></i> <span>Dashboard</span></a>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/agent"><i class="fa fa-circle-o"></i>Agent</a></li>
                            <li><a href="/company"><i class="fa fa-circle-o"></i>Client Company</a></li>
                            <li><a href="/holiday"><i class="fa fa-circle-o"></i>Holiday</a></li>
                            
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Project</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/project"><i class="fa fa-circle-o"></i>Project List</a></li>
                            <li><a href="/project/input"><i class="fa fa-circle-o"></i>Input New Project </a></li>
                        </ul>
                    </li>
                    
<!--
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Purchase Order</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/purchase"><i class="fa fa-circle-o"></i> PO List</a></li>
                            <li><a href="/purchase/create"><i class="fa fa-circle-o"></i> Crete New PO </a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="/sales_order">
                            <i class="fa fa-dashboard"></i><span>Sales Order</span><i class="fa fa-angle-left pull-right"></i>
                        </a>
                    </li>        -->             
                    
                </ul>
            </section>
        <!-- /.sidebar -->
        </aside>

      

        
        