<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">SELECT AGENT</h4>
        </div>
        <div class="modal-body">
            <div class="row" >
                <div class="col-sm-12">
                    <table id="table-agent" class="table table-bordered" >
                        <thead>
                            <tr>
                                <th style="width: 5%">&nbsp;</th>
                                <th style="width: 25%">Name</th>
                                <th style="width: 25%">Company</th>
                                <th style="width: 25%">City</th>
                                <th style="width: 20%">Telp</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($agent as $value) { ?>
                                    <tr >
                                        <td class="filterable-cell" style="width: 5%">
                                            <input type="radio" name="radio_agent" id="radio_agent_<?php echo $value->id ?>" value="<?php echo $value->id ?>" class="check_agent" >
                                        </td>
                                        <td class="filterable-cell" style="width: 25%"  >
                                            <label for="radio_agent_<?php echo $value->id ?>" id="agent_name_<?php echo $value->id ?>"><?php echo $value->agent_name ?></label>
                                        </td>
                                        <td class="filterable-cell" style="width: 25%"  >
                                            <label for="radio_agent_<?php echo $value->id ?>" id="agent_company_<?php echo $value->id ?>"><?php echo $value->agent_company_name ?></label>
                                        </td>
                                        <td class="filterable-cell" style="width: 25%">
                                            <label for="radio_agent_<?php echo $value->id ?>" id="agent_city_<?php echo $value->id ?>"><?php echo $value->agent_city ?></label>
                                        </td>
                                        <td class="filterable-cell" style="width: 25%">
                                            <label for="radio_agent_<?php echo $value->id ?>" id="agent_telp_<?php echo $value->id ?>"><?php echo $value->agent_telp ?></label>
                                        </td>
                                    </tr>
                                    <?php
                                } ?>
                            </tbody>
                    </table>
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="select-agent">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>