<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">SELECT NACE</h4>
        </div>
        <div class="modal-body">
            <div class="row" >
                <div class="col-sm-12">
                    <table id="table-eac" class="table table table-bordered" >
                        <thead>
                            <tr>
                                <th style="width: 10%">Code</th>
                                <th style="width: 85%">Description</th>
                                <th style="width: 5%">Select</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($eac as $value) { ?>
                                    <tr >
                                        <td style="width: 10%"><?php echo $value->id ?></td>
                                        <td id="eac_desc_<?php echo $value->id ?>" style="width: 65%"><?php echo $value->eac_description ?></td>
                                        <td style="width: 5%">
                                            <label><input type="checkbox" value="" class="check_eac" rel="<?php echo $value->id ?>">&nbsp;</label>
                                        </td>
                                    </tr>
                                    <?php
                                } ?>
                            </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <textarea rows="2" class="form-control" id="text-eac" readonly ></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <textarea rows="2" class="form-control" id="text-eac-desc" readonly ></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="select-eac">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>