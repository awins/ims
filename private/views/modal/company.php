<?php 
foreach ($company as $value) { ?>
    <input type="hidden" id="company_address_1_<?php echo $value->company_id ?>" value="<?php echo $value->company_address_1 ?>" >
    <input type="hidden" id="company_address_2_<?php echo $value->company_id ?>" value="<?php echo $value->company_address_2 ?>" >
    <input type="hidden" id="company_zip_<?php echo $value->company_id ?>" value="<?php echo $value->company_zip ?>" >
    <input type="hidden" id="website_user_<?php echo $value->company_id ?>" value="<?php echo $value->website_user ?>" >
    <input type="hidden" id="website_password_<?php echo $value->company_id ?>" value="<?php echo $value->website_password ?>" >
    <input type="hidden" id="eac_<?php echo $value->company_id ?>" value="<?php echo $value->eac ?>" >
    <input type="hidden" id="nace_<?php echo $value->company_id ?>" value="<?php echo $value->nace ?>" >
    <input type="hidden" id="director_name_<?php echo $value->company_id ?>" value="<?php echo $value->director_name ?>" >
    <input type="hidden" id="company_phone_<?php echo $value->company_id ?>" value="<?php echo $value->company_phone ?>" >

    <input type="hidden" id="company_name_<?php echo $value->company_id ?>" value="<?php echo $value->company_name ?>" >
    <input type="hidden" id="company_city_<?php echo $value->company_id ?>" value="<?php echo $value->company_city ?>" >
    <input type="hidden" id="company_province_<?php echo $value->company_id ?>" value="<?php echo $value->company_province ?>" >
    <input type="hidden" id="agent_name_<?php echo $value->company_id ?>" value="<?php echo $value->agent_name ?>" >
    <?php
} ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">SELECT COMPANY</h4>
        </div>
        <div class="modal-body">
            <div class="row" >
                <div class="col-sm-12">
                    <table id="table-company" class="table table table-bordered" >
                        <thead>
                            <tr>
                                <th style="width: 5%">&nbsp;</th>
                                <th style="width: 35%">Name</th>
                                <th style="width: 20%">City</th>
                                <th style="width: 20%">Province</th>
                                <th style="width: 20%">Agent</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($company as $value) { ?>
                                    <tr >
                                        <td class="filterable-cell" style="width: 5%">
                                            <input type="radio" name="radio_company" id="radio_company_<?php echo $value->company_id ?>" value="<?php echo $value->company_id ?>" class="check_company" >
                                        </td>
                                        <td style="width: 35%">
                                            <label for="radio_company_<?php echo $value->company_id ?>">
                                                <?php echo $value->company_name ?>
                                            </label>
                                        </td>
                                        <td  style="width: 20%">
                                            <label for="radio_company_<?php echo $value->company_id ?>">
                                                <?php echo $value->company_city ?>
                                            </label>
                                        </td>
                                        <td style="width: 20%">
                                            <label for="radio_company_<?php echo $value->company_id ?>">
                                                <?php echo $value->company_province ?>
                                            </label>
                                        </td>
                                        <td style="width: 20%">
                                            <label for="radio_company_<?php echo $value->company_id ?>">
                                                <?php echo $value->agent_name ?>
                                            </label>
                                        </td>
                                    </tr>
                                    <?php
                                } ?>
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="select-company">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>