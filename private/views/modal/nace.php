<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">SELECT NACE</h4>
        </div>
        <div class="modal-body">
            <div class="row" >
                <div class="col-sm-12">
                    <table id="table-nace" class="table table table-bordered" >
                        <thead>
                            <tr>
                                <th style="width: 10%">Div</th>
                                <th style="width: 10%">Group</th>
                                <th style="width: 10%">Class</th>
                                <th style="width: 65%">Description</th>
                                <th style="width: 5%">Select</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $row = 0;
                                foreach ($nace as $value) { 
                                    $row++;
                                    switch ($value->level) {
                                        case 1: 
                                            $division = $value->code;
                                            $group = '';
                                            $class = '';
                                            $row_class = 'section';
                                            $element = "section";
                                            break;
                                        case 2: 
                                            $division = $value->code;
                                            $group = '';
                                            $class = '';
                                            $row_class = 'division section_'. $value->parent_id ;
                                            $element = "division";
                                            break;
                                        case 3: 
                                            $division = '';
                                            $group = $value->code;
                                            $class = '';
                                            $row_class = 'group division_'. $value->parent_id ;
                                            $element = "group";
                                            break;
                                        case 4: 
                                            $division = '';
                                            $group = '';
                                            $class = $value->code;
                                            $row_class = 'class group_'. $value->parent_id ;
                                            $element = "class";
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    } ?>
                                    
                                    <tr class="<?php echo $row_class ?>" rel="hide" id="<?php echo $element . '_' . $value->id ?>" elm="<?php echo $element ?>">
                                        <td class="filterable-cell" style="width: 10%"><?php echo $division ?></td>
                                        <td class="filterable-cell" style="width: 10%"><?php echo $group ?></td>
                                        <td class="filterable-cell class" style="width: 10%"><?php echo $class ?></td>
                                        <td class="filterable-cell description" style="width: 65%"><?php echo $value->description ?></td>
                                        <td class="filterable-cell" style="width: 5%">
                                            <?php 
                                            if ($value->level == 4){ ?>
                                                <label><input type="checkbox" value="<?php echo $class ?>" class="check_nace" rel="<?php echo $value->id ?>">&nbsp;</label>
                                                <?php 
                                            } ?>
                                        </td>
                                         
                                    </tr>
                                    <?php
                                } ?>
                            </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <textarea rows="2" class="form-control" id="text-nace" readonly ></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <textarea rows="2" class="form-control" id="text-nace-desc" readonly ></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="select-nace">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>