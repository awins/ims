<?php 
    $script['css'][] = '/application/public/css/agent.css';
    $script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.js';
	$script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.extensions.js';
    $script['js'][] = '/application/public/js/jquery.form.js';
    $script['js'][] = '/application/public/js/agent.js';

    $this->load->view('header',$script); 
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Update Agent
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/agent"><i class="fa fa-dashboard"></i> Agent</a></li>
            <li class="active">Update Agent</li>
        </ol>
    </section>

    <section class="content">
		<div class="row">
			<div class="col-sm-12">
				<form class="form-horizontal" role="form" id="form_agent_input" action="/agent/input" method="post" >
					<input type="hidden"  name="form_agent_input" value="1" >
					<input type="hidden"  name="id" value="<?php echo (isset($agent))?$agent->id: set_value('id') ?>" >
					
					<div class="panel panel-primary">
				    	<div class="panel-heading">Agent Detail</div>
				    	<div class="panel-body">
				    		<div class="col-md-12 col-sm-12">
					    		<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Name</label>
								    <div class="col-sm-9">
									    <input type="text" name="agent_name" class="form-control" value="<?php echo (isset($agent))?$agent->agent_name: set_value('agent_name') ?>" >
									    <?php echo form_error('agent_name') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Address</label>
								    <div class="col-sm-9">
								    	<textarea  name="agent_address" class="form-control"><?php echo (isset($agent))?$agent->agent_address: set_value('agent_address') ?></textarea>
									    
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Company</label>
								    <div class="col-sm-9">
									    <input type="text" name="agent_company_name" class="form-control" value="<?php echo (isset($agent))?$agent->agent_company_name: set_value('agent_company_name') ?>" >
									    <?php echo form_error('agent_company_name') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Company Address</label>
								    <div class="col-sm-9">
								    	<textarea  name="agent_company_address" class="form-control"><?php echo (isset($agent))?$agent->agent_company_address: set_value('agent_company_address') ?></textarea>
									    
								    </div>
								</div>

								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">City</label>
								    <div class="col-sm-9">
									    <input type="text" name="agent_city" class="form-control" value="<?php echo (isset($agent))?$agent->agent_city: set_value('agent_city') ?>" >
									    <?php echo form_error('agent_city') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Email</label>
								    <div class="col-sm-9">
									    <input type="text" name="agent_email" class="form-control" value="<?php echo (isset($agent))?$agent->agent_email: set_value('agent_email') ?>" >
									    <?php echo form_error('agent_email') ?>
								    </div>
								</div>
								
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Telp</label>
								    <div class="col-sm-9">
									    <input type="text" name="agent_telp" class="form-control" value="<?php echo (isset($agent))?$agent->agent_telp: set_value('agent_telp') ?>" >
									    <?php echo form_error('agent_telp') ?>
								    </div>
								</div>
								
				    		</div>
				    	</div>
				    </div>
				    <div class="row">
						<div class="col-sm-offset-6 col-sm-6" >
					        <button type="submit" class="btn btn-warning " id="btn-save"><i class="glyphicon glyphicon-floppy-disk"></i> SAVE</button>
					    </div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>

<?php
    $this->load->view('footer',$script); 
?>