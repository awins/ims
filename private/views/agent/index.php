
<?php 
    $header['css'] = array('/application/public/bootstrap/plugins/datatables/jquery.dataTables.css');
    $footer['js'][] = '/application/public/bootstrap/plugins/datatables/jquery.dataTables.js';
    $footer['js'][] = '/application/public/js/agent.js';

    $this->load->view('header',$header); 
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Agent List
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Agent</li>
        </ol>
    </section>

    <section class="content">
        
        <div class="row">
            <div class="col-sm-12">
                <a href="/agent/input">
                    <button class="btn btn-primary pull-right" type="button"><i class="glyphicon glyphicon-plus" ></i>&nbsp;&nbsp;&nbsp;Input New</button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-body">
                    <table id="table-agent" class="table table-bordered black">
                        <thead>
                          <tr>
                            <th style="width: 3%;  text-align: center" >No. </th>
                            <th style="width: 15%; text-align: center">Name</th>
                            <th style="width: 15%; text-align: center">Adress</th>
                            <th style="width: 20%; text-align: center">Company</th>
                            <th style="width: 20%; text-align: center">Email</th>
                            <th style="width: 10%; text-align: center">City</th>
                            <th style="width: 10%; text-align: center">Telp</th>
                            <th style="width: 7%; text-align: center">Action</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            
                            foreach ($agents as $value) { ?>
                                <tr rel="<?php echo $value->id ?>" >
                                    <td>
                                        <?php $no++; echo $no ?>
                                    </td>
                                    
                                    <td>
                                        <?php echo $value->agent_name ?>
                                    </td>

                                    <td>
                                        <?php echo $value->agent_address ?>
                                    </td>
                                    
                                    <td>
                                        <?php echo $value->agent_company_name ?>
                                        <br />
                                        <?php echo $value->agent_company_address ?>
                                    </td>
                                    
                                    <td>
                                        <?php echo $value->agent_email ?>
                                    </td>
                                    <td style="">
                                        <?php echo $value->agent_city  ?>
                                    </td>
                                    <td>
                                        <?php echo $value->agent_telp  ?>
                                    </td>
                                    <td>
                                        <a href="/agent/update/<?php echo $value->id ?>">
                                            <button type="button" class="btn btn-xs btn-info " id="btn-edit"><i class="glyphicon glyphicon-pencil"></i></button>
                                        </a>
                                        <button type="button" class="btn btn-xs btn-danger btn-remove" rel="<?php echo $value->id ?>"  ><i class="glyphicon glyphicon-remove"></i></button>
                                        
                                    </td>
                                </tr>
                                <?php
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        
    </section>
</div><!-- /.content-wrapper -->

<?php 

$this->load->view('footer',$footer) ?>