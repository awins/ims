<div class="row" style="margin-bottom: 20px">
	<div class="col-xs-12 " >
		<ol class="breadcrumb" id="step-breadcrumb">
            <li id="step-1" > 1. Project &amp; Company</a></li>
            <li class="active"> 2. Certificate &amp; Surveilance</li>
            <li id="step-3"> 3. Finish</li>
        </ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<form class="form-horizontal" role="form" id="form_project_input_2">
			<input type="hidden"  name="project_input_2" value="1" >
			<?php
			$post_1 = $this->session->userdata('post_1');
			$post_2 = $this->session->userdata('post_2');
			$new_number = $post_1['last_certificate_number'] ; 
						
			foreach ($post_1['iso_type'] as $key => $value) { 
				$new_number++;
				$certificate_number_full = date('y') . '-' . isoType($value,'number') . '/' . isoType($value,'acronym') . '/' . $new_number ;				

				?>
				<input type="hidden"  name="iso_type[]" value="<?php echo $value ?>" >
				<input type="hidden"  name="certificate_number[]" value="<?php echo $new_number ?>" >
				<div class="panel panel-primary">
		    		<div class="panel-heading"><?php echo isoType($value) ?></div>
			    	<div class="panel-body">
		    			<div class="form-group col-md-6 col-xs-12">
						    <label class="control-label col-xs-3" for="certificate_number_full">Cert No.</label>
						    <div class="col-xs-9">
							      <input readonly type="text" name="certificate_number_full[]" class="form-control" value="<?php echo $certificate_number_full ?>">
						    </div>
						</div>
						<div class="form-group col-md-6 col-xs-12">
						    <label class="control-label col-xs-3" for="">Cert Date</label>
						    <div class="col-xs-9">
								<div class="input-group">
						            <div class="input-group-addon">
						                <i class="fa fa-calendar"></i>
						            </div>
						            <input id="certificate_date_<?php echo $key ?>"  type="text" class="form-control datemask certificate_date" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="certificate_date[]" value="<?php echo (isset($post_2['certificate_date'][$key])) ? $post_2['certificate_date'][$key] : '' ?>" />

						        </div><!-- /.input group -->
					      		<?php echo form_error('certificate_date[' . $key . ']' ) ?>

						    </div>
						</div>
						<div class="form-group col-md-6 col-xs-12">
						    <label class="control-label col-xs-3" for="">Main Ass. Date</label>
						    <div class="col-xs-9">
								<div class="input-group">
						            <div class="input-group-addon">
						                <i class="fa fa-calendar"></i>
						            </div>
						            <input id="main_assessment_date_<?php echo $key ?>" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="main_assessment_date[]" value="<?php echo (isset($post_2['main_assessment_date'][$key])) ? $post_2['main_assessment_date'][$key] : '' ?>" />

						        </div><!-- /.input group -->
					      		<?php echo form_error('main_assessment_date[' . $key . ']' ) ?>
						    </div>
						</div>
						<div class="form-group col-md-6 col-xs-12">
						    <label class="control-label col-xs-3" for="">Surv #1 Date</label>
						    <div class="col-xs-9">
								<div class="input-group">
						            <div class="input-group-addon">
						                <i class="fa fa-calendar"></i>
						            </div>
						            <input id="surveillance_1_date_<?php echo $key ?>" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="surveillance_1_date[]" value="<?php echo (isset($post_2['surveillance_1_date'][$key])) ? $post_2['surveillance_1_date'][$key] : '' ?>" />

						        </div><!-- /.input group -->
					      		<?php echo form_error('surveillance_1_date[' . $key . ']' ) ?>
						    </div>
						</div>
						<div class="form-group col-md-6 col-xs-12">
						    <label class="control-label col-xs-3" for="">Surv #2 Date</label>
						    <div class="col-xs-9">
								<div class="input-group">
						            <div class="input-group-addon">
						                <i class="fa fa-calendar"></i>
						            </div>
						            <input id="surveillance_2_date_<?php echo $key ?>" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="surveillance_2_date[]" value="<?php echo (isset($post_2['surveillance_2_date'][$key])) ? $post_2['surveillance_2_date'][$key] : '' ?>" />

						        </div><!-- /.input group -->
					      		<?php echo form_error('surveillance_2_date[' . $key . ']' ) ?>
						    </div>
						</div>
						<div class="form-group col-md-6 col-xs-12">
						    <label class="control-label col-xs-3" for="">Surv #3 Date</label>
						    <div class="col-xs-9">
								<div class="input-group">
						            <div class="input-group-addon">
						                <i class="fa fa-calendar"></i>
						            </div>
						            <input id="renewal_assessment_date_<?php echo $key ?>" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="renewal_assessment_date[]" value="<?php echo (isset($post_2['renewal_assessment_date'][$key])) ? $post_2['renewal_assessment_date'][$key] : '' ?>" />

						        </div><!-- /.input group -->
					      		<?php echo form_error('renewal_assessment_date[' . $key . ']' ) ?>
						    </div>
						</div>
			    	</div>
			  	</div>
			  	<?php 
			} ?>
		</form>
	</div>
</div>

<div class="row">
	<div class="col-xs-6" >
        <button type="button" class="btn btn-warning pull-right" id="btn-back-2"><i class="glyphicon glyphicon-hand-left"></i> BACK</button>
    </div>
    <div class="col-xs-6" >
        <button type="button" class="btn btn-warning " id="btn-step-2">NEXT <i class="glyphicon glyphicon-hand-right"></i></button>
    </div>
</div>
