<?php 
    $script['css'][] = '/application/public/bootstrap/plugins/datatables/jquery.dataTables.css';
    $script['css'][] = '/application/public/css/project.css';
    
    $script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.js';
	$script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.numeric.extensions.js';
	$script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.date.extensions.js';
	$script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.extensions.js';
    $script['js'][] = '/application/public/bootstrap/plugins/datatables/jquery.dataTables.js';
    $script['js'][] = '/application/public/js/jquery.form.js';

    $script['js'][] = '/application/public/js/project.js';

    $this->load->view('header',$script); 
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Input New Project
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/project"><i class="fa fa-dashboard"></i> Project</a></li>
            <li class="active">New Project</li>
        </ol>
    </section>

    <section class="content">
        
        <div class="row">
            <div class="box">
                <div class="box-body" id="box-step">
    				<?php $this->load->view('project/new_step_1');  ?>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="modal  fade" id="box-company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <?php echo $box_company ?>
            </div>
        </div>


    </section>
</div>

<?php
    $this->load->view('footer',$script); 
?>