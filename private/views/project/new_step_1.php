<?php 
	$post_1 = $this->session->userdata('post_1');
	$proposal_index = (isset($new_proposal_index)) ? $new_proposal_index : $post_1['proposal_index'];
?>	
<div class="row" style="margin-bottom: 20px">
	<div class="col-sm-12 " >
		<ol class="breadcrumb" id="step-breadcrumb">
            <li class="active"> 1. Project &amp; Company</a></li>
            <li id="step-2" > 2. Certificate &amp; Surveilance</li>
            <li id="step-3"> 3. Finish</li>
        </ol>
        
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<form class="form-horizontal" role="form" id="form_project_input_1">
			<input type="hidden"  name="project_input_1" value="1" >
			<input type="hidden"  id="company_id" name="company_id" value="<?php echo (isset($sel_company))? $sel_company->company_id: null ?>" >
			<input type="hidden" name="proposal_index" class="form-control" value="<?php echo $proposal_index ?>">
			<?php 
			if (isset($post_1['project_id'])){ ?>
				<input type="hidden"  name="project_id" value="<?php echo $post_1['project_id'] ?>" >
				<?php
			} ?>
			<div class="panel panel-primary">
	    		<div class="panel-heading">Project Information</div>
		    	<div class="panel-body">
		    		<div class="col-md-6 col-sm-12">

						<div class="form-group ">
						    <label class="control-label col-sm-3" for="project_number">Project Number</label>
						    <div class="col-sm-9">
							      <input type="text" name="project_number" readonly class="form-control" value="<?php echo (isset($new_project_number)) ? $new_project_number : $post_1['project_number'] ?>">
						    </div>
						</div>
						
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Request Date</label>
						    <div class="col-sm-9">
								<div >
						            <div class="input-group">
							            <div class="input-group-addon">
							                <i class="fa fa-calendar"></i>
							            </div>
							            <input type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="request_date" value="<?php echo (isset($post_1['request_date'])) ? $post_1['request_date'] : '' ?>" />
							        </div>
						      		<?php echo form_error('request_date') ?>
						        </div>
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Standard</label>
						    <div class="col-sm-9">
						    	<?php
						    	for ($i=1; $i <=3 ; $i++) { ?>
									<label class="checkbox-inline" style="margin-right: 10px">
										<input type="checkbox" name="iso_type[]" value="<?php echo $i ?>" <?php echo (isset($post_1['iso_type']) && in_array($i, $post_1['iso_type'] )) ? 'checked' : null ?> ><?php echo isoType($i) ?>
									</label>
						    		<?php 
						    	} ?>
					      			<?php echo form_error('iso_type') ?>
						    </div>
						</div>
					</div>
		    		<div class="col-md-6 col-sm-12">
		    			<?php
		    			if ((is_array($post_1))){
		    				$proposal_number  = $post_1['proposal_number'];
		    			}else{
		    				$date = mktime(0, 0, 0, date("m"), date("d")-14,   date("Y"));
			    			$proposal_number = "CERT-" . $proposal_index . '/IMS-IDN/CON/' . romawi(date('m',$date )) . '-' .date('y');
			    			
		    			} ?>
						<div class="form-group ">
					    <label class="control-label col-sm-3" for="">Proposal No.</label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" name="proposal_number" value="<?php echo $proposal_number ?>">
					      	<?php echo form_error('proposal_number') ?>
					    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Cert. Amount</label>
						    <div class="col-sm-9">
						        <input name="certificate_amount" class="form-control numericmask" style="text-align: right;" value="<?php echo (is_array($post_1)) ? $post_1['certificate_amount'] : '' ?>" />
							     <?php echo form_error('certificate_amount') ?>
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Surv. Amount</label>
						    <div class="col-sm-9">
						        <input name="surveilance_amount" class="form-control numericmask" style="text-align: right;" value="<?php echo (is_array($post_1)) ? $post_1['surveilance_amount'] : '' ?>" />
							     <?php echo form_error('surveilance_amount') ?>
						    </div>
						</div>
					</div>
		    	</div>
		  	</div>
		  	<div class="panel panel-primary">
		    	<div class="panel-heading">Company Detail</div>
		    	<div class="panel-body">
		    		<div class="col-md-6 col-sm-12">
			    		<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Company Name</label>
						    <div class="col-sm-9">
						    	<div class="input-group">
							      	<input type="text" id="company_name" class="form-control" value="<?php echo (isset($sel_company))? $sel_company->company_name: null ?>" >
						            <span class="input-group-btn" style="vertical-align: top">
						                <button class="btn btn-default" id="btn-browse-company" type="button" rel=""  data-toggle="modal" data-target="#box-company" >
						                 	<i class="glyphicon glyphicon-search"></i>
						                </button>
						            </span>
						        </div>

						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Address</label>
						    <div class="col-sm-9">
							      <input type="text" class="form-control" id="company_address_1" value="<?php echo (isset($sel_company))? $sel_company->company_address_1: null ?>">
							      <input type="text" class="form-control" id="company_address_2" value="<?php echo (isset($sel_company))? $sel_company->company_address_2: null ?>">
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">City</label>
						    <div class="col-sm-9">
							     <input type="text" class="form-control" id="company_city" value="<?php echo (isset($sel_company))? $sel_company->company_city: null ?>">
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Province</label>
						    <div class="col-sm-9">
						    	<div class="row">
		    						<div class="col-sm-12">
										<div class="form-group ">
											<div class="col-sm-6">
												<?php
												$arr_province = provinceList();
												$sel_province = (isset($sel_company))? $sel_company->company_province: null ?>
												<select class="form-control" id="company_province">
													<?php
													foreach ($arr_province as $value) { ?>
														<option <?php echo ($sel_province == $value) ? 'selected' : null ?>><?php echo $value ?></option>
														<?php
													} ?>
												</select>
											</div>
											<div class="col-sm-6">
											    <label class="control-label col-sm-3" for="">Zip</label>
												<div class="col-sm-9">
												     <input type="text" class="form-control" id="company_zip" value="<?php echo (isset($sel_company))? $sel_company->company_zip: null ?>">
												</div>
											</div>

										</div>
									</div>
								</div>							      
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Web User</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="website_user" value="<?php echo (isset($sel_company))? $sel_company->website_user: null ?>">
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Web Password</label>
						    <div class="col-sm-9">
							    <input type="text" class="form-control" id="website_password" value="<?php echo (isset($sel_company))? $sel_company->website_password: null ?>">
						    </div>
						</div>
						<div class="form-group ">
						    <?php echo form_error('company_id') ?>
						    
						</div>

		    		</div>
		    		<div class="col-md-6 col-sm-12">
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">EAC</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="eac" value="<?php echo (isset($sel_company))? $sel_company->eac: null ?>">
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">NACE</label>
						    <div class="col-sm-9">
						      	<textarea class="form-control" row="2" id="nace" ><?php echo (isset($sel_company))? $sel_company->nace: null ?></textarea>
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Dir. Name</label>
						    <div class="col-sm-9">
							      <input type="text" class="form-control" id="director_name" value="<?php echo (isset($sel_company))? $sel_company->director_name: null ?>">
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Phone</label>
						    <div class="col-sm-9">
							      <input type="text" class="form-control" id="company_phone" value="<?php echo (isset($sel_company))? $sel_company->company_phone: null ?>">
						    </div>
						</div>
						<div class="form-group ">
						    <label class="control-label col-sm-3" for="">Agent</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control"  id="agent_name" value="<?php echo (isset($sel_company))? $sel_company->agent_name: null ?>" />
						    </div>
						</div>
					</div>
		    	</div>

		    </div>
		    <div class="row">
				<div class="col-sm-offset-6 col-sm-6" >
			        <button type="button" class="btn btn-warning " id="btn-step-1">NEXT <i class="glyphicon glyphicon-hand-right"></i></button>
			    </div>
			</div>
		</form>
	</div>
</div>



