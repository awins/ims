<div class="box-body">
    <div class="row" >
        <div class="col-sm-2">
           

            <div class="btn-print" >
                <a href="/preview/proposal/<?php echo $project[0]->project_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Proposal</button>
                </a>
            </div>

            <div class="btn-print">
                <a href="/preview/transmittal/<?php echo $project[0]->project_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Transmittal</button>
                </a>
            </div>
            <div class="btn-print">
                <a href="/preview/invoice/<?php echo $project[0]->project_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Invoice</button>
                </a>
            </div>

        </div>
   
        <div class="col-sm-2">
            <?php
            foreach ($project as $value) { ?>
                <div class="btn-print">
                    <a href="/preview/certificate/<?php echo $value->certificate_id ?>" target="blank" >
                        <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Certificate <?php echo isoType($value->iso_type,'number') ?></button>
                    </a>
                </div>
                <?php
            } ?>
        </div>

        <div class="col-sm-2">
            <?php
            foreach ($project as $value) { ?>
                <div class="btn-print">
                    <a href="/preview/manual_mutu/<?php echo isoType($value->iso_type,'number') . '/' .  $value->project_id ?>" target="blank" >
                        <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Manual Mutu <?php echo isoType($value->iso_type,'number') ?></button>
                    </a>
                </div>
                <?php
            } ?>
        </div>
        <div class="col-sm-2">
            <?php
            foreach ($project as $value) { ?>
                <div class="btn-print">
                    <a href="/preview/audit/<?php echo isoType($value->iso_type,'number') . '/' .  $value->project_id ?>" target="blank" >
                        <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Report Audit <?php echo isoType($value->iso_type,'number') ?></button>
                    </a>
                </div>
                <?php
            } ?>
        </div>
        <div class="col-sm-2">
            <div class="btn-print">
                <a href="/preview/envelope/<?php echo $project[0]->company_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Amplop</button>
                </a>
            </div>
            <div class="btn-print">
                <a href="/preview/cd_cover/<?php echo $project[0]->company_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> CD Cover</button>
                </a>
            </div>
            <div class="btn-print">
                <a href="/preview/bantex/<?php echo $project[0]->project_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Bantex</button>
                </a>
            </div>
        </div>
        <div class="col-sm-2" >
            <div class="btn-print" >
                <a href="/preview/form_issued/<?php echo $project[0]->project_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Form Issued</button>
                </a>
            </div>
            <div class="btn-print">
                <a href="/preview/map_label/<?php echo $project[0]->company_id ?>" target="blank" >
                    <button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-print"></i> Map Label</button>
                </a>
            </div>
            
        </div>
    </div>
    <div class="row" style="padding-bottom: 10px" >
        <div class="col-sm-12">
            <a href="/preview/save_all/<?php echo $project[0]->project_id ?>" target="blank" >
                <button class="btn btn-primary btn-sm " style="width: 100%" ><i class="glyphicon glyphicon-print"></i> SAVE ALL</button>
            </a>        
        </div>
    </div>

    <div style="display: block;width: 100%;position: relative;clear:both;text-align: center;border: 1px solid black;background-color: black;color: white" > 
        FORM ISSUED CERTIFICATION
    </div>
    <div style="display: block;width: 100%;position: relative;clear:both;" > 
        <div style="float: left;width:200px" >FORM / PROJECT NUMBER</div>
        <div style="float: left;width:200px" >: <?php echo $project[0]->project_number ?></div>
        <div style="float: left;width:200px" >NO. PROPOSAL</div>
        <div style="float: left;width:200px" >: <?php echo $project[0]->proposal_number ?></div>
    </div>
    
    <div style="display: block;width: 100%;position: relative;clear:both;" > 
        <div style="float: left;width:200px" >REQUEST DATE</div>
        <div style="float: left;width:200px" >: <?php echo date('d-F-Y', strtotime($project[0]->request_date)) ?></div>
        <div style="float: left;width:200px" >INVOICE</div>
        <div style="float: left;width:200px" >: Rp. <?php echo number_format($project[0]->certificate_amount,0) ?></div>
    </div>
    <div style="display: block;width: 100%;position: relative;clear:both;" > 
        <div style="float: left;width:200px" >ID WEBSITE / Username</div>
        <div style="float: left;width:200px" >: <?php echo $project[0]->website_user ?></div>
        <div style="float: left;width:200px" >AGENT</div>
        <div style="float: left;width:200px" >: <?php echo $project[0]->agent_name ?></div>
    </div>

    <div style="display: block;width: 100%;position: relative;clear:both;" > 
        <div style="float: left;width:200px" >Password</div>
        <div style="float: left;width:200px" >: <?php echo $project[0]->website_password ?></div>
    </div>
    <?php
    $no = 0;
   
    foreach ($project as $value) { ?>
        <div style="clear: both;margin-bottom: 10px">
            <table id="table-detail" class="table table-bordered black" border="1"  >
                <thead>
                  <tr>
                    <th style="width: 3%;  text-align: center" >No. </th>
                    <th style="width: 17%; text-align: center">Company Name</th>
                    <th style="width: 18%; text-align: center" colspan="2">Address</th>
                    <th style="width: 10%; text-align: center">Cert Number</th>
                    <th style="width: 12%; text-align: center">Date of Certificate</th>
                    <th style="width: 5%; text-align: center">Surv.</th>
                    <th style="width: 15%; text-align: center">Schedule</th>
                    
                  </tr>
                </thead>
                <tbody>
                    <tr >
                        <td rowspan="5" style="vertical-align: top;text-align: center">
                            <?php $no++; echo $no ?>
                        </td>
                        
                        <td rowspan="5" style="vertical-align: top;padding-left: 5px">
                            <?php echo $value->company_name ?>
                            <br />
                            (DIRUT: <?php echo $value->director_name ?>)
                            <br />
                            TELP: <?php echo $value->company_phone ?>
                        </td>
                        <td colspan="2"  rowspan="2" style="text-align: center;padding-left: 5px">
                            <?php echo $value->company_address_1 . '<br />' . $value->company_address_2 . '<br />' . $value->company_city . ', ' . $value->company_province . '&#8211 Indonesia ' . $value->company_zip ?>
                        </td>
                        <td rowspan="3" style="text-align: center" ><?php echo $value->certificate_number_full ?></td>
                        <td  style=";padding-left: 5px" >Date of Certificate</td>
                        <td style=";padding-left: 5px" ></td>
                        <td style=";padding-left: 5px" ><?php echo date('d-F-Y', strtotime($value->certificate_date)) ?></td>
                    </tr>
                    <tr>
                        <td style=";padding-left: 5px" >Main Assessment</td>
                        <td style=";padding-left: 5px" ></td>
                        <td style=";padding-left: 5px" ><?php echo date('d-F-Y', strtotime($value->main_assessment_date)) ?></td>
                    </tr>
                    <tr>
                        <td style=";padding-left: 5px" >EAC</td>
                        <td style="width: 20%;padding-left: 5px" ><?php echo $value->eac ?></td>
                        <td style=";padding-left: 5px"></td>
                        <td style=";padding-left: 5px;text-align: center">S1</td>
                        <td style=";padding-left: 5px"><?php echo date('d-F-Y', strtotime($value->surveillance_1_date)) ?></td>
                    </tr>
                    <tr>
                        <td style=";padding-left: 5px" >NACE</td>
                        <td style=";padding-left: 5px" ><?php echo $value->nace ?></td>
                        <td style=";padding-left: 5px;text-align: center" rowspan="2"><?php echo isoType($value->iso_type) ?></td>
                        <td style=";padding-left: 5px"></td>
                        <td style=";padding-left: 5px;text-align: center">S2</td>
                        <td style=";padding-left: 5px"><?php echo date('d-F-Y', strtotime($value->surveillance_2_date)) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <div style="text-align: center" >
                                <?php echo $value->nace_detail ?>
                            </div>
                        </td>
                        <td style=";padding-left: 5px" >Renewall Main Assessment</td>
                        <td style=";padding-left: 5px;text-align: center" >S3</td>
                        <td style=";padding-left: 5px" ><?php echo date('d-F-Y', strtotime($value->renewal_assessment_date)) ?></td>

                    </tr>
                </tbody>
            </table>
        </div>
        <?php
    } ?>

</div>