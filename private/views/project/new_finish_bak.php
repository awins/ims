<?php 
	$post_1 = $this->session->userdata('post_1');
	$post_2 = $this->session->userdata('post_2');
?>	
<div class="row" style="margin-bottom: 20px">
	<div class="col-sm-12 " >
		<ol class="breadcrumb" id="step-breadcrumb">
            <li > 1. Project &amp; Company</a></li>
            <li > 2. Certificate &amp; Surveilance</li>
            <li class="active"> 3. Finish</li>
        </ol>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="box">
                <div class="box-body">
                    <table id="table-project" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th style="width: 3%;  text-align: center" >No. </th>
                            <th style="width: 17%; text-align: center">Project</th>
                            <th style="width: 17%; text-align: center">Company Name</th>
                            <th style="width: 33%; text-align: center">Address</th>
                            <th style="width: 10%; text-align: center">9001</th>
                            <th style="width: 10%; text-align: center">14001</th>
                            <th style="width: 10%; text-align: center">18001</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            $project_id = 0;
                            
                            $arr_project = array();

                            foreach ($companies as $value) {
                                if ($project_id != $value->project_id){
                                    if ($project_id>0){
                                        $arr_project[] = $temp;
                                    }

                                    $temp = array();
                                    $temp['project_id'] = $value->project_id;
                                    $temp['project_number'] = $value->project_number;
                                    $temp['request_date'] = date('d F Y',strtotime($value->request_date));
                                    $temp['proposal_number'] = $value->proposal_number;
                                    $temp['agent_name'] = $value->agent_name;
                                    $temp['company_name'] = $value->company_name;
                                    $temp['address'] = $value->company_address_1 . '<br />' . $value->company_address_2 . '<br />' . $value->company_city . ', ' . $value->company_province . '&#8211 Indonesia ' . $value->company_zip ;
                                    $temp['director_name'] = $value->director_name;
                                    $temp['phone'] = $value->company_phone;
                                    $temp['eac'] = $value->eac;
                                    $temp['nace'] = $value->nace;
                                }

                                switch ($value->iso_type) {
                                    case 1:
                                        $temp['iso_9001'] = $value->certificate_number_full;
                                        $temp['iso_9001_id'] = $value->certificate_id;
                                        $temp['iso_9001_date'] = date('d F Y',strtotime($value->certificate_date));
                                        break;
                                    case 2:
                                        $temp['iso_14001'] = $value->certificate_number_full;
                                        $temp['iso_14001_id'] = $value->certificate_id;
                                        $temp['iso_14001_date'] = date('d F Y',strtotime($value->certificate_date));
                                        break;
                                    case 3:
                                        $temp['iso_18001'] = $value->certificate_number_full;
                                        $temp['iso_18001_id'] = $value->certificate_id;
                                        $temp['iso_18001_date'] = date('d F Y',strtotime($value->certificate_date));
                                        break;
                                    default:
                                        break;
                                }
                                $project_id = $value->project_id;
                                
                            }
                            $arr_project[] = $temp;
                            foreach ($arr_project as $value) { ?>
                                <tr rel="<?php echo $value['project_id'] ?>" >
                                    <td>
                                        <?php $no++; echo $no ?>
                                    </td>
                                    <td style="position: relative;">
                                        <div style="display: block;height: 100%">
                                            Form No: <?php echo $value['project_number']; ?>
                                            <br />
                                            Req Date: <?php echo $value['request_date']; ?>
                                           <!--  <br />
                                            Proposal No:<br /> <?php echo $value['proposal_number']; ?>
                                            <br /> -->
                                            <div style="position: absolute; bottom: 10px">
                                                Agent: <?php echo $value['agent_name']; ?>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <?php echo $value['company_name'] ?>
                                        <br />
                                        (DIRUT: <?php echo $value['director_name'] ?>)
                                        <br />
                                        TELP: <?php echo $value['phone'] ?>
                                    </td>
                                    <td>
                                        <?php echo $value['address'] ?>
                                        <table border="1" style="width: 100%" >
                                            <tbody>
                                                <tr>
                                                    <td style="width: 30%;padding-left: 5px" >EAC</td>
                                                    <td style="width: 70%;padding-left: 5px"><?php echo $value['eac'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 30%;padding-left: 5px">NACE</td>
                                                    <td style="width: 70%;padding-left: 5px"><?php echo $value['nace'] ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td style="text-align: center">
                                        <?php 
                                        if (isset($value['iso_9001'])){
                                            echo $value['iso_9001'];
                                            echo "<br />";
                                            echo "(". $value['iso_9001_date'] .")"; ?>
                                            <br/>
                                            <br/>
                                            <a href="preview/certificate/<?php echo $value['iso_9001_id'] ?>" target="blank" >
                                                <button class="btn btn-primary btn-sm" >Print</button>
                                            </a>
                                            <?php
                                        }else {
                                            echo "-";
                                        } ?>
                                    </td>
                                    <td style="text-align: center">
                                        <?php 
                                        if (isset($value['iso_14001'])){
                                            echo $value['iso_14001'];
                                            echo "<br />";
                                            echo "(". $value['iso_14001_date'] .")"; ?>
                                            <br/>
                                            <br/>
                                            <a href="preview/certificate/<?php echo $value['iso_14001_id'] ?>" target="blank" >
                                                <button class="btn btn-primary btn-sm" >Print</button>
                                            </a>
                                            <?php
                                        }else {
                                            echo "-";
                                        } ?>
                                    </td>
                                    <td style="text-align: center">
                                        <?php 
                                        if (isset($value['iso_18001'])){
                                            echo $value['iso_18001'];
                                            echo "<br />";
                                            echo "(". $value['iso_18001_date'] .")"; ?>
                                            <br/>
                                            <br/>
                                            <a href="preview/certificate/<?php echo $value['iso_18001_id'] ?>" target="blank" >
                                                <button class="btn btn-primary btn-sm" >Print</button>
                                            </a>
                                            <?php
                                        }else {
                                            echo "-";
                                        } ?>
                                    </td>
                                </tr>
                                <?php
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
	</div>
</div>

