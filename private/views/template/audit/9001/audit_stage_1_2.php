<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/audit.css">
	<style type="text/css">
		table {
			text-align: left;
			border-collapse: collapse;
		}

		table tr td {
			border: 1px solid black;
			padding: 5px;
			vertical-align: top;
			text-align: justify;
		}



		table.no-border tr td {
			border: 0;
			padding: 5px;

		}

		table tr.bold td {
			font-weight: bold;
		}

		table tr.padding-top td {
			padding-top: 10px;
		}


	</style>
</head>
<body>
	<?php 
	foreach ($project as $value) {
		if ($value->iso_type == 1){
			$date = strtotime($value->main_assessment_date . '-14 day') ;
			break;
		}
	}
	?>
	<div style="width: 650px;margin: auto;font-size: 16pt; font-weight: bold;text-align: center;padding:  10px 0 15px;">
		RENCANA AUDIT
	</div>
	<div style="width: 650px;margin: auto;">
		<table >
			<tbody>
				<tr>
					<td style="width: 110px"  rowspan="11"><?php echo indDate($date) ?></td>
					<td style="width: 110px" >09.00 WIB</td>
					<td style="width: 140px" >Bpk. F. Rahman &amp; Bpk. I. Akbar</td>
					<td style="width: 110px" >Opening Meeting </td>
					<td style="width: 110px" >Semua Divisi</td>
					<td style="width: 110px" >M.R</td>
				</tr>
				<tr>
					<td>09.30 WIB</td>
					<td>Bpk. F. Rahman</td>
					<td>Audit Lapangan</td>
					<td>Divisi HRD</td>
					<td>Manager HRD</td>
				</tr>
				<tr>
					<td>09.30 WIB</td>
					<td>Bpk. F. Rahman </td>
					<td>Audit Lapangan</td>
					<td>Divisi Operasional</td>
					<td>Manager Operasional</td>
				</tr>
				<tr>
					<td>11.00 WIB</td>
					<td>Bpk. I. Akbar</td>
					<td>Audit Lapangan</td>
					<td>Divisi Marketing</td>
					<td>Manager Marketing</td>
				</tr>
				<tr>
					<td>11.00 WIB</td>
					<td>Bpk. F. Rahman </td>
					<td>Audit Lapangan</td>
					<td>Divisi Keuangan</td>
					<td>Manager Keuangan</td>
				</tr>
				<tr>
					<td>12.00 WIB</td>
					<td></td>
					<td>ISOMA</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>13.00 WIB</td>
					<td>Bpk. I. Akbar</td>
					<td>Audit Lapangan</td>
					<td>Divisi ISO</td>
					<td>Manager Marketing</td>
				</tr>
				<tr>
					<td>13.00 WIB</td>
					<td>Bpk. F. Rahman </td>
					<td>Audit Lapangan</td>
					<td>TOP Management</td>
					<td>Manager Keuangan</td>
				</tr>
				<tr>
					<td>14.00 WIB</td>
					<td>Bpk. F. Rahman &amp; I. Akbar</td>
					<td>Perumusan hasil audit </td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>14.30 WIB</td>
					<td>Bpk. F. Rahman &amp; I. Akbar</td>
					<td>Closing Meeting</td>
					<td>Semua Divisi</td>
					<td>M.R</td>
				</tr>
				<tr>
					<td>15.00 WIB</td>
					<td>Bpk. F. Rahman &amp; I. Akbar</td>
					<td>Selesai </td>
					<td></td>
					<td></td>
				</tr>
				
			</tbody>
		</table>

		<table class="no-border">
			<tr>
				<td colspan="" >•</td>
				<td colspan="4" >Waktu perkiraan dan akan dikonfirmasi pada pertemuan pembuka sebelum dimulainya audit.</td>
			</tr>
			<tr>
				<td colspan="" >•</td>
				<td colspan="4" >ISO 9001:2008 sebagai pernyataan akan dibahas disetiap Departemen / fungsi dalam Organisasi yang diperlukan :</td>
			</tr>
			<tr>
				<td style="width: 30px" ></td>
				<td style="width: 15px" >-</td>
				<td style="width: 280px">4.2.3 Document control</td>
				<td style="width: 15px" >-</td>
				<td style="width: 300px">6.3. infrastructure</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >-</td>
				<td colspan="" >4.2.4 control of quality record</td>
				<td colspan="" >-</td>
				<td colspan="" >6.4 working environment</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >-</td>
				<td colspan="" >5.4.1 quality objective</td>
				<td colspan="" >-</td>
				<td colspan="" >8.2.3 monitoring and measurement of processes</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >-</td>
				<td colspan="" >5.4.2 quality management system planning</td>
				<td colspan="" >-</td>
				<td colspan="" >8.5.1 continual improvement</td>
			</tr>
			<tr>
				<td colspan="" >•</td>
				<td colspan="4" >Auditor IMS-Indonesia berhak untuk mengubah atau menambah elemen yang terdaftar sebelum atau selama audit, tergantung pada hasil penyelidikan di tempat.</td>
			</tr>
			<tr>
				<td colspan="" >•</td>
				<td colspan="4" >Sebuah tempat khusus untuk mempersiapkan, review dan laporan yang diminta untuk penggunaan auditor. Kontrak Anda dengan IMS-Indonesia merupakan bagian integral dari rencana audit dan pengaturan rincian kerahasiaan, ruang lingkup audit, informasi tentang kegiatan tindak lanjut dan persyaratan pelaporan khusus.</td>
			</tr>
			
			
		</table>

	</div>

</body>
</html>