<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/audit.css">
	<style type="text/css">
		table {
			text-align: left;
			border-collapse: collapse;
		}

		table tr td {
			border: 1px solid black;
			padding: 5px;
			vertical-align: top;
			text-align: justify;
		}



		table.no-border tr td {
			border: 0;
			padding: 5px;

		}

		table tr.bold td {
			font-weight: bold;
		}

		table tr.padding-top td {
			padding-top: 10px;
		}


	</style>
</head>
<body>
	<?php 
	foreach ($project as $value) {
		if ($value->iso_type == 1){
			$date = $value->main_assessment_date;
			break;
		}
	}
	?>
	<div style="width: 650px;margin: auto;font-size: 16pt; font-weight: bold;text-align: center;padding:  10px 0 15px;">
		<div>Corrective Action Request</div>
		<div style="font-size: 11pt" >
			<div style="float: left;width: 315px;text-align: right;padding-right: 5px"><img src="/application/public/img/element/checkbox-empty.gif"> Major</div>
			<div style="float: left;width: 315px;text-align: left;padding-left: 5px"><img src="/application/public/img/element/checkbox-cross.gif"> Minor</div>
		</div>
	</div>
	<div style="width: 650px;margin: auto;">
		<table >
			<tbody>
				
				<tr>
					<td colspan="5" >Organisasi : <?php echo $project[0]->company_name ?></td>
				</tr>
				<tr>
					<td colspan="5" >Alamat: <?php echo $project[0]->company_address_1 . ' ' . $project[0]->company_address_2 . ', ' . $project[0]->company_city . ', '. $project[0]->company_province ?> - Indonesia</td>
				</tr>
				<tr>
					<td >Auditor  : Bpk. F. Rahman</td>
					<td colspan="4" >Tanggal Audit: <?php echo indDate($date) ?></td>
				</tr>
				<tr>
					<td colspan="5" >Standard  : ISO 9001:2008  </td>
				</tr>
				<tr>
					<td colspan="5" >Wakil Manajemen : M.R</td>
				</tr>
				<tr>
					<td colspan="5" >Area/Departemen/Divisi: Arsip Divisi HRD</td>
				</tr>
				<tr>
					<td colspan="" >Referensi Dokumen:  ISO 9001:2008</td>
					<td colspan="4" >Referensi Klausul:  Klausul 6.6.2</td>
				</tr>
				<tr>
					<td colspan="" >Nomor CAR : 004</td>
					<td colspan="4" >Batas Akhir Perbaikan CAR: <?php echo indDate( strtotime($date . '3 month' )) ?></td>
				</tr>
				<tr>
					<td colspan="5" >
						Temuan Ketidak Sesuaian :
						<br />
						Permasalahan : terjadi temuan ketidaksesuaian penerapan kompetensi, kesadaran dan pelatihan dengan kurang terpeliharanya daftar hadir pelatihan yang diadakan perusahaan.
						<br />
						Lokasi : ditemukan di divisi HRD
						<br />
						Bukti Obyektif : daftar hadir training/pelatihan
						<br />
						Referensi Klausul :   klausul 6.2.2 ISO 9001:2008
					</td>
				</tr>
				<tr>
					<td colspan="" ><b>Wakil Manajemen: M.R</b></td>
					<td colspan="4" ><b>Auditor: Bpk. F. Rahman</b></td>
				</tr>
				<tr>
					<td colspan="5" >
						Untuk mencegah terulangnya temuan maka Tindakan korektif yang dilakukan adalah:
						<br />
						Divisi HRD akan lebih mempersiapkan kebutuhan pelatihan sebelum pelaksanaan.
					</td>
				</tr>
				<tr>
					<td colspan="" ><b>Wakil Manajemen:</b></td>
					<td colspan="4" ><b>Tanggal:</b> <?php echo indDate( strtotime($date . '+2 day' )) ?></td>
				</tr>
				<tr>
					<td colspan="5" >
						Penerimaan Corrective Action / Komentar (gunakan lembar tambahan jika perlu):
						<br />
						Manager HRD akan lebih aktif dalam memantau pelaksanaan kompetensi, kesadaran dan pelatihan.
					</td>
				</tr>
				<tr>
					<td colspan="" ><b>Auditor: Bpk. F. Rahman</b></td>
					<td colspan="4" ><b>Tanggal:</b> <?php echo indDate( strtotime($date . '+4 day' )) ?></td>
				</tr>
				<tr>
					<td colspan="" >Response required (in months)</td>
					<td colspan="2" >Major</td>
					<td colspan="2" >Minor</td>
				</tr>
				
				<tr>
					<td style="width: 330px;" rowspan="2">Tindakan Perbaikan dan Pencegahan untuk menghilangkan penyebab ketidaksesuaian yang ditemukan.</td>
					<td style="width: 82px;">Auditor</td>
					<td style="width: 82px;">Batas Akhir</td>
					<td style="width: 82px;">Auditor</td>
					<td style="width: 82px;">Batas Akhir</td>
				</tr>

				<tr>
					<td colspan="" ></td>
					<td colspan="" >3 Bulan</td>
					<td colspan="" ></td>
					<td colspan="" >Next visit</td>
				</tr>
				
			</tbody>
		</table>

	</div>

</body>
</html>