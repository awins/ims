<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/audit.css">
	<style type="text/css">
		table {
			text-align: left;
			border-collapse: collapse;
		}

		table tr td {
			border: 1px solid black;
			padding: 5px;
			vertical-align: top;
			text-align: justify;
		}



		table.no-border tr td {
			border: 0;
			padding: 5px;

		}

		table tr.bold td {
			font-weight: bold;
		}

		table tr.padding-top td {
			padding-top: 10px;
		}


	</style>
</head>
<body>
	<div   style="width: 650px;margin: auto;">
		<table class="no-border">
			<tr>
				<td style="width: 30px" ></td>
				<td style="width: 30px" ></td>
				<td style="width: 30px"></td>
				<td style="width: 550px"></td>
			</tr>

			<tr class="bold">
				<td colspan="" >1.</td>
				<td colspan="3" >Tujuan Audit</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Tujuan dari audit ini :</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >•</td>
				<td colspan="2" >Untuk mengkonfirmasi bahwa sistem manajemen sesuai dengan semua persyaratan standar.</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >•</td>
				<td colspan="2" >Untuk mengkonfirmasi bahwa organisasi telah efektif menerapkan sistem manajemen yang telah direncanakan.</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >•</td>
				<td colspan="2" >Untuk mengkonfirmasi bahwa sistem manajemen mampu mencapai tujuan kebijakan organisasi.</td>
			</tr>
			<tr class="bold">
				<td colspan="" >2.</td>
				<td colspan="3" >Ruang Lingkup sertifikasi</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Audit mencakup: <b><i>“Kontraktor Umum, Sipil, Mekanikal, Elektrikal dan Perdagangan Barang”.</i></b></td>
			</tr>
		</table>

		<table class="no-border">
			<tr>
				<td style="width: 30px" ></td>
				<td style="width: 510px">Ini adalah audit multi-situs dan Lampiran daftar semua situs yang relevan dan / atau lokasi terpencil telah ditetapkan (terlampir) dan setuju dengan klien</td>
				<td style="width: 50px;vertical-align: bottom" ><img src="/application/public/img/element/checkbox-empty-yes.gif" /></td>
				<td style="width: 50px;vertical-align: bottom" ><img src="/application/public/img/element/checkbox-filled-no.gif" /></td>
			</tr>
		</table>
		<table class="no-border">
			<tr>
				<td style="width: 30px" ></td>
				<td style="width: 30px" ></td>
				<td style="width: 30px"></td>
				<td style="width: 550px"></td>
			</tr>

			<tr class="bold">
				<td colspan="" >3.</td>
				<td colspan="3" >Temuan Audit Saat ini dan Solusi</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Tim audit melakukan audit proses-berbasis berfokus pada aspek penting / resiko / tujuan diperlukan oleh standar (s). Metode audit yang digunakan adalah wawancara, observasi kegiatan dan review dokumentasi dan catatan.
					<br />
					Struktur audit sudah sesuai dengan rencana audit dan matriks perencanaan audit yang disertakan sebagai lampiran laporan ringkasan. 
				</td>
			</tr>
		</table>

		<table class="no-border">
			<tr>
				<td style="width: 30px" ></td>
				<td style="width: 510px;padding-top: 15px">Tim audit menyimpulkan bahwa organization telah ditetapkan dan dipelihara sistem manajemen sesuai dengan persyaratan standar dan menunjukkan kemampuan sistem untuk secara sistematis mencapai persyaratan yang disepakati untuk produk atau jasa dalam lingkup dan kebijakan organisasi dan tujuan.</td>
				<td style="width: 50px;vertical-align: bottom" ><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td style="width: 50px;vertical-align: bottom" ><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
			</tr>
		</table>
		<table class="no-border">
			<tr>
				<td style="width: 30px" ></td>
				<td style="width: 30px" ></td>
				<td style="width: 30px"></td>
				<td style="width: 550px"></td>
			</tr>

			<tr>
				<td colspan=""  ></td>
				<td colspan="3" style="padding-top: 10px">Jumlah ketidaksesuaian diidentifikasi: <b>.............(__) Mayor, enam (6) Minor.</b></td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Oleh karena itu tim audit merekomendasikan bahwa, berdasarkan hasil dari audit dan keadaan sistem menunjukkan perkembangan dan kematangan, sertifikasi sistem manajemen menjadi:</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td ><img src="/application/public/img/element/checkbox-cross.gif" /></td>
				<td colspan="2" >Diberikan</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td ><img src="/application/public/img/element/checkbox-empty.gif" /></td>
				<td colspan="2" >Dilanjutkan</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td ><img src="/application/public/img/element/checkbox-empty.gif" /></td>
				<td colspan="2" >Dirahasiakan</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td ><img src="/application/public/img/element/checkbox-empty.gif" /></td>
				<td colspan="2" >Ditangguhkan sampai tindakan korektif yang memuaskan selesai.</td>
			</tr>
			<tr class="bold">
				<td colspan="" >4.</td>
				<td colspan="3" >Hasil Audit Sebelumnya (N/A)</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Hasil audit terakhir dari sistem ini telah ditinjau, khususnya untuk memastikan koreksi yang sesuai.</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td ><img src="/application/public/img/element/checkbox-cross.gif" /></td>
				<td colspan="2" >Setiap ketidaksesuaian yang diidentifikasi selama audit sebelumnya telah diperbaiki dan tindakan perbaikan terus menjadi efektif.</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td ><img src="/application/public/img/element/checkbox-empty.gif" /></td>
				<td colspan="2" >Sistem manajemen belum ditangani ketidaksesuaian yang diidentifikasi selama kegiatan audit sebelumnya dan isu tertentu telah kembali didefinisikan dalam bagian ketidaksesuaian laporan ini.</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td ><img src="/application/public/img/element/checkbox-empty.gif" /></td>
				<td colspan="2" >Tidak ada Corrective Action Request (CAR) dari kunjungan sebelumnya untuk ditindaklanjuti.</td>
			</tr>
			<tr class="bold">
				<td colspan="" >5.</td>
				<td colspan="3" >Temuan Audit</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Tim audit melakukan proses audit berdasarkan berfokus pada aspek yang signifikan / risiko / tujuan. Metode audit yang digunakan adalah wawancara, observasi kegiatan dan review dokumentasi dan catatan.</td>
			</tr>
		</table>

		<table class="" style="margin-left: 30px" >
			<tr>
				<td style="width: 520px">Dokumentasi sistem manajemen memperlihatkan kesesuaian dengan persyaratan standar audit dan memberikan struktur yang memadai untuk mendukung implementasi dan pemeliharaan sistem manajemen.</td>
				<td style="width: 30px" ><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td style="width: 30px" ><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
				<td style="width: 30px" ></td>
			</tr>
			<tr>
				<td>Organisasi telah menunjukkan implementasi yang efektif dan pemeliharaan / perbaikan sistem manajemen.</td>
				<td><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
				<td></td>
			</tr>
			<tr>
				<td>Organisasi telah menunjukkan pembentukan dan pelacakan yang tepat sasaran dan target kinerja kunci dan kemajuan dipantau terhadap prestasi mereka.</td>
				<td><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
				<td></td>
			</tr>
			<tr>
				<td>Program audit internal telah sepenuhnya dilaksanakan dan menunjukkan efektivitas sebagai alat untuk mempertahankan dan meningkatkan sistem manajemen.</td>
				<td><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
				<td></td>
			</tr>
			<tr>
				<td>Proses tinjauan manajemen memperlihatkan kemampuan untuk memastikan kesesuaian, kecukupan dan efektivitas sistem manajemen.</td>
				<td><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
				<td></td>
			</tr>
			<tr>
				<td>Selama proses audit, sistem manajemen keseluruhan memperlihatkan kesesuaian dengan persyaratan standar audit.</td>
				<td><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
				<td></td>
			</tr>
			<tr>
				<td>Sertifikasi pernyataan yang akurat dan sesuai dengan bimbingan IMS-Indonesia.</td>
				<td><img src="/application/public/img/element/checkbox-filled-yes.gif" /></td>
				<td><img src="/application/public/img/element/checkbox-empty-no.gif" /></td>
				<td><img src="/application/public/img/element/checkbox-empty-na.gif" /></td>
			</tr>
			
		</table>
		<table class="no-border">
			<tr class="bold">
				<td colspan="" >6.</td>
				<td colspan="3" >Significant Audit Trails Followed</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Proses spesifik, kegiatan dan fungsi terakhir yang rinci dalam Matrix Perencanaan Audit dan Audit Plan. Dalam melakukan audit dan keterkaitan berbagai audit yang dikembangkan. Jalur Audit yang diikuti, termasuk bukti obyektif dan pengamatan terhadap keseluruhan proses dan kontrol yang dicatat dalam "Catatan Audit" yang merupakan bagian dari paket sertifikasi permanen tetapi tidak disampaikan kepada klien.
					Peluang untuk perbaikan serta pengamatan positif atau negatif khusus yang dijelaskan di bawah bagian 8 sementara ketidaksesuaian dicatat dalam lampiran "Permintaan Tindakan Korektif (CAR)"
				</td>
			</tr>
			<tr class="bold">
				<td colspan="" >7.</td>
				<td colspan="3" >Ketidaksesuaian</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="3" >Ketidaksesuaian rinci dalam lampiran "Permintaan Tindakan Korektif (CAR)" harus ditangani melalui proses tindakan korektif organisasi, sesuai dengan persyaratan yang relevan tindakan korektif dan pencegahan dari standar audit dan catatan lengkap dipelihara</td>
			</tr>
		</table>

		<table class="" style="margin-left: 30px" >
			<tr>
				<td style="width: 30px" ><img src="/application/public/img/element/checkbox-empty.gif" /></td>
				<td style="width: 580px">Tindakan korektif untuk mengatasi ketidaksesuaian <b>MAJOR</b> diidentifikasi harus segera dilakukan dan <b>IMS-Indonesia diberitahu tentang tindakan yang diambil dalam waktu 30 hari.</b> Auditor <b>IMS-Indonesia</b> akan melakukan <b>tindak lanjut kunjungan</b> dalam waktu 90 hari untuk mengkonfirmasi tindakan yang diambil, evaluasi terhadap keefektifan mereka, dan menentukan apakah sertifikasi dapat diberikan atau dilanjutkan.</td>
			</tr>
			<tr>
				<td style="width: 30px" ><img src="/application/public/img/element/checkbox-cross.gif" /></td>
				<td style="width: 580px">Tindakan korektif untuk mengatasi ketidaksesuaian <b>MINOR</b> harus segera dilakukan identifikasi dan <b>catatan dengan bukti pendukung yang dikirim ke auditor IMS-Indonesia untuk close-out dalam waktu 90 hari</b>. Pada kunjungan Audit jadwal berikutnya, tim audit <b>IMS-Indonesia</b> akan menindaklanjuti semua ketidaksesuaian diidentifikasi untuk mengkonfirmasi efektivitas tindakan perbaikan dan pencegahan yang diambil.</td>
			</tr>
		</table>
		<table class="no-border">
			<tr>
				<td style="width: 30px" ></td>
				<td style="width: 30px" ></td>
				<td style="width: 30px"></td>
				<td style="width: 550px"></td>
			</tr>

			<tr class="bold">
				<td colspan="" >8.</td>
				<td colspan="3" >Observasi dan Peluang Peningkatan</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >1.</td>
				<td colspan="2" >harap dibuatkan daftar isi pada setiap dokumen HRD</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >2.</td>
				<td colspan="2" >harap dilaksanakan “house keeping” atau rewarding untuk kebersihan dan tata ruang kerja.</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >3.</td>
				<td colspan="2" >harap tindakan perbaikan dan pencegahan segera dilakukan.</td>
			</tr>
			<tr>
				<td colspan="" ></td>
				<td colspan="" >4.</td>
				<td colspan="2" >Penggunaan kertas atau barang recycle agar lebih diefektifkan lagi.</td>
			</tr>
			
			
		</table>

	</div>

</body>
</html>