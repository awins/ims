<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/audit.css">
	<style type="text/css">
		table {
			text-align: left;
			border-collapse: collapse;
		}

		table tr td {
			border: 1px solid black;
			padding: 5px;
			vertical-align: top;
			text-align: justify;
		}



		table.no-border tr td {
			border: 0;
			padding: 5px;

		}

		table tr.bold td {
			font-weight: bold;
		}

		table tr.padding-top td {
			padding-top: 10px;
		}


	</style>
</head>
<body>
	<?php 
	foreach ($project as $value) {
		if ($value->iso_type == 2){
			$date = $value->main_assessment_date;
			break;
		}
	}
	?>
	<div style="width: 650px;margin: auto;font-size: 16pt; font-weight: bold;text-align: center;padding:  10px 0 15px;">
		Sertifikasi Sistem Manajemen
		<br />
		Laporan Audit Tahap 1
	</div>
	<div style="width: 650px;margin: auto;">
		<table >
			
			<tbody>
				
				<tr>
					<td>Organisasi:</td>
					<td colspan="3"><?php echo $project[0]->company_name ?></td>
				</tr>

				<tr>
					<td colspan="">Alamat :</td>
					<td colspan="3"><?php echo $project[0]->company_address_1 . ' '. $project[0]->company_address_2. ', '. $project[0]->company_city. ', ' . $project[0]->company_province . ' - Indonesia' ?>
					</td>
				</tr>
				<tr>
					<td colspan="">Job Number :</td>
					<td colspan="3"><?php echo $project[0]->project_number ?>B</td>
				</tr>
				<tr>
					<td colspan="">Standard:</td>
					<td colspan="">ISO 14001:2004</td>
					<td colspan="2">Badan Akreditasi : -    </td>
				</tr>
				<tr>
					<td colspan="">MR. Organisasi:</td>
					<td colspan="3">Bpk / Ibu </td>
				</tr>
				<tr>
					<td colspan="">Lokasi(l) audit:</td>
					<td colspan="3"><?php echo $project[0]->company_name ?></td>
				</tr>
				<tr>
					<td style="width: 165px;">Tanggal Audit Stage 1:</td>
					<td style="width: 165px;"><?php echo indDate(strtotime($date . ' -14 day')) ?></td>
					<td style="width: 165px;">Tanggal Audit Stage 2 :</td>
					<td style="width: 165px;"><?php echo indDate($date) ?></td>
				</tr>
				<tr>
					<td colspan="">Kode EAC:</td>
					<td colspan=""><?php echo $project[0]->eac ?></td>
					<td colspan="">Kode NACE : </td>
					<td colspan=""><?php echo $project[0]->nace ?></td>
				</tr>
				<tr>
					<td colspan="">Jumlah Karyawan:</td>
					<td colspan="">20 orang  </td>
					<td colspan="">Jumlah Cabang :	</td>
					<td colspan="">1</td>
				</tr>
				<tr>
					<td colspan="">Lead auditor:</td>
					<td colspan="">Bpk. F. Rahman</td>
					<td colspan="">Tambahan Tim anggota :</td>
					<td colspan="">Bpk. I. Akbar</td>
				</tr>
				<tr>
					<td colspan="4">Laporan ini bersifat rahasia dan distribusi terbatas kepada tim audit, perwakilan klien dan Kantor IMS-Indonesia.</td>
				</tr>
				
			</tbody>
		</table>

		<table class="no-border" border="0"  >
			<thead>
				<tr>
					<td style="width: 20pt"></td>
					<td style="width: 20pt"></td>
					<td style="width: 325pt"></td>
					<td style="width: 35pt"></td>
					<td style="width: 35pt"></td>
					<td style="width: 35pt"></td>
				</tr>
			</thead>
			<tbody>
				<tr class="bold padding-top">
					<td colspan="">1.</td>
					<td colspan="5">Tujuan Audit</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="5">Tujuan dari Audit ini adalah:</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">•</td>
					<td colspan="4">Untuk mengkonfirmasi bahwa sistem manajemen telah direncanakan agar sesuai dengan semua persyaratan standar audit;</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">•</td>
					<td colspan="4">Untuk mengkonfirmasi bahwa sistem manajemen dirancang untuk mencapai tujuan kebijakan organisasi;</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">•</td>
					<td colspan="4">Untuk mengevaluasi kemampuan sistem manajemen yang mengelola kepatuhan dengan undang-undang, persyaratan peraturan dan kontrak;</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">•</td>
					<td colspan="4">Untuk mendapatkan informasi yang berkaitan untuk memberikan efektifitas dan perencanaa audit tahap 2, evaluasi lokasi klien dan kondisi lokasi yang spesifik, kumpulan informasi yang berhubungan dengan proses dan operasi dalam lingkup dan tujuan sistem manajemen dan identifikasi kinerja utama atau aspek yang signifikan.</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">•</td>
					<td colspan="4">Untuk mengevaluasi keadaan kesiapan sistem manajemen untuk audit tahap 2, termasuk evaluasi audit internal dan meninjau perencanaan kinerja manajemen dan penentuan tingkat keseluruhan pelaksanaan sistem manajemen;</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">•</td>
					<td colspan="4">Untuk mengkonfirmasi pengaturan yang direncanakan untuk audit 2 Tahap.</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="5">Untuk memberikan umpan balik kepada organisasi dengan memfasilitasi perbaikan berkelanjutan.</td>
				</tr>
				
				<tr class="bold padding-top">
					<td>2.</td>
					<td colspan="5">Ruang Lingkup Audit</td>
				</tr>
				<tr class="bold padding-top">
					<td></td>
					<td colspan="5"><i><?php echo $project[0]->company_scope ?></i></td>
				</tr>
				
				<tr>
					<td colspan=""></td>
					<td colspan="2"><i>Untuk multi-site audit Lampiran daftar semua situs yang relevan atau lokasi yang jauh telah ditetapkan (terlampir) dan disepakati dengan klien.</i></td>
					<td colspan=""> <img src="/application/public/img/element/checkbox-filled-na.gif"></td>
					<td colspan=""> <img src="/application/public/img/element/checkbox-empty-yes.gif"></td>
					<td colspan=""> <img src="/application/public/img/element/checkbox-empty-no.gif"></td>
				</tr>
				<tr class="bold padding-top">
					<td colspan="">3</td>
					<td colspan="5">Temuan Audit yang Critical</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="5">Temuan berikut, jika tidak tepat ditangani, dapat mengakibatkan ketidaksesuaian major yang diidentifikasi pada Audit tahap 2.</td>
				</tr>
				<tr class="bold padding-top">
					<td colspan="">4.</td>
					<td colspan="5">Temuan Audit yang tidak Critical</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="5">Temuan berikut, jika tidak tepat ditangani, bisa menyebabkan kekurangan yang diidentifikasi pada audit tahap 2.</td>
				</tr>
				<tr class="bold padding-top">
					<td colspan="">5.</td>
					<td colspan="5">Kesimpulan Audit</td>
				</tr>
				
			</tbody>
		</table>

	</div>
	<div style="width: 650px;margin: auto;">
		<table >
			<tbody>
				<tr>
					<td colspan="2">Audit Tahap 1 berhasil dalam memenuhi sasaran yang dinyatakan :</td>
					<td colspan=""><img src="/application/public/img/element/checkbox-filled-yes.gif"> </td>
					<td colspan=""><img src="/application/public/img/element/checkbox-empty-no.gif"> </td>
				</tr>
				<tr>
					<td colspan="4">Audit Tahap 1 terbatas dalam waktu dan ruang lingkup dengan tujuan yang dinyatakan dan mungkin saja kekurangan tambahan yang akan diidentifikasi selama kegiatan audit di masa depan. Dengan pertimbangan untuk temuan diidentifikasi dalam bagian 3 dan 4 dari laporan ini, kesimpulan keseluruhan audit adalah sebagai berikut:</td>
				</tr>
				<tr>
					<td colspan="2">Sistem manajemen telah direncanakan agar sesuai dengan semua persyaratan standar audit:</td>
					<td colspan=""><img src="/application/public/img/element/checkbox-filled-yes.gif"> </td>
					<td colspan=""><img src="/application/public/img/element/checkbox-empty-no.gif"> </td>
				</tr>
				<tr>
					<td colspan="2">Sistem manajemen dirancang untuk mencapai tujuan kebijakan organisasi:</td>
					<td colspan=""><img src="/application/public/img/element/checkbox-filled-yes.gif"> </td>
					<td colspan=""><img src="/application/public/img/element/checkbox-empty-no.gif"> </td>
				</tr>
				<tr>
					<td colspan="2">Berdasarkan informasi yang diberikan, sistem ini dirancang untuk mengidentifikasi dan mengelola kepatuhan dengan undang-undang, persyaratan kontrak dan peraturan:  </td>
					<td colspan=""><img src="/application/public/img/element/checkbox-filled-yes.gif"> </td>
					<td colspan=""><img src="/application/public/img/element/checkbox-empty-no.gif"> </td>
				</tr>
				<tr>
					<td colspan="2">Audit internal dan tinjauan manajemen perencanaan menjamin cakupan yang tepat sebelum Audit Tahap 2:</td>
					<td colspan=""><img src="/application/public/img/element/checkbox-filled-yes.gif"> </td>
					<td colspan=""><img src="/application/public/img/element/checkbox-empty-no.gif"> </td>
				</tr>
				<tr>
					<td style="width: 400pt">Setiap pengecualian lingkup yang sesuai dan dibenarkan:</td>
					<td style="width: 35pt"><img src="/application/public/img/element/checkbox-empty-na.gif">  </td>
					<td style="width: 35pt"><img src="/application/public/img/element/checkbox-filled-yes.gif">  </td>
					<td style="width: 35pt"><img src="/application/public/img/element/checkbox-empty-no.gif">  </td>
				</tr>
				<tr>
					<td colspan="2">Audit Tahap 2 harus dilanjutkan seperti yang sudah direncanakan:</td>
					<td colspan=""><img src="/application/public/img/element/checkbox-filled-yes.gif"> </td>
					<td colspan=""><img src="/application/public/img/element/checkbox-empty-no.gif"> </td>
				</tr>
			</tbody>
			
		</table>
	</div>
	<div style="width: 650px;margin: auto;">
		Komentar atau rincian perubahan yang diperlukan untuk rencana Audit tahap 2 :
		<br />
		Tahap 2 audit dapat dilakukan sesuai rencana audit atas NC dan perlu diverifikasi pada tahap 2.
		<br />
		<div style="font-size: 14pt; font-weight: bold;text-align: center" >Audit Plan</div>
		<table>
			<tbody>
				<tr>
					<td colspan="" >Organisasi :</td>
					<td colspan="2" ><?php echo $project[0]->company_name ?></td>
				</tr>
				<tr>
					<td colspan="" >Alamat :</td>
					<td colspan="2" ><?php echo $project[0]->company_address_1 . ' '. $project[0]->company_address_2. ', '. $project[0]->company_city. ', ' . $project[0]->company_province . ' - Indonesia' ?></td>
				</tr>
				<tr>
					<td style="width: 165px" >Tanggal kunjungan aktual:</td>
					<td style="width: 275px" ><?php echo indDate($date) ?></td>
					<td style="width: 200px" > Kunjungan berdasarkan :
						<br />
						Jadwal yang telah ditetapkan
					</td>
				</tr>
				<tr>
					<td colspan="" >Lead Auditor:</td>
					<td colspan="2" >Bpk. Bpk. F. Rahman</td>
				</tr>
				<tr>
					<td colspan="" >Anggota Tim :</td>
					<td colspan="2" >Bpk. I. Akbar</td>
				</tr>
				<tr>
					<td colspan="" >Standar :</td>
					<td colspan="2" >ISO 14001:2004</td>
				</tr>
				<tr>
					<td colspan="" >Bahasa Audit :</td>
					<td colspan="2" >Bahasa Indonesia</td>
				</tr>
				
			</tbody>
		</table>
		<div>&nbsp;</div>
		<table>
			<tbody>
				<tr>
					<td colspan="2" >CATATAN: Rencana audit untuk surveilans Berikutnya atau Sertifikasi ulang disampaikan oleh auditor selama pertemuan penutupan.</td>
				<tr>
					<td style="width: 320px" >Pengakuan untuk menerima Rencana Audit untuk kunjungan surveilans berikutnya atau Re-Sertifikasi oleh Perwakilan Manajemen</td>
					<td style="width: 320px"  >Diterima oleh      :  
						<br />
						Tanda Tangan     :
					</td>
				</tr>
			</tbody>
		</table>
		<div style="text-align: center" >Tujuan Audit: untuk mengkonfirmasi bahwa sistem manajemen telah ditetapkan dan dilaksanakan sesuai dengan persyaratan standar audit.</div>
	</div>

</body>
</html>