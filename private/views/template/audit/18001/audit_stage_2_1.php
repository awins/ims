<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/audit.css">
	<style type="text/css">
		table {
			text-align: left;
			border-collapse: collapse;
		}

		table tr td {
			border: 1px solid black;
			padding: 5px;
			vertical-align: top;
			text-align: justify;
		}



		table.no-border tr td {
			border: 0;
			padding: 5px;

		}

		table tr.bold td {
			font-weight: bold;
		}

		table tr.padding-top td {
			padding-top: 10px;
		}


	</style>
</head>
<body>
	<?php 
	foreach ($project as $value) {
		if ($value->iso_type == 3){
			$date = $value->main_assessment_date;
			break;
		}
	}
	?>
	<div style="width: 650px;margin: auto;font-size: 16pt; font-weight: bold;text-align: center;padding:  10px 0 15px;">
		Sertifikasi Sistem Manajemen
		<br />
		Audit Stage 2 Report
	</div>
	<div style="width: 650px;margin: auto;">
		<table >
			<tbody>
				<tr>
					<td style="width: 250px;">Organisasi</td>
					<td style="width: 410px;"><?php echo $project[0]->company_name ?></td>
				</tr>
				<tr>
					<td colspan="">Alamat</td>
					<td colspan=""><?php echo $project[0]->company_address_1 . ' '. $project[0]->company_address_2. ', '. $project[0]->company_city. ', ' . $project[0]->company_province . ' - Indonesia' ?>
					</td>
				</tr>
				<tr>
					<td colspan="">Standard</td>
					<td colspan="">ISO 18001:2007</td>
				</tr>
				<tr>
					<td colspan="">No. Audit</td>
					<td colspan=""><?php echo $project[0]->project_number ?>C</td>
				</tr>
				<tr>
					<td colspan="">No Kunjungan</td>
					<td colspan="">001</td>
				</tr>
				<tr>
					<td colspan="">Tanggal Kunjungan Selanjutnya</td>
					<td colspan=""><b>16 Juni 2016</b></td>
				</tr>
				<tr>
					<td colspan="">Tipe Kunjungan</td>
					<td colspan="">Main Audit</td>
				</tr>
			</tbody>
		</table>
	</div>
	<br />
	<div style="width: 650px;margin: auto;">
		<table >
			<tbody>
				
				<tr>
					<td style="width: 250px;">Perwakilan Perusahaan</td>
					<td style="width: 410px;"><b>Bpk/Ibu</b></td>
				</tr>
				<tr>
					<td>Jumlah Lokasi Audit:</td>
					<td>Single Site </td>
				</tr>
				<tr>
					<td>Tanggal Audit:</td>
					<td><?php echo indDate(strtotime($date )) ?></td>
				</tr>
				<tr>
					<td>Kode EAC/NACE</td>
					<td><?php echo $project[0]->eac . '/' . $project[0]->nace ?></td>
				</tr>
				<tr>
					<td colspan="2" ><i>* Untuk multi-situs audit, semua situs diaudit akan tercantum dalam lingkup audit atau dalam lampiran</i></td>
				</tr>
				<tr>
					<td>Lead Auditor:</td>
					<td>Bpk. F. Rahman</td>
				</tr>
				<tr>
					<td>Anggota :</td>
					<td>Bpk. Fandie S.</td>
				</tr>
				<tr>
					<td>Inisial Lead Auditor:</td>
					<td>FR</td>
				</tr>
				<tr>
					<td colspan="2"><i>Laporan ini bersifat rahasia dan didistribusi-kan terbatas kepada tim audit, perwakilan klien dan kantor IMS-Indonesia.</i></td>
				</tr>
				
				
			</tbody>
		</table>

	</div>

</body>
</html>