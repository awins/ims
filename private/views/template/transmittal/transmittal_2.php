<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/transmittal.css">
</head>
<body>
	<div style="height:2px;" ></div>
	<div class="page" style="" >
		<div class="header">
			<strong>IMS-Indonesia</strong>
			<br />
			Graha Rinu's, Ruko Anggrek Blok D-1 No. 11
			<br />
			Jl. Boulevard Raya Grand Depok City, Depok - Indonesia
			<br />
			Phone / Fax: 7783 1389
			<br />
			Website : www.ims-indonesia.co.id
			<br />
			Email : info@ims-indonesia.co.id
		</div>
		<div style="clear: both" ></div>
		<div class="body" >
			<div class="garis" ></div>
			<div class="title" >JADWAL SERTIFIKASI DAN PENGAWASAN</div>
			<?php 
			foreach ($project as $value) { ?>
				<div style="font-size: 20pt; font-weight: bold" ><?php echo isoType($value->iso_type,'name') ?></div>
				<table id="table-iso" border="0" >
					<thead>
						<tr>
							<td style="width: 350px">STANDAR</td>
							<td style="width: 200px">TANGGAL</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td >TANGGAL SERTIFIKAT</td>
							<td ><?php echo indDate(strtotime($value->certificate_date)) ?></td>
						</tr>
						<tr>
							<td >AUDIT UTAMA</td>
							<td ><?php echo indDate(strtotime($value->main_assessment_date)) ?></td>
						</tr>
						<tr>
							<td >KUNJUNGAN PENGAWASAN PERTAMA</td>
							<td ><?php echo indDate(strtotime($value->surveillance_1_date)) ?></td>
						</tr>
						<tr>
							<td >KUNJUNGAN PENGAWASAN KEDUA</td>
							<td ><?php echo indDate(strtotime($value->surveillance_2_date)) ?></td>
						</tr>
						<tr>
							<td >RESERTIFIKASI</td>
							<td ><?php echo indDate(strtotime($value->renewal_assessment_date)) ?></td>
						</tr>
					</tbody>
				</table>
				<div style="font-size: 20pt" >&nbsp;</div>
				<?php
			} ?>
		</div>

	</div>
	<div style="position: absolute; bottom: 50px;width: 100%">
		<div class="body" style="background-color: black;height: 5px;margin: 0 70px 0 100px"> </div>
	</div>
</body>
</html>