<?php
	$header = $project[0];
	$arr_iso = array();
	$project_count = count($project);
	foreach ($project as $value) {
		$iso = array();
		$iso['detail'] = isoType($value->iso_type,'detail');
		$iso['name'] = isoType($value->iso_type,'name');
		$iso['number'] = $value->certificate_number_full; 
		$arr_iso[] = $iso;
	}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/transmittal.css">
</head>
<body>
	<div style="height:2px;" ></div>
	<div class="page" style="" >
		<div class="header">
			<strong>IMS-Indonesia</strong>
			<br />
			Graha Rinu's, Ruko Anggrek Blok D-1 No. 11
			<br />
			Jl. Boulevard Raya Grand Depok City, Depok - Indonesia
			<br />
			Phone / Fax: 7783 1389
			<br />
			Website : www.ims-indonesia.co.id
			<br />
			Email : info@ims-indonesia.co.id
		</div>
		<div style="clear: both" ></div>
		<div class="body" >
			<div class="garis" ></div>
			<table id="table-proyek" border="0" cellspacing="0" cellpadding="0">
				
				<tbody>
					<tr>
						<td style="width: 255px"  class="black">Ditujukan Kepada</td>
						<td style="width: 120px"  class="black">No. Proyek</td>
						<td   class="even" colspan="2"><?php echo $header->project_number ?></td>
					</tr>
					<tr>
						<td rowspan="5" style="vertical-align: top;font-size: 9pt;font-weight: bold" >
							<span style="font-size: 14pt"><?php echo $header->company_name ?></span>
							<br />
							<br />
							<?php echo $header->company_address_1 ?>
							<br />
							<?php echo $header->company_address_2 ?>
							<br />
							<?php echo str_replace(' - Indonesia','',cityAddress($header)) ?>
							<br />
							Indonesia
						</td>
						<td class="black">Jenis  Proyek </td>
						<td colspan="2">Sertifikasi Sistem Manajemen </td>
					</tr>
					<tr>
						<td rowspan="4" class="black" style="text-align:left; vertical-align:top">Perihal</td>
						<td style="width: 5x" class="even">-</td>
						<td style="width: 285px" class="even">Informasi Status</td>
					</tr>
					<tr>
						<td >-</td>
						<td>Informasi Penggunaan ID Klien</td>
					</tr>
					<tr>
						<td class="even">-</td>
						<td class="even" >Jadwal Sertifikasi Dan Pengawasan</td>
					</tr>
					<tr>
						<td style="text-align:left; vertical-align:top">-</td>
						<td >Ketentuan Tentang Pembekuan Dan Pencabutan Sertifikat ISO </td>
					</tr>
				</tbody>
			</table>
			<div class="title" >Informasi Status</div>
			<div style="text-align: justify;margin: 15px 0" >
				Setelah dilakukan pelaksanaan audit pada 
				<?php 
				$i = 0;
				foreach ($arr_iso as $value) {
					$i++;
					if ($i> 1){
						if ($i == $project_count){
							$sambung = " dan ";
						}else{
							$sambung = ", ";
						}
					}else{
						$sambung = null;
					}
					echo $sambung . $value['detail'];
				} ?>
				pada perusahaan anda <?php echo $header->company_name ?>, kami telah memasukkan informasi sertifikat 
				<?php 
				$i = 0;
				foreach ($arr_iso as $value) {
					$i++;
					if ($i> 1){
						if ($i == $project_count){
							$sambung = " dan ";
						}else{
							$sambung = ", ";
						}
					}else{
						$sambung = null;
					}
					echo $sambung . $value['name'];
				} ?>			
				perusahaan anda dalam website kami.
				<br />
				Berikut ini adalah Identitas <?php echo $header->company_name ?>.
			</div>
			<table id="table-certificate" border="1" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<td style="width: 225px"  class="black">Standard</td>
						<td style="width: 200px"  class="black">Nomor Sertifikat</td>
						<td style="width: 240px"  class="black">ID Klien</td>
					</tr>
				</thead>			
				<tbody>
					<?php
					$i=0;
					foreach ($arr_iso as $value) { 
						$i++;
						?>
						<tr>
							<td class="left" style="font-size: 12pt; font-weight: bold"><?php echo $value['name'] ?></td>	
							<td style="font-size: 12pt;"><?php echo $value['number'] ?></td>
							<?php
							if ($i == 1){ ?>
								<td class="right" style="font-size: 14pt;" rowspan="<?php echo $project_count ?>">
									Username : <?php echo $header->website_user ?>
									<br />
		      						Password : <?php echo $header->website_password ?>
								</td>	
								<?php
							} ?>
						</tr>
						<?php
					} ?>
					
				</tbody>
			</table>
			<div class="title" style="margin-top: 40px" >Informasi Penggunaan ID Klien</div>
			<div style="text-align: justify;margin: 15px 0" >
				Untuk kenyamanan anda dalam mengakses informasi ini, anda dapat mengikuti langkah-langkah di bawah ini:
			</div>
			<table class="table-information" border="0" >
				<tbody>
					<tr>
						<td class="left">1.</td>
						<td >Ketik <b>www.ims-indonesia.co.id</b> pada browser anda</td>
					</tr>
					<tr>
						<td class="left" >2.</td>
						<td >Pilih menu <b>Customer Area</b> Kemudian Pilih <b>Klient Sertifikasi</b> pada tampilan awal Website</td>
					</tr>
					<tr>
						<td class="left" >3.</td>
						<td>Pilih Client Login di pojok kanan bawah tampilan website, kemudian masukan Username beserta Password yang telah kami berikan.</td>
					</tr>
					<tr>
						<td class="left" >4.</td>
						<td>Pada area klien (setelah login), Klik <b>Recent Page</b> untuk informasi keabsahan sertifikat anda.</td>
					</tr>
					<tr>
						<td class="left" >5.</td>
						<td>Website akan menampilkan informasi mengenai status sertifikat perusahaan anda</td>
					</tr>

				</tbody>
			</table>
		</div>

	</div>
	<div style="position: absolute; bottom: 50px;width: 100%">
		<div class="body" style="background-color: black;height: 5px;margin: 0 70px 0 100px"> </div>
	</div>
</body>
</html>