<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/transmittal.css">
</head>
<body>
	<?php 

	$i = 0;
	$project_count = count($project);
	$certificate = null;
	foreach ($project as $value) {
		$i++;
		if ($i> 1){
			if ($i == $project_count){
				$sambung = " dan ";
			}else{
				$sambung = ", ";
			}
		}else{
			$sambung = null;
		}
		$certificate .= $certificate. $sambung . isoType($value->iso_type,'name');
	} ?>

	<div style="height:2px;" ></div>
	<div class="page" style="" >
		<div class="header">
			<strong>IMS-Indonesia</strong>
			<br />
			Graha Rinu's, Ruko Anggrek Blok D-1 No. 11
			<br />
			Jl. Boulevard Raya Grand Depok City, Depok - Indonesia
			<br />
			Phone / Fax: 7783 1389
			<br />
			Website : www.ims-indonesia.co.id
			<br />
			Email : info@ims-indonesia.co.id
		</div>
		<div style="clear: both" ></div>
		<div class="body" >
			<div class="garis" ></div>
			<div class="title" >KETENTUAN TENTANG PEMBEKUAN DAN PENCABUTAN SERTIFIKAT ISO</div>
			<div style="text-align: justify;margin: 15px 0" >
				Berdasarkan ISO 17021:2013 dan Peraturan Perusahaan PT. IMS-Indonesia
			</div>
			<table class="table-information" border="0" >
				<tbody>
					<tr>
						<td class="left">1.</td>
						<td >Apabila klien tidak mengikuti kegiatan surveillance pada waktu yang telah ditetapkan berdasarkan kontrak kesepakatan yang telah ditanda tangani oleh kedua belah pihak.</td>
					</tr>
					<tr>
						<td class="left">2.</td>
						<td >IMS-Indonesia akan melakukan <strong>PEMBEKUAN</strong> Sertifikat <?php echo $certificate ?> yang telah diperoleh oleh pihak klien. Selama masa <strong>PEMBEKUAN</strong> Sertifikat <?php echo $certificate ?>, pihak klien dilarang menggunakan Sertifikat, Logo dan seluruh perangkat <?php echo $certificate ?> yang diberikan PT. IMS-Indonesia untuk keperluan promosi lebih lanjut atau keperluan lain. 
							<br />
							Status <strong>PEMBEKUAN</strong> disediakan diakses publik PT. IMS-Indonesia.
							(Proses <strong>PENCABUTAN</strong> akan diterapkan jika pihak klien melanggar proses <strong>PEMBEKUAN</strong>)
						</td>
					</tr>
					<tr>
						<td class="left">3.</td>
						<td >
							Apabila klien tidak mengikuti aturan penggunaan tanda sertifikasi seperti yang telah dijelaskan oleh PT. IMS-Indonesia, maka :
							<table class="inner" border="0" >
								<tr>
									<td class="left">-</td>
									<td >PT. IMS-Indonesia menginstruksikan kepada pihak klien untuk memperbaiki ketidaksesuaian dalam batas waktu tertentu.</td>
								</tr>
								<tr>
									<td class="left">-</td>
									<td >Bila pihak klien tidak memperbaiki ketidaksesuaian dalam batas waktu yang telah disepakati. PT. IMS-Indonesia akan membekukan status sertifikat yang telah diterima oleh pihak klien dengan ketentuan proses pembekuan yang berlaku.</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td class="left">4.</td>
						<td >
							Bila ada ketidaksesuaian/temuan “major’ yang ditemukan selama audit surveillance dan tidak dapat diperbaiki oleh pihak klien dalam jangka waktu tertentu, maka :
							<table class="inner" border="0" >
								<tr>
									<td class="left">-</td>
									<td >PT. IMS-Indonesia akan menanggguhkan penggunaan sertifikat dan menginstruksikan pihak klien untuk memperbaiki ketidaksesuaian “major” dalam jangka waktu tertentu.</td>
								</tr>
								<tr>
									<td class="left">-</td>
									<td >Bila klien tidak dapat memperbaiki ketidaksesuaian “major” yang ada dalam jangka waktu yang telah disepakati, PT. IMS-Indonesia akan melakukan PEMBEKUAN atas status sertifikasi <?php echo $certificate ?> pihak klien.</td>
								</tr>
							</table>

						</td>
					</tr>
					
				</tbody>
			</table>
			<div style="text-align: justify;margin: 15px 0" >
				Kegiatan Surveillance diadakan setiap satu tahun sekali dari masa berlaku sertifikat ISO yaitu selama tiga tahun. Untuk menjaga komunikasi yang baik  antara <strong>PT. IMS-Indonesia</strong> dengan <strong><?php echo $project[0]->company_name ?></strong> serta untuk <i>pengecekan keabsahan sertifikat ketika perusahaan</i> <strong><?php echo $project[0]->company_name ?></strong> <i>mengikuti program tender atau lelang di pemerintahan atau di</i> <strong>BUMN</strong> <i>lainnya</i>,  kami harap dapat mengirimkan alamat Email perusahaan <strong><?php echo $project[0]->company_name ?></strong> yang valid ke alamat Email kami yaitu “ <span style="color: blue">info@ims-indonesia.co.id</span> “ untuk di input kedalam Website kami sebagai tambahan  informasi tentang <strong><?php echo $project[0]->company_name ?>.</strong>
			</div>
		</div>

	</div>
	<div style="position: absolute; bottom: 50px;width: 100%">
		<div class="body" style="background-color: black;height: 5px;margin: 0 70px 0 100px"> </div>
	</div>
</body>
</html>