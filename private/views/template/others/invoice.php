<html>
<head>
	<style type="text/css">
		body {
		 	font-family: cambria; 
			/*color: rgb(192,0,0);*/
		}

		table {
			border-collapse: collapse;
			width: 100%;

		}

		table#tbl-header tr td{
			border-collapse: collapse;
			border: 1px solid rgb(83,141,213) ;
		}

		table#tbl-body tr th{
			border: 1px solid rgb(83,141,213) ;
		}

		table#tbl-body tr td{
			border: 0.5px solid black ;
			padding: 1px 5px;
		}

		table#tbl-body tr td.left {
			border-left: 1px solid rgb(83,141,213) ;
		}

		table#tbl-body tr td.right{
			border-right: 1px solid rgb(83,141,213) ;
		}

		table#tbl-body tr td.bottom{
			border-bottom: 1px solid rgb(83,141,213) ;
		}

		table#tbl-body tr.no-border td {
			border: 0;
			text-align: right;
		}
		

		
	</style>
</head>
<body>
	<div style="padding: 20px 10px 10px 20px;border: 2px solid rgb(149,179,215);width: 700px; height: 1000px" >
		<div style="float: left; width: 300px ;height: 54.72px " >
			<img src="\application\public\img\logo\logo-IMS.png" style="width: 160.32px ;height: 54.72px">
		</div>
		<div style="float: left; width: 380px ;height: 54.72px;color: rgb(192,0,0);text-align: center;font-size: 16pt;font-weight: bold;line-height: 54px;vertical-align: middle " >
			INVOICE
		</div>
		<div style="float: left; width: 400px ;height: 54.72px " >
			Graha Rinu's, Ruko Anggrek Blok D-1 No. 11
			<br />
			Jl. Boulevard Raya Grand Depok City, Depok - Indonesia
			<br />
			Phone / Fax: 7783 1389
			<br />
			Website : www.ims-indonesia.co.id
			<br />
			Email : info@ims-indonesia.co.id	
		</div>
		<div style="float: left; width: 280px ;height: 54.72px;color: rgb(192,0,0);text-align: center;font-size: 11pt;font-weight: bold " >
			
			<table id="tbl-header" border="0" >
				<thead>
					<tr>
						<th style="width: 120px;height: 15pt"></th>
						<th style=""></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 120px">Invoice Number</td>
						<td style="">IMS-<?php echo date('y') . '-' . $project[0]->proposal_index ?></td>
					</tr>
					<tr>
						<td >Date</td>
						<td ><?php echo date('d-M-Y',strtotime($project[0]->request_date) ) ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="padding-top: 20px" >
			<div style="" ><b>Ditagih Kepada</b></div>
			<div style="float: left;width: 100px" >Perusahaan</div>
			<div style="float: left;width: 30px;text-align: center" >:</div>
			<div style="width: 500px"><b><?php echo $project[0]->company_name ?></b></div>

			<div style="float: left;width: 100px" >Alamat</div>
			<div style="float: left;width: 30px;text-align: center" >:</div>
			<div style=""><?php echo $project[0]->company_address_1 . ' ' . $project[0]->company_address_2 ?></div>

			<div style="float: left;width: 100px" >Kota</div>
			<div style="float: left;width: 30px;text-align: center" >:</div>
			<div style=""><?php echo str_replace(' - Indonesia','',cityAddress($project[0])) ?></div>

			<div style="float: left;width: 100px" >No. Telp</div>
			<div style="float: left;width: 30px;text-align: center" >:</div>
			<div style=""><?php echo $project[0]->company_phone ?></div>
<!-- 
			<div style="float: left;width: 100px" >Email</div>
			<div style="float: left;width: 30px;text-align: center" >:</div>
			<div style=""><?php echo $project[0]->company_email ?></div> -->
		</div>
		<?php
		if (count($project) > 1){
			$row_price = 2;
		}else{
			$row_price = 1;
		} 

		$row_add = 4 + (3 - count($project));
		?>
		<div style="padding-top: 20px" >
			<table id="tbl-body" >
				<thead>
					<tr >
						<th style="width: 100px" >Jumlah</th>
						<th style="width: 250px" >Deskripsi</th>
						<th style="width: 100px" >Harga Unit</th>
						<th style=""  class="right">Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border-top: 0" class="left">&nbsp;</td>
						<td style="border-top: 0">&nbsp;</td>
						<td style="border-top: 0">&nbsp;</td>
						<td style="border-top: 0" class="right">&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align: center" class="left">1</td>
						<td>Pembuatan Sertifikasi</td>
						<td></td>
						<td class="right"></td>
					</tr>
					<?php
					$i = 0;
					foreach ($project as $key => $value) { 
						$i++;
					 	?>
						<tr>
							<td class="left"></td>
							<td><?php echo isoType($value->iso_type,'name') ?></td>
							<td style="text-align: center">
								<?php echo ($i == $row_price)? number_format($value->certificate_amount,0): null; ?>
							</td>
							<td style="text-align: right;" class="right">
								<?php echo ($i == $row_price)? number_format($value->certificate_amount,0): null; ?>
							</td>
						</tr>
						<?php
					} 

					for ($row=0; $row < $row_add ; $row++) {  ?>
						<tr>
							<td style="border-top: 0" class="left">&nbsp;</td>
							<td style="border-top: 0" class="">&nbsp;</td>
							<td style="border-top: 0" class="">&nbsp;</td>
							<td style="border-top: 0" class=" right">&nbsp;</td>
						</tr>
						<?php
					} ?>

					<tr>
						<td style="border-top: 0" class="left bottom">&nbsp;</td>
						<td style="border-top: 0" class="bottom">&nbsp;</td>
						<td style="border-top: 0" class="bottom">&nbsp;</td>
						<td style="border-top: 0" class="bottom right">&nbsp;</td>
					</tr>

					<tr class="no-border">
						<td></td>
						<td></td>
						<td>Subtotal:</td>
						<td><?php echo number_format($value->certificate_amount,0)?></td>
					</tr>

					<tr class="no-border">
						<td></td>
						<td></td>
						<td>Sales Tax:</td>
						<td>0</td>
					</tr>

					<tr class="no-border">
						<td></td>
						<td></td>
						<td>Total Due:</td>
						<td><?php echo number_format($value->certificate_amount,0)?></td>
					</tr>

				</tbody>
			</table>
		</div>
		<div style="padding-top: 40px;width: 100%" >
			Account Bank :
			<br />
			MANDIRI 157.000.4069.705
			<br />
			a/n PT. Internasional Manajemen Sertifikasi
		</div>
		<div style="padding-top: 20px;width: 100%" >
			<div style="float: left;width: 300px;height: 300px" >
				TERBILANG = <?php echo terbilang($project[0]->certificate_amount) ?> Rupiah
			</div>
			<div style="float: left;width: 380px;" >
				<div style="" >
					&nbsp;
				</div>
				<div style="border-top: 1px solid black;text-align: center" >
					Ria Anggraeni
				</div>
				
			</div>
		</div>
	</div>
    <div style="width: 100%;position: absolute;bottom: 70px;text-align: center;color: rgb(150,54,52); font-size: 11pt" >Thank you for your business!</div>
</body>
</html>