<html>
<head>
	<style type="text/css">
		body {
		 	font-family: cambria; 
			color: rgb(192,0,0);
			font-weight: bold;

		}

		div.border{
			border-radius: 25px;
		    border: 3pt solid rgb(192,0,0);
		    height: 168px;
		    display: inline-block;
		    position: absolute;
		    width: 355px;
		    margin: 0 0 40px 0;
		}

		div.image{
			float: left;
			padding: 20px 0 0 0;
		    display: inline-block;
		    width: 103px;
			height: 53px;
		}

		div.com_name{
			padding: 10px;
			font-size: 20pt;
			float:left;
			width: 232px;
		    display: inline-block;
		}

		div.address{
			width: 100%;
			padding: 5px 5px 5px 10px;
			font-size: 12pt;
		}

		img {
			width: 103px;
			height: 53px;
		}
	</style>
</head>
<body>
	<div style="padding: 20px" >
		<?php 
		for ($i=1; $i <= 2 ; $i++) {  ?>
			<div class="border"  >
				<div style="position: relative" >
					<div  class="com_name" >
						<?php echo $company->company_name ?>
					</div>
					<div class="image" >
						<img src="/application/public/img/logo/logo-IMS-shadow.png">
					</div>
				</div>
				<div class="address" >
					<?php echo $company->company_address_1 ?>
					<br />
					<?php echo $company->company_address_2 ?>
					<br />
					<?php //echo $company->company_city . ', ' . $company->company_province . ' ' . $company->company_zip ?>
					<?php echo cityAddress($company) ?>
				</div>
			</div>
			<?php
		} ?>
	</div>
    
</body>
</html>