<html>
<head>
	<style type="text/css">
		body {
		 	font-family: cambria; 
		}
		div#border{
			width: 189px;
			height: 575px;
			border: 1px solid black;
			text-align: center;
			padding-top: 5px;
			position: relative;
		}

		div#project_number {
			padding: 5px 15px 5px 80px;
			height: 76px;
		}
		div#project_number div{
			float: right;
			width: 100%;
			border: 2px solid rgb(237,125,49);
			text-align: center;
			line-height: 76px;
			vertical-align: middle;
			font-size: 25pt;
			font-weight: bold;
		}

		div.initial{
			padding: 5px 5px 5px 5px;
		}
		div.initial div{
			
			border: 2px solid red;
			font-size: 60pt;
			font-weight: bold;
		}

		div.initial div.span{
			font-size: 14pt;
			font-weight: bold;
			color: red;
			width: 100%;
			border: 0;

		}

		div.initial img{
			width: 50px;
			height: 50px;
			border: 1px solid rgb(192,0,0);
		}


	</style>
</head>
<body>
	<div id="border" >
		<div class="image" >
			<img src="/application/public/img/logo/logo-IMS.png">
			<span style="font-size: 7pt">PT. Internasional Manajemen Sertifikasi</span>
		</div>

		<div id="project_number" >
			<div>
				<?php echo $project[0]->project_number ?>
			</div>
		</div>
		<div class="initial">
			<div>
				<?php echo $project[0]->company_initial ?>
			</div>
		</div>

		<div class="initial">
			<?php echo $project[0]->company_city ?>
			<br />
			<?php echo $project[0]->company_province ?>
			<br />
			Indonesia
		</div>

		<div class="initial" style="padding-top: 10px">

			<div class="span">STANDAR</div>
			<?php 
			foreach ($project as $key => $value) { ?>
				<img src="/application/public/img/logo/thumb/CERT_<?php echo $value->iso_type ?>.png">
				<?php 
			} ?>
		</div>
		<div class="initial" style="padding-top: 20px">
			<?php 
			foreach ($project as $key => $value) { 
				echo $value->certificate_number_full ;
			} ?>
		</div>

	</div>
</body>
</html>