<html>
<head>
	<style type="text/css">
		body {
		 	font-family: cambria; 
			color: rgb(192,0,0);
			font-weight: bold;

		}

		div.border{
			border-radius: 25px;
		    border: 3pt solid rgb(192,0,0);
		    height: 168px;
		    display: inline-block;
		    position: absolute;
		    width: 355px;
		    margin: 0 0 40px 0;
		}

		div.image{
			float: left;
			padding: 20px 0 0 0;
		    display: inline-block;
		    width: 143px;
			height: 112px;
		    border: 2pt solid rgb(192,0,0);
		    vertical-align: middle;

		}

		div.image div {
			margin: auto;
			width: 103px;
			height: 53px;
		}

		div.com_name{
			padding: 10px;
			font-size: 10pt;
			float:left;
			width: 468px;
			height: 112px;
		    display: inline-block;
		    border: 2pt solid rgb(192,0,0);
		}

		div.address{
			width: 100%;
			padding: 5px 5px 5px 10px;
			font-size: 12pt;

		}

		img {
			width: 103px;
			height: 53px;
		}
	</style>
</head>
<body>
	<div style="padding: 20px" >
		<div class="image" >
			<div>
				<img src="/application/public/img/logo/logo-IMS.png">
			</div>
		</div>
		<div class="com_name" >
			<div style="float: left; width: 70pt">
				Kpd. Yth :
			</div>
			<div style="float: left;">
				<span style="font-size: 16pt; font-weight: bold" ><?php echo $company->company_name ?></span>
				<br />
   				<?php echo $company->company_address_1 ?>
				<br />
				<?php echo $company->company_address_2 ?>
				<br />
				<?php echo $company->company_city . ', ' . $company->company_province . ' - Indonesia ' . $company->company_zip ?>

			</div>

		</div>
		<div style="height: 165px;border-bottom: 1px solid rgb(192,0,0);width: 100%" >&nbsp;</div>
		<div style="color: rgb(192,0,0);text-align: right;font-size: 10pt" >
			IMS-Indonesia
			<br />
			Jl. Boulevard Raya, DOC Blok A.18
			<br />
			Grand Depok City, Depok, Indonesia
			<br />
			Phone / Fax : (021) 29416128
			<br />
			Website : www.ims-indonesia.co.id
			<br />
			Email : info@ims-indonesia.co.id

		</div>
	</div>

    
</body>
</html>