<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/proposal.css">
</head>
<body>
	<div style="padding-left: 30px;">
		<table class="table-information" border="0" >
			<thead>
				<tr>
					<td style="width: 30px" class=""></td>
					<td style="width: 35px"></td>
					<td style="width: 30px"></td>
					<td style=""></td>
				</tr>
			</thead>
			<tbody>
				<tr class="title">
					<td  class="">1.</td>
					<td colspan="3" ><u>Perjanjian</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td colspan="3" >
						Perjanjian ini dibuat pada tanggal tersebut, disini antara <b>pihak (1) PT. IMS-Indonesia</b> dan <b>pihak (2) Klien</b>, yang beralamat sesuai dengan yang tertera di atas.
						Apabila dalam pelaksanaan Perjanjian Bersama ini para pihak merasa perlu melakukan perubahan, maka perubahan tersebut hanya dapat dilakukan atas kesepakatan PARA PIHAK yang dituangkan dalam Addendum Perjanjian ini dan merupakan  bagian yang tidak dapat dipisahkan dari Perjanjian ini.

					</td>
				</tr>

				<tr class="title">
					<td  class="">2.</td>
					<td colspan="3" ><u>Layanan-layanan</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td colspan="3" ><b>PT. IMS-Indonesia</b> akan memberikan layanan-layanan sebagai berikut  : </td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">2.1</td>
					<td colspan="2" >IMS-Indonesia melaksanakan penilaian awal (main assessment) terhadap sistem manajemen pihak klien dengan mengaudit bukti objektif dan bukti kesesuaian yang memadai sebagai dasar pengambilan keputusan sertifikasi. penilaian awal (main assessment) terbagi dalam dua tahap audit yaitu audit tahap 1 dan audit tahap 2.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">2.2</td>
					<td colspan="2" >Memberikan laporan yang tertulis untuk setiap audit. Tim audit dapat mengidentifikasi peluang untuk perbaikan namun tidak dapat merekomendasikan penyelesaian tertentu. <b>PT. IMS-Indonesia</b> memelihara kepemilikan laporan audit.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">2.3</td>
					<td colspan="2" >Mengkaji koreksi dan tindakan korektif yang diajukan oleh Klien untuk menentukan keberterimaannya. <b>PT. IMS-Indonesia</b> memastikan bahwa klien telah mengidentifikasi secara efektif penyebab seluruh ketidaksesuaian dan memverifikasi keefektifan setiap tindakan koreksi dan korektif yang diambil.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">2.4</td>
					<td colspan="2" >Kunjungan audit surveillance akan dilaksan akan sesuai yang tertera pada jadwal. Tujuan dilakukan audit surveillance oleh <b>PT. IMS-Indonesia</b> adalah memelihara kepercayaan bahwa sistem manajemen yang disertifikasi tetap memenuhi persyaratan diantara audit sertifikasi ulang.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">2.5</td>
					<td colspan="2" >
						2.5	Melakukan Kunjungan untuk memperbaharui sertifikat (Renewal) sekitar3 (tiga) tahun untuk mengkonfirmasi keberlanjutan kesesuaian dan efektifitas sistem manajemen secara keseluruhan, serta relevansi dan kemampuan organisasi terhadap lingkup sertifikasi.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">2.6</td>
					<td colspan="2" >Apabila terdapat ketidak sesuaian yang dilaporkan selama  kunjungan - kunjungan  penilaian,  maka sertifikat akan diterbitkan kepada pihak klien, dengan ketentuan adanya tambahan kunjungan khusus yang telah dijadwalkan dan dilaksanakan untuk melakukan verifikasi terhadap tindakan – tindakan perbaikan.
					</td>
				</tr>


				<tr class="title">
					<td  class="">3.</td>
					<td colspan="3" ><u>Kewajiban-kewajiban klien</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">3.1</td>
					<td colspan="2" >Menyediakan akses keseluruh informasi dan fasilitas yang diperlukan bagi pihak <b>PT. IMS-Indonesia</b>.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">3.2</td>
					<td colspan="2" >Mengelola sistem manajemennya sesuai dengan standard-standard :
						<br />
						<?php 
						$i = 0;
						$project_count = count($project);
						foreach ($project as $value) {
							$i++;
							if ($i> 1){
								if ($i == $project_count){
									$sambung = " dan ";
								}else{
									$sambung = ", ";
								}
							}else{
								$sambung = null;
							}
							echo '<span style="color: blue">' . $sambung . isoType($value->iso_type,'name') . '</span>' ;
						} ?>
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">3.3</td>
					<td colspan="2" >Jika terkena proses “Pembekuan”, pihak klien dilarang menggunakan  sertifikasi sistem manajemen untuk keperluan promosi lebih lanjut.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">3.4</td>
					<td colspan="2" >Status pembekuan sertifikasi disediakan diakses publik <b>PT. IMS-Indonesia</b> dan bersifat rahasia.
					<br />Beberapa contoh kasus yang menyebabkan pembekuan sertifikasi :
					</td>
				</tr>

				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">a.</td>
					<td class="">Sistem manajemen yang disertifikasi gagal secara total dan serius dalam memenuhi persyaratan sertifikasi, termasuk persyaratan efektivitas sistem manajemen.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">b.</td>
					<td class="">Pelanggan yang disertifikasi tidak memperbolehkan audit surveillance atau sertifikasi ulang dilaksanakan pada frekuensi yang dipersyaratkan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">c.</td>
					<td class="">Pelanggan yang disertifikasi telah meminta pembekuan secara sukarela.
					</td>
				</tr>

				<tr>
					<td class=""></td>
					<td class="">3.5</td>
					<td colspan="2" >Bila Klien gagal secara total memenuhi persyaratan sertifikasi untuk bagian-bagian dari ruang lingkup sertifikasi tersebut maka <b>PT. IMS-Indonesia</b>  akan mengurangi ruang lingkup sertifikasi pelanggan untuk bagian-bagian yang tidak memenuhi persyaratan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">3.6</td>
					<td colspan="2" >Klien tidak melanjutkan penggunaan status sertifikasi pada materi periklanan  yang memuat referensi status sertifikasinya dan mengembalikan sertifikat berdasarkan persyaratan pencabutan. Kondisi status pencabutan adalah atas tindak lanjut dari proses pembekuan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">3.7</td>
					<td colspan="2" >Memberitahu pihak <b>PT. IMS-Indonesia</b> mengenai segenap rencana untuk mengubah sistem manajemen yang disetujui tersebut dan segenap perubahan yang mungkin dapat mempengaruhi kesesuaian sistem manajemennya dengan kriteria yang dirujuk dalam perjanjian ini dan  segenap persetujuan yang dikeluarkan menurut perjanjian ini sebelum setiap perubahan - perubahan tersebut diimplementasi-kan.
					</td>
				</tr>
				
				<tr class="title">
					<td class="">4.</td>
					<td colspan="3" ><u>Kewajiban-kewajiban PT. IMS-Indonesia</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">4.1</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> bertanggung-jawab untuk menyediakan personil yang sesuai untuk melaksanakan layanan-layanan tersebut.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">4.2</td>
					<td colspan="2" >Pada tahap akhir suatu penilaian yang memuaskan atas system manajemen pihak Klien, pihak <b>PT. IMS-Indonesia</b>  akan menerbitkan sebuah Sertifikat.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">4.3</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b>  bertanggung jawab untuk menyelesaikan proses penanganan banding dan keluhan yang terkait dengan proses sertifikasi yang dilakukan <b>PT. IMS-Indonesia</b>.
					</td>
				</tr>
				<tr class="title">
					<td class="">5.</td>
					<td colspan="3" ><u>Sertifikat Persetujuan dan Komplain terhadap pihak Klien</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">5.1</td>
					<td colspan="2" >Sertifikat tersebut berlaku sejak tanggal persetujuan tersebut diterbitkan hingga waktu 3 (tiga) tahun,dan berlaku pula untuk kunjungan - kunjungan rutin surveillance.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">5.2</td>
					<td colspan="2" >Sertifikat tersebut harus diperbaharui pada tahun ke – 3 sejak tanggal penerbitannya. Jadwal waktu kunjungan tersebut akan diberitahukan pada saat penyelesaian kunjungan sebelumnya
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">5.3</td>
					<td colspan="2" >Sertifikat tersebut tetap menjadi hak milik pihak <b>PT. IMS-Indonesia</b> . Apabila persetujuan ini berakhir atau dicabut, maka merupakan tanggung – jawab pihak Klien untuk mengembalikan sertifikat tersebut kepada <b>PT. IMS-Indonesia</b> .
					</td>
				</tr>

				<tr class="title">
					<td class="">6.</td>
					<td colspan="3" ><u>Biaya – biaya &amp; Jangka Waktu Pembayaran</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.1</td>
					<td colspan="2" >Pada First Payment, <b>PT. IMS-Indonesia</b>  akan memproses invoice terhitung 14 hari kerja sejak invoice diterima client.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.2</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b>  akan mengirimkan Surat Pemberitahuan pertama 7 hari sebelum tanggal jatuh tempo pembayaran ke pihak klien, untuk mengingatkan Klien tanggal jatuh tempo pembayaran.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.3</td>
					<td colspan="2" >Surat Pemberitahuan kedua akan dikirimkan 3 hari sebelum tanggal jatuh tempo pembayaran kepada pihak klien oleh <b>PT. IMS-Indonesia</b> , untuk mengingatkan agar segera melakukan pembayaran.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.4</td>
					<td colspan="2" >3 hari sebelum batas waktu pembayaran (14 hari sejak tanggal yang tertera dalam faktur) klien belum melakukan pembayaran maka <b>PT. IMS-Indonesia</b>  akan mengirimkan surat pemberitahuan mengenai sanksi yang diberikan terhitung setelah batas waktu pembayaran.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.5</td>
					<td colspan="2" >Klien yang belum melakukan pembayaran setelah 14 hari sejak tanggal yang tertera dalam fakturnya maka akan dikenakan denda sebesar 1/1000 dari tagihan biaya layanan tersebut perhari keterlambatan dengan jumlah maksimum sebesar 5% (Lima Persen) dari jumlah tagihan biaya layanannya dan berakibatkan penangguhan setiap atau segenap layanannya sampai dengan tunggakan tersebut termasuk bunganya telah dilunasi.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.6</td>
					<td colspan="2" >Apabila klien tetap tidak melakukan pembayaran setelah 7 hari batas waktu pembayaran maka <b>PT. IMS-Indonesia</b>  berhak memutuskan status kontrak sertifkasi klien tersebut.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.7</td>
					<td colspan="2" >6Untuk Second Payment akan diproses 14-21 hari kerja sejak invoice diterima client yang dilakukan dengan cara dihubungi dan kunjungan ke client. Ketentuan proses penagihan dan sanksi yang dikenakan sesuai yang telah diatur.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">6.8</td>
					<td colspan="2" >Biaya transportasi dan biaya lain timbul akibat pelaksanaan kunjungan audit ke lokasi Klien, yang akan ditagihkan kepada Klien
						<br />
						<b>Biaya-biaya transportasi sebagai berikut :</b>
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">•</td>
					<td class="">Dengan jalur udara, minimal kelas ekonomi</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">•</td>
					<td class="">Dengan kereta api, minimal kelas bisnis</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">•</td>
					<td class="">Dengan kendaraan lain, sesuai tagihan aktual / biaya yang wajar</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td colspan="2" ><b>Biaya-biaya akomodasi :</b>
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">•</td>
					<td class="">Hotel, minimal bintang tiga</td>
				</tr>

				<tr>
					<td class=""></td>
					<td class="">6.9</td>
					<td colspan="2" >Total biaya audit awal akan ditagihkan 50% (lima puluh persen) setelah penandatanganan kontrak dan 50% (lima puluh persen) berikutnya setelah audit tahap kedua dilaksanakan.Dan total biaya audit peninjauan / pengawasan akan dibayarkan sebelum pelaksaan audit dilakukan.
					</td>
				</tr>

				<tr class="title">
					<td class="">7.</td>
					<td colspan="3" ><u>Biaya – biaya Pembatalan/Penundaan</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td colspan="3" >Apabila pihak Klien membatalkan atau menunda tanggal kunjungan 2 minggu sebelum jadwal yang sudah disetujui, maka pihak Klien diwajibkan untuk mengganti biaya – biaya yang timbul sehubungan dengan jadwal yang telah disetujui.
					</td>
				</tr>

				<tr class="title">
					<td class="">8.</td>
					<td colspan="3" ><u>Kerahasiaan</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td colspan="3" >Pihak <b>PT. IMS-Indonesia</b>, wajib untuk menjaga kerahasiaan dan tidak menggunakan atau mengungkapkan kepada pihak ketiga, setiap informasi yang diperoleh dari pihak Klien berkaitan dengan Layanan tersebut tanpa persetujuan pihak Klien, kecuali sepanjang hal tersebut diperlukan agar Layanan tersebut dapat dilakukan sesuai dengan syarat – syarat Perjanjiannya.
	Kewajiban tersebut akan terus berlaku penuh dan mengikat selama jangka waktu Perjanjian dan setelah berakhirnya Perjanjian ini, asalkan termasuk dalam pembatasan – pembatasan berikut:
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">(i)</td>
					<td colspan="2" >Setiap informasi yang telah dimiliki oleh pihak <b>PT. IMS-Indonesia</b>  sebelum pengungkapannya  kepada pihak Klien atau ;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">(ii)</td>
					<td colspan="2" >Setiap informasi yang secara hukum merupakan hak masyarakat, atau ;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">(iii)</td>
					<td colspan="2" >setiap informasi yang selain itu menjadi tersedia untuk pihak <b>PT. IMS-Indonesia</b>  dari sebuah sumber independen pihak Klien, atau ;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">(iv)</td>
					<td colspan="2" >setiap informasi lainnya yang mungkin disyaratkan untuk disediakan dalam rangka memperoleh atau mengelola Akreditasi pihak <b>PT. IMS-Indonesia</b>
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">(v)</td>
					<td colspan="2" >setiap informasi mengenai klien dari sumber selain klien (mis. Keluhan, regulasi) akan diperlakukan secara rahasia dan selaras sesuai kebijakan yang berlaku di <b>PT. IMS-Indonesia</b> .
					</td>
				</tr>
				
				<tr>
					<td class=""></td>
					<td colspan="3" >Jika informasi rahasia dibuat untuk badan-badan lainnya (misalnya badan akreditasi, kesepakatan kelompok sebuah skema penilaian sejawat), <b>PT. IMS-Indonesia</b>  akan menginformasikan pihak klien dari tindakan ini
					</td>
				</tr>

				<tr class="title">
					<td class="">9.</td>
					<td colspan="3" ><u>Kewajiban</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">9.1</td>
					<td colspan="2" >Dalam memberikan pelayanan, informasi, atau pun saran, <b>PT. IMS-Indonesia</b>  menjamin keakuratan informasi, review, audit, sertifikasi, atau saran yang diberikan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">9.2</td>
					<td colspan="2" >Kecuali sebagai mana yang diuraikan disini, pihak  <b>PT. IMS-Indonesia</b>  tidak bertanggung – jawab atas setiap kehilangan, kerusakan, atau pengeluaran apapun yang disebabkan oleh pihak <b>PT. IMS-Indonesia</b>, atau yang disebabkan oleh kurangnya keakuratan apapun, dalam setiap pemberitahuan, tinjauan, audit, sertifikasi, atau pemberitahuan yang diberikan dengan cara apapun juga yang diberikan sebagi pelayanan atas nama <b>PT. IMS-Indonesia</b>, 
						juga bila diadakan sehubungan dengan pelanggaran pernyataan
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">9.3</td>
					<td colspan="2" >Meskipun demikian, apabila setiap orang yang merupakan salah satu pihak dalam Perjanjian yang menurut  Perjanjian tersebut adalah pihak yang menggunakan layanan PT. IMS-Indonesia  atau berdasarkan kepada informasi, tinjauan audit, sertifikasi, atau pemberitahuan yang diberikan oleh atau atas nama pihak PT. IMS-Indonesia  dan menderita kerugian, kerusakan, atau pengeluaran yang terbukti disebabkan dengan setiap tindakan kelalaian, penghapusan, 
					atau kekeliruan pihak PT. IMS-Indonesia , atau setiap ketidak-akuratan dalam informasi, tinjauan, audit, sertifikasi atau pemberitahuan yang diberikan oleh atau atas nama pihak PT. IMS-Indonesia, maka pihak PT. IMS-Indonesia  akan membayar kompensasi kepada orang tersebut untuk kerugiannya yang terbukti sampai dengan tetapi tidak melebihi jumlah biaya (apabila ada) yang dikenakan oleh pihak PT. IMS-Indonesia  untuk informasi atau pemberitahuan layanan tertentu tersebut.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">9.4</td>
					<td colspan="2" >Pihak <b>PT. IMS-Indonesia</b>  tidak akan berkewajiban atau bertanggung – jawab  terhadap kelalaian atau dalam hal bagaimanapun juga terhadap setiap orang yang bukan merupakan salah satu pihak dalam Perjanjian ini berkenaan dengan setiap informasi atau pemberitahuan yang secara  jelas  atau tersirat  oleh pihak <b>PT. IMS-Indonesia</b>  atau berkenaan dengan setiap tindakan, penghapusan  atau  ketidak  –  akuratan  oleh pihak <b>PT. IMS-Indonesia</b>.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">9.5</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b>  mensyaratkan kepada organisasi pelanggan untuk :
					</td>
				</tr>				
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >a.</td>
					<td >Memenuhi persyaratan <b>PT. IMS-Indonesia</b>  pada saat membuat acuan status sertifikasinya dalam media komunikasi seperti internet, brosur atau iklan, atau dokumen lainnya.</td>
				</tr>
				
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >b.</td>
					<td >Tidak membuat atau mengijinkan pernyataan yang menyesatkan berkenaan dengan sertifikasi <b>PT. IMS-Indonesia</b> .</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >c.</td>
					<td >Tidak menggunakan atau mengizinkan penggunaan dokumen sertifikasi atau bagiannya dalam cara yang menyesatkan.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >d.</td>
					<td >Menghentikan penggunaan seluruh materi periklanan yang memuat acuan sertifikasi, sebagaimana ditentukan oleh lembaga sertifikasi bila terjadi pembekuan atau pencabutan sertifikasi.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >e.</td>
					<td >Merubah seluruh materi periklanan pada saat lingkup sertifikasi dikurangi</td>
				</tr>
				
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >f.</td>
					<td >Menggunakan acuan sertifikasi sistem manajemen yang dapat menyiratkan bahwa <b>PT. IMS-Indonesia</b> memberikan sertifikasi produk (termasuk jasa) atau proses.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >g.</td>
					<td >Sertifikasi tidak berlaku untuk kegiatan diluar lingkup sertifikasi.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >h.</td>
					<td >Tidak menggunakan sertifikasi yang dapat membawa <b>PT. IMS-Indonesia</b>  dan/atau sistem sertifikasi kehilangan reputasi dan kepercayaan publik.</td>
				</tr>
				


				<tr class="title">
					<td class="">10.</td>
					<td colspan="3" ><u>Ganti Rugi</u></td>
				</tr>
				<tr >
					<td class=""></td>
					<td colspan="3" >Pihak Klien bertanggung-jawab untuk mengganti rugi pihak <b>PT. IMS-Indonesia</b>  atas setiap kerugian-kerugian yang diderita oleh atau atas klaim - klaim yang diajukan terhadap pihak <b>PT. IMS-Indonesia</b>  sebagai akibat dari penyalah-gunaan setiap Persetujuan atau Sertifikat Persetujuan atau Surat Ijinuntuk menggunakan setiap tanda  Akreditasi yang diberikan oleh pihak <b>PT. IMS-Indonesia</b>  menurut Perjanjian kepada pihak Klien.</td>
				</tr>


				<tr class="title">
					<td class="">11.</td>
					<td colspan="3" ><u>Pemutusan perjanjian kontrak kerjasama dan keluhan dari pihak external</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.1</td>
					<td colspan="2" >Perjanjian ini akan terus berlaku dan mengikat sepenuhnya kecuali dan sampai dengan diputuskan / diakhiri oleh salah satu pihak dengan memberikan  suatu pemberitahuan secara tertulis selambat-lambatnya 30 (tiga puluh) hari sebelum pemutusan tersebut  kepada pihak lain ;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.2</td>
					<td colspan="2" >Pada tanggal pemutusan perjanjian ini, Sertifikat yang diberikan dengan segera akan menjadi tidak sah dan pihak Klien akan mengembalikan Sertifikat  tersebut ;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.3</td>
					<td colspan="2" >Pada saat pemutusan Perjanjian tersebut olehsalah satu pihak,segenap biaya untuk layanan-layanan yang diselesaikan sebelum tanggal pemutusan akan segera menjadi jatuh tempo dan wajib dibayar sesuai faktur yang telah diterbitkan;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.4</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> akan mengambil tindakan korektif untuk kondisi yang menyebabkan kondisi ini untuk meneruskan tindakan yang akan diambil dan bahwa kondisi diselesaikan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.5</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> akan mengkonfirmasikan keterkaitan keluhan, bila terkait dengan kegiatan sertifikasi maka harus diselesaikan karena menjadi tanggung jawab dari <b>PT. IMS-Indonesia</b>.
					</td>
				</tr>
				
				<tr class="title">
					<td class="">12.</td>
					<td colspan="3" ><u>Tata Cara Penggunaan Logo</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.1</td>
					<td colspan="2" >Penggunaan tanda atau logo yang telah diberikan hak penggunaannya kepada klien yang telah disertifikasi oleh <b>PT. IMS-Indonesia</b> berisi ketelusuran <b>PT. IMS-Indonesia</b> .
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.2</td>
					<td colspan="2" >Alternatif penggunaan  symbol <b>PT. IMS-Indonesia</b>  dan  logo akreditasi, Apabila klien ingin menampilkan simbol dengan cara yang tidak dijelaskan dalam hal dokumen harus diajukan ke Auditor Manageruntuk saran.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.3</td>
					<td colspan="2" >Laporan uji laboratorium, kalibrasi atau inspeksi dianggap sebagai produk sehingga penggunaan tanda atau logo <b>PT. IMS-Indonesia</b> pada laporan tersebut tidak diizinkan oleh <b>PT. IMS-Indonesia</b>.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.4</td>
					<td colspan="2" >Penggunaan yang tidak benar symbol dan logo oleh klien, Apabila ditemukan klien yang salah menggunakan simbol <b>PT. IMS-Indonesia</b> dan atau logo akreditasi oleh:
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">(i)</td>
					<td class="">Saat kunjungan audit peninjauan / pengawasan, Auditor harus memberi saran klien dan memerlukan, mengkonfirmasi dan merekam dalam laporan penilaian bahwa tindakan korektif diambil, dan Auditor dapat meminta nasihat dari Managing Director pada penggunaan dan validitas tindakan korektif.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">(ii)</td>
					<td class="">Keluhan dari pihak luar atau badan eksternal lainnya dikelola sesuai dengan prosedur penanganan keluhan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">(iii)</td>
					<td class="">Keluhan dari Badan Akreditasi untuk penggunaan symbol dan logo.</td>
					</td>
				</tr>
				
				<tr>
					<td class=""></td>
					<td class="">12.5</td>
					<td colspan="2" >Penyalahgunaan logo
					</td>
				</tr>

				<tr>
					<td class=""></td>
					<td class=""></td>
					<td colspan="2" >Dimana  kejadian penyalahgunaan dari simbol <b>PT. IMS-Indonesia</b>  melalui keluhan atau saluran lain, maka akan dicatat dan diproses sebagai keluhan.
					</td>
				</tr>
				

				<tr class="title">
					<td class="">13.</td>
					<td colspan="3" ><u>Keadaan Diluar Persyaratan</u></td>
				</tr>

				<tr class="">
					<td class=""></td>
					<td colspan="3" >Tidakada kegagalan atau penghapusan oleh salah satu pihak untuk melaksanakan atau mematuhi setiap keputusan –keputusan, persyaratan, atau jaminan yang hendak dilaksanakan sebagaimana dikemukakan dalam Perjanjian ini akan menimbulkan klaim terhadap pihak tersebut atau dianggap sebagai sebuah pelanggaran kontrak sampai sejauh bahwa kegagalan atau penghapusan tersebut timbul dari sebab-sebab yang secara wajar berada di luar kendali pihak tersebut.</td>
				</tr>

				<tr class="title">
					<td class="">14.</td>
					<td colspan="3" ><u>Hukum dan Yuridikasi</u></td>
				</tr>

				<tr class="">
					<td class=""></td>
					<td colspan="3" >Perjanjian ini  akan diatur  oleh dan ditafsirkan sesuai dengan hukum  Indonesia. Para pihak dengan ini tunduk pada yurisdiksi  eksklusif pengadilan Indonesia.</td>
				</tr>

				<tr class="title">
					<td class="">15.</td>
					<td colspan="3" ><u>Ketidak Berpihakan</u></td>
				</tr>

				<tr>
					<td class=""></td>
					<td class="">15.1</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b>  memegang prinsip bahwa keputusan harus didasarkan pada kriteria yang obyektif , bukan atas dasar  perhitungan strategis atau ekonomis, prasangka , atau perhitungan keuntungan terhadap pihak lain untuk alasan yang tidak tepat.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.2</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak dapat memberikan sertifikasi jika terjadi suatu hubungan yang menimbulkan ancaman ketidak berpihakan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.3</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak mensertifikasi suatu lembaga yang bergerak di bidang sertifikasi sistem manajemen.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.4</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak menawarkan atau menyediakan konsultasi sistem manajemen.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.5</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak menawarkan atau menyediakan audit internal Klien yang disertifikasinya.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.6</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak mensertifikasi sistem manajemen pada pelanggan yang telah menerima konsultasi sistem manajemen atau audit internal, dimana hubungan antara organisasi konsultan dengan <b>PT. IMS-Indonesia</b>  menunjukkan ancaman yang mempengaruhi ketidakberpihakkan <b>PT. IMS-Indonesia</b> .
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.7</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak memberikan jasa audit pada organisasi konsultan sistem manajemen.
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>