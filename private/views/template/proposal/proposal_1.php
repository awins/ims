<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/proposal.css">
</head>
<body>
	<?php
	$header = $project[0]; ?>
	<div class="page" style="" >
		<div class="header" >
			<table cellspacing="0" >
				<tr>
					<td>NO. PROPOSAL</td>
					<td><?php echo $header->proposal_number ?></td>
				</tr>
				<tr>
					<td>TANGGAL</td>
					<td><?php echo indDate(date('Y-m-d', strtotime($header->certificate_date . ' -1 month' ))) ?></td>
				</tr>
			</table>
		</div>
		<div style="height: 20px">&nbsp;</div>
		<div style="margin-left: 40px;">
			<?php 
			foreach ($project as $key => $value) {
				echo isoType($value->iso_type,'name'). ' (' . isoType($value->iso_type,'text') . ')';
				echo '<br />';
			} ?>
		</div>
	</div>
</body>
</html>