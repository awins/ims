<?php
	$project_count = count($project);
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/proposal.css">
</head>
<body >
	<div style="">
		<div class="proposal-content" >
			<div class="title" style=""   >A. LAYANAN</div>
			<div style="width: 380px;height:135px" class="f-10 top" >
				Layanan berikut ini disediakan oleh PT. IMS-Indonesia untuk:
				<br />
				<br />
				<span style="font-size: 18pt;font-weight: bold" ><?php echo $project[0]->company_name ?></span>
				<br />
				<?php echo $project[0]->company_address_1 ?>
				<br />
				<?php echo $project[0]->company_address_2 . ', ' . $project[0]->company_city ?>
				<br />
				<?php echo $project[0]->company_province . ' - Indonesia'  ?>
			</div>
			<div style="width: 318px;height:120px;text-align: justify;vertical-align: middle;padding-top: 20px" class="f-10 right top">
				Melakukan kunjungan penilaian (<i>assessment</i>) dan kunjungan pengawasan (<i>surveillance</i>) dan melakukan kunjungan lebih lanjut untuk memverifikasi tindakan perbaikan dan mengeluarkan laporan yang berisi rangkuman temuan saat kunjungan.
			</div>
			<div style="width: 200px;text-align: justify;vertical-align: middle;" class="f-10 top">
				<strong>STANDAR PENILAIAN</strong>
			</div>
			<div style="width: 498px;text-align: justify;vertical-align: middle;" class="f-10 right top">
				<strong>
					<?php 
					$i = 0;
					$project_count = count($project);
					foreach ($project as $value) {
						$i++;
						if ($i> 1){
							if ($i == $project_count){
								$sambung = " dan ";
							}else{
								$sambung = ", ";
							}
						}else{
							$sambung = null;
						}
						echo $sambung . isoType($value->iso_type,'name');
					} ?>
				</strong>
			</div>
			<div style="width: 720px;text-align: justify;vertical-align: middle;" class="f-10 top">
				<strong>RUANG LINGKUP</strong>
				<br />
				Persetujuan ruang lingkup berdasarkan dengan aktivitas di organisasi/perusahaan sebelum dilakukan penilaian awal (assessment). Ruang lingkup yang disetujui sesuai dengan ruang lingkup pada sertifikat.

			</div>
			<div style="width: 720px;text-align: justify;vertical-align: middle;" class="f-10 top">
				<strong>JADWAL</strong>
				<br />
				Pelayanan yang diberikan oleh PT. IMS-INDONESIA sudah termasuk penilaian terhadap Sistem Manajemen Klien. Yang termasuk dalam penilaian ini adalah: 
				Tahap 1) Tinjauan dokumen; Tahap 2) Kunjungan penilaian awal, kunjungan pengawasan dan kunjungan perpanjangan sertifikat sekitar 36 bulan untuk mengkonfirmasi keabsahan dari sistem manajemen yang telah diterima dan Sertifikat Pendaftaran. Ketidaksesuaian (Temuan Major) bila perlu akan diberikan yang dimana pada akhirnya akan memerlukan “Kunjungan Audit Khusus” untuk memeriksa tindakan perbaikan yang dilakukan.
			</div>
			<div style="width: 720px;text-align: justify;vertical-align: middle;" class="f-10">
				<strong>KUNJUNGAN KHUSUS</strong>
				<br />
				Ketika ketidaksesuaian ditemukan pada saat kunjungan penilaian, kunjungan pengawasan atau kunjungan perpanjangan sertifikat, maka hal ini akan diberitahukan kepada klien dan jika diperlukan akan diadakan penjadwalan kunjungan tambahan yang dilaksanakan untuk memeriksa tindakan perbaikan yang dilakukan Klien. Biaya untuk kunjungan khusus ini akan dikenakan sesuai dengan harga man-day yang berlaku pada saat kunjungan (maksimal 1 man-day).

			</div>
			<div class="title" style=""   >B. HARGA UNTUK <?php echo $project_count ?> STANDAR</div>
			<div style="width: 120px;text-align: center;vertical-align: middle;background-color: rgb(191,191,191)" class="f-10 top">
				<strong>KETERANGAN</strong>
			</div>
			<div style="width: 120px;text-align: center;vertical-align: middle;background-color: rgb(191,191,191)" class="f-10 right top">
				<strong>HARGA</strong>
			</div>
			<div style="width: 436px;text-align: center;vertical-align: middle;background-color: rgb(191,191,191)" class="f-10 right top">
				<strong>WAKTU PEMBAYARAN</strong>
			</div>

			<div style="width: 120px;text-align: justify;vertical-align: middle;" class="f-10 top">
				<strong>SERTIFIKASI</strong>
			</div>
			<div style="width: 120px;text-align: right;vertical-align: middle;" class="f-10 right top">
				<strong><?php echo number_format($project[0]->certificate_amount,0) ?>,-</strong>
			</div>
			<div style="width: 436px;text-align: justify;vertical-align: middle;" class="f-10 right top">
				50% pada saat penanda tanganan kontrak, 50% pada saat Audit Stage 2
			</div>

			<div style="width: 120px;text-align: justify;vertical-align: middle;" class="f-10 top">
				<strong>PENGAWASAN</strong>
			</div>
			<div style="width: 120px;text-align: right;vertical-align: middle;" class="f-10 right top">
				<strong><?php echo number_format($project[0]->surveilance_amount,0) ?>,-</strong>
			</div>
			<div style="width: 436px;text-align: justify;vertical-align: middle;" class="f-10 right top">
				Setiap 1 (Satu) Tahun Sekali
			</div>

			<div style="width: 120px;text-align: justify;vertical-align: middle;" class="f-10 top ">
				<strong>RENEWALL</strong>
			</div>
			<div style="width: 120px;text-align: right;vertical-align: middle;" class="f-10 right top ">
				<strong><?php echo number_format($project[0]->certificate_amount,0) ?>,-</strong>
			</div>
			<div style="width: 436px;text-align: justify;vertical-align: middle;" class="f-10 right top ">
				<?php echo date('Y',strtotime($project[0]->request_date)) + 3 ?>
			</div>
			<div style="width: 720px;text-align: justify;vertical-align: middle;padding-top: 20px;background-color: rgb(191,191,191)" class="f-10 top">
				<strong>
					Catatan :
					<br />
					•	Harga Non PPN
					<br />
					•	Harga diatas diluar biaya transportasi dan akomodasi Auditor.
				</strong>
			</div>
			<div style="width: 350px;vertical-align: middle;font-weight: bold;" class="">
				<div style="border: 0;line-height: 10px"><?php echo $project[0]->company_name ?></div>
				<div style="width: 60px;border: 0;line-height: 10px">Nama</div><div style="border: 0;line-height: 10px">:</div>
				<div style="width: 60px;border: 0;line-height: 10px">Jabatan</div><div style="border: 0;line-height: 10px">:</div>
				<div style="border: 0;line-height: 10px">&nbsp;</div>
				<div style="border: 0;line-height: 10px">&nbsp;</div>
				<div style="border: 0;line-height: 10px">&nbsp;</div>
				<div style="border: 0;line-height: 10px">Tanda Tangan &amp; Stempel</div>
				<div style="width: 60px;border: 0;line-height: 10px">Tanggal</div><div style="border:0;line-height: 10px">:</div>
			</div>
			<div style="width: 350px;vertical-align: middle;font-weight: bold;" class="right">
				<div style="border: 0;line-height: 10px">PT. IMS-INDONESIA</div>
				<div style="width: 70px;border: 0;line-height: 10px">Prop. No.</div><div style="border: 0;line-height: 10px">: <?php echo $project[0]->proposal_number ?></div>
				<div style="width: 70px;border: 0;line-height: 10px">Nama</div><div style="border: 0;line-height: 10px">: M.I. Akbar</div>
				<div style="width: 70px;border: 0;line-height: 10px">Jabatan</div><div style="border: 0;line-height: 10px">: Manajer Operasional</div>
				<div style="border: 0;line-height: 10px">&nbsp;</div>
				<div style="border: 0;line-height: 10px">&nbsp;</div>
				<div style="border: 0;line-height: 10px">Tanda Tangan &amp; Stempel</div>
				<div style="width: 70px;border: 0;line-height: 10px">Tanggal</div><div style="border:0;line-height: 10px">: <?php echo indDate(date('Y-m-d', strtotime($project[0]->certificate_date . ' -1 month' ))) ?></div>
			</div>
		</div>
		
	</div>
</body>
</html>


