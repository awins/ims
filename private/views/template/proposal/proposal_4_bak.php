<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/proposal.css">
</head>
<body>
	<div style="padding-left: 30px;">
		<table class="table-information" border="0" >
			<thead>
				<tr>
					<td style="width: 30px" class=""></td>
					<td style="width: 35px"></td>
					<td style="width: 30px"></td>
					<td style=""></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class=""></td>
					<td class="">9.5</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b>  mensyaratkan kepada organisasi pelanggan untuk :
					</td>
				</tr>				
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >a.</td>
					<td >Memenuhi persyaratan <b>PT. IMS-Indonesia</b>  pada saat membuat acuan status sertifikasinya dalam media komunikasi seperti internet, brosur atau iklan, atau dokumen lainnya.</td>
				</tr>
				
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >b.</td>
					<td >Tidak membuat atau mengijinkan pernyataan yang menyesatkan berkenaan dengan sertifikasi <b>PT. IMS-Indonesia</b> .</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >c.</td>
					<td >Tidak menggunakan atau mengizinkan penggunaan dokumen sertifikasi atau bagiannya dalam cara yang menyesatkan.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >d.</td>
					<td >Menghentikan penggunaan seluruh materi periklanan yang memuat acuan sertifikasi, sebagaimana ditentukan oleh lembaga sertifikasi bila terjadi pembekuan atau pencabutan sertifikasi.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >e.</td>
					<td >Merubah seluruh materi periklanan pada saat lingkup sertifikasi dikurangi</td>
				</tr>
				
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >f.</td>
					<td >Menggunakan acuan sertifikasi sistem manajemen yang dapat menyiratkan bahwa <b>PT. IMS-Indonesia</b> memberikan sertifikasi produk (termasuk jasa) atau proses.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >g.</td>
					<td >Sertifikasi tidak berlaku untuk kegiatan diluar lingkup sertifikasi.</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td >h.</td>
					<td >Tidak menggunakan sertifikasi yang dapat membawa <b>PT. IMS-Indonesia</b>  dan/atau sistem sertifikasi kehilangan reputasi dan kepercayaan publik.</td>
				</tr>
				


				<tr class="title">
					<td class="">10.</td>
					<td colspan="3" ><u>Ganti Rugi</u></td>
				</tr>
				<tr >
					<td class=""></td>
					<td colspan="3" >Pihak Klien bertanggung-jawab untuk mengganti rugi pihak <b>PT. IMS-Indonesia</b>  atas setiap kerugian-kerugian yang diderita oleh atau atas klaim - klaim yang diajukan terhadap pihak <b>PT. IMS-Indonesia</b>  sebagai akibat dari penyalah-gunaan setiap Persetujuan atau Sertifikat Persetujuan atau Surat Ijinuntuk menggunakan setiap tanda  Akreditasi yang diberikan oleh pihak <b>PT. IMS-Indonesia</b>  menurut Perjanjian kepada pihak Klien.</td>
				</tr>


				<tr class="title">
					<td class="">11.</td>
					<td colspan="3" ><u>Pemutusan perjanjian kontrak kerjasama dan keluhan dari pihak external</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.1</td>
					<td colspan="2" >Perjanjian ini akan terus berlaku dan mengikat sepenuhnya kecuali dan sampai dengan diputuskan / diakhiri oleh salah satu pihak dengan memberikan  suatu pemberitahuan secara tertulis selambat-lambatnya 30 (tiga puluh) hari sebelum pemutusan tersebut  kepada pihak lain ;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.2</td>
					<td colspan="2" >Pada tanggal pemutusan perjanjian ini, Sertifikat yang diberikan dengan segera akan menjadi tidak sah dan pihak Klien akan mengembalikan Sertifikat  tersebut ;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.3</td>
					<td colspan="2" >Pada saat pemutusan Perjanjian tersebut olehsalah satu pihak,segenap biaya untuk layanan-layanan yang diselesaikan sebelum tanggal pemutusan akan segera menjadi jatuh tempo dan wajib dibayar sesuai faktur yang telah diterbitkan;
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.4</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> akan mengambil tindakan korektif untuk kondisi yang menyebabkan kondisi ini untuk meneruskan tindakan yang akan diambil dan bahwa kondisi diselesaikan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">11.5</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> akan mengkonfirmasikan keterkaitan keluhan, bila terkait dengan kegiatan sertifikasi maka harus diselesaikan karena menjadi tanggung jawab dari <b>PT. IMS-Indonesia</b>.
					</td>
				</tr>
				
				<tr class="title">
					<td class="">12.</td>
					<td colspan="3" ><u>Tata Cara Penggunaan Logo</u></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.1</td>
					<td colspan="2" >Penggunaan tanda atau logo yang telah diberikan hak penggunaannya kepada klien yang telah disertifikasi oleh <b>PT. IMS-Indonesia</b> berisi ketelusuran <b>PT. IMS-Indonesia</b> .
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.2</td>
					<td colspan="2" >Alternatif penggunaan  symbol <b>PT. IMS-Indonesia</b>  dan  logo akreditasi, Apabila klien ingin menampilkan simbol dengan cara yang tidak dijelaskan dalam hal dokumen harus diajukan ke Auditor Manageruntuk saran.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.3</td>
					<td colspan="2" >Laporan uji laboratorium, kalibrasi atau inspeksi dianggap sebagai produk sehingga penggunaan tanda atau logo <b>PT. IMS-Indonesia</b> pada laporan tersebut tidak diizinkan oleh <b>PT. IMS-Indonesia</b>.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">12.4</td>
					<td colspan="2" >Penggunaan yang tidak benar symbol dan logo oleh klien, Apabila ditemukan klien yang salah menggunakan simbol <b>PT. IMS-Indonesia</b> dan atau logo akreditasi oleh:
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">(i)</td>
					<td class="">Saat kunjungan audit peninjauan / pengawasan, Auditor harus memberi saran klien dan memerlukan, mengkonfirmasi dan merekam dalam laporan penilaian bahwa tindakan korektif diambil, dan Auditor dapat meminta nasihat dari Managing Director pada penggunaan dan validitas tindakan korektif.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">(ii)</td>
					<td class="">Keluhan dari pihak luar atau badan eksternal lainnya dikelola sesuai dengan prosedur penanganan keluhan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class="">(iii)</td>
					<td class="">Keluhan dari Badan Akreditasi untuk penggunaan symbol dan logo.</td>
					</td>
				</tr>
				
				<tr>
					<td class=""></td>
					<td class="">12.5</td>
					<td colspan="2" >Penyalahgunaan logo
					</td>
				</tr>

				<tr>
					<td class=""></td>
					<td class=""></td>
					<td colspan="2" >Dimana  kejadian penyalahgunaan dari simbol <b>PT. IMS-Indonesia</b>  melalui keluhan atau saluran lain, maka akan dicatat dan diproses sebagai keluhan.
					</td>
				</tr>
				

				<tr class="title">
					<td class="">13.</td>
					<td colspan="3" ><u>Keadaan Diluar Persyaratan</u></td>
				</tr>

				<tr class="">
					<td class=""></td>
					<td colspan="3" >Tidakada kegagalan atau penghapusan oleh salah satu pihak untuk melaksanakan atau mematuhi setiap keputusan –keputusan, persyaratan, atau jaminan yang hendak dilaksanakan sebagaimana dikemukakan dalam Perjanjian ini akan menimbulkan klaim terhadap pihak tersebut atau dianggap sebagai sebuah pelanggaran kontrak sampai sejauh bahwa kegagalan atau penghapusan tersebut timbul dari sebab-sebab yang secara wajar berada di luar kendali pihak tersebut.</td>
				</tr>

				<tr class="title">
					<td class="">14.</td>
					<td colspan="3" ><u>Hukum dan Yuridikasi</u></td>
				</tr>

				<tr class="">
					<td class=""></td>
					<td colspan="3" >Perjanjian ini  akan diatur  oleh dan ditafsirkan sesuai dengan hukum  Indonesia. Para pihak dengan ini tunduk pada yurisdiksi  eksklusif pengadilan Indonesia.</td>
				</tr>

				<tr class="title">
					<td class="">15.</td>
					<td colspan="3" ><u>Ketidak Berpihakan</u></td>
				</tr>

				<tr>
					<td class=""></td>
					<td class="">15.1</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b>  memegang prinsip bahwa keputusan harus didasarkan pada kriteria yang obyektif , bukan atas dasar  perhitungan strategis atau ekonomis, prasangka , atau perhitungan keuntungan terhadap pihak lain untuk alasan yang tidak tepat.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.2</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak dapat memberikan sertifikasi jika terjadi suatu hubungan yang menimbulkan ancaman ketidak berpihakan.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.3</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak mensertifikasi suatu lembaga yang bergerak di bidang sertifikasi sistem manajemen.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.4</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak menawarkan atau menyediakan konsultasi sistem manajemen.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.5</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak menawarkan atau menyediakan audit internal Klien yang disertifikasinya.
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.6</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak mensertifikasi sistem manajemen pada pelanggan yang telah menerima konsultasi sistem manajemen atau audit internal, dimana hubungan antara organisasi konsultan dengan <b>PT. IMS-Indonesia</b>  menunjukkan ancaman yang mempengaruhi ketidakberpihakkan <b>PT. IMS-Indonesia</b> .
					</td>
				</tr>
				<tr>
					<td class=""></td>
					<td class="">15.7</td>
					<td colspan="2" ><b>PT. IMS-Indonesia</b> tidak memberikan jasa audit pada organisasi konsultan sistem manajemen.
					</td>
				</tr>

			</tbody>
		</table>
	</div>
	
</body>
</html>