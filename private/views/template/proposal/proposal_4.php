<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/proposal.css">
</head>
<body>
	<div style="padding-left: 30px;">
		<table class="table-information" border="0" >
			<thead>
				<tr>
					<td style="width: 30px" class=""></td>
					<td style="width: 35px"></td>
					<td style="width: 30px"></td>
					<td style=""></td>
				</tr>
			</thead>
			<tbody>
				
				<tr>
					<td colspan="4">
						<b>
							KETENTUAN TENTANG PEMBEKUAN DAN PENCABUTAN SERTIFIKAT ISO
							<br />
							<br />
							<b>Berdasarkan ISO 17021:2013 dan Peraturan Perusahaan PT. IMS-Indonesia, Akan dilakukan pembekuan atau pencabutan sertifikat : </b>
							<br />
						</b>
					</td>
				</tr>
				
				<tr class="title-normal">
					<td >1.</td>				
					<td colspan="3">Apabila klien tidak mengikuti kegiatan surveillance pada waktu yang telah ditetapkan berdasarkan kontrak kesepakatan yang telah ditanda tangani oleh kedua belah.</td>				
				</tr>
				<tr class="title-normal">
					<td >2.</td>				
					<td colspan="3">IMS-Indonesia akan melakukan PEMBEKUAN Sertifikat ISO 9001:2008, ISO 14001:2004 atau OHSAS 18001:2007 yang telah diperoleh oleh pihak klien. Selama masa PEMBEKUAN Sertifikat ISO 9001:2008, ISO 14001:2004 atau OHSAS 18001:2007, pihak klien dilarang menggunakan Sertifikat, Logo dan seluruh perangkat ISO 9001:2008, ISO 14001:2004 atau OHSAS 18001:2007 yang diberikan PT. IMS-Indonesia untuk keperluan promosi lebih lanjut atau keperluan lain.
						<br />
						<br />
						Status <b>PEMBEKUAN</b> disediakan, diakses publik PT. IMS-Indonesia.
						<br />
						Proses <b>PENCABUTAN</b> akan diterapkan jika pihak klien melanggar proses <b>PEMBEKUAN</b>
					</td>
				</tr>
				<tr class="title-normal">
					<td >3.</td>				
					<td colspan="3">Apabila klien tidak mengikuti aturan penggunaan tanda sertifikasi seperti yang telah dijelaskan oleh PT. IMS-Indonesia, maka :</td>				
				</tr>
				
				<tr>
					<td ></td>				
					<td >-</td>				
					<td colspan="2">PT. IMS-Indonesia menginstruksikan kepada pihak klien untuk memperbaiki ketidaksesuaian dalam batas waktu tertentu.</td>				
				</tr>
				<tr>
					<td ></td>				
					<td >-</td>				
					<td colspan="2">Bila pihak klien tidak memperbaiki ketidaksesuaian dalam batas waktu yang telah disepakati. PT. IMS-Indonesia akan membekukan status sertifikat yang telah diterima oleh pihak klien dengan ketentuan proses pembekuan yang berlaku.</td>				
				</tr>
				<tr class="title-normal">
					<td >4.</td>				
					<td colspan="3">Bila ada ketidaksesuaian/temuan “major’ yang ditemukan selama audit surveillance dan tidak dapat diperbaiki oleh pihak klien dalam jangka waktu tertentu, maka :</td>				
				</tr>
				<tr>
					<td ></td>				
					<td >-</td>				
					<td colspan="2">PT. IMS-Indonesia akan menanggguhkan penggunaan sertifikat dan menginstruksikan pihak klien untuk memperbaiki ketidaksesuaian “major” dalam jangka waktu tertentu.</td>				
				</tr>
				<tr>
					<td ></td>				
					<td >-</td>				
					<td colspan="2">Bila klien tidak dapat memperbaiki ketidaksesuaian “major” yang ada dalam jangka waktu yang telah disepakati, PT. IMS-Indonesia akan melakukan PEMBEKUAN atas status sertifikasi ISO 9001:2008, ISO 14001:2004 atau OHSAS 18001:2007 pihak klien.</td>				
				</tr>
				<tr>
					<td colspan="4">Kegiatan Surveillance diadakan setiap satu tahun sekali masa berlaku sertifikasi yaitu selama tiga tahun.</td>				
				</tr>
				
				
			</tbody>
		</table>
	</div>
	
</body>
</html>