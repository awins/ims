<html>
<head>
<title>CERTIFCATE</title>
<style type="text/css">
@page {
    margin-top: 2.54cm;
    margin-bottom: 2.54cm;
    margin-left: 1.175cm;
    margin-right: 1.175cm;
}

body { font-family: cambria; font-size: 11pt }
/*.pos { position: absolute; z-index: 0; left: 0px; top: 0px }*/
	
</style>
</head>
<body>
		<div style="height: 70px;width:100%" >&nbsp;</div>
		<div class="pos" id="_0:0" style="position:relative; width: 180px;float: left">
			<img height="170px" width="153px" border="0" src="/application/public/img/logo/CERT_<?php echo $data->iso_type ?>.png">
			&nbsp;
		</div>
		<div style="width: 520px;position:relative; float: left">
			<div id="_15.2" style=" ; font-size:15.2px; color:#000000;height: 32px">
				is to certify that the</div>
			<div id="_18.2" style="font-weight:bold; ; font-size:14pt; color:#000000;height: 35px">
				<?php echo isoType($data->iso_type,'text') ?></div>
			<div id="_15.0" style="color:#000000;height: 32px">
				of</div>
			<div  >
				<span id="_23.5" style="font-weight:bold; ; font-size:18pt; color:#000000">
					<?php echo $data->company_name ?></span>
				<br />
				<span id="_14.9" style="font-weight:bold; ; color:#000000">
					<?php echo $data->company_address_1 ?> 
					<br />
					<?php echo $data->company_address_2 ?>
					<br />
					<?php echo cityAddress($data) ?>
				</span>
			</div>
			<div id="_14.9" style=" ; font-size:14.9px; color:#000000;margin: 17px 0;display: block">
				Has been audited and certified as meeting the requirements of</div>
			
			<span id="_48.6" style="font-weight:bold; ; font-size:36pt; color:#000000;margin-top: 24px">
				<?php echo isoType($data->iso_type,'name') ?></span>

			<div id="_14.9" style=" color:#000000;margin-top: 17px">
				This certificate valid for the following scope of operation :</div>
			<div id="_12.1" style="font-weight:bold;font-size: 9pt;color:#000000;text-align: justify;margin-top: 17px">
				Provision of <?php echo $data->nace_detail ?>
			</div>
			<div id="_14.0" style="font-style:italic; ; color:#000000;margin-top: 17px">
				EAC : <?php echo $data->eac ?></div>
			<div id="_14.0" style="font-style:italic; ; color:#000000">
				NACE : <?php echo $data->nace ?></div>
			<div id="_15.1" style="font-weight:bold; ; color:#000000;margin-top: 17px">
				This certificate is valid from <?php echo  dateSuperScript(strtotime($data->certificate_date)) ?>
			<br />
				Certificate valid until <?php echo dateSuperScript(strtotime(date("Y-m-d", strtotime($data->certificate_date)) . " + 3 year")) ?></div>
			<div id="_15.1" style=" color:#000000;text-align: justify;margin-top: 17px">
				Recertification audit before <?php echo  dateSuperScript(strtotime($data->renewal_assessment_date)) ?>. This certificate is the property of 
				IMS Indonesia and remains valid subject to satisfactory annual surveillance audit</div>
			
			<div style="clear: both; margin: 0pt; padding: 0pt; "></div>
				
			<div style="display: inline-block; width: 300px;float: left;margin-top: 10px" >Main Audit by IMS-Indonesia</div>
			<div style="display: inline-block; width: 150px;float: left">: <?php echo dateSuperScript(strtotime($data->main_assessment_date),false) ?></div>
			<div style="display: inline-block; width: 50px;float: left"><?php echo date('Y',strtotime($data->main_assessment_date)) ?></div>

			<div style="display: inline-block; width: 300px;float: left" >1<sup>st</sup> Surveillance</div>
			<div style="display: inline-block; width: 150px;float: left">: <?php echo dateSuperScript(strtotime($data->surveillance_1_date),false) ?></div>
			<div style="display: inline-block; width: 50px;float: left"><?php echo date('Y',strtotime($data->surveillance_1_date)) ?></div>

			<div style="display: inline-block; width: 300px;float: left" >2<sup>nd</sup> Surveillance</div>
			<div style="display: inline-block; width: 150px;float: left">: <?php echo dateSuperScript(strtotime($data->surveillance_2_date),false) ?></div>
			<div style="display: inline-block; width: 50px;float: left"><?php echo date('Y',strtotime($data->surveillance_2_date)) ?></div>

			<div style="display: inline-block; width: 300px;float: left" >3<sup>rd</sup> Recertification</div>
			<div style="display: inline-block; width: 150px;float: left">: <?php echo dateSuperScript(strtotime($data->renewal_assessment_date),false) ?></div>
			<div style="display: inline-block; width: 50px;float: left"><?php echo date('Y',strtotime($data->renewal_assessment_date)) ?></div>
			
			<div style="clear: both; margin: 0pt; padding: 0pt; "></div>
			<div style="margin-top: 17px;position: relative;width: 520px">
				<div style="display: inline-block; width: 200px;float: left" >Authorized by :</div>
				<div style="display: inline-block; width: 320px; float: left;font-weight: bold" >Certification Number : <?php echo $data->certificate_number_full ?></div>
			</div>
			<div style="clear: both; margin: 0pt; padding: 0pt; "></div>

			<div style="clear:both" >&nbsp;</div>
			
			
		</div>
	
</body>
</html>
