<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB I</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>PENDAHULUAN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 30pt"></td>
						<td style="width: 375pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">1.1</td>
						<td colspan="3">TUJUAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Untuk memastikan bahwa semua Dokumen Sistem Manajemen Mutu telah memenuhi Persyaratan ISO 9001:2008 <i>(klausul 4.2.3)</i> yang dikendalikan untuk :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.1.1</td>
						<td colspan="2">Menyetujui Dokumen akan kecukupannya sebelum diterbitkan</td>
					</tr>

					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.1.2</td>
						<td colspan="2">Meninjau Ulang, Memuktahirkan seperlunya dan Menyetujuinya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.1.3</td>
						<td colspan="2">Memastikan bahwa Perubahan, Status Revisi terkini teridentifikasi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.1.4</td>
						<td colspan="2">Memastikan Dokumen selalu dapat dibaca dan mudah diidentifikasi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.1.5</td>
						<td colspan="2">Memastikan Dokumen Eksternal diidentifikasi dan dikendalikan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.1.6</td>
						<td colspan="2">Memastikan Dokumen Usang, diidentifikasi agar tidak digunakan</td>
					</tr>
					
					<tr class="sub-bab" >
						<td colspan="">1.2</td>
						<td colspan="3">RUANG LINGKUP</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Ruang Lingkup  Sistem  Manajemen  Mutu  (SMM) mencakup  semua  Proses  Bisnis Jasa yaitu <i>“ <?php echo $project[0]->company_scope ?> ”</i>. yang dikelola <b><?php echo $project[0]->company_name ?></b>.
Aplikasi Sistem Manajemen Mutu <b><?php echo $project[0]->company_name ?></b> memenuhi Persyaratan dalam ISO 9001:2008 serta mematuhi Persyaratan Perundangan yang berlaku terkait dengan Peraturan Perhubungan dan Telekomunikasi di Indonesia.
</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">1.3</td>
						<td colspan="3">REFERENSI <span style="font-weight: normal;"><i>(Dokument Terkait)</i></span></td>
						<td colspan=""></td>
						<td colspan=""></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.3.1</td>
						<td colspan="2">ISO 9001:2008  tentang  Persyaratan Sistem Manajemen Mutu</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.3.2</td>
						<td colspan="2">Instruksi Kerja Pembuat Dokumen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.3.3</td>
						<td colspan="2">Pepres (Peraturan Presiden) No.28 tentang Standarisasi</td>
					</tr>

					<tr class="sub-bab" >
						<td colspan="">1.4</td>
						<td colspan="3">DEFINISI DAN ISTILAH</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Definisi dan Istilah Umum :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="2">Dokumen Sistem Mutu adalah Dokumen yang dibuat dan diterbitkan oleh <b><?php echo $project[0]->company_name ?></b>, untuk memastikan kegiatan Perusahaan berjalan sesuai Standard ISO 9001:2008, yang meliputi Peraturan Perusahaan, Surat Keputusan Direksi, Kebijakan Mutu, Sasaran Mutu, Manual Mutu, Prosedur Mutu, Instruksi Kerja dan Formulir.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="2">Dokumen Eksternal adalah Dokumen yang diterbitkan oleh pihak diluar <b><?php echo $project[0]->company_name ?></b>, yang digunakan sebagai Acuan Kerja antara lain meliputi: Peraturan Perundangan, Standar Nasional dan Standar International, Manual Operasi dan Pemeliharaan Peralatan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="2">Petugas Pengendali Dokumen (Document Control) adalah Petugas yang bertanggung jawab untuk mengendalikan Dokumen di Perusahaan meliputi Dokumen Sistem Mutu dan Dokumen Eksternal yang diterbitkan serta disimpan.</td>
					</tr>

					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
					</tr>
				</tbody>
			</table>
		</div>
	
</body>
</html>