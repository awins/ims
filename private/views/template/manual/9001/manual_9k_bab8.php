<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt;">
	
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB VIII</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>PEMANTAUAN, PENGUKURAN, ANALISIS, DAN PERBAIKAN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 20pt"></td>
						<td style="width: 385pt"></td>
					</tr>
				</thead>
				<tbody>

					<tr class="sub-bab" >
						<td colspan="">8.1</td>
						<td colspan="3">UMUM</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> merencanakan dan melaksanakan pemantauan, pengukuran, analisa dan proses peningkatan yang dibutuhkan untuk menjamin kesesuaian sistem manajemen mutu, dan untuk peningkatan efektifitas sistem manajemen mutu yang berkesinambungan :</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.1.1</td>
						<td colspan="2">Untuk kesesuaian produk konstruksi dilaksanakan pemantauan pelaksanaan proyek oleh manajemen proyek dan pemeriksaan mutu produk oleh tim pemeriksa.</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.1.2</td>
						<td colspan="2">Untuk menjamin kesesuaian sistem manajemen mutu dilaksanakan Audit Internal</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.1.3</td>
						<td colspan="2">Untuk peningkatan efektifitas system manajemen	yang terus menerus dilaksanakan tinjauan manajemen dan tindakan perbaikan dan pencegahan.</td>
					</tr>					
					
					<tr class="sub-bab" >
						<td colspan="">8.2</td>
						<td colspan="3">PEMANTAUAN DAN PENGUKURAN</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.2.1</td>
						<td colspan="2">Kepuasan Pelanggan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Pengukuran kepuasan Pelanggan dilaksanakan pada setiap Proyek   sepanjang Proyek berlangsung atau di akhir Proyek. Manajer Proyek bertanggung jawab untuk mengukur tingkat kepuasan pelanggan dengan cara mengirimkan pertanyaan (kuisoner) kepada pelanggan dan menganalisa data yang didapat, dan melaporkannya kepada manajemen.</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.2.2</td>
						<td colspan="2">Audit Internal</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Audit internal dilaksanakan pada periode yang direncanakan untuk menilai :</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Kesesuaian Sistem Manajemen Mutu dengan aturan yang  direncanakan, persyaratan Standar ISO  9001:2008, dan Sistem Manajemen Mutu yang telah ditetapkan oleh <b><?php echo $project[0]->company_name ?></b></td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Efektifitas penerapan dan pemeliharaan Sistem Manajemen Mutu.</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">Program audit direncanakan dengan mempertimbangkan status dan kepentingan proses serta area yang diaudit maupun hasil audit sebelumnya. Kriteria, Ruang Lingkup, Frekuensi dan metode ditentukan. Pemilihan Auditor dan pelaksanaan Audit dijamin Objektif dan Independen.
							<br />
							Auditor tidak mengaudit pekerjaan mereka sendiri. Auditee menjamin ketepatan waktu dan tindakan yang  dilakukan untuk menghilangkan ketidak sesuaian. Kegiatan tindakan lanjut meliputi verifikasi tindakan yang dilakukan dan pelaporan hasilnya.
						</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.2.3</td>
						<td colspan="2">Pemantauan dan Pengukuran Proses</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> menetapkan metode yang sesuai untuk pemantauan proses sistem manajemen   mutu   dan   menunjukkan   kemampuan   proses   dalam   mencapai   hasil   yang direncanakan. Hasil yang  direncanakan ditetapkan dalam sasaran mutu di setiap Fungsi / Bagian. Rekaman di masing-masing bagian menunjukkan apakah hasil tersebut tercapai atau tidak. Apabila hasil yang direncanakan tidak tercapai, dilakukan perbaikan sesuai  kebutuhan, untuk menjamin kesesuaian produk.</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.2.4</td>
						<td colspan="2">Pemantauan dan Pengukuran Produk</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Pemantauan dan pengukuran karateristik produk dilaksanakan melalui kegiatan pemeriksaan oleh tim pemeriksa untuk memverifikasi bahwa persyaratan yang terkait telah dipenuhi. Hal ini akan dilakukan pada tahapan-tahapan proyek berdasarkan aturan yang direncanakan pada prosedur dan petunjuk kerja yang terkait. 
							<br />
							Bukti kesesuaian dengan persyaratan ditunjukkan dengan pengesahan dokumen pemeriksaan. Serah terima produk konstruksi ke pelanggan hanya bisa dilaksanakan bila produk konstruksi telah memenuhi persyaratan.
						</td>
					</tr>					
					<tr class="sub-bab" >
						<td colspan="">8.3</td>
						<td colspan="3">PENGENDALIAN PRODUK YANG TIDAK SESUAI</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Produk yang tidak sesuai yang ditemukan pada tahapan pelaksanaan proyek konstruksi diambil tindakan untuk mengatasi ketidaksesuaian tersebut dengan cara :</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.3.1</td>
						<td colspan="2">Perbaikan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.3.2</td>
						<td colspan="2">Dibongkar dan dikerjakan ulang</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Setelah pelaksanaan perbaikan atau pengerjaan ulang dilaksanakan pemeriksaan ulang. Bila  produk  konstruksi  yang  tidak  sesuai  tersebut  diketahui setelah serah terima ke pelanggan, <b><?php echo $project[0]->company_name ?></b> akan mengambil tindakan yang sesuai dengan persyaratan yang berlaku.</td>
					</tr>					
					<tr class="sub-bab" >
						<td colspan="">8.4</td>
						<td colspan="3">ANALISA DATA</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Data yang diperlukan untuk menunjukkan kesesuaian dan efektifitas sistem manajemen mutu ditentukan,  dikumpulkan dan dianalisa. Kegiatan ini juga dilaksanakan untuk mengevaluasi peningkatan berkesinambungan Sistem Manajemen Mutu.
							<br />
							Analisa data mencakup :
						</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.4.1</td>
						<td colspan="2">Data mengenai Kepuasan Pelanggan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.4.2</td>
						<td colspan="2">Data tentang kesesuaian akan Persyaratan Produk.</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.4.3</td>
						<td colspan="2">Data tentang karakteristik dan kecenderungan proses dan produk </td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.4.4</td>
						<td colspan="2">Data seleksi dan Evaluasi terhadap Supplier</td>
					</tr>					
					
					<tr class="sub-bab" >
						<td colspan="">8.5</td>
						<td colspan="3">PENINGKATAN</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.5.1</td>
						<td colspan="2">Peningkatan Berlanjut</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> secara berkesinambungan meningkatkan efektifitas sistem manajemen mutu melalui penggunaan kebijakan mutu, sasaran mutu, hasil audit, analisa data, tindakan perbaikan dan pencegahan, dan tinjauan manajemen.</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.5.2</td>
						<td colspan="2">Tindakan Perbaikan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Tindakan perbaikan dilaksanakan untuk menghilangkan penyebab ketidaksesuaian dan untuk mencegah berulangnya ketidaksesuaian dengan cara :</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Peninjauan Ketidaksesuaian</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Penentuan Penyebab Ketidaksesuaian</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Evaluasi kebutuhan tindakan untuk menjamin ketidaksesuaian tidak terjadi lagi</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Penentuan dan pelaksanaan yang dibutuhkan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Pencatatan hasil dari tindakan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">f.</td>
						<td colspan="">Peninjauan tindakan perbaikan yang telah dilakukan.</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.5.3</td>
						<td colspan="2">Tindakan Pencegahan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Tindakan Pencegahan dilaksanakan untuk menghilangkan penyebab   potensial  ketidaksesuaian untuk mencegah terulangnya kejadian yang sama. Tindakan pencegahan dilaksanakan sesuai dengan akibat dari masalah potensial, dengan cara : </td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Penentuan Ketidaksesuaian Potensial dan Penyebabnya</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Evaluasi Kebutuhan terhadap Tindakan untuk mencegah ketidaksesuaian terulang kembali</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Penentuan dan pelaksanaan tindakan yang dibutuhkan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Pencatatan hasil dari tindakan</td>
					</tr>					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Peninjauan tindakan pencegahan yang telah dilakukan</td>
					</tr>					
					
				</tbody>
			</table>
		</div>
</body>
</html>