<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt;">
	
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB V</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>TANGGUNG JAWAB MANAJEMEN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 20pt"></td>
						<td style="width: 385pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">5.1</td>
						<td colspan="3">KOMITMEN MANAJEMEN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Manajemen <b><?php echo $project[0]->company_name ?></b> menunjukkan komitmennya terhadap penerapan sistem manajemen mutu dengan :</td>
					</tr>

					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.1.1</td>
						<td colspan="2">Menetapkan dan memenuhi persyaratan pelanggan dan persyaratan perundangan yang terkait.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.1.2</td>
						<td colspan="2">Mengkomunikasikan pentingnya pemenuhan persyaratan pelanggan dan  peraturan perundangan yang terkait :</td>
					</tr>

					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Menetapkan Kebijakan Mutu</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Memastikan Sasaran Mutu ditetapkan </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Melaksanakan Tinjauan Manajemen </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Menyediakan sumber daya yang memadai.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">5.2</td>
						<td colspan="3">FOKUS TERHADAP PELANGGAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Manajemen memberi perhatian terhadap pemenuhan Persyaratan Pelanggan untuk mencapai Kepuasan Pelanggan. Perhatian ini dilakukan antara lain melalui :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.2.1</td>
						<td colspan="2">Identifikasi kebutuhan dan permintaan pelanggan, baik yang dinyatakan langsung dalam persyaratan ataupun tidak.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.2.2</td>
						<td colspan="2">Mereview   kemampuan   <b><?php echo $project[0]->company_name ?></b>   dalam   memenuhi   kebutuhan dan permintaan pelanggan, termasuk terhadap perubahan-perubahannya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.2.3</td>
						<td colspan="2">Menanggapi dan menyelesaikan setiap keluhan pelanggan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.2.4</td>
						<td colspan="2">Memberikan informasi yang diperlukan pelanggan menyangkut produk ataupun proses - proses lain yang terkait.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Mengukur dan menganalisa persepsi pelanggan tentang sejauh mana mereka terpuaskan dengan produk dan layanan yang diterimanya.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">5.3</td>
						<td colspan="3">KEBIJAKAN MUTU</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Manajemen <b><?php echo $project[0]->company_name ?></b> menetapkan kebijakan mutu yang   :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.3.1</td>
						<td colspan="2">Sesuai dengan visi-misi dan tujuan perusahaan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.3.2</td>
						<td colspan="2">Mencakup Komitmen untuk memenuhi Persyaratan Pelanggan dan untuk perbaikan berkesinambungan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.3.3</td>
						<td colspan="2">Memberi kerangka  untuk menetapkan dan  meninjau Sasaran  Mutu.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.3.4</td>
						<td colspan="2">Memastikan bahwa Kebijakan Mutu tersebut dipahami, diterapkan, dan dipelihara pada semua tingkatan organisasi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.3.5</td>
						<td colspan="2">Ditinjau agar selalu sesuai.</td>
					</tr>

					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Kebijakan Mutu <b><?php echo $project[0]->company_name ?></b> yang telah ditetapkan,  dikomunikasikan  kepada seluruh personel yang ada dalam lingkup perusahaan untuk dipahami dan dilaksanakan.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">5.4</td>
						<td colspan="3">PERENCANAAN</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.4.1</td>
						<td colspan="2">Sasaran Mutu</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Sasaran Mutu ditetapkan pada setiap level dan fungsi di dalam perusahaan untuk mendukung kebijakan  mutu  perusahaan.  Sasaran  mutu  ditetapkan  secara  spesifik  dan  terukur  yang mencerminkan hasil yang ingin dicapai dalam kurun waktu tertentu.
Sasaran mutu dipantau pencapaiannya dalam kurun waktu yang ditentukan. Sasaran mutu yang tidak tercapai, dilakukan identifikasi masalah dan tindakan perbaikan yang diperlukan, termasuk tindakan untuk menghilangkan penyebab masalah yang diidentifikasi.
</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.4.2</td>
						<td colspan="2">Perencanaan Sistem Manajemen Mutu</td>
					</tr>


					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen  merencanakan  Sistem  Manajemen  Mutu  <b><?php echo $project[0]->company_name ?></b>  yang  sesuai dengan  persyaratan dalam ISO 9001:2008  dan dapat diterapkan untuk mengelola proses- proses untuk memenuhi  Persyaratan Pelanggan, Persyaratan Perundangan yang berlaku, dan Persyaratan Perusahaan.
Sistem Manajemen Mutu ini selalu dipelihara dan dipertahankan, sehingga jika dikemudian hari terjadi perubahan terhadap sistem yang ada maupun adanya integrasi dengan sistem manajemen lain yang diadopsi, maka Sistem Manajemen Mutu tetap dapat diterapkan dengan penyesuaian kegiatan dilapangan.
Perencanaan dalam Sistem Manajemen Mutu ini mencakup pula perencanaan untuk mencapai sasaran mutu yang ditetapkan.
</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">5.5</td>
						<td colspan="3">TANGGUNG JAWAB, WEWENANG DAN KOMUNIKASI</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.5.1</td>
						<td colspan="2">Tanggung jawab dan Wewenang</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen <b><?php echo $project[0]->company_name ?></b> menetapkan Struktur Organisasi dalam penerapan Sistem Manajemen Mutu, termasuk uraian tentang Tanggung Jawab dan Wewenang. Tanggung jawab dan Wewenang juga ditetapkan dalam Prosedur dan Dokumen lainnya.
								<br />
								Semua  manajer  bertanggung  jawab  terhadap  mutu  di  bagian-nya  masing-masing,  yang mencakup tanggung jawab sebagai berikut :
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Memprakarsai tindakan untuk mencegah ketidaksesuaian proses,produk, dan sistem mutu.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Mengidentifikasi dan mencatat penyimpangan yang berkaitan dengan proses, produk, danistem mutu.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Memberikan alternatif pemecahan melalui jalur media yang sesuai.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Memverifikasi pelaksanaan dan memantau suatu pemecahan ketidaksesuaian hingga penyelesainnya.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.5.2</td>
						<td colspan="2">Wakil Manajemen Mutu</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen  <b><?php echo $project[0]->company_name ?></b>  menunjuk  salah  seorang  anggota  Manajemen  atau Pegawai  Senior  perusahaan  yang  mengetahui  proses-proses  yang  dikelola  sebagai  Wakil Manajemen, dengan tanggung jawab sebagai berikut : </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Menjamin bahwa Sistem Manajemen Mutu ditetapkan, diterapkan, dan dipelihara sesuai dengan standar ISO 9001 : 2008.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Melaporkan kepada  Manajemen  <b><?php echo $project[0]->company_name ?></b>  mengenai  kinerja  Sistem Manajemen Mutu, termasuk memberikan masukan untuk 
perbaikan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Mengkomunikasikan Persyaratan Pelanggan di perusahaan melalui media yang sesuai.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Menjadi penghubung  antara  Manajemen  <b><?php echo $project[0]->company_name ?></b>  dengan  pihak  luar perusahaan yang berkaitan dengan Sistem Manajemen Mutu (seperti Pelanggan, Lembaga Sertifikasi maupun Pemasok).</td>
					</tr>
					
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.5.3</td>
						<td colspan="2">Komunikasi Internal</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen  mengatur  adanya  kegiatan  Komunikasi  Internal  yang  dilakukan  antar  bagian maupun dalam  bagian itu sendiri. Metoda komunikasi yang digunakan dapat berupa Rapat Harian, Rapat Mingguan, Rapat Kerja, dan lain-lain. Sedang media pencatatannya dapat berupa Risalah Rapat, Pengumuman, Instruksi, Memo dan lain-lain. Masing-masing bagian menyimpan dan  memelihara  hasil  kegiatan  komunikasl  internal  dan   salinannya  diberikan  kepada Pengendali Dokumen sebagai bukti bahwa kegiatan komunikasi telah diterapkan.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">5.6</td>
						<td colspan="3">TINJAUAN MANAJEMEN</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.6.1</td>
						<td colspan="2">Umum</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen meninjau penerapan Sistem Manajemen Mutu minimal 1 (satu) tahun sekali, atau dilakukan lebih sering jika diperlukan,untuk menjamin efektifitasnya. Tinjauan ini termasuk melihat kemungkinan   pengembangan  dan  perubahan  sistem  manajemen  serta  tinjauan terhadap Kebijakan Mutu dan Sasaran Mutu.
							<br />
							Tinjauan Manajemen melibatkan semua bagian yang tercakup dalam Sistem Manajemen Mutu, baik untuk   memberi  masukan  dalam  tinjauan  maupun  menindaklanjuti  hasil  tinjauan. Rekaman Pelaksanaan Tinjauan Manajemen dan hasil-hasilnya disimpan.
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.6.2</td>
						<td colspan="2">Masukan Tinjauan Manajemen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Masukan tinjauan manajemen antara lain mencakup :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Hasil Audit.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Umpan Balik Pelanggan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Kinerja setiap proses dan kesesuaian Produk</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Status tindakan Perbaikan dan Pencegahan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Tindak lanjut dari Tinjauan Manajemen sebelumnya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">f.</td>
						<td colspan="">Perubahan yang dapat mempengaruhi Sistem Manajemen Mutu.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">g.</td>
						<td colspan="">Rekomendasi untuk peningkatan Sistem Manajemen Mutu.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">h.</td>
						<td colspan="">Hasil tindak lanjut dari Tinjauan Manajemen sebelumnya</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">5.6.3</td>
						<td colspan="2">Keluaran Tinjauan Manajemen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Keluaran dari tinjauan manajemen, berupa keputusan menyangkut :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Peningkatan keefektifan sistem manajemen mutu dan prosesnya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Peningkatan Produk yang berhubungan dengan Persyaratan Pelanggan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Penyediaan kebutuhan Sumberdaya.</td>
					</tr>
					
				</tbody>
			</table>
		</div>
</body>
</html>