<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body>
	<div style="text-align: center;padding: 15px 0 0 0">
		<b>DAFTAR ISI</b>
	</div>
	<div style="width: 500pt;margin: auto" >
		<table id="table-dafar-isi" class="" border="0"  >
			<thead>
				<tr class="bab">
					<td style="width: 50pt" class=""></td>
					<td style="width: 40pt"></td>
					<td style="width: 390pt"></td>
					<td style="width: 20pt"></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="3">COVER &amp; LEMBAR PENGESAHAN</td>
					<td colspan="">1</td>
				</tr>
				<tr>
					<td colspan="3">RIWAYAT REVISI DOKUMEN</td>
					<td colspan="">2</td>
				</tr>
				<tr>
					<td colspan="3">DAFTAR ISI</td>
					<td colspan="">3</td>
				</tr>
				<tr class="bab">
					<td colspan="">BAB I</td>
					<td colspan="2">: PENDAHULUAN</td>
					<td colspan="">5</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">I.1.</td>
					<td colspan="">Tujuan</td>
					<td colspan="">5</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">I.2.</td>
					<td colspan="">Ruang Lingkup</td>
					<td colspan="">5</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">I.3.</td>
					<td colspan="">Referensi</td>
					<td colspan="">5</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">I.4.</td>
					<td colspan="">Definisi dan Istilah</td>
					<td colspan="">5</td>
				</tr>
				<tr class="bab">
					<td colspan="">BAB II</td>
					<td colspan="2">: URAIAN ORGANISASI PERUSAHAAN</td>
					<td colspan="">6</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">II.1.</td>
					<td colspan="">Profil Perusahaan</td>
					<td colspan="">6</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">II.2.</td>
					<td colspan="">Struktur Organisasi Perusahaan</td>
					<td colspan="">6</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">II.3.</td>
					<td colspan="">Tanggung Jawab dan Wewenang</td>
					<td colspan="">6</td>
				</tr>

				<tr class="bab">
					<td colspan="">BAB III</td>
					<td colspan="2">: KEBIJAKAN MUTU</td>
					<td colspan="">7</td>
				</tr>

				<tr class="bab">
					<td colspan="">BAB IV</td>
					<td colspan="2">: SISTEM MANAJEMEN MUTU</td>
					<td colspan="">8</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">IV.1.</td>
					<td colspan="">Umum</td>
					<td colspan="">8</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">IV.2.</td>
					<td colspan="">Dokumentasi</td>
					<td colspan="">8</td>
				</tr>
				<tr class="bab">
					<td colspan="">BAB V</td>
					<td colspan="2">: TANGGUNG JAWAB MANAJEMEN</td>
					<td colspan="">10</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">V.1.</td>
					<td colspan="">Komitmen Manajemen</td>
					<td colspan="">10</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">V.2.</td>
					<td colspan="">Fokus Terhadap Pelanggan</td>
					<td colspan="">10</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">V.3.</td>
					<td colspan="">Kebijakan Mutu</td>
					<td colspan="">10</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">V.4.</td>
					<td colspan="">Perencanaan</td>
					<td colspan="">11</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">V.5.</td>
					<td colspan="">Tanggung Jawab, Wewenang, dan Komunikasi</td>
					<td colspan="">11</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">V.6.</td>
					<td colspan="">Tinjauan Manajemen</td>
					<td colspan="">12</td>
				</tr>
				<tr class="bab">
					<td colspan="">BAB VI</td>
					<td colspan="2">: PENGELOLAAN SUMBER DAYA</td>
					<td colspan="">14</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VI.1.</td>
					<td colspan="">Penyediaan Sumber  Daya</td>
					<td colspan="">14</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VI.2.</td>
					<td colspan="">Sumber Daya Manusia</td>
					<td colspan="">14</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VI.3.</td>
					<td colspan="">Infrastruktur</td>
					<td colspan="">15</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VI.4.</td>
					<td colspan="">Lingkungan Kerja</td>
					<td colspan="">15</td>
				</tr>
				<tr class="bab">
					<td colspan="">BAB VII</td>
					<td colspan="2">: REALISASI PRODUK</td>
					<td colspan="">16</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VII.1.</td>
					<td colspan="">Perencanaan Realisasi Produk</td>
					<td colspan="">16</td>
				</tr>
				
				
			</tbody>
		</table>
	</div>
</body>
</html>