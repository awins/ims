<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt;">
	
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB IV</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>SISTEM MANAJEMEN MUTU</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 20pt"></td>
						<td style="width: 385pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">4.1</td>
						<td colspan="3">UMUM</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> menetapkan, mendokumentasikan, menerapkan sistem manajemen mutu   sesuai   persyaratan   dalam ISO   9001:2008,   serta   secara   bertahap   meningkatkan efektifitasnya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Pengembangan dan penerapan sistem manajemen mutu di <b><?php echo $project[0]->company_name ?></b> dilakukan dengan :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.1.1</td>
						<td colspan="2">Mengenai proses-proses yang dikelola, urutan dan interaksinya, yang dituangkan diagram proses bisnis.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.1.2</td>
						<td colspan="2">Menentukan  kriteria  dan  metode  yang  diperlukan  untuk  menjamin  pelaksanaan  dan pengendalian   proses-proses  agar  berjalan  efektif,  yang  dituangkan  dalam  prosedur, rencana mutu dan dokumen lainnya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.1.3</td>
						<td colspan="2">Menjamin tersedianya sumber daya dan informasi yang diperlukan untuk mendukung pelaksanaan dan pemantauan proses-proses tersebut. </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.1.4</td>
						<td colspan="2">Memantau, mengukur serta menganalisa proses-proses tersebut, yang dilakukan dengan mengevaluasi pencapaian sasaran mutu, audit internal, dan sebagainya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.1.5</td>
						<td colspan="2">Melakukan tindakan-tindakan yang diperlukan untuk mencapai hasil yang diinginkan dan melakukan peningkatan terus menerus terhadap proses-proses tersebut.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.1.6</td>
						<td colspan="2">Proses-proses yang dikelola tersebut memenuhi persyaratan dalam ISO 9001:2008 , termasuk proses  yang  diserahkan  kepada  pihak  lain  (subkon).  Pengendalian  terhadap  proses  yang dikerjakan subkon dituangkan dalam sistem manajemen mutu.</td>
					</tr>
					
					<tr class="sub-bab" >
						<td colspan="">4.2</td>
						<td colspan="3">4.2	DOKUMENTASI</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.2.1</td>
						<td colspan="2">Umum</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen <b><?php echo $project[0]->company_name ?></b> telah mendokumentasikan Sistem Manajemen Mutu sesuai persyaratan dalam standar ISO 9001:2008 untuk :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Menjadi alat komunikasi kebijakan Sistem Manajemen Mutu serta alur proses di antara Unit Kerja maupun antar Personel.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Memberikan kerangka dasar bagi Perencanaan Mutu, Pengendalian, Pencegahan, Ketidaksesuaian, serta Perbaikan yang terus menerus pada kegiatan yang mempengaruhi Mutu.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Memberikan jaminan akan tersedianya Sumber Daya dan Informasinya untuk setiap proses kegiatan yang dilakukan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Memberikan bukti objektif kepada pihak eksternal yang terkait bahwa telah ada Sistem Manajemen Mutu yang dapat diterapkan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Dokumentasi Sistem Manajemen Mutu <b><?php echo $project[0]->company_name ?></b>  dibagi dalam empat level dokumen yang mencakup :</td>
					</tr>
					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">
							<table>
								<tbody>
									<tr>
										<td style="width: 25px"  >1.</td>
										<td style="width: 55px"  >Level I</td>
										<td style="width: 10px">:</td>
										<td>Manual Mutu, berikut Kebijakan Mutu dan Sasaran Mutu</td>
									</tr>
									<tr>
										<td>2.</td>
										<td>Level II</td>
										<td>:</td>
										<td>Prosedur Pengendalian Sistem dan Operasional</td>
									</tr>
									<tr>
										<td>3.</td>
										<td>Level III</td>
										<td>:</td>
										<td>Instruksi Kerja, dan Dokumen  pendukung lainnya</td>
									</tr>
									<tr>
										<td>4.</td>
										<td>Level IV</td>
										<td>:</td>
										<td>Rekaman atau Arsip dan Catatan Mutu.</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.2.2</td>
						<td colspan="2">Manual Mutu</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen <b><?php echo $project[0]->company_name ?></b> menetapkan dan memelihara Manual  Mutu  yang memuat :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Ruang	Lingkup penerapan Sistem Manajemen Mutu termasuk rincian dan alasan pengecualian persyaratan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Prosedur Mutu yang didokumentasikan untuk penerapan Sistem Manajemen  Mutu  atau Referensinya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Penjelasan tentang interaksi dari proses Sistem Manajemen Mutu</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manual mutu  yang  ditetapkan  akan  ditinjau  dan  direvisi  sesuai  keperluan  untuk  tujuan perbaikan.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.2.3</td>
						<td colspan="2">Pengendalian Dokumen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Wakil Manajemen bersama Pengendali Dokumen (Document Control) yang telah ditunjuk Manajemen bertanggung jawab terhadap pengendalian dokumen yang meliputi :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Pengesahan dokumen sebelum diterbitkan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Peninjauan dan perbaikan (jika diperlukan) serta pengesahan ulang.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Pengidentifikasian perubahan dan status revisi dokumen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Pendistribusian dokumen kepada pihak yang memerlukan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Pengidentifikasian dan pendistribusian Dokumen Eksternal.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">f.</td>
						<td colspan="">Penarikan dan penyimpanan Dokumen Kadaluarsa (tidak dipakai lagi).</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Pengendalian dokumen bertanggung jawab mengendalikan dan memelihara dokumen asli (master) termasuk daftar induknya.</td>
					</tr>
					
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.2.4</td>
						<td colspan="2">Pengendalian Rekaman</td>
						<td colspan=""></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Rekaman dipelihara (disimpan) sebagai bukti penerapan Sistem Manajemen Mutu. Rekaman dapat dalam berbagai jenis media (hard copy atau soft copy) dan memiliki masa simpan yang ditetapkan waktunya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Setiap unit kerja / bagian menunjuk personil untuk menyimpan catatan agar mudah diambil dan terhindar dari kerusakan dan atau kehilangan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Pengendali rekaman menyimpan Daftar Rekaman sebagai alat pengendali penerapannya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Pengendali Rekaman bertanggung jawab untuk menarik dan memusnahkan rekaman yang tidak diperlukan lagi dengan persetujuan Kepala Bagiannya masing-masing.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
					</tr>
					
					
				</tbody>
			</table>
		</div>
</body>
</html>