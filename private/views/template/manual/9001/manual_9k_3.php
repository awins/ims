<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body>
	<div style="width: 500pt;margin: auto" >
		<table id="table-dafar-isi" class="" border="0"  >
			<thead>
				<tr class="bab">
					<td style="width: 50pt" class=""></td>
					<td style="width: 40pt"></td>
					<td style="width: 390pt"></td>
					<td style="width: 20pt"></td>
				</tr>
			</thead>
			<tbody>
				
				<tr>
					<td colspan=""></td>
					<td colspan="">VII.2.</td>
					<td colspan="">Proses Yang Berkaitan Dengan Pelanggan</td>
					<td colspan="">16</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VII.3.</td>
					<td colspan="">Perancangan dan Pengembangan</td>
					<td colspan="">17</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VII.4.</td>
					<td colspan="">Pembelian</td>
					<td colspan="">17</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VII.5.</td>
					<td colspan="">Penyediaan Proses Produksi dan pelayanan pendukung</td>
					<td colspan="">17</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VII.6.</td>
					<td colspan="">Pengendalian sarana pemantauan dan pengukuran</td>
					<td colspan="">18</td>
				</tr>
				<tr class="bab">
					<td colspan="">BAB VIII</td>
					<td colspan="2">: PEMANTAUAN PENGUKURAN, ANALISIS DAN PERBAIKAN</td>
					<td colspan="">19</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VIII.1.</td>
					<td colspan="">Umum</td>
					<td colspan="">19</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VIII.2.</td>
					<td colspan="">Pemantauan dan Pengukuran</td>
					<td colspan="">19</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VIII.3.</td>
					<td colspan="">Pengendalian Produk yang Tidak Sesuai</td>
					<td colspan="">20</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VIII.4.</td>
					<td colspan="">Analisis Data</td>
					<td colspan="">20</td>
				</tr>
				<tr>
					<td colspan=""></td>
					<td colspan="">VIII.5.</td>
					<td colspan="">Peningkatan Berkelanjutan</td>
					<td colspan="">20</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</body>
</html>