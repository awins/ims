<?php 
	foreach ($project as $value) {
		if ($value->iso_type == 1){
			$date = $value->main_assessment_date;
			break;
		}
	}

	$time = strtotime($date . '-4 month');

?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt">
		<div style="width: 470pt;margin: auto">
			<div style="text-align: center;padding: 15px 0">
				<span style="font-size: 14pt;font-weight: bold" ><b>BAB III</b></span>
				<br />
				<span style="font-size: 12pt;font-weight: bold" ><b>KEBIJAKAN MUTU</b></span>
			</div>

			<div style="text-align: justify;padding: 15px 0">
				<b><?php echo $project[0]->company_name ?></b> bergerak dalam “ <?php echo $project[0]->company_scope ?>  ”.
			</div>

			<div style="text-align: justify;padding: 15px 0">
				<b><?php echo $project[0]->company_name ?></b> Manajemen memiliki  Komitmen  untuk  melibatkan  semua karyawan dalam pelayanan yang berkelanjutan serta peningkatan kualitas. Hal terpenting adalah komitmen  kami  untuk   menyediakan  layanan  yang  berkualitas  yaitu  Memiliki  Karyawan Berkualitas yang mengerjakan Tugas dengan Baik. Salah satu Kebijakan Pelatihan Perusahaan adalah mengembangkan Kemampuan karyawan kami di semua tingkat  kualifikasi  yang  sesuai  dan  dilatih  untuk  tugas-tugas  mereka,  Perusahaan  diwajibkan untuk menyediakan program pelatihan yang sesuai demi pengembangan skill bagi karyawan. Management  Representative  atau  Wakil  Manajemen  Mutu (WMM) bertanggung jawab untuk mengkomunikasikan  kebijakan  mutu  ini  didalam  organisasi  agar  dapat   dipahami  secara berkesinambungan terus-menerus dengan cara memberikan informasi, melaksanakan pemantauan dan pemeliharaan sistem sesuai dengan perkembangan organisasi yang  perusahaan ini guna memenuhi standard internasional ISO 9001 : 2008. Wakil  Manajemen  Mutu ditugaskan oleh Presiden Direktur memiliki  otoritas penuh  untuk menyelidiki   masalah kualitas memastikan bahwa  tindakan  yang  tepat waktu dan efektif dilaksanakan oleh masing-masing departemen. Selain itu, melalui program-program perbaikan secara terus-menerus, di mana semua karyawan diwajibkan untuk  berpartisipasi,  meninjau   ulang   sistim   ini   secara   terus-menerus   untuk mengidentifikasi  perbaikan.
			</div>

			<div style="text-align: center;padding: 15px 0;width: 200px">
				<?php echo $project[0]->company_city . ', ' .indDate($time) ?>
			</div>
			<div style="text-align: center;padding: 55px 0;width: 200px">
		      	(Direktur Utama)
			</div>



		</div>
</body>
</html>