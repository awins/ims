<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt;">
	
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB VI</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>MANAJEMEN SUMBER DAYA</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 20pt"></td>
						<td style="width: 385pt"></td>
					</tr>
				</thead>
				<tbody>
					
					<tr class="sub-bab" >
						<td colspan="">6.1</td>
						<td colspan="3">SUMBER DAYA</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Manajemen <b><?php echo $project[0]->company_name ?></b> menentukan dan menyediakan  sumber  daya  yang dibutuhkan   untuk   menerapkan   dan   memelihara   sistem   manajemen   mutu   dan   untuk meningkatkan kepuasan pelanggan. 
							<br />
							Sumber  daya  yang  disediakan  mencakup Sumber  Daya  Manusia  (SDM),  infrastruktur,  dan lingkungan kerja yang digunakan untuk mengelola proses-proses. Sumber daya yang disediakan dapat berupa milik <b><?php echo $project[0]->company_name ?></b> maupun milik pihak lain yang digunakan sesuai kepentingan bisnis perusahaan.
						</td>

					</tr>
					<tr class="sub-bab" >
						<td colspan="">6.2</td>
						<td colspan="3">SUMBER DAYA MANUSIA</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">6.2.1</td>
						<td colspan="2">Umum</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> menetapkan Kompetensi atau  Kemampuan  bagi  Personil yang pekerjaannya mempengaruhi Mutu Produk, berdasarkan Pendidikan, Pelatihan, Ketrampilan, dan Pengalaman yang sesuai.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">6.2.2</td>
						<td colspan="2">Kompetensi, Kesadaran dan Pelatihan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Bagian Personalia bertanggung jawab untuk mendokumentasikan syarat kompetensi SDM untuk setiap pekerjaan, mengevaluasi kompetensi setiap pegawai, dan semua kegiatan yang berkaitan dengan perencanaan, pelaksanaan, dan evaluasi pelatihan pegawai.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Pelatihan pegawai disusun dan dilaksanakan secara terencana atas dasar kebutuhan pelatihan pegawai serta kebijakan perusahaan dengan tujuan untuk menjamin bahwa seluruh pegawai berkompeten  dalam  bidang  yang menjadi  tanggung  jawabnya  dan  untuk  meningkatkan prestasi pegawai.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Pelatihan diberikan kepada personel yang mengelola, melaksanakan dan melakukan verifikasi terhadap  pekerjaan  yang  mempengaruhi  Mutu  Produk  dan  kepada  personel  yang  akan melaksanakan Audit Internal.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Pegawai yang belum mempunyai kompetensi yang dipersyaratkan diberikan pelatihan atau tindakan lain yang sesuai.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Pelaksanaan Pelatihan dan hasil-hasilnya dievaluasi untuk mengetahui keefektifan pelatihan terhadap tujuan yang ditetapkan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Bagian Personalia bertanggung jawab terhadap rekaman yang berhubungan  dengan kompetensi pegawai yang mencakup pelatihan, pendidikan, ketrampilan dan pengalaman.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">6.3</td>
						<td colspan="3">INFRASTRUKTUR</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Manajemen <b><?php echo $project[0]->company_name ?></b> menyediakan sarana kerja, baik perangkat keras dan lunak, alat transportasi dan komunikasi yang sesuai, sehingga pegawai dapat bekerja dengan baik untuk menghasilkan mutu produk yang dapat memenuhi persyaratannya.
							<br />
							Bagian HRD bertanggung jawab untuk menjaga dan memelihara sarana kerja, mencakup pemeliharaan dan perbaikan gedung, sarana transportasi, sarana komunikasi, unit komputer, dan fasilitas kantor lainnya. 
							<br />
							Bagian Inventori bertanggung jawab untuk menjaga dan memelihara sarana dan peralatan yang digunakan untuk pelaksanaan proyek.
						</td>
						<td colspan=""></td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">6.4</td>
						<td colspan="3">LINGKUNGAN KERJA</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Manajemen	<b><?php echo $project[0]->company_name ?></b> mengelola lingkungan kerja yang diperlukan untuk memenuhi persyaratan  produk  yang  dihasilkan,  diantaranya  dengan  menjaga  kebersihan, keteraturan dan keamanan.
							<br />
							Bagian Human Resources Development (HRD) bertanggung  jawab  untuk  memastikan  Lingkungan  Kerja  di  lingkungan perusahaan dalam kondisi aman, teratur, dan bersih. 
							<br />
							Manajer Proyek bertanggung jawab untuk memastikan Lingkungan Kerja di Proyek dalam kondisi aman dan sesuai untuk pelaksanaan proyek.
						</td>
						<td colspan=""></td>
					</tr>
				</tbody>
			</table>
		</div>
</body>
</html>