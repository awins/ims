<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt;">
	
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB VII</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>REALISASI PRODUK</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 20pt"></td>
						<td style="width: 385pt"></td>
					</tr>
				</thead>
				<tbody>

					<tr class="sub-bab" >
						<td colspan="">7.1</td>
						<td colspan="3">PERENCANAAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> merencanakan dan mengembangkan proses-proses realisasi produk yang  konsisten  dengan  persyaratan  proses  lain  dari  sistem  manajemen  mutu  perusahaan, antara lain berupa :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.1.1</td>
						<td colspan="2">Penetapan sasaran mutu dan persyaratan yang berkaitan dengan produk yang dihasilkan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.1.2</td>
						<td colspan="2">Penetapan proses, dokumen, dan sumber daya yang diperlukan untuk menghasilkan produk, yang dituangkan dalam Perencanaan Mutu.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.1.3</td>
						<td colspan="2">Kegiatan Verifikasi, Validasi, Pemantauan, dan Inspeksi yang diperlukan serta kriteria penerimaan produk yang pengaturannya didokumentasikan pada prosedur dan petunjuk kerja yang terkait.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.1.4</td>
						<td colspan="2">Rekaman yang dibutuhkan untuk menunjukkan bukti bahwa proses realisasi dan hasil produk memenuhi persyaratan</td>
					</tr>
					
					<tr class="sub-bab" >
						<td colspan="">7.2</td>
						<td colspan="3">PROSES BERKAITAN DENGAN PELANGGAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.2.1</td>
						<td colspan="2">Persyaratan yang berkaitan dengan Produk.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> mengenali dan menetapkan persyaratan yang terkait dengan Produk / Proyek dengan cara :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Mempelajari persyaratan yang tertuang dalam Dokumen Tender dan Kontrak.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Mempelajari dokumen rencana kerja dan syarat-syarat.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Mempelajari persyaratan lain yang terkait dengan proyek tersebut.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Bagian Pemasaran dan Teknik bertanggung jawab mempelajari dan menetapkan persyaratan yang berkaitan dengan produk.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.2.2</td>
						<td colspan="2">Tinjauan Persyaratan yang berkaitan dengan Produk</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> meninjau persyaratan yang berkaitan dengan Produk / Proyek untuk mengetahui kemampuan perusahaan dalam memenuhi persyaratan tersebut. Tinjauan ini	dilakukan oleh pihak yang berkompeten sesuai bidangnya. Seperti bagian Pemasaran, Teknik, dan Manajemen. Keputusan dari hasil tinjauan dituangkan dalam dokumen yang selanjutnya menjadi rekaman yang harus disimpan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.2.3</td>
						<td colspan="2">Komunikasi dengan Pelanggan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> menentukan dan melaksanakan komunikasi dengan pelanggan yang pelaksanaannya  dilakukan  oleh  bagian  yang  relevan  seperti  pemasaran  dan  manajemen proyek, berkaitan dengan :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Informasi mengenai Produk / Proyek</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Permintaan, kontrak atau order, termasuk perubahannya;</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Umpan balik dan keluhan pelanggan.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">7.3</td>
						<td colspan="3">PERANCANGAN DAN PENGEMBANGAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> tidak melaksanakan proses perancangan dan pengembangan, karena semua desain disediakan oleh pelanggan.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">7.4</td>
						<td colspan="3">PEMBELIAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.4.1</td>
						<td colspan="2">Proses Pembelian</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> mengendalikan proses pembelian dan mengendalikan pemasok sesuai dengan status produk yang dibeli, terutama barang yang digunakan untuk pelaksanaan proyek. Kriteria untuk memilih dan mengevaluasi pemasok ditetapkan. Bukti Pemilihan dan Evaluasi Pemasok disimpan sebagai rekaman.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.4.2</td>
						<td colspan="2">Bagian Logistik bertanggung jawab untuk memastikan kebutuhan material untuk pelaksanaan proyek terpenuhi dan barang yang dibeli memenuhi persyaratan yang ditentukan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.4.3</td>
						<td colspan="2">Informasi Pembelian</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Setiap pembelian yang dilakukan oleh <b><?php echo $project[0]->company_name ?></b> disertai informasi tentang persyaratan  pembelian,  antara  lain  menyangkutspesifikasi  produk  yang  dibeli,  waktu pengiriman, tempat pengiriman, dan persyaratan lain yang spesifik.
							<br />
							Bagian Logistik bertanggung jawab melaksanakan Pembelian sesuai kewenangannya, dan harus mencatat semua pembelian yang dilakukan. Dokumen Pembelian menjadi rekaman yang dikendalikan.
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.4.4</td>
						<td colspan="2">Verifikasi Produk yang dibeli</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> melaksanakan Inspeksi terhadap barang yang dibeli untuk memastikan memenuhi persyaratan pembelian.
							<br />
							Bagian Logistik dan bagian yang terkait bertanggung jawab melaksanakan inspeksi terhadap barang yang dibeli sesuai dokumen pembelian dan syarat lain yang ditentukan.
						</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">7.5</td>
						<td colspan="3">PENYEDIAAN PROSES PRODUKSI DAN PELAYANAN PENDUKUNG</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.5.1</td>
						<td colspan="2">Pengendalian Produksi dan Pelayanan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> merencanakan dan melaksanakan proyek konstruksi agar selalu dalam keadaan terkendali. Ketentuan yang diatur tersebut meliputi :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Ketersediaan Informasi yang menjelaskan karakteristik produk, seperti Spesifikasi Produk, Drawing, dll.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Ketersediaan instruksi kerja yang diperlukan untuk realisasi produk, sesuai dengan proses masing-masing</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Penggunaan peralatan yang sesuai</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Ketersediaan dan penggunaan peralatan pengukuran dan pengujian</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Pelaksanaan pengiriman produk dan pasca pengiriman.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen Proyek bertanggung jawab dalam Pengendalian Proyek yang di tangani.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.5.2</td>
						<td colspan="2">Validasi Proses</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> melaksanakan validasi terhadap proses konstruksi yang tidak dapat diperiksa hasilnya melalui serangkaian  pemantauan  dan pengukuran selama prosesnya berlangsung. Validasi dilakukan antara melalui  :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Persetujuan terhadap Proses yang akan dilaksanakan,</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Persetujuan terhadap Material dan Peralatan yang akan digunakan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Persetujuan terhadap metoda/prosedur dan kualifikasi personil,</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Pemeriksaan akhir atas hasil proses yang telah dilaksanakan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Rekaman atas validasi proses konstruksi tertentu harus disimpan <i>(seperti izin kerja, hasil pengujian, dll.)</i></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.5.3</td>
						<td colspan="2">Identifikasi dan Mampu Telusur</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> mengidentifikasi Produk Konstruksi yang dihasilkan melalui Gambar Jadi. Gambar dibuat setelah dilaksanakan pemeriksaan produk konstruksi dan produk  konstruksi  dinyatakan  telah  memenuhi  persyaratan.  Identifikasi  juga  dapat dilakukan secara fisik dengan memberi  tanda pada produk konstruksi bila dimungkinkan. Selain itu dokumentasi foto dan laporan pelaksanaan  proyek dapat dijadikan acuan untuk identifikasi produk konstruksi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.5.4</td>
						<td colspan="2">Barang Milik Pelanggan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> menjaga dan merawat barang milik pelanggan yang digunakan untuk pelaksanaan Proyek Konstruksi seperti Barang, Peralatan, dll. yang disediakan oleh Pelanggan. Bagian Logistik dan bagian  lain  yang terkait bertanggung jawab menangani barang milik pelanggan, termasuk melaporkannya bila  ditemukan hilang, rusak atau tidak layak pakai. Laporan tersebut selanjutnya menjadi Rekaman yang harus disimpan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.5.5</td>
						<td colspan="2">Pemeliharaan Produk</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> akan menjaga dan memelihara Produk Konstruksi yang dihasilkan sebelum diserahterimakan kepada pelanggan. Pemeliharaan  produk konstruksi  dilaksanakan  sesuai  ketentuan  yang  termuat  dalam  persyaratan  kontrak  atau lainnya.
							<br />
							Manajemen Proyek Bertanggung Jawab melaksanakan <i>Kegiatan Pemeliharaan dan Serah Terima Produk Konstruksi.</i>
						</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">7.6</td>
						<td colspan="3">PENGENDALIAN SARANA PEMANTAUAN DAN PENGUKURAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> mengendalikan alat ukur yang digunakan untuk kegiatan pemeriksaan dan pengukuran. Alat ukur yang ketelitian hasil pembacaannya diperlukan untuk keabsahan hasil pengukuran maka harus dikalibrasi, sementara Alat Ukur yang ketelitian pembacaannya tidak diperlukan harus diverifikasi.
							<br />
							<br />
							Bagian Teknik bertanggung jawab mengidentifikasi semua Alat Ukur dan memonitor Status Alat Ukur tersebut, termasuk status kalibrasinya.
						</td>
					</tr>
									
				</tbody>
			</table>
		</div>
</body>
</html>