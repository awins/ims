<?php 
	foreach ($project as $value) {
		if ($value->iso_type == 1){
			$date = $value->main_assessment_date;
			break;
		}
	}

	$time = strtotime($date . '-4 month');

?>

<div style="font-family: andalus;font-size: 11pt;color: rgb(0, 112, 192) ">
	<div style="border: 1px solid  rgb(0, 176, 240);width:300pt;float: left;text-align: center;font-size: 14pt;height: 40.5pt;vertical-align: middle;line-height: 30pt" ><?php echo $project[0]->company_name ?></div>
	<div style="float: left;height: 30pt">
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;width:65pt;float: left;padding-left: 3px" >No. Dok</div>
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;width:150pt;float: left;padding-left: 3px" ><?php echo 'MM/' . $project[0]->company_initial .'-QMS/01' ?></div>
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;border-top: 0;width:65pt;float: left;padding-left: 3px" >Revisi</div>
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;border-top: 0;width:150pt;float: left;padding-left: 3px" >00</div>
	</div>
</div>

<div style="font-family: andalus;font-size: 11pt;color: rgb(0, 112, 192) ">
	<div style="border: 1px solid  rgb(0, 176, 240);width:300pt;float: left;text-align: center;font-size: 14pt;height: 40.5pt;vertical-align: middle;line-height: 30pt" >MANUAL MUTU ISO 9001:2008</div>
	<div style="float: left;height: 30pt">
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;width:65pt;float: left;padding-left: 3px" >Tanggal</div>
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;width:150pt;float: left;padding-left: 3px" ><?php echo indDate($time) ?></div>
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;border-top: 0;width:65pt;float: left;padding-left: 3px" >Halaman</div>
		<div style="border: 1px solid  rgb(0, 176, 240);border-left: 0;border-top: 0;width:150pt;float: left;padding-left: 3px" >{PAGENO} dari {nbpg}</div>
	</div>
</div>
<div style="width: 550pt;height: 5pt;" >
</div>

<div style="border: 1px solid rgb(0,176, 240);width: 521pt;height: 712pt;" >
</div>

