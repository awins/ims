<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB II</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>ORGANISASI PERUSAHAAN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 30pt"></td>
						<td style="width: 375pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">2.1</td>
						<td colspan="3">PROFIL PERUSAHAAN</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">2.1.1</td>
						<td colspan="2">Profil Perusahaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> adalah perusahaan yang bergerak dalam bidang <i>“ <?php echo $project[0]->company_scope ?> ”</i>.</td>
					</tr>
						<?php
						if (!is_null($project[0]->company_document)){
							$arr_document = explode('*', $project[0]->company_document );
							for ($doc=1; $doc < count($arr_document) ; $doc++) { ?>
								<tr class="" >
									<td colspan=""></td>
									<td colspan=""></td>
									<td colspan="" style="text-align: right" ><?php echo $doc ?>.</td>
									<td colspan=""><?php echo $arr_document[$doc] ?></td>
								</tr>
								<?php 
							} 
						} ?>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">2.1.2</td>
						<td colspan="2">Lingkup Usaha (Core Business)</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">2.2</td>
						<td colspan="3">STRUKTUR ORGANISASI</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">2.2.1</td>
						<td colspan="2">Struktur Organisasi Perusahaan <b><?php echo $project[0]->company_name ?></b></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">
							<?php 
							$company_id =  $project[0]->company_id;
		    				$img_file = '/application/public/img/organitation_structure/' . $company_id .'.png';
		    				if (!file_exists( $_SERVER['DOCUMENT_ROOT'] . '/application/public/img/organitation_structure/' . $company_id . '.png')){
		    					$img_file = '/application/public/img/organitation_structure/default.png';
		    				} ?>
							<img src="<?php echo $img_file ?>">
						</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">2.3</td>
						<td colspan="3">TANGGUNG JAWAB DAN WEWENANG</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Tanggung Jawab dan Wewenang dalam penerapan Sistem Manajemen Mutu ISO 9001:2008 dalam jajaran manajemen dan fungsi-fungsi di organisasi <b><?php echo $project[0]->company_name ?></b>  dituangkan dalam job description pada tiap-tiap divisi.</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	
</body>
</html>