<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}
	</style>
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB I</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>PENDAHULUAN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 40pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">1.1</td>
						<td colspan="3">LATAR BELAKANG</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3"><?php echo $project[0]->company_name ?> sangat peduli untuk menyesuaikan diri dalam Standarisasi Nasional dan Internasional, dan telah menerapkan Sistem Manajemen Mutu melalui Standard ISO 9001:2008 Sistem Manajemen Mutu, sebagai Dasar Penetapan Standarisasi Manajemen dan melengkapinya dengan ISO 14001:2004 Sistem Manajemen Lingkungan sebagai bagian dari keseluruhan Sistem Manajemen yang mempunyai Standar dalam membuat Kebijakan dan Tujuan Obyektif, sesuai Persyaratan Hukum serta dampak Lingkungan yang signifikan sehingga dapat mengidentifikasi, dan memahami dampak Negatif kegiatan Perusahaan terhadap Lingkungan.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="" >1.2</td>
						<td colspan="3" >PENGANTAR MANAJEMEN LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="3" >
							Sistem Manajemen Lingkungan adalah Bagian dari keseluruhan Sistem Manajemen yang meliputi Struktur Organisasi, Kegiatan Perencanaan, Tanggung Jawab, Pelaksanaan, Prosedur, Proses, dan Sumber Daya untuk mengembangkan, melaksanakan, meninjau, dan memelihara Kebijakan Lingkungan. 
							<br />
							Pedoman Lingkungan boleh diterbitkan baik sebagai salinan yang dikendalikan maupun yang tidak dikendalikan, seluruh salinan yang dikendalikan diberi Nomor Salinan dan dicatat dalam Daftar Pengendalian Distribusi Pedoman Lingkungan.
							<br />
							Pedoman Lingkungan ISO 14001 diterbitkan dengan urutan abjad A dan semua bagian Pedoman Lingkungan kembali ke Revisi 00. Pada setiap 1 (satu) bagian Pedoman Lingkungan yang telah direvisi 10 (sepuluh) kali harus diterbitkan kembali dan diidentifikasikan dalam urutan abjad, misal terbitan C menggantikan terbitan B. Dalam hal terbitan baru, seluruh bagian Pedoman Lingkungan kembali ke Revisi 00.
							<br />
							Tinjauan Tahunan Pedoman Lingkungan akan dilakukan pada Rapat Tinjauan Manajemen dari Sistem Manajemen Lingkungan yang diberlakukan. Meskipun demikian, tinjauan Pedoman Lingkungan yang terjadwal dapat dilakukan sebagaimana perlu dan apabila dianggap layak oleh Wakil Manajemen.
							<br />
							Pedoman Lingkungan yang dibagikan kepada Pelanggan atau Organisasi Eksternal, merupakan Dokumen yang diberi status: Tidak Dikendalikan. Penerbitan dan pendistribusian Pedoman Lingkungan ini merupakan tanggung jawab Wakil Manajemen dibantu Pengendali Dokumen yang menyimpan catatan tentang hal tersebut.

						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	
</body>
</html>