<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}
	</style>
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB III</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>LANDASAN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 40pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="" >3.1</td>
						<td colspan="3" >REFERENSI NORMATIF</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.1.1</td>
						<td colspan="2" >Pepres (Peraturan Presiden) No.28 tentang Standarisasi.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.1.2</td>
						<td colspan="2" >SNI-19-14001-1997 tentang Pengertian Manajemen Lingkungan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.1.3</td>
						<td colspan="2" >ISO 14001:2004 tentang Sistem Manajemen Lingkungan / Environmental Management System (EMS).</td>
					</tr>
					
					<tr class="sub-bab" >
						<td colspan="" >3.2</td>
						<td colspan="3" >ISTILAH DAN DEFINISI</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.1</td>
						<td colspan="2" >ISO 9001: 2008 Sistem Manajemen Mutu (SMM) - Dasar dan Kosa Kata. </td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.2</td>
						<td colspan="2" >ASPEK LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Pandangan terhadap suatu Peristiwa Kegiatan Perusahaan dari Awal hingga Akhir yang dihasilkan dan berhubungan dengan lingkungan.</i></td>
					</tr>

					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.3</td>
						<td colspan="2" >KEBIJAKAN LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Persyaratan Perusahaan tentang Prinsip dan Asas, berkaitan dengan Kerja Lingkungan secara Keseluruhan yang memberikan Kerangka Acuan untuk Tindakan dan Penetapan Tujuan serta Sasaran Lingkungan.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.4</td>
						<td colspan="2" >TUJUAN LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Arah dari Kebijakan Lingkungan yang menjadi Harapan dan Cita - Cita Perusahaan untuk mencapai Dampak Lingkungan.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.5</td>
						<td colspan="2" >SASARAN LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Persyaratan Kinerja tentang Lingkungan Perusahaan secara rinci, dan dikualifikasikan serta ditentukan untuk mendukung terpenuhinya Tujuan Lingkungan.</i></td>
						
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.6</td>
						<td colspan="2" >DAMPAK LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Adalah semua Perubahan yang terjadi di Lingkungan Perusahaan, baik yang merugikan maupun menguntungkan, sebagian atau keseluruhan, sebagai efek dari Aspek lingkungan yang dihasilkan Perusahaan.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.7</td>
						<td colspan="2" >KINERJA LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Hasil Sistem Manajemen Lingkungan perusahaan yang Terukur, berhubungan dengan Pengendalian Perusahaan mengenai Aspek Lingkungannya, berdasarkan Kebijakan Lingkungan, Tujuan dan Sasaran Lingkungan.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.8</td>
						<td colspan="2" >AUDIT SISTEM MANAJEMEN LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Proses Verifikasi yang Sistematis dan Terdokumentasi dalam mendapatkan dan mengevaluasi bukti-bukti secara Obyektif untuk menentukan apakah Sistem Manajemen Lingkungan perusahaan sesuai dengan kriteria audit yang ditentukan oleh perusahaan dan untuk mengkomunikasikan hasil proses tersebut ke Manajemen.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.9</td>
						<td colspan="2" >PENCEGAHAN PENCEMARAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Penerapan Proses, Pelaksanaan, Bahan - Bahan atau Produk - Produk untuk menghindari, mengurangi atau mengendalikan pencemaran meliputi di dalamnya Daur Ulang, Pengolahan, Perubahan Proses, Mekanisme Kontrol, Penggunaan Sumber Daya secara Efisien, dan Penggantian Bahan.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.10</td>
						<td colspan="2" >PERBAIKAN BERKESINAMBUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Proses peningkatan Sistem Manajemen Lingkungan untuk mencapai Perbaikan Kinerja Lingkungan keseluruhan yang sejalan dengan Kebijakan Lingkungan Perusahaan.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.11</td>
						<td colspan="2" >SISTEM MANAJEMEN LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Bagian dari keseluruhan Sistem Manajemen yang meliputi Struktur Organisasi, Kegiatan Perencanaan, Tanggung Jawab, Pelaksanaan, Prosedur, Proses, dan Sumber Daya untuk mengembangkan, melaksanakan, meninjau, dan memelihara Kebijakan Lingkungan.</i></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >3.2.12</td>
						<td colspan="2" >WAKIL MANAJEMEN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><i>Personil yang ditunjuk oleh Manajemen untuk mewakili perusahaan dalam bidang penanganan dan peningkatan Sistem Manajemen Lingkungan perusahaan.</i></td>
					</tr>
					
				</tbody>
			</table>
		</div>
	
</body>
</html>