<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		body{
			font-size: 10pt;
		}

		table{
			border-collapse: collapse;
			border: 0;
		}

		#table-daftar-isi{
		}
		#table-daftar-isi tr th, #table-daftar-isi tr td {
			font-size: 10pt;
			text-align: center;
			padding-top:10px; 
			padding-bottom:10px; 
		}

		#table-daftar-isi tr th {
			font-weight: bold;
		}

		
	</style>
</head>
<body>
	<div style="width: 650px;margin: auto;font-size: 14pt; font-weight: bold;text-align: center;padding:  10px 0 15px;">
		DAFTAR DISTRIBUSI DOKUMEN
	</div>
	<div style="width: 650px;margin: auto;">
		<table border="1" id="table-daftar-isi" style=""  border="1" >
			<thead>
				<tr style="">
					<th style="width: 80px;height: 50px">NO</th>
					<th style="width: 300px;">PEMEGANG DOKUMEN</th>
					<th style="width: 160px;">STATUS DOKUMEN</th>
					<th style="width: 120px;">NO SALINAN</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td style="text-align: left;padding-left: 5px ">
						PERWAKILAN MANAJEMEN
						<br />
						<i>(Management Representative)</i>
					</td>
					<td>MASTER</td>
					<td>-</td>
				</tr>	
				<tr>
					<td>2</td>
					<td style="text-align: left;padding-left: 5px ">
						PENGENDALI DOKUMEN
						<br />
						<i>(Document Control)</i>
					</td>
					<td>Copy Terkendali</td>
					<td>01</td>
				</tr>
				<tr>
					<td>3</td>
					<td style="text-align: left;padding-left: 5px ">
						DIREKTUR UTAMA
						<br />
						<i>(President Director)</i>
					</td>
					<td>Copy Terkendali</td>
					<td>02</td>
				</tr>		
				<tr>
					<td>4</td>
					<td style="text-align: left;padding-left: 5px ">
						MANAGER ADM. Umum
						<br />
						<i>(Administration Manager)</i>
					</td>
					<td>Copy Terkendali</td>
					<td>03</td>
				</tr>		
				<tr>
					<td>5</td>
					<td style="text-align: left;padding-left: 5px ">
						MANAGER GUDANG
						<br />
						<i>(Purchasing &amp; Logistic Manager)</i>
					</td>
					<td>Copy Terkendali</td>
					<td>04</td>
				</tr>		
				<tr>
					<td>6</td>
					<td style="text-align: left;padding-left: 5px ">
						MANAGER ENGINEER
						<br />
						<i>(Engineer Manager)</i>
					</td>
					<td>Copy Terkendali</td>
					<td>05</td>
				</tr>		
				<tr>
					<td>7</td>
					<td style="text-align: left;padding-left: 5px ">
						MANAGER HRD
						<br />
						<i>(HRD Manager)</i>
					</td>
					<td>Copy Terkendali</td>
					<td>06</td>
				</tr>		
			</tbody>
		</table>
	</div>
	
</body>
</html>