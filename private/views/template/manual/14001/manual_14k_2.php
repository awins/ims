<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		body{
			font-size: 10pt;
		}

		table{
			border-collapse: collapse;
			border: 0;
		}

		#table-daftar-isi{
		}

		#table-daftar-isi tr th {
			font-size: 12pt;
			font-weight: bold;
		}

		#table-daftar-isi tr td {
			text-align: center;
			line-height: 30px;
			border: 0;
			border-left: 1px solid black;
		}
		
		#table-daftar-isi tr.bottom td {
			border-bottom: 1px solid black;
		}
		#table-daftar-isi td.right {
			border-right: 1px solid black;
		}

		#table-daftar-isi tr.odd td {
			background-color: rgb(219,229,241);
		}

		#table-daftar-isi tr.even td {
			background-color: rgb(234,241,221);
		}


		#table-daftar-isi tr td.keterangan {
			text-align: left;
			padding-left: 5px;
		}
	</style>
</head>
<body>
	<div style="width: 650px;margin: auto;font-size: 14pt; font-weight: bold;text-align: center;padding:  10px 0 15px;">
		DAFTAR ISI
	</div>
	<div style="width: 650px;margin: auto;">
		<table border="1" id="table-daftar-isi" style=""  border="0" >
			<thead>
				<tr style="heigth: 0">
					<th style="width: 80px;">NO</th>
					<th style="width: 420px;">KETERANGAN JUDUL</th>
					<th style="width: 80px;">REVISI</th>
					<th style="width: 80px;">HAL</th>
				</tr>
			</thead>
			<tbody>
				<tr class="odd">
					<td style="" >
						A	
						<br />
						B
						<br />
						C	
						<br />
						D	
					</td>
					<td class="keterangan">
						LEMBAR PENGESAHAN
						<br />
						DAFTAR ISI
						<br />
						LEMBAR PERUBAHAAN
						<br />
						DAFTAR DISTRIBUSI DOKUMEN
					</td>
					<td>
						00
						<br />
						00
						<br />
						00
						<br />
						00
					</td>
					<td class="right">
						01
						<br />
						02
						<br />
						03
						<br />
						04
					</td>
				</tr>
				<tr  class="even">
					<td>
						BAB I
						<br />
						1.1	
						<br />
						1.2	
					</td>
					<td class="keterangan">
						PENDAHULUAN		
						<br />
						LATAR BELAKANG
						<br />
						PENGANTAR MANAJEMEN LINGKUNGAN
					</td>
					<td>
						<br />
						00
						<br />
						00
					</td>
					<td class="right">
						<br />
						05
						<br />
						05
					</td>
				</tr>
				<tr  class="odd">
					<td>
						BAB II
						<br />
						2.1	
					</td>
					<td class="keterangan">
						PROFIL PERUSAHAAN 		
						<br />
						SEJARAH PERUSAHAAN
					</td>
					<td>
						<br />
						00
					</td>
					<td class="right">
						<br />
						06
					</td>
				</tr>
				<tr  class="even">
					<td>
						BAB III
						<br />
						3.1	
						<br />
						3.2	
					</td>
					<td class="keterangan">
						LANDASAN 		
						<br />
						REFERENSI
						<br />
						ISTILAH DAN DEFINISI
					</td>
					<td>
						<br />
						00
						<br />
						00
					</td>
					<td class="right">
						<br />
						07
						<br />
						07
					</td>
				</tr>
				<tr class="bottom odd">
					<td>
						BAB IV
						<br />
						4.1	
						<br />
						4.2	
						<br />
						4.3	
						<br />
						4.4	
						<br />
						4.5	
						<br />
						4.6	
					</td>
					<td class="keterangan">
						SISTEM MANAJEMEN LINGKUNGAN
						<br />
						PERSYARATAN UMUM
						<br />
						KEBIJAKAN LINGKUNGAN
						<br />
						PERENCANAAN SISTEM MANAJEMEN LINGKUNGAN
						<br />
						PENERAPAN DAN OPERASIONAL
						<br />
						PEMERIKSAAN DAN TINDAKAN PERBAIKAN
						<br />
						TINJAUAN MANAJEMEN
					</td>
					<td>
						<br />
						00
						<br />
						00
						<br />
						00
						<br />
						00
						<br />
						00
						<br />
						00
					</td>
					<td class="right">
						<br />
						09
						<br />
						09
						<br />
						10
						<br />
						10
						<br />
						15
						<br />
						16
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</body>
</html>