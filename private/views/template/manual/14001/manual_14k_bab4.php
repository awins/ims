<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		table#table-bingkai tr td {
			text-align: justify;
			vertical-align: top;
			line-height: 14pt;
		}
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}

		#table-manual tr.height td{
			/*padding-top: 20px 0 15px;*/
		}

	</style>
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB IV</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>SISTEM MANAJEMEN LINGKUNGAN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 40pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="" >4.1</td>
						<td colspan="3" >PERSYARATAN UMUM</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="3" >Standar ISO 14001 adalah standar Sistem Manajemen Lingkungan dan bukan merupakan Standar Keadaan Lingkungan. Standar ISO 14001 ini dapat dijabarkan menjadi lima prinsip pokok yaitu: <i>Penetapan, Pendokumentasian, Penerapan, Pemeliharaan, dan Peningkatan Berkesinambungan terhadap Sistem Manajemen Lingkungan ISO 14001:2004.</i></td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="" >4.2</td>
						<td colspan="3" >KEBIJAKAN LINGKUNGAN</td>
					</tr>
				</tbody>
			</table>
			<div  style="background-image: url('/application/public/img/element/bingkai.gif');width:432px;height: 505px;margin-left: 30px;padding:  60px 70px;  " >
				<div style="text-align: center;font-size: 14pt;font-weight: bold " >KEBIJAKAN LINGKUNGAN</div>
				<div style="text-align: center;font-size: 12pt;font-weight: bold " ><?php echo $project[0]->company_name ?></div>
				<div>
					<table id="table-bingkai"  >
						<thead>
							<tr>
								<th style="width: 15pt"></th>
								<th style=""></th>
							</tr>
						</thead>
						<tbody>
							<tr class="" >
								<td colspan="2" >Komitmen kami adalah :</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Melindungi Karyawan dan Lingkungan sekitarnya terhadap Bahaya yang berkaitan dengan Kegiatan, Produk, dan Jasa Layanan Perusahaan, serta akibat yang ditimbulkannya.</td>
							</tr>
							<tr>
								<td>2.</td>
								<td>Melakukan Perbaikan Pengelolaan Lingkungan secara terus¬ menerus, mencegah terjadinya pencemaran lingkungan. dan berupaya meminimalkan limbah yang berpotensi merusak lingkungan.</td>
							</tr>
							<tr>
								<td>3.</td>
								<td>Mentaati Perundangan serta Persyaratan lainnya di bidang Lingkungan, Keselamatan, dan Kesehatan Kerja yang sesuai dengan Aspek Lingkungan Perusahaan.</td>
							</tr>
							<tr>
								<td>4.</td>
								<td>Melakukan efisiensi penggunaan Sumber Daya Alam maupun Energi yang digunakan.</td>
							</tr>
							<tr>
								<td>5.</td>
								<td>Membina Kepekaan, Kesadaran, dan Kepedulian seluruh Karyawan dan mengkomunikasikan Kebijakan Lingkungan kepada Pemasok serta Badan / Perseorangan yang bekerja di <?php echo $project[0]->company_name ?> agar ambil bagian di dalam Pengelolaan Lingkungan.</td>
							</tr>
							<tr>
								<td colspan="2">Kebijakan Lingkungan ini didokumentasikan, diterapkan, dan dipelihara sesuai dengan persyaratan ISO 14001:2004.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div style="text-align: center;font-size: 14pt;font-weight: bold " >DIREKTUR UTAMA</div>
				<div style="text-align: center;font-size: 12pt;font-weight: bold " ><?php echo $project[0]->company_name ?></div>
			</div>
			<div>&nbsp;</div>
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 30pt"></td>
						<td style="width: 375pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="" >4.3</td>
						<td colspan="3" >PERENCANAAN SISTEM MANAJEMEN LINGKUNGAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="3" >Perencanaan Sistem Manajemen Lingkungan bertujuan untuk memastikan bahwa kegiatan-kegiatan yang berdampak pada Lingkungan ditetapkan, diterapkan, dipelihara, dan ditingkatkan Perbaikannya. Pedoman, Prosedur, Daftar Aspek Penting Lingkungan serta Perundangan dan Persyaratan lainnya, serta ditinjau sekurang-kurangnya 1 (satu) tahun sekali oleh Wakil Manajemen.</td>
					</tr>
					<tr class="height" >
						<td colspan="" ></td>
						<td colspan="" >4.3.1</td>
						<td colspan="2" >Aspek Lingkungan</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> Menetapkan, Menerapkan, dan Memelihara Prosedur untuk mengidentifikasi dan mengevaluasi Aspek Lingkungan dari Kegiatan, Produk maupun Jasa Layanan sehingga dapat di kontrol dan diketahui Dampak Lingkungannya.
							<br />
							Identifikasi dan Evaluasi Aspek Lingkungan mencakup Aktivitas yang direncanakan, Pengembangan Baru, Aktivitas Baru, atau Aktivitas yang dimodifikasi, Produk dan Jasa. Daftar Aspek Lingkungan selalu diperbaharui dan dilakukan peninjauan minimal 1 (satu) kali oleh Wakil Manajemen (MR).
						</td>
					</tr>
					<tr class="height" >
						<td colspan="" ></td>
						<td colspan="" >4.3.2</td>
						<td colspan="2" >Perundangan dan Persyaratan Lainnya</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan, dan memelihara suatu Prosedur untuk mengidentifikasi Perundangan dan Persyaratan Iainnya di bidang Lingkungan, Keselamatan, dan Kesehatan Kerja yang sesuai dengan Aspek Lingkungan Perusahaan.</td>
					</tr>
					<tr class="height" >
						<td colspan="" ></td>
						<td colspan="" >4.3.3</td>
						<td colspan="2" >Tujuan dan Sasaran-Lingkungan</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan, serta memelihara Tujuan, Sasaran, dan Program Lingkungan yang dapat diukur dan diterapkan secara konsisten sesuai dengan Kebijakan Lingkungan.
							<br />
							Tujuan dan Sasaran Lingkungan mempertimbangkan Persyaratan Hukum dan Perundangan yang diterapkan, Aspek Lingkungan, Pilihan Teknologi, Persyaratan Bisnis dan Operasional serta Kondisi Keuangan.
						</td>
					</tr>
					<tr class="height" >
						<td colspan="" ></td>
						<td colspan="" >4.3.4</td>
						<td colspan="2" >Program Manajemen Lingkungan</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" >Seiring perkembangan jaman, Perusahaan menerapkan Prinsip Bekerja yang Aman, Sehat dan Ramah Lingkungan baik terhadap para Pelanggan, Karyawan dan Masyarakat sekitarnya. Untuk itu <?php echo $project[0]->company_name ?> berketetapan untuk menetapkan Kebijakan dan mengimplementasikan Standar Manajemen Lingkungan berstandar Internasional ISO 14001:2004 yang akan dituangkan dalam Manual ini.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="" >4.4</td>
						<td colspan="3" >PENERAPAN DAN OPERASIONAL</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.4.1</td>
						<td colspan="2" >Struktur dan Tanggung Jawab</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.1.1</td>
						<td colspan="" >Struktur Organisasi </td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="3" >
						<?php 
							$company_id =  $project[0]->company_id;
		    				$img_file = '/application/public/img/organitation_structure/' . $company_id .'.png';
		    				if (!file_exists( $_SERVER['DOCUMENT_ROOT'] . '/application/public/img/organitation_structure/' . $company_id . '.png')){
		    					$img_file = '/application/public/img/organitation_structure/default.png';
		    				} ?>
							<img src="<?php echo $img_file ?>">
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="3" style="text-align: center" ><b>Gb.1 STRUKTUR ORGANISASI</b></td>
					</tr>

					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.1.2</td>
						<td colspan="" >Tanggung Jawab</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ><?php echo $project[0]->company_name ?> menjamin tersedianya Sumber Daya untuk menetapkan, menerapkan, memelihara, dan meningkatkan Sistem Manajemen Lingkungan. Sumber daya tersebut mencakup Sumber Daya Manusia (SDM), SDM dengan Keahlian Khusus, Internal Infrastruktur, Teknologi dan Keuangan.
							<br />
							<?php echo $project[0]->company_name ?> menetapkan Peran, Tanggung Jawab dan Wewenang, mendokumentasikan, serta mengkomunikasikannya agar Sistem Manajemen Lingkungan dapat berjalan dengan efektif.
							<br />
							<?php echo $project[0]->company_name ?> menunjuk seseorang sebagai Wakil Manajemen (WM). yang mempunyai Peran, Tanggung Jawab, dan Wewenang untuk menjamin bahwa Sistem Manajemen Lingkungan ditetapkan, diterapkan, dan dipelihara sesuai dengan Persyaratan ISO.
							<br />
							Wakil Manajemen akan dibantu oleh Asistennya untuk melaporkan kinerja Sistem Manajemen Lingkungan termasuk rekomendasi perbaikan berkesinambungannya kepada Top Manajemen.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.4.2</td>
						<td colspan="2" >Kompentensi, Pelatihan, dan Kepedulian</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> harus melakukan Identifikasi Kompetensi bagi orang yang mengerjakan Tugas (Karyawan atau Orang yang Bekerja atas Nama Perusahaan) memiliki Potensi untuk menyebabkan Dampak penting Lingkungan. Kompetensi mempertimbangkan tingkat Pendidikan, Pelatihan dan Pengalamannya. Catatan mengenai ketiga hal terkait kompetensi tersebut harus dapat ditunjukkan.
							<br />
							Pelatihan merupakan salah satu cara untuk membina dan meningkatkan pengetahuan keterampilan serta kepedulian sumber daya manusia demi suksesnya penerapan Sistem Manajemen Lingkungan di <?php echo $project[0]->company_name ?>. Pelatihan Karyawan disusun dan dilaksanakan secara terencana atas Dasar Identifikasi kebutuhan Pelatihan Karyawan sesuai dengan Analisa Aspek dan Dampak Lingkungan
							<br />
							Untuk Jabatan tertentu setiap Personil diberi Pelatihan khusus sesuai bidangnya. Evaluasi Hasil Pelatihan dipantau dari Hasil Kerja dan Efektifitas dari Pelatihan yang diberikan dengan melihat sejauh mana Sasaran Manajemen Lingkungan dapat dicapai.
							<br />
							<?php echo $project[0]->company_name ?> harus menetapkan, menerapkan, dan memelihara Prosedur agar Karyawan atau orang yang bekerja atas Nama Perusahaan peduli terhadap Sistem Manajemen Lingkungan, sehingga kepedulian Karyawan terhadap Aspek Penting Lingkungan yang terkait dengan Pekerjaannya meningkat.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.4.3</td>
						<td colspan="2" >Komunikasi</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.3.1</td>
						<td colspan="" >Komunikasi Internal</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan, dan memelihara Prosedur mengenai Komunikasi Internal antara berbagai Fungsi dan Bagian di Lingkungan <?php echo $project[0]->company_name ?>.
							<br />
							Wakil Manajemen Bertanggung Jawab mengenai pengkomunikasian hal¬-hal yang berkenaan dengan Sistem Manajemen Lingkungan kepada Manajemen Puncak dan kepada personil yang terkait dengan penerapan Sistem Manajemen Lingkungan di Lingkungan <?php echo $project[0]->company_name ?>.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.3.2</td>
						<td colspan="" >Komunikasi Eksternal</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan, dan memelihara prosedur untuk menerima, mendokumentasikan, dan menanggapi komunikasi dari pihak eksternal, termasuk di dalamnya mendokumentasikan suatu keputusan dalam hal mengkomunikasikan aspek penting Lingkungan kepada pihak luar.
							<br />
							<?php echo $project[0]->company_name ?> menetapkan komunikasi kepada pihak luar yang terkait dengan aspek penting Lingkungan, serta memutuskan metoda untuk mengkomunikasikannya.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.4.4</td>
						<td colspan="2" >Persyaratan Dokumentasi</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" >Sistem Manajemen Lingkungan yang digunakan di <?php echo $project[0]->company_name ?> mengikuti standard sistem ISO - 14001. Untuk panduan penerapan Sistem Manajemen Lingkungan ini, dokumentasi Sistem Manajemen Lingkungan diklasifikasikan menjadi 4 (empat) level dokumen yang saling terkait dan mempunyai referensi silang antara satu dengan yang lain , yaitu :</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.4.1</td>
						<td colspan="" >Dokumen Level 1 (satu)</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >Yaitu Pedoman Manajemen Lingkungan yang berisikan penjelasan tentang kebijakan perusahaan dalam penerapan Sistem Manajemen Lingkungan ISO 14001. Pengelolaan Pedoman Manajemen Lingkungan menjadi tanggung jawab Wakil Manajemen.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.4.2</td>
						<td colspan="" >Dokumen Level 2 (dua)</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >Yaitu Prosedur Sistem Manajemen Lingkungan yang menjelaskan tentang siapa mengerjakan apa, kapan waktu pelaksanaan, dimana dikerjakan dan bagaimana pekerjaan dilakukan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.4.3</td>
						<td colspan="" >Dokumen Level 3 (tiga)</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >Yaitu Dokumen yang digunakan dalam Operasi Sistem Manajemen Lingkungan yang berupa Instruksi Kerja yang merupakan Tahapan dan Spesifik dari suatu Kegiatan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.4.4</td>
						<td colspan="" >Dokumen Level 4 (empat)</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >Yaitu Rekaman atau Form yang digunakan dalam Operasi Sistem Manajemen Lingkungan untuk melakukan Evaluasi terhadap pelaksanaan prosedur dan instruksi kerja dari suatu kegiatan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.4.5</td>
						<td colspan="2" >Pengendalian Dokumen</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.5.1</td>
						<td colspan="" >Dokumen yang diperlukan untuk Pencapaian Sistem Manajemen Lingkungan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.5.2</td>
						<td colspan="" >Dokumen yang diperlukan untuk Efektifitas Operasi Sistem Manajemen Lingkungan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.5.3</td>
						<td colspan="" >Penerbitan Dokumen.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >Wakil Manajemen Bertanggung Jawab mengendalikan Penerbitan Dokumen untuk memastikan semua dokumen teridentifikasi dan tercatat. Bagian penerbit dokumen bertanggung jawab dalam mengelola dan memelihara semua, dokumen sistem manajemen Lingkungan yang telah didistribusikan yaitu dokumen yang menjadi tanggung jawabnya.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.5.4</td>
						<td colspan="" >Pengesahan Dokumen</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >Sebelum Dokumen diterbitkan, Pejabat yang berwenang untuk mengesahkan dan menyetujui Penerbitan, harus melakukan Tinjauan Ulang untuk memastikan isi dan substansi serta persyaratan untuk Penerbitan Dokumen telah dipenuhi dan telah sesuai dengan ketentuan yang ditetapkan pada prosedur Pengendalian Dokumen.
							<br />
							Daftar Induk Dokumen digunakan sebagai Alat untuk Identifikasi dan berfungsi sebagai Alat untuk memastikan hanya Dokumen yang sah yang beredar dan digunakan sebagai Acuan.
							<br />
							Pengendali Dokumen menyimpan seluruh Daftar Induk Dokumen yang sah di Pusat Dokumen. Masing-masing Kepala Bagian atau Kepala Seksi memegang Daftar Induk Dokumen untuk sitenya masing-masing dan bagian Penerbit Dokumen memegang Salinan Daftar Induk Dokumen yang dikeluarkannya
							<br />
							Semua dokumen Sistem Manajemen Lingkungan yang Asli disimpan di Pusat Dokumen dan dikelola oleh Pengendali Dokumen. Semua Kepala Bagian atau Kepala Seksi menyimpan salinan dan memegang Dokumen yang diberi status: <b>Dikendalikan</b>.
							<br />
							Dokumen Kadaluarsa tidak boleh digunakan sebagai Acuan Kerja. Mekanisme Pengelolaan dan Penyimpanan serta Pemusnahan Kadaluarsa diatur dalam Prosedur yang telah ditetapkan.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.5.5</td>
						<td colspan="" >Perubahan Dokumen</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >Penanggung Jawab Penerbit Dokumen harus melakukan penyesuaian terhadap adanya Perubahan Proses, Teknologi, Struktur Organisasi atau Pengembangan Perusahaan dan kegiatan lainnya yang menyebabkan perubahan pada Prosedur dan Dokumen Sistem Manajemen Lingkungan. Perubahan atau Revisi terhadap Dokumen dilakukan dengan Mekanisme yang diatur dalam Prosedur.
							<br />
							Tinjauan Ulang Dokumen Sistem Manajemen Lingkungan dilakukan sesuai dengan kebutuhan dan minimal dilakukan 1 (satu) tahun sekali. Prosedur rinci yang mengatur Mekanisme Pengendalian Dokumen diatur di dalam Prosedur Pengendalian Dokumen.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.4.6</td>
						<td colspan="2" >Pengendalian Operasional</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> mengidentifikasi dan merencanakan operasi dan kegiatan yang berkaitan dengan aspek penting lingkungan untuk menjamin kegiatan tersebut dilaksanakan pada kondisi tertentu dengan :</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.6.1</td>
						<td colspan="" >Menetapkan, Menerapkan, dan Memelihara Prosedur yang terdokumentasi. </td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.6.2</td>
						<td colspan="" >Menetapkan Kriteria Operasi dalam Prosedur.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="" >4.4.6.3</td>
						<td colspan="" >Mengkomunikasikan Prosedur dan Persyaratan yang berlaku kepada supplier dan kontraktor dan atau orang yang bekerja atas nama <?php echo $project[0]->company_name ?>. </td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.4.7</td>
						<td colspan="2" >Kesiagaan dan Tanggap Darurat</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan, dan memelihara Prosedur untuk mengidentifikasi Potensi Keadaan Bahaya-dan Potensi Kecelakaan yang dapat mempengaruhi Lingkungan, seperti kebakaran serta akibat yang ditimbulkannya.
							<br />
							Secara periodik <?php echo $project[0]->company_name ?> meninjau dan bilamana diperlukan merevisi Prosedur Kesiapsiagaan dan Tanggap Darurat, khususnya setelah terjadinya kecelakaan lingkungan atau keadaan darurat. Prosedur tersebut diuji secara berkala bilamana memungkinkan.
						</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="" >4.5</td>
						<td colspan="3" >PEMERIKSAAN DAN TINDAKAN PERBAIKAN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.5.1</td>
						<td colspan="2" >Pemantauan dan Pengukuran</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan, dan memelihara Prosedur untuk memantau dan mengukur secara berkala karakteristik kunci dari kegiatan operasional perusahaan yang mempunyai dampak penting terhadap Lingkungan.
							<br />
							<?php echo $project[0]->company_name ?> menjamin bahwa Peralatan Pemantauan Lingkungan dikalibrasi / diverifikasi dan dipelihara guna memastikan ketelitian dan toleransi Hasil Inspeksi, Pengukuran dan Pengujian senantiasa sesuai dengan Persyaratan.
							<br />
							Pemantauan dan Pengukuran Karakteristik Kunci dari Kegiatan Operasional Perusahaan yang mempunyai Aspek penting terhadap Lingkungan dapat dilakukan oleh pihak luar yang ditunjuk. Penunjukan didasarkan pada Akreditas Badan yang bersangkutan.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.5.2</td>
						<td colspan="2" >Evaluasi Pemenuhan Kepatuhan</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> menetapkan, dan memelihara prosedur untuk mengevaluasi secara berkala kepatuhan terhadap perundangan dan persyaratan lainnya yang sesuai dengan aspek Lingkungan. Bukti Catatan atau Rekaman terkait dengan hasil evaluasi secara berkala tersebut dipelihara oleh <?php echo $project[0]->company_name ?>.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.5.3</td>
						<td colspan="2" >Ketidaksesuaian, Tindakan Perbaikan dan Pencegahan</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan dan memelihara prosedur untuk mengidentifikasi tanggung jawab dan wewenang untuk menangani dan menyelidiki ketidaksesuaian, mengambil tindakan untuk mengurangi dampak yang disebabkannya dan untuk mengawali dan menyelesaikan tindakan perbaikan dan pencegahan. Tindakan Pencegahan terhadap Potensi Ketidaksesuaian didokumentasi dan dievaluasi keefektifan Tindakan Perbaikan di atas.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.5.4</td>
						<td colspan="2" >Pengendalian Catatan Lingkungan</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> <i>Menetapkan, Menerapkan dan Memelihara Prosedur Pengendalian Rekaman / Catatan Lingkungan yang digunakan dalam Penerapan Sistem Manajemen Lingkungan yang meliputi Identifikasi, Pengumpulan, Penyimpanan, dan Pemeliharaan.</i>
							<br />
							Metode, Penanggung Jawab, Dokumentasi Rekaman / Catatan Manajemen Lingkungan dinyatakan pada Prosedur Pengendalian Catatan Manajemen Lingkungan.
						</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.5.5</td>
						<td colspan="2" >Audit Sistem Manajemen Lingkungan Internal</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" ><?php echo $project[0]->company_name ?> menetapkan, menerapkan dan memelihara prosedur audit internal untuk memastikan keefektifan Sistem Manajemen Lingkungan yang diterapkan, dilakukan Audit Sistem Manajemen Lingkungan internal secara periodik minimal 2 (dua) kali dalam setahun. Audit dilakukan oleh tim auditor yang independent yang ditunjuk oleh Wakil Manajemen dan auditor tidak boleh terlibat langsung dalam aktivitas yang di audit.
							<br />
							Audit Internal dilakukan berdasarkan Prosedur Audit Sistem Manajemen Lingkungan Internal dengan ketentuan yang telah ditetapkan dan dilaksanakan oleh auditor yang telah terlatih.
							<br />
							Hasil audit dicatat dan direkam berdasarkan Prosedur Audit Sistem Manajemen Lingkungan Internal dan didiskusikan dengan auditee (bagian yang diaudit) untuk dilaksanakan tindakan koreksi. Tindakan koreksi yang diambil dimonitor oleh Tim Auditor.
							<br />
							Wakil Manajemen melaporkan pelaksanaan hasil Audit Sistem Manajernen Lingkungan Internal kepada Top manajemen sebagai bahan masukan untuk Rapat Tinjauan Manajemen.
						</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="" >4.6	</td>
						<td colspan="3" >TINJAUAN MANAJEMEN</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="3" >Untuk kesinambungan terhadap kesesuaian dan efektifitas Sistem Manajemen Lingkungan, secara berkala dilakukan Peninjauan Ulang Sistem Manajemen Lingkungan melalui Rapat Tinjauan Manajemen. Rapat Tinjauan Manajemen dilaksanakan sesuai dengan prosedur dan dengan memperhatikan</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.1</td>
						<td colspan="2" >Tindak lanjut dari hasil Rapat Tinjauan Manajemen sebelumnya. </td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.2</td>
						<td colspan="2" >Hasil internal audit.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.3</td>
						<td colspan="2" >Kesesuaian terhadap perundangan dan persyaratan lainnya. </td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.4</td>
						<td colspan="2" >Komunikasi dengan pihak luar.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.5</td>
						<td colspan="2" >Kinerja Lingkungan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.6</td>
						<td colspan="2" >Keluhan lingkungan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.7</td>
						<td colspan="2" >Saran untuk peningkatan berkesinambungan.</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.8</td>
						<td colspan="2" >Tindak lanjut dari Permintaan Tindakan Koreksi dan Pencegahan (PTKP).</td>
					</tr>
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" >4.6.9</td>
						<td colspan="2" >Perubahan-perubahan yang mempengaruhi Sistem Manajemen Lingkungan untuk meninjau ulang Kebijakan Lingkungan di <?php echo $project[0]->company_name ?>.</td>
					</tr>
					
					<tr class="" >
						<td colspan="" ></td>
						<td colspan="" ></td>
						<td colspan="2" >Rapat Tinjauan Manajemen mengevaluasi efektifitas penerapan Sistem Manajemen Lingkungan, selanjutnya memutuskan tindakan selanjutnya untuk mengadakan perbaikan¬perbaikan pada. Ketidaksesuaian pelaksanaan Sistem Manajemen lingkungan yang berkaitan dengan ketentuan perundangan dan persyaratan lainnya, Program Pengelolaan Lingkungan dan Pengelolaan Sumber Daya serta Elemen lainnya.</td>
					</tr>
					
					
				</tbody>
			</table>
		</div>
	
</body>
</html>