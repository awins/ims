<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		body{
			font-size: 10pt;
		}
	</style>
</head>
<body>
	<?php 
	$time = strtotime($date . '-3 month');
	?>
	<div style="text-align: center;padding:0;font-size: 14pt">
		<b>LEMBAR PENGESAHAN</b>
	</div>
	<div style="width: 600px;margin: auto;">
		Manual Mutu ini diterbitkan sebagai Pedoman Pengoperasian <?php echo $project[0]->company_name ?> baik dari aspek Manajemen, Operasional, dan Pemasaran yang bergerak dan berkonsentrasi di bidang “ <?php echo $project[0]->company_scope ?> ”. berdasarkan Sistem Manajemen Lingkungan (SML) ISO 14001:2004.
		<br />
		<br />
		Manual Mutu ini bersifat dinamis mengikuti Standar, Regulasi ataupun Kode, sehingga diharapkan bahwa dalam Proses lmplementasinya memiliki tingkat Penyempurnaan yang akan mendorong lebih cepat tercapainya sasaran secara efektif.

	</div>

	<div style="width: 600px;margin: auto;">
		<table border="1" style="border-collapse: collapse;align: center;text-align: center" >
			<thead>
				<tr style="heigth: 0">
					<th style="width: 140px;">TANGGAL</th>
					<th style="width: 140px;">DIBUAT</th>
					<th style="width: 200px;">DIPERIKSA</th>
					<th style="width: 140px;">DISETUJUI</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo indDate($time) ?></td>
					<td>DOCUMENT CONTROL</td>
					<td>MANAGEMENT REPRESENTATIVE</td>
					<td>DIREKTUR UTAMA</td>
				</tr>
				<tr>
					<td style="height: 80px">&nbsp;</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>STAMP / STEMPEL</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div style="width: 600px;margin: auto;padding-top: 20px">
		<div style="float: left;border: 1px solid black;width: 100px;text-align: center" >STATUS DOKUMENTASI</div>
		<div style="float: left;border: 0;width: 50px ;text-align: center;padding: 0 5px;vertical-align: middle;line-height:30pt" >MASTER</div>
		<div style="float: left;width: 30px ;text-align: center;padding-top: 5px" >
			<div style="border: 1px solid black;height: 30px;width: 30px;" >&nbsp;</div>
		</div>
		<div style="float: left;border: 0;width: 80px ;text-align: center;padding: 0 5px;vertical-align: middle;line-height:30pt" >TERKENDALI</div>
		<div style="float: left;width: 30px ;text-align: center;padding-top: 5px" >
			<div style="border: 1px solid black;height: 30px;width: 30px;" >&nbsp;</div>
		</div>
		<div style="float: left;border: 0;width: 130px ;text-align: center;padding: 0 5px;vertical-align: middle;line-height:30pt" >TIDAK TERKENDALI</div>
		<div style="float: left;width: 30px ;text-align: center;padding-top: 5px" >
			<div style="border: 1px solid black;height: 30px;width: 30px;" >&nbsp;</div>
		</div>
		<div style="float: left;border: 0;width: 60px ;text-align: center;padding: 0 5px;vertical-align: middle;line-height:30pt" >COPY NO</div>
		<div style="float: left;width: 30px ;text-align: center;padding-top: 5px" >
			<div style="border: 1px solid black;height: 30px;width: 30px;" >&nbsp;</div>
		</div>
	</div>
	<div style="width: 600px;margin: auto;padding: 5px;clear: both">
		&nbsp;
	</div>
	<div style="width: 600px;margin: auto;padding: 5px;border: 1px solid black;clear: both">
		<div style="color: red;text-align: center;font-size: 12pt;font-weight: bold">PERINGATAN !</div>
		<div style="text-align: center;line-height: 40px;vertical-align: middle">Perlindungan Hak Cipta</div>
		<div style="text-align: justify;line-height: 30px;">
			Tidak sebagianpun dari terbitan Buku Manual Mutu ini dapat digandakan, disimpan dalam Sistem yang diperbaiki atau dipindahkan dalam bentuk atau dengan cara apapun baik <i>Elektronik, Mekanik, Photo Copy</i>, dicatat atau lainnya <i>tanpa izin tertulis</i> Wakil Manajemen.
		</div>
		<div style="text-align: center;line-height: 30px;">
			<div style="font-size: 12pt;font-weight: bold"><?php echo $project[0]->company_name ?></div>
			<div style="font-weight: bold">“ <?php echo $project[0]->company_scope ?> ”.</div>
			<div><?php echo $project[0]->company_address_1 ?>,</div>
			<div><?php echo $project[0]->company_address_2 ?></div>
			<div><?php echo $project[0]->company_city . ' '. $project[0]->company_province ?> - Indonesia</div>

		</div>
	</div>
</body>
</html>