<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		table#table-bingkai tr td {
			text-align: justify;
			vertical-align: top;
			line-height: 14pt;
		}
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}

		#table-manual tr.height td{
			/*padding-top: 20px 0 15px;*/
		}

		#table-manual tr.sub-sub-bab td {
			font-weight: bold;
			padding-top: 10px;
		}

	</style>
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB IV</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>SISTEM MANAJEMEN KESEHATAN DAN KESELAMATAN KERJA (K3)</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 40pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				<tbody>
					
					<tr class="sub-bab" >
						<td colspan="">4.1</td>
						<td colspan="3">Ketentuan Umum</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.1.1</td>
						<td colspan="2">Proses Sistem Manajemen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Dalam rangka mencapai visi perusahaan menjadi perusahaan yang kuat dan tumbuh dalam perdagangan besar bahan konstruksi di tingkat nasional maupun regional, dan melaksanakan misi perusahaan yaitu melakukan kegiatan proses perdagangan dengan memperhatikan aspek mutu secara menyeluruh. Upaya untuk melaksanakan misi perusahaan diperlukan adanya suatu komitmen untuk menerapkan system manajemen keselamatan dan kesehatan kerja yang baik di lingkungan perusahaan dengan mengacu pada standar OHSAS 18001:2007.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.1.2</td>
						<td colspan="2">Kebijakan dan Kepimipinan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Manajemen K3 dimulai dengan menetapkan kebijakan yang menjadi landasan strategi penerapan K3 dalam perusahaan.Kebijakan ini ditandatangani oleh pimpinan tertinggi dalam perusahaan yang menunjukkan komitmen manajemen terhadap K3.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.1.3</td>
						<td colspan="2">Perencanaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Setelah menetapkan kebijakan, disusun rencana penerapan system menajemen berdasarkan potensi bahaya atau risiko yang ada dalam kegiatan perusahaan. Identifikasi bahaya, penilaian dan rencana pengendalian risiko juga didasarkan kepada persyaratan perundangan yang berlaku. Berdasarkan hasil tersebut, disusun sasaran program kerja K3 untuk mengendalikan semua potensi risiko yang ada.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.1.4</td>
						<td colspan="2">Penerapan dan Operasional</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Berdasarkan sasaran dan program kerja dilaksanakan berbagai elemen kegiatan seperti pelatihan, komunikasi, dokumentasi, pengendalian dokumen, pengendalian operasi, tanggap darurat dan lainnya sesuai dengan kebutuhan organisasi atau kebutuhan pelanggan.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.1.5</td>
						<td colspan="2">Pengukuran dan pemantauan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Hasil penerapan K3 tersebut diukur dan dipantau secara berkaala untuk memastikan bahwa system manajemen telah berjalan baik sesuai harapan dan jika perlu segera dilakukan tindakan koreksi atau perbaikan berkelanjutan.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.1.6</td>
						<td colspan="2">Tinjauan Manajemen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Secara berkala dilakukan tinjauan manajemen oleh manajemen puncak untuk memastikan bahwa sistem manajemen telah berjalan baik sesuai harapan dan jika perlu segera dilakukan tindakan koreksi atau perbaikan berkelanjutan.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">4.2</td>
						<td colspan="3">Kebijakan  K3</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="3">Sesuai dengan persyaratan standar system manajemen K3 perusahaan menetapkan kebijakan K3 untuk diterapkan secara menyeluruh dalam organisasi. Kebijakan kesehatan dan keselamatan kerja yang ditetapkan perusahaan bertujuan mendukung pencapaian prestasi dan kenyamanan kerja karyawan. Sehingga menghasilkan kualitas dan kenyamanan hidup lebih baik. Perusahaan telah menyediakan sarana dan prasarana kesehatan yang memadai.
							<br />
							Manajemen Puncak telah menetapkan Kebijakan Perusahaan memuat Visi dan  Misi  yang hendak dicapai oleh <b><?php echo $project[0]->company_name ?></b>. Kebijakan Perusahaan ini sebagai wujud komitmen dan arahan  dari Manajemen Puncak  dan sesuai dengan  sifat dan skala risiko , komitmen untuk memberikan kepuasan terhadap pelanggan, menaati peraturan perundangan yang berlaku dan ketentuan lainnya yang diikuti, perbaikan secara berkelanjutan serta mencegah  terjadinya cedera,  penyakit akibat kerja dan menetapkan kerangka kerja untuk menetapkan dan meninjau sasaran Perusahaan.
							<br />
							Manajemen Puncak memastikan bahwa Kebijakan Perusahaan  telah dikomunikasikan dan dimengerti oleh seluruh karyawan dan pekerja yang bekerja untuk dan atau atas nama <b><?php echo $project[0]->company_name ?></b> melalui komunikasi internal dan training.
							<br />
							Peninjauan Kebijakan Perusahaan dilakukan sesuai dengan kebutuhan Perusahaan minimal 1 tahun sekali. 
							<br />
							Kebijakan Perusahaan ini tersedia bagi pihak-pihak yang berkepentingan.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	
</body>
</html>