<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		table#table-bingkai tr td {
			text-align: justify;
			vertical-align: top;
			line-height: 16pt;
		}
		
		#table-bingkai tr.nomor td{
			padding-top: 15px;
		}

		#table-manual tr.sub-sub-bab td {
			font-weight: bold;
			padding-top: 10px;
		}
		
	</style>
</head>
<body style="font-size: 10pt">
	<div style="width: 470pt;margin: auto;">
		<div  style="background-image: url('/application/public/img/element/Bingkai_18001.gif');width:450px;height: 700px;padding:  30px 70px;margin-left: 30px  " >
			<div style="text-align: center;font-size: 14pt;font-weight: bold " >KEBIJAKAN</div>
			<div style="text-align: center;font-size: 14pt;font-weight: bold " >KESELAMATAN DAN KESEHATAN KERJA</div>
			<div style="text-align: center;font-size: 12pt;font-weight: bold " ><?php echo $project[0]->company_name ?></div>
			<div>
				<table id="table-bingkai"  >
					<thead>
						<tr>
							<th style="width: 15pt"></th>
							<th style=""></th>
						</tr>
					</thead>
					<tbody>
						<tr class="" >
							<td colspan="2" >Dalam menjalankan Kegiatan Usaha, kami mengutamakan Kesehatan dan Keselamatan Kerja dalam Rangka memberikan Kepuasan dan Kepercayaan kepada Pelanggan dan Pihak - Pihak yang berkepentingan dengan menerapkan Sistem Manajemen Perusahaan mengacu kepada standar OHSAS 18001 : 2007, Komitmen kami adalah :</td>
						</tr>
						<tr class="nomor">
							<td>1.</td>
							<td>Melindungi Karyawan dalam hal Kesehatan dan Keselamatan Kerja untuk mencegah terjadinya Cedera dan Penyakit akibat Pekerjaan yang berkaitan dengan Kegiatan, Produk, dan Jasa Layanan Perusahaan, serta akibat yang ditimbulkannya.</td>
						</tr>
						<tr class="nomor">
							<td>2.</td>
							<td>Mentaati Perundangan serta Persyaratan lainnya di bidang Keselamatan, dan Kesehatan Kerja.</td>
						</tr>
						<tr class="nomor">
							<td>3.</td>
							<td>Membina Kepekaan, Kesadaran, dan Kepedulian seluruh Karyawan tentang Program Kesehatan dan Keselamatan Kerja (K3) Lingkungan kepada Pemasok serta Badan / Perseorangan yang bekerja di <b><?php echo $project[0]->company_name ?></b> agar ambil bagian di dalam Pengelolaan Lingkungan.</td>
						</tr>
						<tr class="nomor">
							<td colspan="2">Kebijakan Program Kesehatan dan Keselamatan Kerja (K3) ini didokumentasikan, diterapkan, dan dipelihara sesuai dengan persyaratan OHSAS 18001:2007.</td>
						</tr>
						
						<tr>
							<td colspan="2" style="text-align: center" >
								<?php 
								$time = strtotime($date . '-2 month');
								?>
								<br />
								<?php echo $project[0]->company_city .', ' . indDate($time) ?>
								<br />
								<br />
								<br />
								<br />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div style="text-align: center;font-size: 14pt;font-weight: bold " >DIREKTUR UTAMA</div>
			<div style="text-align: center;font-size: 12pt;font-weight: bold " ><?php echo $project[0]->company_name ?></div>
		</div>
		<div>&nbsp;</div>
	</div>
	
</body>
</html>