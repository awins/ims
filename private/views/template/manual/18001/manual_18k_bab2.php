<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}

		#table-manual tr.sub-sub-bab td {
			font-weight: bold;
			padding-top: 10px;
		}
		
	</style>
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB II</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>REFERENSI</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 40pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">2.1</td>
						<td colspan="3">Referensi Normatif</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="2">Pepres (Peraturan Presiden) No.28 tentang Standarisasi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="2">OHSAS 18001:2007 tentang Persyaratan Sistem Manajemen Kesehatan dan Keselamatan Kerja.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="2">Undang-undang No.1 Tahun 1970 tentang Keselamatan Kerja.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="2">Undang-undang No.13 Tahun 2003 tentang Ketenagakerjaan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="2">Kepmenaker No. 05 tentang SMK3.</td>
					</tr>
				</tbody>
			</table>
		</div>
	
</body>
</html>