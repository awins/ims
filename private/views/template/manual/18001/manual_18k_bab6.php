<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		table#table-bingkai tr td {
			text-align: justify;
			vertical-align: top;
			line-height: 14pt;
		}
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}

		#table-manual tr.height td{
			/*padding-top: 20px 0 15px;*/
		}

		#table-manual tr.bold td{
			font-weight: bold;
		}
		#table-manual tr.sub-sub-bab td {
			font-weight: bold;
			padding-top: 10px;
		}

	</style>
</head>
<body style="font-size: 10pt">
		
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 20pt"></td>
						<td style="width: 20pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				<tbody>
					
					<tr class="sub-bab" >
						<td colspan="">4.3</td>
						<td colspan="4">PERENCANAAN</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.3.1</td>
						<td colspan="3">Identifikasi Bahaya,  Penilaian dan Menentukan Pengendalian Risiko </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Untuk mengendalikan  risiko yang ditimbulkan dari setiap kegiatan operasional Perusahaan,  manajemen telah menetapkan tata cara  pengelolaan aspek Perusahaan  melalui upaya :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a)</td>
						<td colspan="2">Mendeteksi/mengidentifikasi risiko sedini mungkin pada setiap aktivitas.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b)</td>
						<td colspan="2">Melakukan pengukuran tingkat besarnya setiap risiko, dengan memperhitungkan besarnya dampak dan kemungkinan terjadinya peluang risiko. </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c)</td>
						<td colspan="2">Melakukan evaluasi terhadap sumber risiko dan penyebab terjadinya risiko, sebagai dasar untuk memetakan dan mengendalikan risiko yang signifikan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d)</td>
						<td colspan="2">Menyusun rencana strategi pengendalian terhadap risiko yang mempunyai prioritas tinggi/risiko signifikan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e)</td>
						<td colspan="2">Melakukan kegiatan strategi pengendalian risiko yang membahayakan kelangsungan hidup perusahaan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">f)</td>
						<td colspan="2">Melakukan pemantauan risiko secara terus menerus, khususnya yang mempunyai dampak cukup signifikan terhadap kondisi perusahaan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Setiap pimpinan unit kerja bertanggung jawab atas dilaksanakannya kebijakan manajemen risiko di unit kerjanya masing masing, guna mewujudkan terciptanya suatu system pengelolaan risiko yang akurat dan komperehensif untuk mendukung pencapaian tujuan perusahaan secara keseluruhan.
							<br />
							Pengelolaan identifikasi bahaya, penilaian dan menentukan pengendalian dan mekanisme pemeliharaan kekinian informasinya  ditetapkan dalam prosedur terdokumentasi.
							<br />
							Identifikasi Bahaya, Penilaian dan Pengendalian Risiko akan dikaji ulang setiap tahun dan atau apabila terjadi perubahan yang dapat mempengaruhi bahaya dan risiko. Perubahan ini dapat bersumber dari perubahan kegiatan, fasilitas, produk dan jasa, perubahan peraturan perundangan dan persyaratan lainnya.
							<br />
							Untuk menyamakan cara identifikasi, penilaian dan pengendalian risiko Perusahaan telah menetapkan Prosedur Identifikasi Bahaya, Penilaian dan Pengendalian Risiko. 
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.3.2</td>
						<td colspan="3">Perundang undangan dan persyaratan K3 Lainnya</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Membuat dan memelihara prosedur untuk mengidentifikasi dan mengakses peraturan perundang undangan dan persyaratan lainnya.
							<br />
							Peraturan perundang undangan dan persyaratan lainnya yang diidentifikasi mencakup :
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="2">Perundangan dan persyaratan K3 Internasional.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="2">Perundangan dan persyaratan K3 Nasional.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="2">Perundangan dan persyaratan K3 Daerah.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="2">Perijinan terkait lainnya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="2">Peraturan kegiatan lainnya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">f.</td>
						<td colspan="2">Standar K3 yang berlaku.</td>
					</tr>
					
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Peraturan perundangan yang berlaku dan persyaratan lain yang diikuti harus berlaku terhadap risiko. Identifikasi dan akses terhadap peraturan dan persyaratan lainnya dapat dilakukan melalui kunjungan langsung, media elektronik (website :www.Menakertrans.go.id, www.Menlh.go.id, www. Menkes.go.id ), media cetak maupun melalui pihak-pihak terkait dan atau instansi terkait.
							<br />
							<br />
							Seluruh peraturan perundangan dan persyaratan lainnya yang diikuti oleh Perusahaan didokumentasikan dan ditinjau ulang secara berkala untuk menjamin agar selalu dalam kondisi terbaru (up-to date) dan dikomunikasikan kepada pihak-pihak yang terkait. 
							<br />
							<br />
							Tatacara identifikasi dan akses peraturan perundangan dan persyaratan lainnya dituangkan dalam Prosedur  Hukum dan  Persyaratan Lainnya. 
						</td>
					</tr>

					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.3.3</td>
						<td colspan="3">Sasaran dan Program K3 <b><?php echo $project[0]->company_name ?></b></td>
					</tr>

					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Perusahaan  telah menetapkan, memantau, mengkaji ulang dan memperbaharui Sasaran dan Program sejalan dengan Kebijakan Perusahaan dan komitmen terhadap pencegahan terhadap cedera dan  penyakit akibat kerja pada setiap fungsi dan tingkat yang relevan. 
							<br />
							Penetapan Sasaran telah mempertimbangkan peraturan perundangan dan persyaratan lainnya, bahaya, risiko, pilihan teknologi, kemampuan finansial, operasional, bisnis dan pandangan dari pihak-pihak terkait. Sasaran  konsisten dengan Kebijakan Perusahaan dan dikuantifikasi.
							<br />
							Untuk memastikan Sasaran Perusahaan dapat dicapai Perusahaan menetapkan program Perusahaan mencakup :
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Tahapan kegiatan untuk mencapai Sasaran.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Penanggung jawab pencapaian Sasaran.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Batas waktu pencapaian.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Kemajuan dari pencapaian tujuan dan sasaran dipantau oleh penanggung jawab program dan dilaporkan kepada Wakil Manajemen. Secara detail ditetapkan dalam Prosedur Sasaran dan Program Perusahaan.</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">4.4</td>
						<td colspan="4">Pelaksanaan dan Operasional</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.4.1</td>
						<td colspan="4">Sumber daya, Tanggung Jawab, Tanggung Gugat dan Wewenang</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Menentukan dan mendokumentasikan serta mengkomunikasikan tentang peran, tanggung jawab dan wewenang untuk pelaksanaan SMK3 di seluruh lapisan pekerja.
							<br />
							Pihak manajemen menyediakan sumber daya yang diperlukan (mencakup sumber daya manusia, teknis, sarana dan keuangan) ditetapkan dalam rangka untuk :
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Menerapkan dan memelihara Sistem Manajemen K3 yang secara terus menerus diperbaiki keefektifannya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Selalu berupaya menigkatkan kepuasan pelanggan dengan memenuhi persyaratan pelanggan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Pihak manajemen menunjuk seorang wakil manajemen yang memiliki peran dan tanggung jawab sebagai berikut :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Memastikan bahwa persyaratan SMK3 ditetapkan dilaksanakan dan dipelihara sesuai SMK3.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Melaporkan kinerja SMK3 kepada puncak pimpinan untuk ditinjau ulang sebagai dasar perbaikan SMK3.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="4">
							<?php 
							$company_id =  $project[0]->company_id;
		    				$img_file = '/application/public/img/organitation_structure/' . $company_id .'.png';
		    				if (!file_exists( $_SERVER['DOCUMENT_ROOT'] . '/application/public/img/organitation_structure/' . $company_id . '.png')){
		    					$img_file = '/application/public/img/organitation_structure/default.png';
		    				} ?>
							<img src="<?php echo $img_file ?>">
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="4" style="text-align: center" ><b>Gb.1 STRUKTUR ORGANISASI</b></td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.4.2</td>
						<td colspan="3">Pelatihan, Kepedulian dan Kompetensi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Untuk meningkatkan  kompetensi dan kepedulian sumber daya manusia yang dimiliki, Perusahaan telah menetapkan Prosedur Pengelolaan SDM yang meliputi :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="2">Penerimaan Karyawan, </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="2">Pelatihan dan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="2">Penilaian Karyawan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Pelatihan yang diberikan kepada seluruh karyawan dan personil yang bekerja untuk dan atau atas nama Perusahaan. Untuk memastikan tingkat kompetensi yang dibutuhkan HRD telah menetapkan persyaratan kompetensi yang harus dimiliki oleh masing-masing karyawan dan pihak-pihak yang bekerja untuk dan atau atas nama <b><?php echo $project[0]->company_name ?></b>.
							<br />
							Divisi HRD setiap tahun melakukan identifikasi kebutuhan pelatihan berdasarkan tingkatan tanggung jawab, kemampuan, pendidikan dan risiko, membuat program pelatihan, melaksanakan pelatihan dan melakukan evaluasi terhadap efektifitas dari pelatihan yang sudah dilaksanakan. Personil yang menjalankan tugas yang dapat menyebabkan dampakterhadap Perusahaan di tempat kerja, harus memiliki kompetensi berdasarkan pendidikan, pelatihan dan atau pengalaman yang memadai. 
							<br />
							Persyaratan Kompetensi, Pelatihan dan Kesadaran ditetapkan secara detail masing-masing pada Dokumen Job Desc dan Specs,  Prosedur Pengelolaan SDM.
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.4.3</td>
						<td colspan="3">Konsultasi, Komunikasi dan Partisipasi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> menetapkan dan memelihara pentingnya proses Komunikasi, Partisipasi dan Konsultasi K3 yang berhubungan dengan aspek K3 dan SMK3.</td>
					</tr>
					<tr class="bold" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">A.</td>
						<td colspan="2">Komunikasi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Perusahaan telah menetapkan tata cara komunikasi yang berhubungan dengan K3:</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Komunikasi Internal sesuai dengan Tingkat yang Relevan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Komunikasi dengan Supplier dan Subkontraktor dan Pengunjung Tamu ke tempat Kerja.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Menerima, Mendokumentasikan dan Menanggapi setiap Komunikasi dengan Pihak Eksternal termasuk Komplain.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Perusahaan juga menetapkan dan Mendokumentasikan Tata Cara Komunikasi Aspek K3 kepada Pihak Eksternal yang berkepentingan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Komunikasi yang dijalankan adalah secara lisan dan atau tertulis, misalnya melalui rapat internal, safety meeting, safety talk, nota dinas, papan pengumuman.</td>
					</tr>
					<tr class="bold" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">B.</td>
						<td colspan="2">Partisipasi dan Konsultasi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Partisipasi dengan melibatkan Pekerja, Supllier dalam mengembangkan Kebijakan dan Prosedur Pengendalian Resiko,</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Dikonsultasikan bila terjadi perubahan K3,</td>
					</tr>

					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Diwakili dalam K3 dan diinformasikan pada Perwakilan Karyawan yang ditunjuk</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Dilibatkan dalam Investigasi Insiden</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.4.4</td>
						<td colspan="3">Dokumentasi Sistem Manajemen K3</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> menetapkan prosedur untuk system dokumentasi untuk memastikan bahwa semua dokumen yang berkaitan dengan SMK3 telah tersedia dan dipelihara dengan baik.
							<br />
							Dokumen system manajemen K3 harus mencakup :
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Kebijakan dan objektif K3</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Uraian lingkup system manajemen K3</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Uraian elemen utama dari system manajemen K3, interaksi dan referensi untuk dokumen terkait.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Dokumen, termasuk rekaman yang disyaratkan OHSAS 18001:2007</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Dokumen termasuk rekaman, yang ditentukan dan diperlukan oleh organisasi.</td>
					</tr>
					
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.4.5</td>
						<td colspan="3">Pengendalian Dokumen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Perusahaan telah menetapkan Prosedur dan metodologi pengendalian dokumen baik dokumen internal maupun dokumen eksternal untuk memastikan seluruh dokumen terawat dengan baik, mudah ditemukan dan disimpan dengan baik. Pengendali Dokumen bertanggung jawab terhadap tata cara pengendalian dokumen yang mencakup kegiatan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Klasifikasi dan Identifikasi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Terkendali</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Tidak Terkendali</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Kadaluarsa</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Status Revisi Terakhir</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Perubahan Penggantian dan Pemusnahan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Distribusi Dokumen kepada Bagian Terkait</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Dokumen yang masih berlaku terdapat di semua tempat yang menggunakan sesuai dengan Fungsi dan Jabatannya. Dokumen yang sudah tidak diperlukan (Dokumen Kadaluarsa)dapat dimusnahkan dari tempat penggunaannya. Untuk keperluan referensi, Master Dokumen Kadaluarsa dapat disimpan tersendiri sesuai Ketentuan.
							<br />
							Untuk keperluan penyempurnaan, maka seluruh Dokumen Sistem Manajemen K3 dikaji ulang setiap Tahun dan bila perlu dilakukan Perbaikan.Hasil perbaikan disahkan oleh pejabat yang berwenang.
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.4.6</td>
						<td colspan="3">Pengendalian Operasional</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Mengidentifikasi operasi dan aktivitas yang berkaitan dengan identifikasi risiko dimana pengendalian perlu diadakan perencanaan kegiatan, termasuk perawatan atau harus sesuai dengan kondisi berikut :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Tersedianya prosedur yang terdokumentasikan untuk menghindari penyampaian dari kebijakan dan sasaran.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Menetapkan kriteria operasi dalam prosedur.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Menyediakan prosedur identifikasi risiko K3 dari barang barang, peralatan dan jasa yang dibeli atau digunakan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Mengkomunikasikan prosedur tersebut ke supplier dan customer.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Menyediakan prosedur untuk desain tempat kerja, peralatan kerja, prosedur operasi, dll. Untuk menghilangkan atau mengurangi risiko</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.4.7</td>
						<td colspan="3">Kesiagaan dan Tanggap Darurat</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Prosedur  Kesiagaan dan Tanggap  Darurat  yang telah dibuat secara berkala dilakukan uji coba dan dilakukan pengkajian dan penyempurnaan khususnya setelah dilakukan simulasi dan terjadi kecelakaan atau situasi darurat. Hasil pelaksanaannya dijadikan sebagai salah satu masukan bagi perbaikan sistem penanganan keadaan darurat di dalam Perusahaan jika dianggap perlu, dilakukan perubahan terhadap Prosedur dan Rencana Tanggap Darurat yang telah ditetapkan dan dilakukan pengkajian dan penyempurnaan khususnya setelah dilakukan uji coba dan atau terjadi situasi darurat
							<br />
							Pimpinan Perusahaan telah membentuk suatu Tim Kesiagaan dan Tanggap Darurat yang diketuai oleh Direktur Utama dan anggota dari setiap perwakilan setiap bagian disertai dengan tugas dan tanggung jawab. Secara detail diatur dalam Prosedur  Kesiagaan dan Tanggap Darurat  dan Rencana Tanggap Darurat .
						</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">4.5</td>
						<td colspan="4">Pemeriksaan dan Tindakan Perbaikan</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.5.1</td>
						<td colspan="3">Pemantauan dan Pengukuran</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3"><b><?php echo $project[0]->company_name ?></b> membuat dan merawat prosedur untuk pemantauan dan pengukuruan kinerja K3. Prosedur tersebut mencakup :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Mengukur baik secara kuantitatif maupun kualitatif</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Memantau kesesuaiannya dengan tujuan dan sasaran yang telah ditetapkan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Mengukur dan merencanakan kesesuaiannya dengan program yang telah ditetapkan, kriteria operasi dan persyaratan perundangan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Mengukur dan memantau kecelakaan, sakit dan insiden.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Mencatat semua hasilnya untuk tujuan tindakan perbaikan pencegahan dan alat alat ukur harus dikalibrasi dan dirawat, rekaman harus dipelihara.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.5.2</td>
						<td colspan="3">Evaluasi Pemenuhan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Untuk memastikan tingkat penaatan terhadap peraturan perundangan dan persyaratan lain yang berlaku, perusahaan secara periodik paling sedikit setiap satu (1) tahun sekali melakukan evaluasi untuk memastikan tingkat penataan terhadap peraturan perundangan yang berlaku dan persyaratan lain yang diikuti.
							<br />
							Evaluasi Tingkat Penataan terhadap peraturan perundangan yang berlaku dan persyaratan lainnya terkait dengan kegiatan, fasilitas, produk dan jasa Perusahaan sejalan dengan ketentuan peraturan perundangan yang berlaku dan persyaratan lainnya yang diikuti.
							<br />
							<b><?php echo $project[0]->company_name ?></b> menetapkan, menjalankan dan memelihara prosedur untuk mengevaluasi secara berkala pemenuhan persyaratan hukum yang sesuai dengan ketentuan: 
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Menyimpan rekaman hasil evaluasi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Mengevaluasi pemenuhan persyaratan lainnnya yang berlaku bagi perusahaan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="2">Menyimpan rekaman hasil evaluasi berkala yang dilakukan.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.5.3</td>
						<td colspan="3">Kecelakaan, insiden, ketidaksesuaian, serta tindakan perbaikan dan pencegahan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Perusahaan secara terus menerus meningkatkan Sistem Manajemen K3 melaksanakan Audit Internal, melaporkan setiap terjadi insiden, melakukan Investigasi, Tindakan Perbaikan dan Pencegahan.</td>
					</tr>
					<tr class="bold" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">A.</td>
						<td colspan="2">Investigasi Insiden</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Setiap terjadi insiden perusahaan melakukan investigasi, menganalisa dan merekam setiap insiden yang terjadi dengan :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="">Menentukan tingkat penyimpangan K3 dan faktor lainnya</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="">Mengindentifikasi kebutuhan untuk tindakan perbaikan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="">Mengidentifikasi peluang untuk melakukan tindakan pencegahan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="">Melakukan perbaikan dan pencegahan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="">Mengkomunikasikan setiap hasil investigasi sesuai dengan waktu kejadiannya dan mengacu kepada peraturan yang berlaku.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Perusahaan telah menetapkan pejabat yang bertanggung jawab untuk melakukan investigasi Insiden.</td>
					</tr>
					<tr class="bold" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">B.</td>
						<td colspan="2">Ketidaksesuaian, Tindakan Perbaikan dan Tindakan Pencegahan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Perusahaan melakukan tindakan perbaikan untuk menghilangkan penyebab ketidaksesuaian dan penyebab potensial ketidaksesuaian untuk mencegah terulangnya kembali. Tindakan perbaikan dan pencegahan yang dilakukan harus sesuai dengan penyebab atau akar masalah dan resiko potensial yang mungkin terjadi.
							<br />
							Tindakan perbaikan dan pencegahan dari hasil Investigasi Insiden dan Ketiaksesuaian yang terjadi dilakukan penilaian Resiko ulang sebelum tindakan perbaikan dan pencegahan diterapkan untuk memastikan pelaksanaan Tindakan Perbaikan dan Pencegahan telah dilaksanakan dengan benar dan efektif
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.5.4</td>
						<td colspan="3">Pengendalian Rekaman</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Perusahaan telah menetapkan Prosedur untuk identifikasi dan pemeliharaan catatan untuk diperoleh dan disimpan di tempat yang terlindung dari kerusakan dan kehilangan.
							<br />
							Catatan dapat berbentuk dalam berbagai jenis media dan memiliki masa simpan yang ditetapkan untuk waktu tertentu minimal 3 (tiga) tahun dan dimusnahkan bila telah kadaluarsa. Khusus untuk hasil pemantauan kesehatan karyawan (medical record) minimal selama karyawan tersebut bekerja.
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">4.5.5</td>
						<td colspan="3">Audit Internal</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Untuk menjamin efektifitas penerapan Sistem Manajemen Perusahaan. <b><?php echo $project[0]->company_name ?></b> telah menetapkan Prosedur Audit Internal untuk memastikan sistem manajemen Perusahaan :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="2">Dilaksanakan sesuai dengan rencana yang telah ditetapkan dan kesesuaiannya dengan persyaratan OHSAS 18001 : 2007,</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="2">Menentukan efektifitas pelaksanaan sistem manajemen Perusahaan,</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="2">Memberikan informasi dan masukan kepada Manajemen Puncak  sebagai bahan evaluasi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="3">Penetapan jadwal audit akan mempertimbangkan status dan kepentingan dan pentingnya proses serta area yang diaudit maupun hasil audit sebelumnya.
							<br />
							Hasil audit ditindaklanjuti oleh bagian yang terkait sesuai dengan temuan audit dan harus dilakukan verifikasi ulang untuk memastikan tindakan perbaikan dan pencegahan telah sesuai dan berjalan dengan efektif.
							<br />
							<br />
							Hasil audit akan dievaluasi dalam Tinjauan Manajemen, dan seluruh catatan audit disimpan sampai periode waktu tertentu.
							<br />
							Audit dilaksanakan oleh auditor yang terlatih dan mandiri serta dilakukan oleh  auditor yang tidak  mempunyai tanggung jawab terhadap bagian/area yang diaudit dan hasil audit. Secara detail diatur dalam Prosedur  Audit Internal.
						</td>
					</tr>
					<tr class="sub-bab" >
						<td colspan="">4.6</td>
						<td colspan="4">Tinjauan Manajemen</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="4">Manajemen Perusahaan telah menetapkan Prosedur untuk melakukan Tinjauan Manajemen. Tinjauan Manajemen paling sedikit dilaksanakan  satu (1)  kali dalam setahun yaitu pada akhir tahun dan dipimpin oleh Management Representative dan dihadiri oleh Manajemen Puncak, Manager, Kepala Bagian dan Perwakilan dari setiap Bagian.
							<br />
							Tinjauan Manajemen akan mengkaji kesesuaian, kecukupan, dan efektifitas berjalannya sistem manajemen Perusahaan. Tinjauan ini termasuk pengkajian peluang peningkatan dan perubahan-perubahan bilamana diperlukan terhadap Kebijakan Perusahaan, Sasaran Perusahaan dan elemen-elemen lain dari sistem manajemen Perusahaan.
							<br />
							Dalam pelaksanaan Tinjauan Manajemen harus mencakup agenda sebagai berikut :
						</td>
					</tr>
					<tr class="bold" >
						<td colspan=""></td>
						<td colspan="4">Masukan Tinjauan Manajemen :</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.</td>
						<td colspan="3">Hasil Audit Internal dan Eksternal</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">2.</td>
						<td colspan="3">Tindak Lanjut dari Tinjauan Manajemen sebelumnya</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">3.</td>
						<td colspan="3">Status Tindakan Perbaikan dan Pencegahan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.</td>
						<td colspan="3"> Partisipasi dan Konsultasi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.</td>
						<td colspan="3">Komunikasi eksternal dan Keluhan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">6.</td>
						<td colspan="3">Tingkat pencapaian Sasaran dan Program Perusahaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">7.</td>
						<td colspan="3">Kinerja Perusahaan </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">8.</td>
						<td colspan="3">Evaluasi penaatan terhadap peraturan perundangan dan persyaratan lainnya yang berlaku </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">9.</td>
						<td colspan="3">Status Investigasi Insiden</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">10.</td>
						<td colspan="3">Perubahan yang berpengaruh terhadap sistem manajemen Perusahaan termasuk perkembanganpada peraturan perundangan dan persyaratan yang berlaku</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">11.</td>
						<td colspan="3">Rekomendasi Perbaikan</td>
					</tr>
					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="4">Keluaran Tinjauan Manajemen mencakup setiap keputusan dan tindakan yang berhubungan dengan kemungkinan  :</td>
						
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">1.</td>
						<td colspan="3">Kinerja Perusahaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">2.</td>
						<td colspan="3">Kebijakan Perusahaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">3.</td>
						<td colspan="3">Sasaran dan Program Perusahaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">4.</td>
						<td colspan="3">Unsur-unsur lain yang terdapat dalam Sistem Manajemen Perusahaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">5.</td>
						<td colspan="3">Sumber daya</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">6.</td>
						<td colspan="3">Komitmen untuk Perbaikan Berkelanjutan.</td>
					</tr>
					
					<tr class="" >
						<td colspan=""></td>
						<td colspan="4">Hasil tinjauan manajemen ini harus dikomunikasikan kepada bagian terkait.
							<br />
							Secara detail Tinjauan Manajemen ditetapkan dalam Prosedur Tinjauan Manajemen. 
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	
</body>
</html>