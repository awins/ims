<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}
		
		#table-manual tr.sub-sub-bab td {
			font-weight: bold;
			padding-top: 10px;
		}
	</style>
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB I</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>PENDAHULUAN</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 40pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">1.1</td>
						<td colspan="3">Profil Perusahaan</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">1.1.1</td>
						<td colspan="2">Sejarah Perusahaan</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2"><b><?php echo $project[0]->company_name ?></b> dalam perjalanannya telah dipercaya mengerjakan berbagai pekerjaan, yang dengan berbagai upaya melalui berbagai kesulitan dan hambatan sehingga membuatnya menjadi lebih matang, mantap dan dewasa dalam menjalankan usaha.
							<br />
							<b><?php echo $project[0]->company_name ?></b> adalah perusahaan yang bergerak di bidang “<?php echo $project[0]->company_scope ?>” telah berkembang dengan pesat dalam persaingan dunia bisnis karena komitmen yang tinggi dari Manajemen Puncak terhadap peningkatan kualitas Sistem Manajemen Perusahaan secara berkesinambungan.
						</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">1.1.2</td>
						<td colspan="2">Tujuan Penerapan SMK3 di <b><?php echo $project[0]->company_name ?></b></td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Untuk merespon dinamika dan kemajuan <b><?php echo $project[0]->company_name ?></b> memiliki tanggung jawab dan komitmen yang kuat untuk memberikan yang terbaik bagi kepuasan Pengguna Jasa, karena kepuasan Pengguna Jasa merupakan hal yang terpenting.
							<br />
							Kemampuan <b><?php echo $project[0]->company_name ?></b> dalam memenuhi tanggung jawab dan komitmen tersebut telah terbukti dengan adanya kepercayaan dari Pengguna Jasa kepada <b><?php echo $project[0]->company_name ?></b> dalam mencapai tujuan-tujuan yang diinginkan oleh para Pengguna Jasa dengan prestasi yang dicapai <b><?php echo $project[0]->company_name ?></b> seperti :
						</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="">Dapat diandalkan dan terpercaya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="">Mengutamakan kepentingan dan kepuasan Pelanggan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="">Melaksanakan pekerjaan dengan cepat, tepat, yang berkualitas dan bermutu tinggi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="">•</td>
						<td colspan="">Struktur dan management keuangan yang kuat dan mantap.Karyawan yang berketerampilan dan penuh motivasi, serta penuh tanggung jawab.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Komitmen dan tanggung jawab yang kuat dari <b><?php echo $project[0]->company_name ?></b> untuk memuaskan Pengguna Jasa dimulai dari proses penawaran hingga selesainya pekerjaan.</td>
					</tr>
					<tr class="sub-sub-bab" >
						<td colspan=""></td>
						<td colspan="">1.1.3</td>
						<td colspan="2">Ruang Lingkup</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan=""></td>
						<td colspan="2">Ruang lingkup penerapan Sistem Manajemen <b><?php echo $project[0]->company_name ?></b> adalah <i>“<?php echo $project[0]->company_scope ?>”</i>
							<br />
							Manual SMK3 ini menggambarkan secara umum bagaimana SMK3 diterapkan dalam ruang lingkup aktivitas yang ada di <b><?php echo $project[0]->company_name ?></b> dan kebijakan perusahaan dalam rangka pemenuhan terhadap standar OHSAS 18001:2007. Manual ini berlaku untuk semua aktivitas yang dilakukan di lingkungan <b><?php echo $project[0]->company_name ?></b>, termasuk aktivitas di lokasi proyek yang menjadi tanggung jawab <b><?php echo $project[0]->company_name ?></b>.
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	
</body>
</html>