<html>
<head>
	<link rel="stylesheet" type="text/css" href="/application/public/css/manual_mutu.css">
	<style type="text/css">
		#table-manual tr.sub-bab td{
			padding-bottom: 10px;
		}

		#table-manual tr.sub-sub-bab td {
			font-weight: bold;
			padding-top: 10px;
		}
		
	</style>
</head>
<body style="font-size: 10pt">
		<div style="text-align: center;padding: 15px 0">
			<span style="font-size: 14pt;font-weight: bold" ><b>BAB III</b></span>
			<br />
			<span style="font-size: 12pt;font-weight: bold" ><b>ISTILAH DAN DEFINISI</b></span>
		</div>
		<div style="width: 470pt;margin: auto;">
			<table id="table-manual" class="" border="0"  >
				<thead>
					<tr>
						<td style="width: 30pt" class=""></td>
						<td style="width: 35pt"></td>
						<td style="width: 40pt"></td>
						<td style="width: 365pt"></td>
					</tr>
				</thead>
				<tbody>
					<tr class="sub-bab" >
						<td colspan="">3.1</td>
						<td colspan="3">Istilah dan Definisi</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">a.</td>
						<td colspan="2"><b>Bahaya</b> adalah sesuatu yang memiliki potensi untuk menyebabkan cidera/ sakit  pada manusia, kerusakan peralatan,  gangguan proses serta kerusakan terhadap lingkungan.  Bahaya yang dimaksud dapat berasal dari peralatan/ proses baik yang sudah terpasang maupun penambahan peralatan baru atau hasil modifikasi, hasil inspeksi, hasil Audit, kondisi tempat kerja, bahan/ material, proyek, dll.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">b.</td>
						<td colspan="2"><b>Identifikasi bahaya</b> adalah proses mengenali bahaya yang ada dan mengenali sifat sifatnya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">c.</td>
						<td colspan="2"><b>Kecelakaan</b> adalah kejadian yang tidak diinginkan dapat menimbulkan cidera, sakit kerusakan atau kehilangan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">d.</td>
						<td colspan="2"><b>Audit</b> adalah pengujian sistematis untuk menentukan apakah kegiatan dan hasil yang bersangkutan sesuai dengan pengaturan yang telah direncanakan dan apakah pengaturan ini diterapkan secara efektif dan sesuai dengan pencapaian kebijakan dan sasaran organisasi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">e.</td>
						<td colspan="2"><b>Risiko</b> adalah kecenderungan terjadi suatu kerugian (cidera, sakit, kerusakan peralatan, gangguan proses produksi, kerusakan lingkungan) akibat paparan bahaya.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">f.</td>
						<td colspan="2"><b>Keselamatan</b> adalah bebas dari resiko atau bahaya yang tidak dapat diterima.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">g.</td>
						<td colspan="2"><b>Penilaian Risiko</b> adalah proses penilaian terhadap suatu risiko dengan menggunakan parameter akibat dan peluang dari bahaya yang ada.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">h.</td>
						<td colspan="2"><b>Pengendalian Risiko</b> adalah pengendalian atau pengelolaan setiap sumber yang dapat mengakibatkan kerugian melalui eliminasi risiko, pengurangan risiko, pemindahan risiko atau penerimaan risiko.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">i.</td>
						<td colspan="2"><b>Risiko yang dapat diterima</b> adalah risiko yang telah dikurangi sampai tingkatan yang dapat diterima oleh organisasi dalam upaya pemenuhan terhadap ketentuan hukum maupun kebijakan K3 yang ditetapkan perusahaan.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">j.</td>
						<td colspan="2"><b>Sistem Manajemen K3</b> adalah bagian dari sistema manajemen keseluruhan yang memudahkan dari pengelolaan risiko risiko K3 yang terkait dengan kegiatan perusahaan.  Hal ini mencakup struktur organisasi, rencana kegiatan, tanggung jawab, turunan, prosedur, proses dan sumber daya untuk pengembangan, penerapan, pencapaian dan peninjauan. </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">k.</td>
						<td colspan="2"><b>Pekerjaan rutin</b> adalah pekerjaan  yang dilakukan secara rutin dari hari ke hari.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">l.</td>
						<td colspan="2"><b>Pekerjaan nonrutin</b> adalah pekerjaan yang dilakukan tidak rutin (sesekali, kadang- kadang seperti perbaikan fasilitas, pemeliharaan, overhoul mesin).</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">m.</td>
						<td colspan="2"><b>Penyakit akibat kerja</b> adalah suatu kondisi pisik atau mental yang menjadi lebih buruk disebabkan oleh akivitas kerja dan atau situasi pekerjaan terkait.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">n.</td>
						<td colspan="2"><b>Tempat Kerja</b> adalah setiap lokasi fisik dimana suatu aktivitas terkait dengan pelaksanaan pekerjaan di bawah pengendalian organisasi.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">o.</td>
						<td colspan="2"><b>Tim Manajemen Risiko (TMR)</b> adalah tim penilaian risiko yang terdiri dari pegawai dan atau manajemen dan bertugas untuk melakukan identifikasi bahaya, penilaian dan pengendalian risiko.</td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">p.</td>
						<td colspan="2"><b>TPKD</b> : Tim Penanggulangan Keadaan Darurat. </td>
					</tr>
					<tr class="" >
						<td colspan=""></td>
						<td colspan="">q.</td>
						<td colspan="2"><b>RTD</b> : Rencana Tanggap Darurat.</td>
					</tr>
					
					
				</tbody>
			</table>
		</div>
	
</body>
</html>