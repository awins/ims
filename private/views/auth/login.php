<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>IMS APPLICATION | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="/application/public/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="/application/public/bootstrap/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />

 
    
  </head>
    <body style="background-color: rgb(220,220,220); height: 100%;width: 100%;position: absolute">
        <div class="container" style="height: 100%;position: relative">
            <div class="row" style="position: absolute; width: 100%;top: 25%">

                <div class="col-md-offset-4 col-md-4">

                    <div class="box box-info" style="" >
                        <div class="box-header with-border">
                            <div style="text-align: center">
                                <img src="/application/public/img/logo/logo-IMS.png" style="margin: 0 auto">
                            </div>
                            
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <form class="form-horizontal" action='/auth/login' method="post" >
                                <input type="hidden" name="user_login" value="1" />
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </div>
                                            <input type="text" id="user_name" name="user_name" class="form-control"  placeholder="User Name" autofocus autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" >
                                        </div>
                                    </div>          
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="glyphicon glyphicon-lock"></i>
                                            </div>
                                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" >
                                        </div>
                                    </div>  
                                </div>
                                <?php echo @$error['login'] ?>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button class="btn btn-primary pull-right col-xs-12" id="login" type="submit"><i class="glyphicon glyphicon-log-in" ></i>&nbsp;&nbsp;&nbsp;Login</button>
                                    </div>          
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>            
        </div> <!-- /container -->
        <script src="/application/public/bootstrap/plugins/jQuery/jQuery-2.1.4.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#user_name').attr('autocomplete','off');
                $('#password').attr('autocomplete','off');
                $('#user_name').val('');
                $('#password').val('');

            });
        </script>
     </body>
</html>