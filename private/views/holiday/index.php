
<?php 
    $header['css'] = array('/application/public/bootstrap/plugins/datatables/jquery.dataTables.css');
    $footer['js'][] = '/application/public/bootstrap/plugins/datatables/jquery.dataTables.js';
    $footer['js'][] = '/application/public/js/holiday.js';
    $this->load->view('header',$header); 
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Holiday List
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Holiday</li>
        </ol>
    </section>

    <section class="content">
        
        <div class="row">
            <div class="col-sm-12">
                <a href="/holiday/input">
                    <button class="btn btn-primary pull-right" type="button"><i class="glyphicon glyphicon-plus" ></i>&nbsp;&nbsp;&nbsp;Input New</button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-body">
                    <table id="table-holiday" class="table table-bordered black">
                        <thead>
                          <tr>
                            <th style="width: 3%;  text-align: center" >No. </th>
                            <th style="width: 20%; text-align: center">Date</th>
                            <th style="width: 70%; text-align: center">Note/Remarks</th>
                            <th style="width: 7%; text-align: center">Action</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            
                            foreach ($holidays as $value) { ?>
                                <tr rel="<?php echo $value->id ?>" >
                                    <td>
                                        <?php $no++; echo $no ?>
                                    </td>
                                    
                                    <td>
                                        <?php echo date('l, d F Y', strtotime($value->holiday_date)); ?>
                                    </td>
                                    <td>
                                        <?php echo $value->holiday_note ?>
                                    </td>
                                    
                                    <td>
                                        <a href="/holiday/update/<?php echo $value->id ?>">
                                            <button type="button" class="btn btn-xs btn-info " id="btn-edit"><i class="glyphicon glyphicon-pencil"></i></button>
                                        </a>
                                        <button type="button" class="btn btn-xs btn-danger btn-remove" rel="<?php echo $value->id ?>"  ><i class="glyphicon glyphicon-remove"></i></button>
                                        
                                    </td>
                                </tr>
                                <?php
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        
    </section>
</div><!-- /.content-wrapper -->

<?php 

$this->load->view('footer',$footer) ?>