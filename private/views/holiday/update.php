<?php 
    /*$script['css'][] = '/application/public/css/holiday.css';*/
    $script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.js';
	$script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.extensions.js';
	$script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.date.extensions.js';
    $script['js'][] = '/application/public/js/jquery.form.js';
    $script['js'][] = '/application/public/js/holiday.js';

    $this->load->view('header',$script); 
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Update Holiday
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/holiday"><i class="fa fa-dashboard"></i> Holiday</a></li>
            <li class="active">Update Holiday</li>
        </ol>
    </section>

    <section class="content">
		<div class="row">
			<div class="col-sm-12">
				<form class="form-horizontal" role="form" id="form_holiday_input" action="/holiday/input" method="post" >
					<input type="hidden"  name="form_holiday_input" value="1" >
					<input type="hidden"  name="id" value="<?php echo (isset($holiday))?$holiday->id: set_value('id') ?>" >
					
					<div class="panel panel-primary">
				    	<div class="panel-heading">Holiday Detail</div>
				    	<div class="panel-body">
				    		<div class="col-md-12 col-sm-12">
					    		
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Holiday Date</label>
								    <div class="col-sm-9">
										<div >
								            <div class="input-group">
									            <div class="input-group-addon">
									                <i class="fa fa-calendar"></i>
									            </div>
									            <?php
									            if(isset($holiday)) {
									            	$arr_tgl = explode('-', $holiday->holiday_date);	
									            	$tgl = $arr_tgl[2] .'/' .$arr_tgl[1] . '/' . $arr_tgl[0] ;
									            }else {
									            	$tgl = '';
									            } ?>
									            <input type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="holiday_date" value="<?php echo $tgl ?>" />
									        </div>
								      		<?php echo form_error('holiday_date') ?>
								        </div>
								    </div>
								</div>
								
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Note/Remarks</label>
								    <div class="col-sm-9">
									    <input type="text" name="holiday_note" class="form-control" value="<?php echo (isset($holiday))?$holiday->holiday_note: set_value('holiday_note') ?>" >
									    <?php echo form_error('holiday_note') ?>
								    </div>
								</div>

				    		</div>
				    	</div>
				    </div>
				    <div class="row">
						<div class="col-sm-offset-6 col-sm-6" >
					        <button type="submit" class="btn btn-warning " id="btn-save"><i class="glyphicon glyphicon-floppy-disk"></i> SAVE</button>
					    </div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>

<?php
    $this->load->view('footer',$script); 
?>