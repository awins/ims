<?php 

    $script['css'][] = '/application/public/css/company.css';
    $script['css'][] = '/application/public/bootstrap/plugins/datatables/jquery.dataTables.css';
    $script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.js';
	$script['js'][] = '/application/public/bootstrap/plugins/input-mask/jquery.inputmask.extensions.js';
    $script['js'][] = '/application/public/js/jquery.form.js';
    $script['js'][] = '/application/public/js/company.js';
    $script['js'][] = '/application/public/bootstrap/plugins/datatables/jquery.dataTables.js';


    $this->load->view('header',$script); 
    $company_id = (isset($company))?$company->company_id: set_value('company_id');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Update Company
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/company"><i class="fa fa-dashboard"></i> Company</a></li>
            <li class="active">Update</li>
        </ol>
    </section>

    <section class="content">
		<div class="row">
			<div class="col-sm-12">
				<form class="form-horizontal" role="form" id="form_company_input" action="/company/update/<?php echo $company_id ?>" method="post" >
					<input type="hidden"  name="form_company_input" value="1" >
					<input type="hidden"  name="company_id" value="<?php echo $company_id ?>" >
					
					<div class="panel panel-primary">
				    	<div class="panel-heading">Company Detail</div>
				    	<div class="panel-body">
				    		<div class="col-md-6 col-sm-12">
					    		<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Company Name</label>
								    <div class="col-sm-9">
									    <input type="text" name="company_name" class="form-control" value="<?php echo (isset($company))?$company->company_name: set_value('company_name') ?>" >
									    <?php echo form_error('company_name') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Address</label>
								    <div class="col-sm-9">
									      <input type="text" class="form-control" name="company_address_1" value="<?php echo (isset($company))?$company->company_address_1: set_value('company_address_1') ?>">
									      <input type="text" class="form-control" name="company_address_2" value="<?php echo (isset($company))?$company->company_address_2: set_value('company_address_2') ?>">
									     <?php echo form_error('company_address_1') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">City</label>
								    <div class="col-sm-9">
									      <input type="text" class="form-control" name="company_city" value="<?php echo (isset($company))?$company->company_city:set_value('company_city') ?>">
									     <?php echo form_error('company_city') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Province</label>
								    <div class="col-sm-9">
								    	<div class="row">
				    						<div class="col-sm-12">
												<div class="form-group ">
													<div class="col-sm-6">
														<?php
														$arr_province = provinceList();
														$sel_province = (isset($company))?$company->company_province:set_value('company_province') ?>
														<select class="form-control" name="company_province">
															<?php
															foreach ($arr_province as $value) { ?>
																<option <?php echo ($sel_province == $value) ? 'selected' : null ?>><?php echo $value ?></option>
																<?php
															} ?>
														</select>
													     <?php echo form_error('company_province') ?>
													</div>
													<div class="col-sm-6">
													    <label class="control-label col-sm-3" for="">Zip</label>
														<div class="col-sm-9">
														     <input type="text" class="form-control" name="company_zip" value="<?php echo (isset($company))?$company->company_zip:set_value('company_zip') ?>">
														     <?php echo form_error('company_zip') ?>
														</div>
													</div>

												</div>
											</div>
										</div>							      
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Web User</label>
								    <div class="col-sm-9">
									      <input type="text" class="form-control" name="website_user" value="<?php echo (isset($company))?$company->website_user:set_value('website_user') ?>">
									     <?php echo form_error('website_user') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Web Password</label>
								    <div class="col-sm-9">
									      <input type="text" class="form-control" name="website_password" value="<?php echo (isset($company))?$company->website_password:set_value('website_password') ?>">
									     <?php echo form_error('website_password') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Agent</label>
								    <div class="col-sm-9">
									     <div class="input-group">
									      	<input type="hidden" id="agent_id" name="agent_id" value="<?php echo (isset($company))?$company->agent_id:set_value('agent_id') ?>" />
									      	<input type="text" class="form-control"  id="agent_name" name="agent_name" value="<?php echo (isset($company))?$company->agent_name:set_value('agent_name') ?>" readonly />
									      	
								            <span class="input-group-btn" style="vertical-align: top">
								                <button class="btn btn-default" id="btn-browse-agent" type="button" rel=""  data-toggle="modal" data-target="#box-agent" >
								                 	<i class="glyphicon glyphicon-search"></i>
								                </button>
								            </span>
								        </div>
										<?php echo form_error('agent') ?>
								    </div>
								</div>
				    		</div>
				    		<div class="col-md-6 col-sm-12">
				    			<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Initial Name</label>
								    <div class="col-sm-9">
									    <input type="text" name="company_initial" class="form-control" value="<?php echo (isset($company))?$company->company_initial: set_value('company_initial') ?>" >
									    <?php echo form_error('company_initial') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">EAC</label>
								    <div class="col-sm-9">
									    <div class="input-group">
									      	<input type="hidden" class="form-control" id="eac_detail" name="eac_detail" value="<?php echo (isset($company))?$company->eac_detail:set_value('eac_detail') ?>">
									      	<input type="text" class="form-control" id="eac" name="eac" value="<?php echo (isset($company))?$company->eac:set_value('eac') ?>">
								            <span class="input-group-btn" style="vertical-align: top">
								                <button class="btn btn-default" id="btn-browse-eac" type="button" rel=""  data-toggle="modal" data-target="#box-eac" >
								                 	<i class="glyphicon glyphicon-search"></i>
								                </button>
								            </span>
								        </div>
										<?php echo form_error('eac') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">NACE</label>
								    <div class="col-sm-9">
									     <div class="input-group">
									      	<input type="hidden" id="nace_detail" name="nace_detail" value="<?php echo (isset($company))?$company->nace_detail: set_value('nace_detail') ?>" />
									      	<textarea class="form-control" row="2" id="nace" name="nace" ><?php echo (isset($company))?$company->nace: set_value('nace') ?></textarea>
								            <span class="input-group-btn" style="vertical-align: top">
								                <button class="btn btn-default" id="btn-browse-nace" type="button" rel=""  data-toggle="modal" data-target="#box-nace" >
								                 	<i class="glyphicon glyphicon-search"></i>
								                </button>
								            </span>
								        </div>
										<?php echo form_error('nace') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Dir. Name</label>
								    <div class="col-sm-9">
									      <input type="text" class="form-control" name="director_name" value="<?php echo (isset($company))?$company->director_name:set_value('director_name') ?>">
									     <?php echo form_error('director_name') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Phone</label>
								    <div class="col-sm-9">
									      <input type="text" class="form-control" name="company_phone" value="<?php echo (isset($company))?$company->company_phone:set_value('company_phone') ?>">
									     <?php echo form_error('company_phone') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Scope</label>
								    <div class="col-sm-9">
									    <textarea class="form-control" row="3" id="company_scope" name="company_scope" ><?php echo (isset($company))?$company->company_scope:set_value('company_scope') ?></textarea>
										<?php echo form_error('company_scope') ?>
								    </div>
								</div>
								<div class="form-group ">
								    <label class="control-label col-sm-3" for="">Documents</label>
								    <div class="col-sm-9">
									    <textarea class="form-control" row="3" id="company_document" name="company_document" ><?php echo (isset($company))?$company->company_document:set_value('company_document') ?></textarea>
										<span style="color:blue">Awali dengan tanda * (bintang) untuk setiap dokumen</span>
										
								    </div>
								</div>
							</div>
							<div class="col-sm-12" >
					    		<div class="form-group"  >
					    			<div class="col-sm-5">
					    				<span style="font-weight: 700;" >ORGANITATION STRUCTURE</span>
					    				<br />
					    				 <span class="btn btn-primary btn-file btn-small ">
									        CHANGE <input type="file" name="structure_file">
									    </span>
					    			</div>
					    			<!-- <div class="col-sm-3">
							    	    <span class="btn btn-primary btn-file pull-right">
									        CHANGE <input type="file" name="structure_file">
									    </span>
							    	</div> -->
					    			<div class="col-sm-12" style="text-align: center">
					    				<?php 
					    				$img_file = '/application/public/img/organitation_structure/' . $company_id .'.png';
					    				if (!file_exists( $_SERVER['DOCUMENT_ROOT'] . '/application/public/img/organitation_structure/' . $company_id . '.png')){
					    				//if (file_exists($img_file) ){
					    					$img_file = '/application/public/img/organitation_structure/default.png';
					    				} ?>
					    				<img src="<?php echo $img_file ?>" rel="<?php echo file_exists($img_file) ?>" id="img_structure" >
					    			</div>
							    </div>
							</div>
				    	</div>
				    </div>
				    <div class="row">
						<div class="col-sm-offset-4 col-sm-4" style="text-align: center" >
					        <button type="submit" class="btn btn-warning " id="btn-save">SAVE <i class="glyphicon glyphicon-floppy-saved"></i></button>
					    </div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<section class="content">
       	<div class="row">
            <div class="modal  fade" id="box-eac" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <?php echo $box_eac ?>
            </div>
        </div>

        <div class="row">
            <div class="modal  fade" id="box-nace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <?php echo $box_nace ?>
            </div>
        </div>

        <div class="row">
            <div class="modal  fade" id="box-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <?php echo $box_agent ?>
            </div>
        </div>

    </section>
</div>

<?php
    $this->load->view('footer',$script); 
?>