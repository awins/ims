
<?php 
    $header['css'] = array('/application/public/bootstrap/plugins/datatables/jquery.dataTables.css');
    $footer['js'][] = '/application/public/bootstrap/plugins/datatables/jquery.dataTables.js';
    $header['css'][] = '/application/public/css/company.css';
    $footer['js'][] = '/application/public/js/company.js';

    $this->load->view('header',$header); 
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Company List
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Company</li>
        </ol>
    </section>

    <section class="content">
        
        <div class="row">
            <div class="col-sm-12">
                <a href="/company/input">
                    <button class="btn btn-primary pull-right" type="button"><i class="glyphicon glyphicon-plus" ></i>&nbsp;&nbsp;&nbsp;Input New</button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-body">
                    <table id="table-company" class="table table-bordered black">
                        <thead>
                          <tr>
                            <th style="width: 3%;  text-align: center" >No. </th>
                            <th style="width: 15%; text-align: center">Company Name</th>
                            <th style="width: 25%; text-align: center">Address</th>
                            <th style="width: 15%; text-align: center">EAC</th>
                            <th style="width: 25%; text-align: center">NACE</th>
                            <th style="width: 10%; text-align: center">Agent</th>
                            <th style="width: 7%; text-align: center">Action</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            
                            foreach ($companies as $value) { ?>
                                <tr rel="<?php echo $value->id ?>" >
                                    <td>
                                        <?php $no++; echo $no ?>
                                    </td>
                                    
                                    <td>
                                        <div style="text-align: center;font-weight: bold;color: blue">
                                            * <?php echo $value->company_initial ?> *
                                        </div>
                                        <?php echo $value->company_name ?>
                                        <br />
                                        (DIRUT: <?php echo $value->director_name ?>)
                                        <br />
                                        TELP: <?php echo $value->company_phone ?>
                                        <br />
                                    </td>
                                    <td style="text-align: justify">
                                        <?php echo $value->company_address_1  ?>
                                        <br />
                                        <?php echo $value->company_address_2  ?>
                                        <br />
                                        <?php echo $value->company_city. ', '. $value->company_province. ' '. $value->company_zip   ?>
                                        <br />
                                    </td>
                                    <td>
                                        <b><?php echo $value->eac ?></b>
                                        <br />
                                        <i><?php echo $value->eac_detail ?></i>
                                    </td>
                                    <td style="text-align: justify">
                                        <b><?php echo $value->nace ?></b>
                                        <br />
                                        <i><?php echo $value->nace_detail ?></i>
                                    </td>
                                    <td>
                                        <?php echo $value->agent_name ?>
                                        <br />
                                        <?php echo $value->agent_city ?>
                                    </td>
                                    <td>
                                        <a href="/company/update/<?php echo $value->company_id ?>">
                                            <button type="button" class="btn btn-xs btn-info " id="btn-edit"><i class="glyphicon glyphicon-pencil"></i></button>
                                        </a>
                                        <button type="button" class="btn btn-xs btn-danger btn-remove" rel="<?php echo $value->company_id ?>"  ><i class="glyphicon glyphicon-remove"></i></button>
                                        
                                    </td>
                                </tr>
                                <?php
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        
    </section>
</div><!-- /.content-wrapper -->

<?php 

$this->load->view('footer',$footer) ?>