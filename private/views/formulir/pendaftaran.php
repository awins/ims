<html>
<style type="text/css">
	body{
		font-family: calibri;
	}
	.blue-box {
		border: solid 0.1px rgb(65,113,156);
		background-color: rgb(91,155,213); 
		color: white;
		margin: auto;
		margin-bottom: 5px;
		padding: 2px;
		width: 712px;
		text-align: center;
	}
	.filling-text{
		text-align: left;
		margin-left: 5px;
		font-size: 11pt;
		height: 30px;
	}
	.filling-text span{
		display: inline-block;
	}
	.filling-text span.nomor{
		width: 15px;
	}
	.filling-text span.text{
		width: 250px;
	}

	.filling-text span.text-2{
		width: 200px;
	}
	.filling-text span.text-full{
		width: 100%;
		padding-left: 20px;
	}

	.filling-text span.checkbox {
		background: url('public/img/element/checkbox-empty.gif') no-repeat;
		padding-left: 25px;
		width: 120px;
		height: 25px;
	}
	.filling-text span.inline-dot {
		width: 120px;
	}


</style>
<body style="width: 730px;border: solid blue 0.1px;text-align: center">
	<img src="application/public/img/kop-surat.png" style="width:716px;margin: 10px 0" >
	<div style="font-size: 9pt;" class="blue-box">
		Untuk dapat mengetahui kondisi Organisasi anda / menerbitkan penawaran harga secara tepat / memulai proses sertifikasi,kami mohon kerjasama anda untuk mengisi formulir ini dengan lengkap sesuai petunjuk yang kami berikan. Selaku lembaga sertifikasi, kami menjaga kerahasiaan informasi yang anda berikan melalui formulir aplikasi ini
	</div>
	<div class="filling-text" >
		<span class="nomor" >1.</span>
		<span class="text">SKEMA SERTIFIKASI YANG DIINGINKAN</span>
		<span>:</span>
		<span class="checkbox">ISO 9001:2008</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >2.</span>
		<span class="text">STATUS APLIKASI SERTIFIKASI</span>
		<span>:</span>
		<span class="checkbox">Sertifikasi Awal</span>
		<span class="checkbox">Sertifikasi Ulang</span>
		<span class="checkbox">Renewal</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >3.</span>
		<span class="text">BIODATA PERUSAHAAN / ORGANISASI</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="nomor" >&nbsp;</span>
		<span class="checkbox">SINGLE SITE</span>
		<span class="checkbox">SISTER COMPANY</span>
		<span class="inline-dot">Jumlah :</span>
		<span class="checkbox">MULTI SITE</span>
		<span class="inline-dot">Jumlah :</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">NAMA ORGANISASI</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">ALAMAT ORGANISASI</span>
		<span>:</span>
	</div>

	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">NO.TLP dan FAX</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">EMAIL /WEB</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">JUMLAH KARYAWAN</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">JUMLAH SIFT KERJA</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">JUMLAH DIVISI</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">RUANG LINGKUP ORGANISASI</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >4.</span>
		<span class="text">PERWAKILAN MANAJEMEN</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">NAMA</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">Posisi/ Jabatan /Bagian</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">No. Telp / HP / Fax</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="text-2">Email</span>
		<span>:</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >5.</span>
		<span class="text-2">RUANG LINGKUP SERTIFIKASI</span>
		<span>:</span>
		<span class="text-full" >Tulis dengan jelas dan singkat ruang lingkup sertifikasi yang diajukan sesuai dengan bisnis proses yang dikendalikan oleh sistem manajemen yang akan disertifikasi.</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
	</div>
	<div class="filling-text" >
		<span class="text-full">Pemohon Harus Melampirkan Kelengkapan Aplikasi Sertifikasi Sebagai berikut :</span>
	</div>
	<div class="filling-text" style="font-weight: bold" >
		<span class="nomor" >&nbsp;</span>
		<span class="nomor" >a.</span>
		<span>Legalitas Pemohon</span>
	</div>
	<div class="filling-text" style="font-weight: bold">
		<span class="nomor" >&nbsp;</span>
		<span class="nomor" >b.</span>
		<span>Struktur Organisasi (lengkap dengan Nama)</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="nomor" >c.</span>
		<span>Manual dan Prosedur Mutu )*</span>
	</div>
	<div class="filling-text" >
		<span class="nomor" >&nbsp;</span>
		<span class="nomor" >d.</span>
		<span>Rekaman IA dan RTM )*</span>
	</div>
	<div class="filling-text" style="font-size: 10px" >
		<span class="nomor" >&nbsp;</span>
		<span>)* data ini diperbolehkan menyusul sebelum audit kecukupan</span>
	</div>

<p>PARAF</p>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>Flow Bisnis Proses )*</p>
<table width="100%">
<tbody>
<tr>
<td>
<p><strong>LEMBAR MULTI SITE ATAU SISTER COMPANY</strong></p>
<p><strong>FORM INI DIISI JIKA MENGGUNAKAN MULTI SITE ATAU SISTER COMPANY</strong></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ol start="6">
<li><strong>BIODATA PERUSAHAAN / ORGANISASI</strong></li>
</ol>
<p><strong>MULTI SITE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SITE KE : .......... </strong></p>
<p><strong>SISTER COMPANY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
<p>NAMA ORGANISASI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ..........................................................................................................</p>
<p>NAMA ORGANISASI INDUK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>ALAMAT ORGANISASI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>NO.TLP dan FAX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>EMAIL /WEB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>JUMLAH KARYAWAN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>JUMLAH SIFT KERJA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>JUMLAH DIVISI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>RUANG LINGKUP ORGANISASI : ...........................................................................................................</p>
<ol start="7">
<li><strong>PERWAKILAN MANAJEMEN</strong></li>
</ol>
<p>Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :............................................................................................................</p>
<p>Posisi / Jabatan /Bagian&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>No. Telp / HP / Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ...........................................................................................................</p>
<p>&nbsp;</p>
<ol start="8">
<li><strong>RUANG LINGKUP SERTIFIKASI</strong></li>
</ol>
<p>Tulis dengan jelas dan singkat ruang lingkup sertifikasi yang diajukan sesuai dengan bisnis proses yang dikendalikan oleh sistem manajemen yang akan disertifikasi.</p>
<p>: ..........................................................................................................................................................</p>
<p>..........................................................................................................................................................</p>
<p>&nbsp;</p>
<p>Pemohon Harus Melampirkan Kelengkapan Aplikasi Sertifikasi Sebagai berikut :</p>
<ol>
<li><strong>Legalitas Pemohon</strong></li>
<li><strong>Struktur Organisasi (lengkap dengan nama)</strong></li>
<li>Manual dan Prosedur Mutu )*</li>
<li>Rekaman IA dan RTM )*</li>
<li></li>
</ol>
<table width="100%">
<tbody>
<tr>
<td>
<p>)* data ini diperbolehkan menyusul sebelum audit kecukupan</p>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>
<p>PARAF</p>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>Flow Bisnis Proses )*</p>
<table width="100%">
<tbody>
<tr>
<td>
<p><strong>INFORMASI TAMBAHAN </strong></p>
</td>
</tr>
</tbody>
</table>
<p><strong>&nbsp;</strong> </p>
<table width="718">
<tbody>
<tr>
<td rowspan="2" width="38">
<p><strong>NO</strong></p>
</td>
<td rowspan="2" width="237">
<p><strong>PERTANYAAN</strong></p>
</td>
<td colspan="2" width="94">
<p><strong>PERNYATAAN</strong></p>
</td>
<td colspan="3" width="350">
<p><strong>Catatan</strong></p>
</td>
</tr>
<tr>
<td width="47">
<p><strong>YES</strong></p>
</td>
<td width="47">
<p><strong>NO</strong></p>
</td>
<td colspan="3" width="350">
<p><strong>&nbsp;</strong></p>
</td>
</tr>
<tr>
<td width="38">
<p><strong>9</strong></p>
</td>
<td width="237">
<p>Apakah Organisasi bagian dari organisasi lain?</p>
</td>
<td width="47">
<p>&nbsp;</p>
</td>
<td width="47">
<p>&nbsp;</p>
</td>
<td colspan="3" width="350">
<p>Jika ya Sebutkan :</p>
</td>
</tr>
<tr>
<td colspan="7" width="718">
<p><strong>&nbsp;</strong></p>
</td>
</tr>
<tr>
<td rowspan="4" width="38">
<p><strong>10</strong></p>
</td>
<td rowspan="4" width="237">
<p>Apakah organisasi sudah pernah mendapatkan sertifikasi Sistim Manajemen ?</p>
<p>&nbsp;</p>
</td>
<td rowspan="4" width="47">
<p>&nbsp;</p>
</td>
<td rowspan="4" width="47">
<p>&nbsp;</p>
</td>
<td colspan="3" width="350">
<p>Jika ya Sebutkan</p>
</td>
</tr>
<tr>
<td width="132">
<p>Nama LS</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Alamat LS</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Berlaku Sertifikat</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td colspan="5" width="501">
<p><strong>&nbsp;</strong></p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td rowspan="6" width="38">
<p><strong>11</strong></p>
</td>
<td rowspan="6" width="237">
<p>Dalam penerapan sistem manjemen di perusahaan anda apakah menggunakan jasa dari pihak konsultan &nbsp; (lembaga Konsultan - LK)?</p>
</td>
<td rowspan="6" width="47">
<p>&nbsp;</p>
</td>
<td rowspan="6" width="47">
<p>&nbsp;</p>
</td>
<td width="132">
<p>Jika ya Sebutkan</p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Nama LK</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Alamat LK</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>No.Tlp</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Nama Konsultan</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>No. Tlp Konsultan</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td colspan="5" width="501">
<p><strong>&nbsp;</strong></p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td rowspan="2" width="38">
<p><strong>12</strong></p>
</td>
<td rowspan="2" width="237">
<p>Apakah Klien Telah Melaksanakan Internal Audit ?</p>
</td>
<td rowspan="2" width="47">
<p>&nbsp;</p>
</td>
<td rowspan="2" width="47">
<p>&nbsp;</p>
</td>
<td width="132">
<p>JiKA Ya</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Waktu Pelaksanaan</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td colspan="5" width="501">
<p>&nbsp;</p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td rowspan="6" width="38">
<p><strong>13</strong></p>
</td>
<td rowspan="6" width="237">
<p>Apakah Audit Internal menggunakan Pihak Luar?</p>
</td>
<td rowspan="6" width="47">
<p>&nbsp;</p>
</td>
<td rowspan="6" width="47">
<p>&nbsp;</p>
</td>
<td width="132">
<p>Jika ya Sebutkan</p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Nama Lembaga</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Alamat Lembaga</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Nama Auditor</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Alamat Auditor</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>No . Tlp auditor</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td colspan="5" width="501">
<p><strong>&nbsp;</strong></p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td rowspan="3" width="38">
<p><strong>14</strong></p>
</td>
<td rowspan="3" width="237">
<p>Apakah Organisasi Pernah Melaksanakan Rapat Tinjauan Manajemen ?</p>
</td>
<td rowspan="3" width="47">
<p>&nbsp;</p>
</td>
<td rowspan="3" width="47">
<p>&nbsp;</p>
</td>
<td width="132">
<p>Jika Ya</p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>Waktu Pelaksanaan</p>
</td>
<td width="19">
<p>:</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="132">
<p>&nbsp;</p>
</td>
<td width="19">
<p>&nbsp;</p>
</td>
<td width="198">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Dengan ini kami nyatakan bahwa data yang kami isikan diatas adalah data yang sebenarnya tanpa ada unsur paksaan dari pihak manapun.</p>
<table width="100%">
<tbody>
<tr>
<td>
<p>JABATAN :</p>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>
<p>NAMA :</p>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>
<p>TANGGAL :</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>