<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 14">
<meta name=Originator content="Microsoft Word 14">
<link rel=File-List href="pendaftaran_files/filelist.xml">
<link rel=Edit-Time-Data href="pendaftaran_files/editdata.mso">

<link rel=dataStoreItem href="pendaftaran_files/item0001.xml"
target="pendaftaran_files/props002.xml">
<link rel=themeData href="pendaftaran_files/themedata.thmx">
<link rel=colorSchemeMapping href="pendaftaran_files/colorschememapping.xml">

<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520092929 1073786111 9 0 415 0;}
@font-face
	{font-family:"Segoe UI";
	panose-1:2 11 5 2 4 2 4 2 2 3;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520084737 -1073683329 41 0 479 0;}
@font-face
	{font-family:"French Script MT";
	panose-1:3 2 4 2 4 6 7 4 6 5;
	mso-font-alt:Zapfino;
	mso-font-charset:0;
	mso-generic-font-family:script;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-priority:99;
	mso-style-link:"Header Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 225.65pt right 451.3pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-priority:99;
	mso-style-link:"Footer Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 225.65pt right 451.3pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"Segoe UI","sans-serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-ansi-language:IN;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:Header;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:Footer;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Balloon Text";
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	font-family:"Segoe UI","sans-serif";
	mso-ascii-font-family:"Segoe UI";
	mso-hansi-font-family:"Segoe UI";
	mso-bidi-font-family:"Segoe UI";}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
.MsoPapDefault
	{mso-style-type:export-only;
	margin-bottom:8.0pt;
	line-height:107%;}
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("pendaftaran_files/header.html") fs;
	mso-footnote-continuation-separator:url("pendaftaran_files/header.html") fcs;
	mso-endnote-separator:url("pendaftaran_files/header.html") es;
	mso-endnote-continuation-separator:url("pendaftaran_files/header.html") ecs;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:99.25pt 42.45pt 92.15pt 49.6pt;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	border:solid #8EAADB 1.0pt;
	mso-border-themecolor:accent5;
	mso-border-themetint:153;
	mso-border-alt:solid #8EAADB .5pt;
	mso-border-themecolor:accent5;
	mso-border-themetint:153;
	padding:24.0pt 24.0pt 24.0pt 24.0pt;
	mso-header:url("pendaftaran_files/header.html") h1;
	mso-footer:url("pendaftaran_files/header.html") f1;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 @list l0
	{mso-list-id:386690250;
	mso-list-type:hybrid;
	mso-list-template-ids:-466731282 -1008571334 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l0:level1
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:64.35pt;
	text-indent:-.25in;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
@list l0:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:100.35pt;
	text-indent:-.25in;}
@list l0:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:136.35pt;
	text-indent:-9.0pt;}
@list l0:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:172.35pt;
	text-indent:-.25in;}
@list l0:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:208.35pt;
	text-indent:-.25in;}
@list l0:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:244.35pt;
	text-indent:-9.0pt;}
@list l0:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:280.35pt;
	text-indent:-.25in;}
@list l0:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:316.35pt;
	text-indent:-.25in;}
@list l0:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:352.35pt;
	text-indent:-9.0pt;}
@list l1
	{mso-list-id:619805478;
	mso-list-type:hybrid;
	mso-list-template-ids:1910427906 69271577 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l1:level1
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l1:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l1:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l1:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l1:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l1:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l1:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l1:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l1:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l2
	{mso-list-id:726075704;
	mso-list-type:hybrid;
	mso-list-template-ids:-1980834186 -798449882 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l2:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-weight:bold;}
@list l2:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l2:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l2:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l2:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l2:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l2:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l2:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l2:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l3
	{mso-list-id:1103913083;
	mso-list-type:hybrid;
	mso-list-template-ids:898259586 69271577 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l3:level1
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:170.6pt;
	text-indent:-.25in;}
@list l3:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:206.6pt;
	text-indent:-.25in;}
@list l3:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:242.6pt;
	text-indent:-9.0pt;}
@list l3:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:278.6pt;
	text-indent:-.25in;}
@list l3:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:314.6pt;
	text-indent:-.25in;}
@list l3:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:350.6pt;
	text-indent:-9.0pt;}
@list l3:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:386.6pt;
	text-indent:-.25in;}
@list l3:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:422.6pt;
	text-indent:-.25in;}
@list l3:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:458.6pt;
	text-indent:-9.0pt;}
@list l4
	{mso-list-id:1150292729;
	mso-list-type:hybrid;
	mso-list-template-ids:1400033120 1296889100 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l4:level1
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:170.6pt;
	text-indent:-.25in;}
@list l4:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:206.6pt;
	text-indent:-.25in;}
@list l4:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:242.6pt;
	text-indent:-9.0pt;}
@list l4:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:278.6pt;
	text-indent:-.25in;}
@list l4:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:314.6pt;
	text-indent:-.25in;}
@list l4:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:350.6pt;
	text-indent:-9.0pt;}
@list l4:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:386.6pt;
	text-indent:-.25in;}
@list l4:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:422.6pt;
	text-indent:-.25in;}
@list l4:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:458.6pt;
	text-indent:-9.0pt;}
@list l5
	{mso-list-id:1249271097;
	mso-list-type:hybrid;
	mso-list-template-ids:1483364040 69271577 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l5:level1
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l5:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l5:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l5:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l5:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l5:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l5:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l5:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l5:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l6
	{mso-list-id:1312755112;
	mso-list-type:hybrid;
	mso-list-template-ids:219041214 -1130078104 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l6:level1
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:71.85pt;
	text-indent:-43.5pt;}
@list l6:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:82.35pt;
	text-indent:-.25in;}
@list l6:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:118.35pt;
	text-indent:-9.0pt;}
@list l6:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:154.35pt;
	text-indent:-.25in;}
@list l6:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:190.35pt;
	text-indent:-.25in;}
@list l6:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:226.35pt;
	text-indent:-9.0pt;}
@list l6:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:262.35pt;
	text-indent:-.25in;}
@list l6:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:298.35pt;
	text-indent:-.25in;}
@list l6:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:334.35pt;
	text-indent:-9.0pt;}
@list l7
	{mso-list-id:1478886457;
	mso-list-type:hybrid;
	mso-list-template-ids:1564135052 775061686 69271555 69271557 69271553 69271555 69271557 69271553 69271555 69271557;}
@list l7:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:.75in;
	text-indent:-.25in;
	font-family:Symbol;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
@list l7:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:1.25in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l7:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:1.75in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l7:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:2.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l7:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:2.75in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l7:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:3.25in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l7:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:3.75in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l7:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:4.25in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l7:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:4.75in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l8
	{mso-list-id:1589651006;
	mso-list-type:hybrid;
	mso-list-template-ids:-1022461222 -807528116 69271555 69271557 69271553 69271555 69271557 69271553 69271555 69271557;}
@list l8:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
@list l8:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l8:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l8:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l8:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l8:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l8:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l8:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l8:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l9
	{mso-list-id:1765879624;
	mso-list-type:hybrid;
	mso-list-template-ids:905357384 -756655756 69271555 69271557 69271553 69271555 69271557 69271553 69271555 69271557;}
@list l9:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
@list l9:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l9:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l9:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l9:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l9:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l9:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l9:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l9:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l10
	{mso-list-id:1917671086;
	mso-list-type:hybrid;
	mso-list-template-ids:-1604787448 69271577 69271577 69271579 69271567 69271577 69271579 69271567 69271577 69271579;}
@list l10:level1
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l10:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l10:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l10:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l10:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l10:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l10:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l10:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l10:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin-top:0in;
	mso-para-margin-right:0in;
	mso-para-margin-bottom:8.0pt;
	mso-para-margin-left:0in;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
table.MsoTableGrid
	{mso-style-name:"Table Grid";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-priority:39;
	mso-style-unhide:no;
	border:solid windowtext 1.0pt;
	mso-border-alt:solid windowtext .5pt;
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-border-insideh:.5pt solid windowtext;
	mso-border-insidev:.5pt solid windowtext;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:IN;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2056"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US style='tab-interval:.5in'>

<div class=WordSection1>

<p class=MsoNormal><v:rect id="Rectangle_x0020_1" o:spid="_x0000_s1051"
 style='position:absolute;margin-left:-20.35pt;margin-top:-1.2pt;width:537.15pt;
 height:43.5pt;z-index:251657216;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
 mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;
 mso-width-relative:margin;mso-height-relative:margin;v-text-anchor:middle'
 o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQBdMVTgegIAAEQFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6L3aCpOuCOkWQosOA
oi3aDj0rshQbkEWNUmJnXz9KdtyiLXYYloNCmuQj+UTq4rJrDDso9DXYgk8nOWfKSihruyv4z6fr
L+ec+SBsKQxYVfCj8vxy9fnTReuWagYVmFIhIxDrl60reBWCW2aZl5VqhJ+AU5aMGrARgVTcZSWK
ltAbk83y/CxrAUuHIJX39PWqN/JVwtdayXCntVeBmYJTbSGdmM5tPLPVhVjuULiqlkMZ4h+qaERt
KekIdSWCYHus30E1tUTwoMNEQpOB1rVUqQfqZpq/6eaxEk6lXogc70aa/P+DlbeHe2R1SXfHmRUN
XdEDkSbszig2jfS0zi/J69Hd46B5EmOvncYm/lMXrEuUHkdKVReYpI9n57Ppeb7gTJJtsZjNF4nz
7CXaoQ/fFTQsCgVHyp6YFIcbHygjuZ5cSInV9PmTFI5GxRKMfVCa2qCMsxSdBkhtDLKDoKsXUiob
pr2pEqXqPy9y+sUmKckYkbQEGJF1bcyIPQDE4XyP3cMM/jFUpfkbg/O/FdYHjxEpM9gwBje1BfwI
wFBXQ+be/0RST01kKXTbjlyiuIXySPeN0C+Cd/K6JtpvhA/3AmnyaUdom8MdHdpAW3AYJM4qwN8f
fY/+NJBk5aylTSq4/7UXqDgzPyyN6rfpfB5XLynzxdcZKfjasn1tsftmA3RjNI5UXRKjfzAnUSM0
z7T065iVTMJKyl1wGfCkbEK/4fRsSLVeJzdaNyfCjX10MoJHguNYPXXPAt0we4Gm9hZOWyeWb0aw
942RFtb7ALpO8/nC60A9rWqaoeFZiW/Baz15vTx+qz8AAAD//wMAUEsDBBQABgAIAAAAIQBIHsok
3gAAAAoBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI/LbsIwEEX3lfoP1lTqDmwgCijEQRVSN5W6gPYD
hngaB/yIYockf1+zanczmqN7z5SHyRp2pz603klYLQUwcrVXrWskfH+9L3bAQkSn0HhHEmYKcKie
n0oslB/die7n2LAU4kKBEnSMXcF5qDVZDEvfkUu3H99bjGntG656HFO4NXwtRM4tti41aOzoqKm+
nQebSpBO82o7Hm+fevpoycxXGmYpX1+mtz2wSFP8g+Ghn9ShSk4XPzgVmJGwyMQ2oWlYZ8AegNhs
cmAXCbssB16V/P8L1S8AAAD//wMAUEsBAi0AFAAGAAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAA
AAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAA
CwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEAXTFU4HoCAABEBQAA
DgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54bWxQSwECLQAUAAYACAAAACEASB7KJN4AAAAK
AQAADwAAAAAAAAAAAAAAAADUBAAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA8wAAAN8FAAAA
AA==
" fillcolor="#5b9bd5 [3204]" strokecolor="#1f4d78 [1604]" strokeweight="1pt">
 <v:textbox>
  <![if !mso]>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td><![endif]>
    <div>
    <p class=MsoNormal align=center style='text-align:center'><b
    style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:8.0pt;
    mso-bidi-font-size:11.0pt;line-height:107%'>Untuk dapat mengetahui kondisi
    Organisasi anda / menerbitkan penawaran harga secara tepat / memulai proses
    sertifikasi,kami mohon kerjasama anda untuk mengisi formulir ini dengan lengkap
    sesuai petunjuk yang kami berikan. Selaku lembaga sertifikasi, kami menjaga
    kerahasiaan informasi yang anda berikan melalui formulir aplikasi ini<o:p></o:p></span></b></p>
    </div>
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]></v:textbox>
</v:rect><v:rect id="Rectangle_x0020_7" o:spid="_x0000_s1050" style='position:absolute;
 margin-left:196.4pt;margin-top:44.55pt;width:15pt;height:15.75pt;z-index:251665408;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-width-relative:margin;
 v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQApG4doWwIAAAkFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6r7aDdF2DOkWQosOA
oi3aDj2rspQYk0SNUuJkXz9KdpyiK3YYdpFFkY8Unx59cbmzhm0VhhZczauTkjPlJDStW9X8+9P1
py+chShcIww4VfO9Cvxy/vHDRednagJrMI1CRklcmHW+5usY/awoglwrK8IJeOXIqQGtiGTiqmhQ
dJTdmmJSlp+LDrDxCFKFQKdXvZPPc36tlYx3WgcVmak53S3mFfP6ktZifiFmKxR+3crhGuIfbmFF
66jomOpKRME22P6RyrYSIYCOJxJsAVq3UuUeqJuqfNPN41p4lXshcoIfaQr/L6283d4ja5uan3Hm
hKUneiDShFsZxc4SPZ0PM4p69Pc4WIG2qdedRpu+1AXbZUr3I6VqF5mkw+q8PC2JeEkueq9ycppy
FkewxxC/KrAsbWqOVDwTKbY3IfahhxDCpcv05fMu7o1KNzDuQWnqggpOMjrrRy0Nsq2gl29+VEPZ
HJkgujVmBFXvgUw8gIbYBFNZUyOwfA94rDZG54rg4gi0rQP8O1j38Yeu+15T2y/Q7OnREHo1By+v
WyLvRoR4L5DkS3zTSMY7WrSBruYw7DhbA/567zzFk6rIy1lH41Dz8HMjUHFmvjnS23k1nab5ycb0
9GxCBr72vLz2uI1dAvFe0fB7mbcpPprDViPYZ5rcRapKLuEk1a65jHgwlrEfU5p9qRaLHEYz40W8
cY9epuSJ1SSOp92zQD8oKJL0buEwOmL2Rkh9bEI6WGwi6Dar7MjrwDfNW9bp8G9IA/3azlHHP9j8
NwAAAP//AwBQSwMEFAAGAAgAAAAhAG6OsIbeAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj8FO
wzAQRO9I/IO1SNyo0xBSGrKpKgQnEBWFA0c3XpIIex3FbpL+Pe4Jjjs7mnlTbmZrxEiD7xwjLBcJ
COLa6Y4bhM+P55t7ED4o1so4JoQTedhUlxelKrSb+J3GfWhEDGFfKIQ2hL6Q0tctWeUXrieOv283
WBXiOTRSD2qK4dbINElyaVXHsaFVPT22VP/sjxbB7bqT2Q7rt/GVVl8vu5BMc/6EeH01bx9ABJrD
nxnO+BEdqsh0cEfWXhiE23Ua0QNCli1BREOWnoUDwuouB1mV8v+C6hcAAP//AwBQSwECLQAUAAYA
CAAAACEAtoM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBL
AQItABQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAAAC8BAABfcmVscy8ucmVsc1BL
AQItABQABgAIAAAAIQApG4doWwIAAAkFAAAOAAAAAAAAAAAAAAAAAC4CAABkcnMvZTJvRG9jLnht
bFBLAQItABQABgAIAAAAIQBujrCG3gAAAAkBAAAPAAAAAAAAAAAAAAAAALUEAABkcnMvZG93bnJl
di54bWxQSwUGAAAAAAQABADzAAAAwAUAAAAA
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><span
lang=IN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=IN><o:p>&nbsp;</o:p></span></p>

<br style='mso-ignore:vglayout' clear=ALL>

<p class=MsoListParagraphCxSpFirst style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;line-height:200%;mso-list:l2 level1 lfo10'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:200%;mso-bidi-font-family:Calibri;
mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><v:rect
 id="Rectangle_x0020_6" o:spid="_x0000_s1049" style='position:absolute;left:0;
 text-align:left;margin-left:412.35pt;margin-top:22.25pt;width:15pt;height:15.75pt;
 z-index:251663360;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-width-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQBH8v/1WwIAAAkFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6r7aDtFuDOkWQosOA
oi3aDj2rspQYk0SNUuJkXz9KdpyiK3YYdpFFkY8Unx59cbmzhm0VhhZczauTkjPlJDStW9X8+9P1
py+chShcIww4VfO9Cvxy/vHDRednagJrMI1CRklcmHW+5usY/awoglwrK8IJeOXIqQGtiGTiqmhQ
dJTdmmJSlmdFB9h4BKlCoNOr3snnOb/WSsY7rYOKzNSc7hbzinl9SWsxvxCzFQq/buVwDfEPt7Ci
dVR0THUlomAbbP9IZVuJEEDHEwm2AK1bqXIP1E1VvunmcS28yr0QOcGPNIX/l1bebu+RtU3Nzzhz
wtITPRBpwq2MYmeJns6HGUU9+nscrEDb1OtOo01f6oLtMqX7kVK1i0zSYXVenpZEvCQXvVc5OU05
iyPYY4hfFViWNjVHKp6JFNubEPvQQwjh0mX68nkX90alGxj3oDR1QQUnGZ31o5YG2VbQyzc/qqFs
jkwQ3Rozgqr3QCYeQENsgqmsqRFYvgc8Vhujc0VwcQTa1gH+Haz7+EPXfa+p7Rdo9vRoCL2ag5fX
LZF3I0K8F0jyJb5pJOMdLdpAV3MYdpytAX+9d57iSVXk5ayjcah5+LkRqDgz3xzp7byaTtP8ZGN6
+nlCBr72vLz2uI1dAvFe0fB7mbcpPprDViPYZ5rcRapKLuEk1a65jHgwlrEfU5p9qRaLHEYz40W8
cY9epuSJ1SSOp92zQD8oKJL0buEwOmL2Rkh9bEI6WGwi6Dar7MjrwDfNW9bp8G9IA/3azlHHP9j8
NwAAAP//AwBQSwMEFAAGAAgAAAAhAAEyxg/fAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj8tO
wzAQRfdI/IM1SOyoTZUmaZpJVSFYgagoLFi68ZBE+BHFbpL+Pe6KLmfm6M655XY2mo00+M5ZhMeF
AEa2dqqzDcLX58tDDswHaZXUzhLCmTxsq9ubUhbKTfaDxkNoWAyxvpAIbQh9wbmvWzLSL1xPNt5+
3GBkiOPQcDXIKYYbzZdCpNzIzsYPrezpqaX693AyCG7fnfVuWL+Pb5R9v+6DmOb0GfH+bt5tgAWa
wz8MF/2oDlV0OrqTVZ5phHyZZBFFSJIVsAjkq8viiJClAnhV8usG1R8AAAD//wMAUEsBAi0AFAAG
AAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQ
SwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAACwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQ
SwECLQAUAAYACAAAACEAR/L/9VsCAAAJBQAADgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54
bWxQSwECLQAUAAYACAAAACEAATLGD98AAAAJAQAADwAAAAAAAAAAAAAAAAC1BAAAZHJzL2Rvd25y
ZXYueG1sUEsFBgAAAAAEAAQA8wAAAMEFAAAAAA==
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><v:rect
 id="Rectangle_x0020_5" o:spid="_x0000_s1048" style='position:absolute;left:0;
 text-align:left;margin-left:305.9pt;margin-top:22.45pt;width:15pt;height:15.75pt;
 z-index:251661312;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-width-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQC0zweJXAIAAAkFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6r7aDZluDOkXQosOA
oi2aFj2rspQYk0SNUuJkXz9KdpyiK3YYdpFFkY8Unx59frGzhm0VhhZczauTkjPlJDStW9X86fH6
01fOQhSuEQacqvleBX4x//jhvPMzNYE1mEYhoyQuzDpf83WMflYUQa6VFeEEvHLk1IBWRDJxVTQo
OspuTTEpy89FB9h4BKlCoNOr3snnOb/WSsY7rYOKzNSc7hbzinl9SWsxPxezFQq/buVwDfEPt7Ci
dVR0THUlomAbbP9IZVuJEEDHEwm2AK1bqXIP1E1VvulmuRZe5V6InOBHmsL/Sytvt/fI2qbmU86c
sPRED0SacCuj2DTR0/kwo6ilv8fBCrRNve402vSlLtguU7ofKVW7yCQdVmfltCTiJbnovcpJzlkc
wR5D/KbAsrSpOVLxTKTY3oRIBSn0EEJGukxfPu/i3qh0A+MelKYuqOAko7N+1KVBthX08s2PKrVC
uXJkgujWmBFUvQcy8QAaYhNMZU2NwPI94LHaGJ0rgosj0LYO8O9g3ccfuu57TW2/QLOnR0Po1Ry8
vG6JvBsR4r1Aki/xTSMZ72jRBrqaw7DjbA34673zFE+qIi9nHY1DzcPPjUDFmfnuSG9n1elpmp9s
nE6/TMjA156X1x63sZdAvFc0/F7mbYqP5rDVCPaZJneRqpJLOEm1ay4jHozL2I8pzb5Ui0UOo5nx
It64pZcpeWI1ieNx9yzQDwqKJL1bOIyOmL0RUh+bkA4Wmwi6zSo78jrwTfOWBTP8G9JAv7Zz1PEP
Nv8NAAD//wMAUEsDBBQABgAIAAAAIQDq6/qm3gAAAAkBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI/N
TsMwEITvSLyDtUjcqBMUuTTNpqoQnEBULRw4uvE2ifBPZLtJ+va4Jzju7Gjmm2ozG81G8qF3FiFf
ZMDINk71tkX4+nx9eAIWorRKamcJ4UIBNvXtTSVL5Sa7p/EQW5ZCbCglQhfjUHIemo6MDAs3kE2/
k/NGxnT6lisvpxRuNH/MMsGN7G1q6ORAzx01P4ezQXC7/qK3fvUxvtPy+20Xs2kWL4j3d/N2DSzS
HP/McMVP6FAnpqM7WxWYRhB5ntAjQlGsgCWDKK7CEWEpCuB1xf8vqH8BAAD//wMAUEsBAi0AFAAG
AAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQ
SwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAACwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQ
SwECLQAUAAYACAAAACEAtM8HiVwCAAAJBQAADgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54
bWxQSwECLQAUAAYACAAAACEA6uv6pt4AAAAJAQAADwAAAAAAAAAAAAAAAAC2BAAAZHJzL2Rvd25y
ZXYueG1sUEsFBgAAAAAEAAQA8wAAAMEFAAAAAA==
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><v:rect
 id="Rectangle_x0020_4" o:spid="_x0000_s1047" style='position:absolute;left:0;
 text-align:left;margin-left:196.4pt;margin-top:20.95pt;width:15pt;height:15.75pt;
 z-index:251659264;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-width-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQDaJn8UWgIAAAkFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6r7aDdFuDOkXQosOA
oi2aFj2rspQYk0SNUuJkXz9KdpyiK3YYdpFFkY8Unx59frGzhm0VhhZczauTkjPlJDStW9X86fH6
01fOQhSuEQacqvleBX4x//jhvPMzNYE1mEYhoyQuzDpf83WMflYUQa6VFeEEvHLk1IBWRDJxVTQo
OspuTTEpy89FB9h4BKlCoNOr3snnOb/WSsY7rYOKzNSc7hbzinl9SWsxPxezFQq/buVwDfEPt7Ci
dVR0THUlomAbbP9IZVuJEEDHEwm2AK1bqXIP1E1VvulmuRZe5V6InOBHmsL/Sytvt/fI2qbmU86c
sPRED0SacCuj2DTR0/kwo6ilv8fBCrRNve402vSlLtguU7ofKVW7yCQdVmflaUnES3LRe5WT05Sz
OII9hvhNgWVpU3Ok4plIsb0JsQ89hBAuXaYvn3dxb1S6gXEPSlMXVHCS0Vk/6tIg2wp6+eZHNZTN
kQmiW2NGUPUeyMQDaIhNMJU1NQLL94DHamN0rggujkDbOsC/g3Uff+i67zW1/QLNnh4NoVdz8PK6
JfJuRIj3Akm+xDeNZLyjRRvoag7DjrM14K/3zlM8qYq8nHU0DjUPPzcCFWfmuyO9nVXTaZqfbExP
v0zIwNeel9cet7GXQLxXNPxe5m2Kj+aw1Qj2mSZ3kaqSSzhJtWsuIx6My9iPKc2+VItFDqOZ8SLe
uKWXKXliNYnjcfcs0A8KiiS9WziMjpi9EVIfm5AOFpsIus0qO/I68E3zlnU6/BvSQL+2c9TxDzb/
DQAA//8DAFBLAwQUAAYACAAAACEAxOUk6N4AAAAJAQAADwAAAGRycy9kb3ducmV2LnhtbEyPwU7D
MBBE70j8g7VI3KjTNGpJyKaqEJxAVBQOHN14SSLsdRS7Sfr3uCc47uxo5k25na0RIw2+c4ywXCQg
iGunO24QPj+e7+5B+KBYK+OYEM7kYVtdX5Wq0G7idxoPoRExhH2hENoQ+kJKX7dklV+4njj+vt1g
VYjn0Eg9qCmGWyPTJFlLqzqODa3q6bGl+udwsghu353NbsjfxlfafL3sQzLN6yfE25t59wAi0Bz+
zHDBj+hQRaajO7H2wiCs8jSiB4RsmYOIhiy9CEeEzSoDWZXy/4LqFwAA//8DAFBLAQItABQABgAI
AAAAIQC2gziS/gAAAOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsB
Ai0AFAAGAAgAAAAhADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsB
Ai0AFAAGAAgAAAAhANomfxRaAgAACQUAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1s
UEsBAi0AFAAGAAgAAAAhAMTlJOjeAAAACQEAAA8AAAAAAAAAAAAAAAAAtAQAAGRycy9kb3ducmV2
LnhtbFBLBQYAAAAABAAEAPMAAAC/BQAAAAA=
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:200%'>SKEMA SERTIFIKASI YANG DIINGINKAN</span></b><span
lang=IN style='font-size:9.0pt;mso-bidi-font-size:11.0pt;line-height:200%'><span
style='mso-tab-count:1'> </span></span><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:200%'>:<b style='mso-bidi-font-weight:
normal'> <span style='mso-tab-count:1'>              </span></b></span><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:9.0pt;
mso-bidi-font-size:11.0pt;line-height:200%'>ISO 9001:2008</span></b><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:200%'><span style='mso-tab-count:2'>                        </span><o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:.25in;mso-add-space:
auto;text-indent:-.25in;line-height:200%;mso-list:l2 level1 lfo10'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:200%;mso-bidi-font-family:Calibri;
mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:200%'>STATUS APLIKASI SERTIFIKASI</span></b><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:200%'><span
style='mso-tab-count:1'>  </span><span style='mso-tab-count:1'>                </span>:<span
style='mso-tab-count:1'>               </span></span><b style='mso-bidi-font-weight:
normal'><span lang=IN style='font-size:9.0pt;mso-bidi-font-size:11.0pt;
line-height:200%'>Sertifikasi Awal</span></b><b style='mso-bidi-font-weight:
normal'><span lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
line-height:200%'><span style='mso-tab-count:2'>                      </span></span></b><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:9.0pt;
mso-bidi-font-size:11.0pt;line-height:200%'>Sertifikasi Ulang</span></b><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:200%'><span style='mso-tab-count:1'>     </span><span
style='mso-tab-count:1'>                </span></span></b><b style='mso-bidi-font-weight:
normal'><span lang=IN style='font-size:9.0pt;mso-bidi-font-size:11.0pt;
line-height:200%'>Renewal</span></b><b style='mso-bidi-font-weight:normal'><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:200%'><o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpLast style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo10'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:107%;mso-bidi-font-family:Calibri;
mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><v:rect
 id="Rectangle_x0020_60" o:spid="_x0000_s1046" style='position:absolute;left:0;
 text-align:left;margin-left:342.35pt;margin-top:19.9pt;width:15pt;height:15.75pt;
 z-index:251686912;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-width-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQCybz21WwIAAAsFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6r7aDtFuDOkWQosOA
oi3aDj2rspQYk0SNUuJkXz9KdpyiK3YYdpFFkY8Unx59cbmzhm0VhhZczauTkjPlJDStW9X8+9P1
py+chShcIww4VfO9Cvxy/vHDRednagJrMI1CRklcmHW+5usY/awoglwrK8IJeOXIqQGtiGTiqmhQ
dJTdmmJSlmdFB9h4BKlCoNOr3snnOb/WSsY7rYOKzNSc7hbzinl9SWsxvxCzFQq/buVwDfEPt7Ci
dVR0THUlomAbbP9IZVuJEEDHEwm2AK1bqXIP1E1VvunmcS28yr0QOcGPNIX/l1bebu+RtU3Nz4ge
Jyy90QOxJtzKKEZnRFDnw4ziHv09Dlagbep2p9GmL/XBdpnU/Uiq2kUm6bA6L09Lyi3JRS9WTk5T
zuII9hjiVwWWpU3NkapnKsX2JsQ+9BBCuHSZvnzexb1R6QbGPShNfVDBSUZnBamlQbYV9PbNj2oo
myMTRLfGjKDqPZCJB9AQm2Aqq2oElu8Bj9XG6FwRXByBtnWAfwfrPv7Qdd9ravsFmj09G0Kv5+Dl
dUvk3YgQ7wWSgIlvGsp4R4s20NUchh1na8Bf752neNIVeTnraCBqHn5uBCrOzDdHijuvptM0QdmY
nn6ekIGvPS+vPW5jl0C8VzT+XuZtio/msNUI9plmd5Gqkks4SbVrLiMejGXsB5WmX6rFIofR1HgR
b9yjlyl5YjWJ42n3LNAPCookvVs4DI+YvRFSH5uQDhabCLrNKjvyOvBNE5d1Ovwd0ki/tnPU8R82
/w0AAP//AwBQSwMEFAAGAAgAAAAhAIRmQ4jeAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj0FP
wzAMhe9I/IfISNxYOobarTSdJgQnEBODA8esMW1F41RJ1nb/HvfEbrbf0/P3iu1kOzGgD60jBctF
AgKpcqalWsHX58vdGkSImozuHKGCMwbYltdXhc6NG+kDh0OsBYdQyLWCJsY+lzJUDVodFq5HYu3H
easjr76WxuuRw20n75MklVa3xB8a3eNTg9Xv4WQVuH177nZ+8z68Yfb9uo/JOKXPSt3eTLtHEBGn
+G+GGZ/RoWSmozuRCaJTkK4fMrYqWG24Ahuy5Xw4zsMKZFnIywblHwAAAP//AwBQSwECLQAUAAYA
CAAAACEAtoM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBL
AQItABQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAAAC8BAABfcmVscy8ucmVsc1BL
AQItABQABgAIAAAAIQCybz21WwIAAAsFAAAOAAAAAAAAAAAAAAAAAC4CAABkcnMvZTJvRG9jLnht
bFBLAQItABQABgAIAAAAIQCEZkOI3gAAAAkBAAAPAAAAAAAAAAAAAAAAALUEAABkcnMvZG93bnJl
di54bWxQSwUGAAAAAAQABADzAAAAwAUAAAAA
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><v:rect
 id="Rectangle_x0020_18" o:spid="_x0000_s1045" style='position:absolute;left:0;
 text-align:left;margin-left:124.5pt;margin-top:20.55pt;width:15pt;height:15.75pt;
 z-index:251668480;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-width-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQBylmvHWwIAAAsFAAAOAAAAZHJzL2Uyb0RvYy54bWysVE1v2zAMvQ/YfxB0X20H6bYGdYogRYcB
RVv0Az2rspQYk0SNUuJkv36U7DhFV+ww7CKTIh8pPpI+v9hZw7YKQwuu5tVJyZlyEprWrWr+9Hj1
6StnIQrXCANO1XyvAr+Yf/xw3vmZmsAaTKOQURAXZp2v+TpGPyuKINfKinACXjkyakArIqm4KhoU
HUW3ppiU5eeiA2w8glQh0O1lb+TzHF9rJeOt1kFFZmpOb4v5xHy+pLOYn4vZCoVft3J4hviHV1jR
Oko6hroUUbANtn+Esq1ECKDjiQRbgNatVLkGqqYq31TzsBZe5VqInOBHmsL/CytvtnfI2oZ6R51y
wlKP7ok14VZGMbojgjofZuT34O9w0AKJqdqdRpu+VAfbZVL3I6lqF5mky+qsPC2Jekkm6lg5OU0x
iyPYY4jfFFiWhJojZc9Uiu11iL3rwYVw6TF9+izFvVHpBcbdK011UMJJRucJUkuDbCuo982Pakib
PRNEt8aMoOo9kIkH0OCbYCpP1Qgs3wMes43eOSO4OAJt6wD/Dta9/6HqvtZU9gs0e2obQj/Pwcur
lsi7FiHeCaQBJr5pKeMtHdpAV3MYJM7WgL/eu0/+NFdk5ayjhah5+LkRqDgz3x1N3Fk1naYNysr0
9MuEFHxteXltcRu7BOK9ovX3MovJP5qDqBHsM+3uImUlk3CSctdcRjwoy9gvKm2/VItFdqOt8SJe
uwcvU/DEahqOx92zQD9MUKTRu4HD8ojZm0HqfRPSwWITQbd5yo68DnzTxuU5Hf4OaaVf69nr+A+b
/wYAAP//AwBQSwMEFAAGAAgAAAAhAF9j2yreAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj8FO
wzAQRO9I/IO1SNyok6hKaMimqhCcQFQUDhzdeEki4nVku0n697gnOM7OaPZNtV3MICZyvreMkK4S
EMSN1T23CJ8fz3f3IHxQrNVgmRDO5GFbX19VqtR25neaDqEVsYR9qRC6EMZSSt90ZJRf2ZE4et/W
GRWidK3UTs2x3AwyS5JcGtVz/NCpkR47an4OJ4Ng9/152LnN2/RKxdfLPiTzkj8h3t4suwcQgZbw
F4YLfkSHOjId7Ym1FwNCtt7ELQFhnaYgYiArLocjQpHlIOtK/l9Q/wIAAP//AwBQSwECLQAUAAYA
CAAAACEAtoM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBL
AQItABQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAAAC8BAABfcmVscy8ucmVsc1BL
AQItABQABgAIAAAAIQBylmvHWwIAAAsFAAAOAAAAAAAAAAAAAAAAAC4CAABkcnMvZTJvRG9jLnht
bFBLAQItABQABgAIAAAAIQBfY9sq3gAAAAkBAAAPAAAAAAAAAAAAAAAAALUEAABkcnMvZG93bnJl
di54bWxQSwUGAAAAAAQABADzAAAAwAUAAAAA
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><v:rect
 id="Rectangle_x0020_13" o:spid="_x0000_s1044" style='position:absolute;left:0;
 text-align:left;margin-left:18pt;margin-top:20.55pt;width:15pt;height:15.75pt;
 z-index:251667456;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-width-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQAoel2nXAIAAAsFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6r7azdFuDOkWQosOA
oi3aDj2rspQYk0SNUuJkXz9KdpyiK3YYdpFF8T1SfCJ9frGzhm0VhhZczauTkjPlJDStW9X8++PV
hy+chShcIww4VfO9Cvxi/v7deednagJrMI1CRkFcmHW+5usY/awoglwrK8IJeOXIqQGtiGTiqmhQ
dBTdmmJSlp+KDrDxCFKFQKeXvZPPc3ytlYy3WgcVmak53S3mFfP6nNZifi5mKxR+3crhGuIfbmFF
6yjpGOpSRME22P4RyrYSIYCOJxJsAVq3UuUaqJqqfFXNw1p4lWshcYIfZQr/L6y82d4haxt6u4+c
OWHpje5JNeFWRjE6I4E6H2aEe/B3OFiBtqnanUabvlQH22VR96OoaheZpMPqrDwtSXpJLnqxcnKa
YhZHsscQvyqwLG1qjpQ9Sym21yH20AOEeOkyffq8i3uj0g2Mu1ea6qCEk8zOHaSWBtlW0Ns3P6oh
bUYmim6NGUnVWyQTD6QBm2gqd9VILN8iHrON6JwRXByJtnWAfyfrHn+ouq81lf0MzZ6eDaHv5+Dl
VUviXYsQ7wRSA5PeNJTxlhZtoKs5DDvO1oC/3jpPeOor8nLW0UDUPPzcCFScmW+OOu6smk7TBGVj
evp5Qga+9Dy/9LiNXQLpXtH4e5m3CR/NYasR7BPN7iJlJZdwknLXXEY8GMvYDypNv1SLRYbR1HgR
r92Dlyl4UjU1x+PuSaAfOihS693AYXjE7FUj9djEdLDYRNBt7rKjroPeNHG5T4e/Qxrpl3ZGHf9h
898AAAD//wMAUEsDBBQABgAIAAAAIQCqQ4jf3AAAAAcBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI/B
TsMwEETvSPyDtUjcqJOCXAjZVBWCE4iKwoGjGy9JhL2OYjdJ/x73RI+jGc28Kdezs2KkIXSeEfJF
BoK49qbjBuHr8+XmHkSImo22ngnhSAHW1eVFqQvjJ/6gcRcbkUo4FBqhjbEvpAx1S06Hhe+Jk/fj
B6djkkMjzaCnVO6sXGaZkk53nBZa3dNTS/Xv7uAQ/LY72s3w8D6+0er7dRuzaVbPiNdX8+YRRKQ5
/ofhhJ/QoUpMe39gE4RFuFXpSkS4y3MQyVcnvUdYLRXIqpTn/NUfAAAA//8DAFBLAQItABQABgAI
AAAAIQC2gziS/gAAAOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsB
Ai0AFAAGAAgAAAAhADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsB
Ai0AFAAGAAgAAAAhACh6XadcAgAACwUAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1s
UEsBAi0AFAAGAAgAAAAhAKpDiN/cAAAABwEAAA8AAAAAAAAAAAAAAAAAtgQAAGRycy9kb3ducmV2
LnhtbFBLBQYAAAAABAAEAPMAAAC/BQAAAAA=
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:107%'>BIODATA PERUSAHAAN / ORGANISASI<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-indent:.5in'><b style='mso-bidi-font-weight:
normal'><span lang=IN>SINGLE SITE <span style='mso-tab-count:2'>                       </span>SISTER
COMPANY<span style='mso-tab-count:1'>            </span>Jumlah : ........<span
style='mso-tab-count:2'>                   </span>MULTI SITE<span
style='mso-tab-count:1'>         </span>Jumlah: .........<o:p></o:p></span></b></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>NAMA ORGANISASI<span style='mso-tab-count:1'>                     </span>
: <span style='mso-tab-count:1 dotted'>.......................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>ALAMAT ORGANISASI<span style='mso-tab-count:1'>                  </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN><span style='mso-tab-count:1'>                                                              </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>NO.TLP dan FAX<span style='mso-tab-count:1'>                              </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>EMAIL /WEB<span style='mso-tab-count:1'>                                     </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>JUMLAH KARYAWAN<span style='mso-tab-count:1'>                    </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>JUMLAH SIFT KERJA<span style='mso-tab-count:1'>                      </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>JUMLAH DIVISI<span style='mso-tab-count:1'>                                </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>RUANG LINGKUP ORGANISASI<span style='mso-tab-count:1'> </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo10;tab-stops:center 141.75pt dotted 467.8pt'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='mso-bidi-font-family:
Calibri;mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><b
style='mso-bidi-font-weight:normal'><span lang=IN>PERWAKILAN MANAJEMEN<o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>Nama<span
style='mso-tab-count:1'>                                         </span>:<span
style='mso-tab-count:1 dotted'>............................................................................................................ </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>Posisi/
Jabatan /Bagian<span style='mso-tab-count:1'>       </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>No.
Telp / HP / Fax<span style='mso-tab-count:1'>               </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>Email<span
style='mso-tab-count:1'>                                          </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:0in;mso-add-space:auto;
tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN style='font-size:1.0pt;
mso-bidi-font-size:11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo10'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='mso-bidi-font-family:
Calibri;mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><b
style='mso-bidi-font-weight:normal'><span lang=IN>RUANG LINGKUP SERTIFIKASI<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%'><span lang=IN>Tulis dengan jelas dan singkat ruang lingkup sertifikasi
yang diajukan sesuai dengan bisnis proses yang dikendalikan oleh sistem
manajemen yang akan disertifikasi.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%;tab-stops:center dotted 467.8pt'><span lang=IN>: <span style='mso-tab-count:
1 dotted'>.......................................................................................................................................................... </span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%;tab-stops:center dotted 467.8pt'><span lang=IN><span
style='mso-spacerun:yes'>  </span><span style='mso-tab-count:1 dotted'>.......................................................................................................................................................... </span></span></p>

<p class=MsoNormal><span lang=IN style='font-size:1.0pt;mso-bidi-font-size:
11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=IN style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;line-height:107%'>Pemohon Harus Melampirkan Kelengkapan Aplikasi
Sertifikasi Sebagai berikut :<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l3 level1 lfo5'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%;mso-bidi-font-family:Calibri;
mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%'>Legalitas
Pemohon<o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l3 level1 lfo5'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%;mso-bidi-font-family:Calibri;
mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%'>Struktur
Organisasi (lengkap dengan Nama)<o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l3 level1 lfo5'><![if !supportLists]><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%;
mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
style='mso-list:Ignore'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%'>Manual dan Prosedur Mutu )*<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l3 level1 lfo5'><![if !supportLists]><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%;
mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
style='mso-list:Ignore'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%'>Rekaman IA dan RTM )*<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l3 level1 lfo5'><![if !supportLists]><span
lang=IN style='mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
style='mso-list:Ignore'>e.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><v:rect id="Rectangle_x0020_16" o:spid="_x0000_s1043"
 style='position:absolute;left:0;text-align:left;margin-left:-22.5pt;
 margin-top:11.95pt;width:249.45pt;height:23.25pt;text-indent:0;z-index:251701248;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:margin;
 mso-height-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQA7kgw6eQIAAEkFAAAOAAAAZHJzL2Uyb0RvYy54bWysVMlu2zAQvRfoPxC8N7JcO4sQOTAcpCgQ
JEGSImeaIm2hJIclaUvu13dILVnqU9GLNPv6hpdXrVZkL5yvwZQ0P5lQIgyHqjabkv54vvlyTokP
zFRMgRElPQhPrxafP102thBT2IKqhCMYxPiisSXdhmCLLPN8KzTzJ2CFQaUEp1lA1m2yyrEGo2uV
TSeT06wBV1kHXHiP0utOSRcpvpSCh3spvQhElRRrC+nr0ncdv9nikhUbx+y25n0Z7B+q0Kw2mHQM
dc0CIztX/xVK19yBBxlOOOgMpKy5SD1gN/nkQzdPW2ZF6gWH4+04Jv//wvK7/YMjdYW7O6XEMI07
esSpMbNRgqAMB9RYX6Ddk31wPeeRjN220un4xz5Im4Z6GIcq2kA4Cr/mp+eTfE4JR930Yj49m8eg
2au3dT58E6BJJErqMH2aJdvf+tCZDiYxmYGbWimUs0KZdwKMGSVZLLgrMVHhoERn/Sgk9opFTVOC
hDKxUo7sGeKDcS5MGKpTBq2jm8Rso2N+zFGFvG+pt41uIqFvdJwcc3yfcfRIWcGE0VnXBtyxANXP
MXNnP3Tf9RzbD+267RY87HIN1QGX7qC7Bm/5TY2Tv2U+PDCH8MdDwZMO9/iRCpqSQk9RsgX3+5g8
2iMqUUtJg+dUUv9rx5ygRH03iNeLfDaL95eY2fxsiox7q1m/1ZidXgFuJMfHw/JERvugBlI60C94
+cuYFVXMcMxdUh7cwKxCd+b4dnCxXCYzvDnLwq15sjwGj3OOyHpuX5izPfwCAvcOhtNjxQcUdrbR
08ByF0DWCaJx0t1c+w3gvSaQ929LfBDe8snq9QVc/AEAAP//AwBQSwMEFAAGAAgAAAAhACTowzXe
AAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj81OwzAQhO9IvIO1SNxah5JCCdlUgIQQ6gFR4O7Y
2yQiXkex89O3xz2V26xmNPtNvp1tK0bqfeMY4WaZgCDWzjRcIXx/vS42IHxQbFTrmBCO5GFbXF7k
KjNu4k8a96ESsYR9phDqELpMSq9rssovXUccvYPrrQrx7CtpejXFctvKVZLcSasajh9q1dFLTfp3
P1iEH3d4nqwu+X08fjTD267XerNDvL6anx5BBJrDOQwn/IgORWQq3cDGixZhka7jloCwun0AEQPp
+iRKhPskBVnk8v+C4g8AAP//AwBQSwECLQAUAAYACAAAACEAtoM4kv4AAADhAQAAEwAAAAAAAAAA
AAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAAIQA4/SH/1gAAAJQBAAAL
AAAAAAAAAAAAAAAAAC8BAABfcmVscy8ucmVsc1BLAQItABQABgAIAAAAIQA7kgw6eQIAAEkFAAAO
AAAAAAAAAAAAAAAAAC4CAABkcnMvZTJvRG9jLnhtbFBLAQItABQABgAIAAAAIQAk6MM13gAAAAkB
AAAPAAAAAAAAAAAAAAAAANMEAABkcnMvZG93bnJldi54bWxQSwUGAAAAAAQABADzAAAA3gUAAAAA
" filled="f" stroked="f" strokeweight="1pt">
 <v:textbox>
  <![if !mso]>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td><![endif]>
    <div>
    <p class=MsoNormal align=center style='text-align:center'><span lang=IN
    style='font-size:6.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>)* </span><span
    lang=IN style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>data
    ini diperbolehkan menyusul sebelum audit kecukupan</span><span lang=IN
    style='font-size:3.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><o:p></o:p></span></p>
    </div>
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]></v:textbox>
</v:rect><v:group id="Group_x0020_39" o:spid="_x0000_s1040" style='position:absolute;
 left:0;text-align:left;margin-left:5.15pt;margin-top:30.1pt;width:87.75pt;
 height:49.5pt;z-index:251680768' coordsize="11144,6286" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQB7poRy+AIAAPEJAAAOAAAAZHJzL2Uyb0RvYy54bWzsVltP2zAUfp+0/2D5faQJbYGIFFUw0CQE
CJh4dh07iebYnu026X79jp0LDLppYhNPvKS+nOt3znfq45O2FmjDjK2UzHC8N8GISaryShYZ/np/
/ukQI+uIzIlQkmV4yyw+WXz8cNzolCWqVCJnBoERadNGZ7h0TqdRZGnJamL3lGYSLrkyNXGwNUWU
G9KA9VpEyWQyjxplcm0UZdbC6Vl3iRfBPueMumvOLXNIZBhic+Frwnflv9HimKSFIbqsaB8GeUUU
NakkOB1NnRFH0NpUL0zVFTXKKu72qKojxXlFWcgBsoknz7K5MGqtQy5F2hR6hAmgfYbTq83Sq82N
QVWe4f0jjCSpoUbBLYI9gNPoIgWZC6Pv9I3pD4pu5/Ntuan9L2SC2gDrdoSVtQ5ROIzjeDpNZhhR
uJsnh/NZjzstoTgv1Gj5+c+K0eA28tGNwTQaWsg+omT/DaW7kmgWwLcegR6lKTRRh9It9BaRhWAI
zgIwQW6EyaYWEPstRtPpwfxg1vXfTqDiw8lRJzDmS1JtrLtgqkZ+kWEDMYS2I5tL6yAIEB1EYOMR
6YIIK7cVzMcj5C3jUHMoTRK0A9vYqTBoQ4AnhFImXYgN7AVpr8YrIUbFeJeicLFPCJR6Wa/GAgtH
xckuxV89jhrBq5JuVK4rqcwuA/m30XMnP2Tf5ezTd+2qDY2eDNVaqXwLZTWqmwpW0/MKYL0k1t0Q
A2MAag2jzV3DhwvVZFj1K4xKZX7sOvfy0Hdwi1EDYyXD9vuaGIaR+CKhI4+ACn4Ohc10dpDAxjy9
WT29kev6VEFFYhiimoall3diWHKj6geYgEvvFa6IpOA7w9SZYXPqunEHM5Sy5TKIwezRxF3KO029
cY+zb5v79oEY3feWg668UgMLSPqsxTpZrynVcu0Ur0L/eaQ7XPsKACP9FHkLagJKL6gZ2sK7Bwr/
LTX76bSTlY+0fWflMET+Cyv331n51qwMf5/wrggTu38D+YfL031g8eNLbfETAAD//wMAUEsDBBQA
BgAIAAAAIQBR02Jz3wAAAAkBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI9Ba8JAEIXvhf6HZQq91U0i
ERuzEZG2JylUC8XbmB2TYHY3ZNck/vuOp/Y2j/d48718PZlWDNT7xlkF8SwCQbZ0urGVgu/D+8sS
hA9oNbbOkoIbeVgXjw85ZtqN9ouGfagEl1ifoYI6hC6T0pc1GfQz15Fl7+x6g4FlX0nd48jlppVJ
FC2kwcbyhxo72tZUXvZXo+BjxHEzj9+G3eW8vR0P6efPLialnp+mzQpEoCn8heGOz+hQMNPJXa32
omUdzTmpYBElIO7+MuUpJz7S1wRkkcv/C4pfAAAA//8DAFBLAQItABQABgAIAAAAIQC2gziS/gAA
AOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAh
ADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAh
AHumhHL4AgAA8QkAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1sUEsBAi0AFAAGAAgA
AAAhAFHTYnPfAAAACQEAAA8AAAAAAAAAAAAAAAAAUgUAAGRycy9kb3ducmV2LnhtbFBLBQYAAAAA
BAAEAPMAAABeBgAAAAA=
">
 <v:rect id="Rectangle_x0020_40" o:spid="_x0000_s1041" style='position:absolute;
  top:4476;width:11144;height:1810;visibility:visible;mso-wrap-style:square;
  v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEAYcd6EcMA
AADbAAAADwAAAGRycy9kb3ducmV2LnhtbERPXWvCMBR9H/gfwhX2NlOHG64aZSiCwphYB5tvl+ba
VJub2mS2/nvzMNjj4XxP552txJUaXzpWMBwkIIhzp0suFHztV09jED4ga6wck4IbeZjPeg9TTLVr
eUfXLBQihrBPUYEJoU6l9Lkhi37gauLIHV1jMUTYFFI32MZwW8nnJHmVFkuODQZrWhjKz9mvVfDy
2crt4pK9meP6Z7n5Pmzsx+mg1GO/e5+ACNSFf/Gfe60VjOL6+CX+ADm7AwAA//8DAFBLAQItABQA
BgAIAAAAIQDw94q7/QAAAOIBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1s
UEsBAi0AFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAAAAAAAAAAAAAAAALgEAAF9yZWxzLy5yZWxz
UEsBAi0AFAAGAAgAAAAhADMvBZ5BAAAAOQAAABAAAAAAAAAAAAAAAAAAKQIAAGRycy9zaGFwZXht
bC54bWxQSwECLQAUAAYACAAAACEAYcd6EcMAAADbAAAADwAAAAAAAAAAAAAAAACYAgAAZHJzL2Rv
d25yZXYueG1sUEsFBgAAAAAEAAQA9QAAAIgDAAAAAA==
" fillcolor="white [3201]" strokecolor="#4472c4 [3208]" strokeweight="1pt">
  <v:textbox>
   <![if !mso]>
   <table cellpadding=0 cellspacing=0 width="100%">
    <tr>
     <td><![endif]>
     <div>
     <p class=MsoNormal align=center style='text-align:center'><span lang=IN
     style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>PARAF<o:p></o:p></span></p>
     </div>
     <![if !mso]></td>
    </tr>
   </table>
   <![endif]></v:textbox>
 </v:rect><v:rect id="Rectangle_x0020_41" o:spid="_x0000_s1042" style='position:absolute;
  width:11144;height:4476;visibility:visible;mso-wrap-style:square;
  v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEADovfisYA
AADbAAAADwAAAGRycy9kb3ducmV2LnhtbESPQWvCQBSE70L/w/IKvenGoqVNXaUogkKxNBWst0f2
mY1m38bs1qT/3hUKPQ4z8w0zmXW2EhdqfOlYwXCQgCDOnS65ULD9WvafQfiArLFyTAp+ycNseteb
YKpdy590yUIhIoR9igpMCHUqpc8NWfQDVxNH7+AaiyHKppC6wTbCbSUfk+RJWiw5LhisaW4oP2U/
VsF408qP+Tl7MYfV92K926/t+3Gv1MN99/YKIlAX/sN/7ZVWMBrC7Uv8AXJ6BQAA//8DAFBLAQIt
ABQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10u
eG1sUEsBAi0AFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAAAAAAAAAAAAAAAALgEAAF9yZWxzLy5y
ZWxzUEsBAi0AFAAGAAgAAAAhADMvBZ5BAAAAOQAAABAAAAAAAAAAAAAAAAAAKQIAAGRycy9zaGFw
ZXhtbC54bWxQSwECLQAUAAYACAAAACEADovfisYAAADbAAAADwAAAAAAAAAAAAAAAACYAgAAZHJz
L2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA9QAAAIsDAAAAAA==
" fillcolor="white [3201]" strokecolor="#4472c4 [3208]" strokeweight="1pt">
  <v:textbox>
   <![if !mso]>
   <table cellpadding=0 cellspacing=0 width="100%">
    <tr>
     <td><![endif]>
     <div>
     <p class=MsoNormal align=center style='text-align:center'><span lang=IN
     style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>
     </div>
     <![if !mso]></td>
    </tr>
   </table>
   <![endif]></v:textbox>
 </v:rect></v:group><span lang=IN style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;line-height:115%'>Flow Bisnis Proses )*</span><span lang=IN
style='mso-fareast-language:IN;mso-no-proof:yes'> </span></p>

<p class=MsoNormal><v:rect id="Rectangle_x0020_31" o:spid="_x0000_s1039"
 style='position:absolute;margin-left:-20.25pt;margin-top:-3.4pt;width:537.15pt;
 height:32.5pt;z-index:251670528;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
 mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;
 mso-width-relative:margin;mso-height-relative:margin;v-text-anchor:middle'
 o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQDKv9/HfgIAAE0FAAAOAAAAZHJzL2Uyb0RvYy54bWysVMFu2zAMvQ/YPwi6r7azNOuCOkXQosOA
oivaDj0rshQbkEWNUmJnXz9KdtyiLXYY5oNMieQj+UTq/KJvDdsr9A3YkhcnOWfKSqgauy35z8fr
T2ec+SBsJQxYVfKD8vxi9fHDeeeWagY1mEohIxDrl50reR2CW2aZl7VqhT8BpywpNWArAm1xm1Uo
OkJvTTbL80XWAVYOQSrv6fRqUPJVwtdayfBDa68CMyWn3EJaMa2buGarc7HconB1I8c0xD9k0YrG
UtAJ6koEwXbYvIFqG4ngQYcTCW0GWjdSpRqomiJ/Vc1DLZxKtRA53k00+f8HK2/3d8iaquSfC86s
aOmO7ok1YbdGMTojgjrnl2T34O5w3HkSY7W9xjb+qQ7WJ1IPE6mqD0zS4eJsVpzlp5xJ0s2L2WK+
iKDZs7dDH74paFkUSo4UPnEp9jc+DKZHE/KL2QzxkxQORsUUjL1XmgqhiLPknVpIXRpke0GXL6RU
NhSDqhaVGo5Pc/rGfCaPlF0CjMi6MWbCHgFie77FHnId7aOrSh04Oed/S2xwnjxSZLBhcm4bC/ge
gKGqxsiD/ZGkgZrIUug3fbrkebSMJxuoDnTxCMNEeCevG2L/RvhwJ5BGgIaFxjr8oEUb6EoOo8RZ
Dfj7vfNoT51JWs46GqmS+187gYoz891Sz34t5vM4g2kzP/0yow2+1GxeauyuvQS6OGpLyi6J0T6Y
o6gR2iea/nWMSiphJcUuuQx43FyGYdTp/ZBqvU5mNHdOhBv74GQEjzzH7nrsnwS6sQUDNe8tHMdP
LF914mAbPS2sdwF0k9r0mdfxBmhmUyuN70t8FF7uk9XzK7j6AwAA//8DAFBLAwQUAAYACAAAACEA
DHuGvd0AAAAKAQAADwAAAGRycy9kb3ducmV2LnhtbEyPwW7CMBBE75X6D9ZW6g1soFAU4qAKqZdK
PUD7AUu8jQOxHcUOSf6+y6m9zWpHM2/y/egacaMu1sFrWMwVCPJlMLWvNHx/vc+2IGJCb7AJnjRM
FGFfPD7kmJkw+CPdTqkSHOJjhhpsSm0mZSwtOYzz0JLn30/oHCY+u0qaDgcOd41cKrWRDmvPDRZb
Olgqr6fecQnScVq8Dofrpx0/amqmC/WT1s9P49sORKIx/Znhjs/oUDDTOfTeRNFomL2oNVtZbHjC
3aBWK1ZnDevtEmSRy/8Til8AAAD//wMAUEsBAi0AFAAGAAgAAAAhALaDOJL+AAAA4QEAABMAAAAA
AAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAOP0h/9YAAACU
AQAACwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEAyr/fx34CAABN
BQAADgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54bWxQSwECLQAUAAYACAAAACEADHuGvd0A
AAAKAQAADwAAAAAAAAAAAAAAAADYBAAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA8wAAAOIF
AAAAAA==
" fillcolor="#5b9bd5 [3204]" strokecolor="#1f4d78 [1604]" strokeweight="1pt">
 <v:textbox>
  <![if !mso]>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td><![endif]>
    <div>
    <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:
    .0001pt;text-align:center'><b style='mso-bidi-font-weight:normal'><span
    lang=IN style='font-size:9.0pt;line-height:107%'>LEMBAR MULTI SITE ATAU
    SISTER COMPANY<o:p></o:p></span></b></p>
    <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:
    .0001pt;text-align:center'><b style='mso-bidi-font-weight:normal'><span
    lang=IN style='font-size:9.0pt;line-height:107%;color:red'>FORM INI DIISI
    JIKA MENGGUNAKAN MULTI SITE ATAU SISTER COMPANY<o:p></o:p></span></b></p>
    </div>
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]></v:textbox>
</v:rect><span lang=IN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:2.0in;line-height:normal'><span lang=IN
style='font-size:3.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<br style='mso-ignore:vglayout' clear=ALL>

<p class=MsoListParagraph style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo10'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='mso-bidi-font-family:
Calibri;mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><v:rect
 id="Rectangle_x0020_21" o:spid="_x0000_s1038" style='position:absolute;left:0;
 text-align:left;margin-left:18pt;margin-top:20.55pt;width:15pt;height:15.75pt;
 z-index:251695104;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-width-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQD55Id0XAIAAAsFAAAOAAAAZHJzL2Uyb0RvYy54bWysVE1v2zAMvQ/YfxB0X+wEzbYGdYqgRYcB
QVv0Az2rspQYk0SNUuJkv36U7DhFF+ww7CKLIh8pPj364nJnDdsqDA24io9HJWfKSagbt6r489PN
p6+chShcLQw4VfG9Cvxy/vHDRetnagJrMLVCRklcmLW+4usY/awoglwrK8IIvHLk1IBWRDJxVdQo
WspuTTEpy89FC1h7BKlCoNPrzsnnOb/WSsY7rYOKzFSc7hbzinl9TWsxvxCzFQq/bmR/DfEPt7Ci
cVR0SHUtomAbbP5IZRuJEEDHkQRbgNaNVLkH6mZcvuvmcS28yr0QOcEPNIX/l1bebu+RNXXFJ2PO
nLD0Rg/EmnAroxidEUGtDzOKe/T32FuBtqnbnUabvtQH22VS9wOpaheZpMPxeTktiXpJLnqxcjJN
OYsj2GOI3xRYljYVR6qeqRTbZYhd6CGEcOkyXfm8i3uj0g2Me1Ca+qCCk4zOClJXBtlW0NvXP3Ir
VDZHJohujBlA41MgEw+gPjbBVFbVACxPAY/VhuhcEVwcgLZxgH8H6y7+0HXXa2r7Feo9PRtCp+fg
5U1D5C1FiPcCScDENw1lvKNFG2grDv2OszXgr1PnKZ50RV7OWhqIioefG4GKM/PdkeLOx2dnaYKy
cTb9MiED33pe33rcxl4B8U6iotvlbYqP5rDVCPaFZneRqpJLOEm1Ky4jHoyr2A0qTb9Ui0UOo6nx
Ii7do5cpeWI1ieNp9yLQ9wqKJL1bOAyPmL0TUhebkA4Wmwi6ySo78trzTROXddr/HdJIv7Vz1PEf
Nv8NAAD//wMAUEsDBBQABgAIAAAAIQCqQ4jf3AAAAAcBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI/B
TsMwEETvSPyDtUjcqJOCXAjZVBWCE4iKwoGjGy9JhL2OYjdJ/x73RI+jGc28Kdezs2KkIXSeEfJF
BoK49qbjBuHr8+XmHkSImo22ngnhSAHW1eVFqQvjJ/6gcRcbkUo4FBqhjbEvpAx1S06Hhe+Jk/fj
B6djkkMjzaCnVO6sXGaZkk53nBZa3dNTS/Xv7uAQ/LY72s3w8D6+0er7dRuzaVbPiNdX8+YRRKQ5
/ofhhJ/QoUpMe39gE4RFuFXpSkS4y3MQyVcnvUdYLRXIqpTn/NUfAAAA//8DAFBLAQItABQABgAI
AAAAIQC2gziS/gAAAOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsB
Ai0AFAAGAAgAAAAhADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsB
Ai0AFAAGAAgAAAAhAPnkh3RcAgAACwUAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1s
UEsBAi0AFAAGAAgAAAAhAKpDiN/cAAAABwEAAA8AAAAAAAAAAAAAAAAAtgQAAGRycy9kb3ducmV2
LnhtbFBLBQYAAAAABAAEAPMAAAC/BQAAAAA=
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><b
style='mso-bidi-font-weight:normal'><span lang=IN>BIODATA PERUSAHAAN / ORGANISASI<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-indent:.5in'><v:rect id="Rectangle_x0020_20"
 o:spid="_x0000_s1037" style='position:absolute;left:0;text-align:left;
 margin-left:18pt;margin-top:20.55pt;width:15pt;height:15.75pt;z-index:251696128;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-width-relative:margin;
 v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQAt/vfcWwIAAAsFAAAOAAAAZHJzL2Uyb0RvYy54bWysVE1v2zAMvQ/YfxB0X20H6bYGdYogRYcB
RVv0Az2rspQYk0SNUuJkv36U7DhFV+ww7CJTIh8pPj36/GJnDdsqDC24mlcnJWfKSWhat6r50+PV
p6+chShcIww4VfO9Cvxi/vHDeednagJrMI1CRklcmHW+5usY/awoglwrK8IJeOXIqQGtiLTFVdGg
6Ci7NcWkLD8XHWDjEaQKgU4veyef5/xaKxlvtQ4qMlNzulvMK+b1Ja3F/FzMVij8upXDNcQ/3MKK
1lHRMdWliIJtsP0jlW0lQgAdTyTYArRupco9UDdV+aabh7XwKvdC5AQ/0hT+X1p5s71D1jY1nxA9
Tlh6o3tiTbiVUYzOiKDOhxnFPfg7HHaBzNTtTqNNX+qD7TKp+5FUtYtM0mF1Vp6WlFuSi16snJym
nMUR7DHEbwosS0bNkapnKsX2OsQ+9BBCuHSZvny24t6odAPj7pWmPqjgJKOzgtTSINsKevvmRzWU
zZEJoltjRlD1HsjEA2iITTCVVTUCy/eAx2pjdK4ILo5A2zrAv4N1H3/ouu81tf0CzZ6eDaHXc/Dy
qiXyrkWIdwJJwMQ3DWW8pUUb6GoOg8XZGvDXe+cpnnRFXs46Goiah58bgYoz892R4s6q6TRNUN5M
T78kveBrz8trj9vYJRDvFY2/l9lM8dEcTI1gn2l2F6kquYSTVLvmMuJhs4z9oNL0S7VY5DCaGi/i
tXvwMiVPrCZxPO6eBfpBQZGkdwOH4RGzN0LqYxPSwWITQbdZZUdeB75p4rJOh79DGunX+xx1/IfN
fwMAAP//AwBQSwMEFAAGAAgAAAAhAKpDiN/cAAAABwEAAA8AAABkcnMvZG93bnJldi54bWxMj8FO
wzAQRO9I/IO1SNyok4JcCNlUFYITiIrCgaMbL0mEvY5iN0n/HvdEj6MZzbwp17OzYqQhdJ4R8kUG
grj2puMG4evz5eYeRIiajbaeCeFIAdbV5UWpC+Mn/qBxFxuRSjgUGqGNsS+kDHVLToeF74mT9+MH
p2OSQyPNoKdU7qxcZpmSTnecFlrd01NL9e/u4BD8tjvazfDwPr7R6vt1G7NpVs+I11fz5hFEpDn+
h+GEn9ChSkx7f2AThEW4VelKRLjLcxDJVye9R1gtFciqlOf81R8AAAD//wMAUEsBAi0AFAAGAAgA
AAAhALaDOJL+AAAA4QEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwEC
LQAUAAYACAAAACEAOP0h/9YAAACUAQAACwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQSwEC
LQAUAAYACAAAACEALf733FsCAAALBQAADgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54bWxQ
SwECLQAUAAYACAAAACEAqkOI39wAAAAHAQAADwAAAAAAAAAAAAAAAAC1BAAAZHJzL2Rvd25yZXYu
eG1sUEsFBgAAAAAEAAQA8wAAAL4FAAAAAA==
" fillcolor="white [3201]" strokecolor="black [3200]" strokeweight="1pt"/><b
style='mso-bidi-font-weight:normal'><span lang=IN>MULTI<span
style='mso-spacerun:yes'>  </span>SITE <span style='mso-tab-count:1'>       </span><span
style='mso-tab-count:1'>                </span>SITE KE : ..........<span
style='mso-tab-count:1'> </span><o:p></o:p></span></b></p>

<p class=MsoNormal style='text-indent:.5in'><b style='mso-bidi-font-weight:
normal'><span lang=IN>SISTER COMPANY<span style='mso-tab-count:3'>                                            </span><o:p></o:p></span></b></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>NAMA ORGANISASI<span style='mso-tab-count:1'>                     </span>
: <span style='mso-tab-count:1 dotted'>.......................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>NAMA ORGANISASI INDUK<span style='mso-tab-count:1'>       </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>ALAMAT ORGANISASI<span style='mso-tab-count:1'>                  </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN><span style='mso-tab-count:1'>                                                              </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>NO.TLP dan FAX<span style='mso-tab-count:1'>                              </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>EMAIL /WEB<span style='mso-tab-count:1'>                                     </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>JUMLAH KARYAWAN<span style='mso-tab-count:1'>                    </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>JUMLAH SIFT KERJA<span style='mso-tab-count:1'>                      </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>JUMLAH DIVISI<span style='mso-tab-count:1'>                                </span>:
<span style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoNormal style='tab-stops:center 141.75pt dotted 467.8pt'><span
lang=IN>RUANG LINGKUP ORGANISASI<span style='mso-tab-count:1'> </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo10;tab-stops:center 141.75pt dotted 467.8pt'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='mso-bidi-font-family:
Calibri;mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><b
style='mso-bidi-font-weight:normal'><span lang=IN>PERWAKILAN MANAJEMEN<o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>Nama<span
style='mso-tab-count:1'>                                         </span>:<span
style='mso-tab-count:1 dotted'>............................................................................................................ </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>Posisi
/ Jabatan /Bagian<span style='mso-tab-count:1'>      </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>No.
Telp / HP / Fax<span style='mso-tab-count:1'>               </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:21.3pt;mso-add-space:
auto;line-height:150%;tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN>Email<span
style='mso-tab-count:1'>                                          </span>: <span
style='mso-tab-count:1 dotted'>........................................................................................................... </span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:0in;mso-add-space:auto;
tab-stops:center 141.75pt dotted 467.8pt'><span lang=IN style='font-size:5.0pt;
mso-bidi-font-size:11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo10'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='mso-bidi-font-family:
Calibri;mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><b
style='mso-bidi-font-weight:normal'><span lang=IN>RUANG LINGKUP SERTIFIKASI<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%'><span lang=IN>Tulis dengan jelas dan singkat ruang lingkup sertifikasi
yang diajukan sesuai dengan bisnis proses yang dikendalikan oleh sistem
manajemen yang akan disertifikasi.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%;tab-stops:center dotted 467.8pt'><span lang=IN>: <span style='mso-tab-count:
1 dotted'>.......................................................................................................................................................... </span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%;tab-stops:center dotted 467.8pt'><span lang=IN><span
style='mso-spacerun:yes'>  </span><span style='mso-tab-count:1 dotted'>.......................................................................................................................................................... </span></span></p>

<p class=MsoNormal><span lang=IN style='font-size:1.0pt;mso-bidi-font-size:
11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=IN>Pemohon Harus Melampirkan Kelengkapan Aplikasi
Sertifikasi Sebagai berikut :</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l4 level1 lfo11'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%;mso-bidi-font-family:Calibri;
mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%'>Legalitas
Pemohon<o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l4 level1 lfo11'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%;mso-bidi-font-family:Calibri;
mso-bidi-theme-font:minor-latin'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%'>Struktur
Organisasi (lengkap dengan nama)<o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l4 level1 lfo11'><![if !supportLists]><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%;
mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
style='mso-list:Ignore'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%'>Manual dan Prosedur Mutu )*<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l4 level1 lfo11'><![if !supportLists]><span
lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%;
mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
style='mso-list:Ignore'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=IN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:115%'>Rekaman IA dan RTM )*<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:35.45pt;mso-add-space:
auto;text-indent:-21.25pt;line-height:115%;mso-list:l4 level1 lfo11'><![if !supportLists]><span
lang=IN style='mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
style='mso-list:Ignore'>e.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><v:rect id="Rectangle_x0020_2" o:spid="_x0000_s1036"
 style='position:absolute;left:0;text-align:left;margin-left:-24.1pt;
 margin-top:12.35pt;width:249.45pt;height:23.25pt;text-indent:0;z-index:251699200;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:margin;
 mso-height-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQBQuA51eQIAAEcFAAAOAAAAZHJzL2Uyb0RvYy54bWysVFtP2zAUfp+0/2D5faTJKJeIFFVFTJMQ
IGDi2XXsNprt49luk+7X79hJQ2F9mvaS+NzPd/wdX113WpGtcL4BU9H8ZEKJMBzqxqwq+uPl9ssF
JT4wUzMFRlR0Jzy9nn3+dNXaUhSwBlULRzCJ8WVrK7oOwZZZ5vlaaOZPwAqDRglOs4CiW2W1Yy1m
1yorJpOzrAVXWwdceI/am95IZym/lIKHBym9CERVFHsL6evSdxm/2eyKlSvH7LrhQxvsH7rQrDFY
dEx1wwIjG9f8lUo33IEHGU446AykbLhIGBBNPvmA5nnNrEhYcDjejmPy/y8tv98+OtLUFS0oMUzj
FT3h0JhZKUGKOJ7W+hK9nu2jGySPx4i1k07HP6IgXRrpbhyp6ALhqPyan11M8iklHG3F5bQ4n8ak
2Vu0dT58E6BJPFTUYfU0Sba986F33bvEYgZuG6VQz0pl3ikwZ9RkseG+xXQKOyV67ychESk2VaQC
iWNioRzZMmQH41yYsO9OGfSOYRKrjYH5sUAV8gHS4BvDROLeGDg5Fvi+4hiRqoIJY7BuDLhjCeqf
Y+Xef4++xxzhh27ZpetNwKJmCfUOr9xBvwve8tsGJ3/HfHhkDsmPa4ILHR7wIxW0FYXhRMka3O9j
+uiPnEQrJS0uU0X9rw1zghL13SBbL/PT07h9STidnhcouEPL8tBiNnoBeCM5Ph2Wp2P0D2p/lA70
K+79PFZFEzMca1eUB7cXFqFfcnw5uJjPkxtunGXhzjxbHpPHOUdmvXSvzNmBfgGJew/7xWPlBxb2
vjHSwHwTQDaJom9zHW4AtzWRfHhZ4nNwKCevt/dv9gcAAP//AwBQSwMEFAAGAAgAAAAhAMrZxDTf
AAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj8tOwzAQRfdI/IM1SOxap1GgURqnAiSEUBeIAnvH
dpOo8TiynUf/nmFFdzOaozvnlvvF9mwyPnQOBWzWCTCDyukOGwHfX6+rHFiIErXsHRoBFxNgX93e
lLLQbsZPMx1jwygEQyEFtDEOBedBtcbKsHaDQbqdnLcy0uobrr2cKdz2PE2SR25lh/ShlYN5aY06
H0cr4MednmeranyfLh/d+HbwSuUHIe7vlqcdsGiW+A/Dnz6pQ0VOtRtRB9YLWGV5SqiANNsCIyB7
SGioBWw3KfCq5NcNql8AAAD//wMAUEsBAi0AFAAGAAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAA
AAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAA
CwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEAULgOdXkCAABHBQAA
DgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54bWxQSwECLQAUAAYACAAAACEAytnENN8AAAAJ
AQAADwAAAAAAAAAAAAAAAADTBAAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA8wAAAN8FAAAA
AA==
" filled="f" stroked="f" strokeweight="1pt">
 <v:textbox>
  <![if !mso]>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td><![endif]>
    <div>
    <p class=MsoNormal align=center style='text-align:center'><span lang=IN
    style='font-size:6.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>)* </span><span
    lang=IN style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>data
    ini diperbolehkan menyusul sebelum audit kecukupan</span><span lang=IN
    style='font-size:3.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><o:p></o:p></span></p>
    </div>
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]></v:textbox>
</v:rect><v:group id="Group_x0020_22" o:spid="_x0000_s1033" style='position:absolute;
 left:0;text-align:left;margin-left:17.9pt;margin-top:29.9pt;width:87.75pt;
 height:49.5pt;z-index:251697152;mso-width-relative:margin;
 mso-height-relative:margin' coordsize="11144,6286" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQDgEzSP9wIAAPEJAAAOAAAAZHJzL2Uyb0RvYy54bWzsVltP2zAUfp+0/2D5faQJ6YWIFFUw0CQE
CJh4dh3nojm2Z7tNu1+/Y+cCg2ya2MQTL64dn+t3znfq45NdzdGWaVNJkeLwYIIRE1RmlShS/PX+
/NMCI2OJyAiXgqV4zww+WX78cNyohEWylDxjGoERYZJGpbi0ViVBYGjJamIOpGICLnOpa2LhqIsg
06QB6zUPoslkFjRSZ0pLyoyBr2ftJV56+3nOqL3Oc8Ms4imG2KxftV/Xbg2WxyQpNFFlRbswyCui
qEklwOlg6oxYgja6emGqrqiWRub2gMo6kHleUeZzgGzCybNsLrTcKJ9LkTSFGmACaJ/h9Gqz9Gp7
o1GVpTiKMBKkhhp5twjOAE6jigRkLrS6Uze6+1C0J5fvLte1+4VM0M7Duh9gZTuLKHwMwzCOoylG
FO5m0WI27XCnJRTnhRotP/9ZMejdBi66IZhGQQuZR5TMv6F0VxLFPPjGIdCjdNijdAu9RUTBGYoO
W6S83ACTSQwg9luM4ng+m0/b/hsFKlxMjlqBIV+SKG3sBZM1cpsUa4jBtx3ZXhoL1QHRXgQODpE2
CL+ze85cPFzcshxqDqWJvLZnGzvlGm0J8IRQyoT1sYE9L+3U8orzQTEcU+Q2dAmBUifr1Jhn4aA4
GVP81eOg4b1KYQfluhJSjxnIvg2eW/k++zZnl77drXe+0Wd9tdYy20NZtWynglH0vAJYL4mxN0TD
GICBAaPNXsOSc9mkWHY7jEqpf4x9d/LQd3CLUQNjJcXm+4ZohhH/IqAjj4AKbg75QzydR3DQT2/W
T2/Epj6VUJEQhqiifuvkLe+3uZb1A0zAlfMKV0RQ8J1ianV/OLXtuIMZStlq5cVg9ihiL8Wdos64
w9m1zf3ugWjV9ZaFrrySPQtI8qzFWlmnKeRqY2Ve+f5zSLe4dhUARrop8hbUjEeoGffFBgr/LTW7
6TTKykfavrOyHyL/hZXzvlDvrHwrVvq/T3hX+IndvYHcw+Xp2bP48aW2/AkAAP//AwBQSwMEFAAG
AAgAAAAhAO/uNWHgAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj0FLw0AQhe+C/2EZwZvdpCES
02xKKeqpCLaC9LbNTpPQ7GzIbpP03zue9PQY3uO9b4r1bDsx4uBbRwriRQQCqXKmpVrB1+HtKQPh
gyajO0eo4IYe1uX9XaFz4yb6xHEfasEl5HOtoAmhz6X0VYNW+4Xrkdg7u8HqwOdQSzPoicttJ5dR
9CytbokXGt3jtsHqsr9aBe+TnjZJ/DruLuft7XhIP753MSr1+DBvViACzuEvDL/4jA4lM53clYwX
nYIkZfKgIH1hZX8ZxwmIEwfTLANZFvL/B+UPAAAA//8DAFBLAQItABQABgAIAAAAIQC2gziS/gAA
AOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAh
ADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAh
AOATNI/3AgAA8QkAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1sUEsBAi0AFAAGAAgA
AAAhAO/uNWHgAAAACQEAAA8AAAAAAAAAAAAAAAAAUQUAAGRycy9kb3ducmV2LnhtbFBLBQYAAAAA
BAAEAPMAAABeBgAAAAA=
">
 <v:rect id="Rectangle_x0020_23" o:spid="_x0000_s1034" style='position:absolute;
  top:4476;width:11144;height:1810;visibility:visible;mso-wrap-style:square;
  v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEATMoBxsYA
AADbAAAADwAAAGRycy9kb3ducmV2LnhtbESPQWvCQBSE7wX/w/IKvdVNlZYaXUUsgkKxNArq7ZF9
ZqPZt2l2a9J/3xUKPQ4z8w0zmXW2EldqfOlYwVM/AUGcO11yoWC3XT6+gvABWWPlmBT8kIfZtHc3
wVS7lj/pmoVCRAj7FBWYEOpUSp8bsuj7riaO3sk1FkOUTSF1g22E20oOkuRFWiw5LhisaWEov2Tf
VsHzppUfi69sZE6rw9t6f1zb9/NRqYf7bj4GEagL/+G/9korGAzh9iX+ADn9BQAA//8DAFBLAQIt
ABQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10u
eG1sUEsBAi0AFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAAAAAAAAAAAAAAAALgEAAF9yZWxzLy5y
ZWxzUEsBAi0AFAAGAAgAAAAhADMvBZ5BAAAAOQAAABAAAAAAAAAAAAAAAAAAKQIAAGRycy9zaGFw
ZXhtbC54bWxQSwECLQAUAAYACAAAACEATMoBxsYAAADbAAAADwAAAAAAAAAAAAAAAACYAgAAZHJz
L2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA9QAAAIsDAAAAAA==
" fillcolor="white [3201]" strokecolor="#4472c4 [3208]" strokeweight="1pt">
  <v:textbox>
   <![if !mso]>
   <table cellpadding=0 cellspacing=0 width="100%">
    <tr>
     <td><![endif]>
     <div>
     <p class=MsoNormal align=center style='text-align:center'><span lang=IN
     style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>PARAF<o:p></o:p></span></p>
     </div>
     <![if !mso]></td>
    </tr>
   </table>
   <![endif]></v:textbox>
 </v:rect><v:rect id="Rectangle_x0020_24" o:spid="_x0000_s1035" style='position:absolute;
  width:11144;height:4476;visibility:visible;mso-wrap-style:square;
  v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEAwyOZssYA
AADbAAAADwAAAGRycy9kb3ducmV2LnhtbESPQWvCQBSE7wX/w/IKvdVNxZYaXUUsgkKxNArq7ZF9
ZqPZt2l2a9J/3xUKPQ4z8w0zmXW2EldqfOlYwVM/AUGcO11yoWC3XT6+gvABWWPlmBT8kIfZtHc3
wVS7lj/pmoVCRAj7FBWYEOpUSp8bsuj7riaO3sk1FkOUTSF1g22E20oOkuRFWiw5LhisaWEov2Tf
VsHzppUfi69sZE6rw9t6f1zb9/NRqYf7bj4GEagL/+G/9korGAzh9iX+ADn9BQAA//8DAFBLAQIt
ABQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10u
eG1sUEsBAi0AFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAAAAAAAAAAAAAAAALgEAAF9yZWxzLy5y
ZWxzUEsBAi0AFAAGAAgAAAAhADMvBZ5BAAAAOQAAABAAAAAAAAAAAAAAAAAAKQIAAGRycy9zaGFw
ZXhtbC54bWxQSwECLQAUAAYACAAAACEAwyOZssYAAADbAAAADwAAAAAAAAAAAAAAAACYAgAAZHJz
L2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA9QAAAIsDAAAAAA==
" fillcolor="white [3201]" strokecolor="#4472c4 [3208]" strokeweight="1pt">
  <v:textbox>
   <![if !mso]>
   <table cellpadding=0 cellspacing=0 width="100%">
    <tr>
     <td><![endif]>
     <div>
     <p class=MsoNormal align=center style='text-align:center'><span lang=IN
     style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>
     </div>
     <![if !mso]></td>
    </tr>
   </table>
   <![endif]></v:textbox>
 </v:rect></v:group><span lang=IN style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;line-height:115%'>Flow Bisnis Proses )*</span><span lang=IN
style='mso-fareast-language:IN;mso-no-proof:yes'> </span></p>

<p class=MsoNormal><v:rect id="Rectangle_x0020_33" o:spid="_x0000_s1032"
 style='position:absolute;margin-left:-20.5pt;margin-top:-4.5pt;width:537.15pt;
 height:20.75pt;z-index:251674624;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
 mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;
 mso-width-relative:margin;mso-height-relative:margin;v-text-anchor:middle'
 o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQAeQqK5fwIAAE0FAAAOAAAAZHJzL2Uyb0RvYy54bWysVFFP2zAQfp+0/2D5fSQtUEpFiioQ0yQE
CJh4dh27ieT4vLPbpPv1OztpQID2MC0Pztl3993d5ztfXHaNYTuFvgZb8MlRzpmyEsrabgr+8/nm
25wzH4QthQGrCr5Xnl8uv365aN1CTaECUypkBGL9onUFr0JwiyzzslKN8EfglCWlBmxEoC1ushJF
S+iNyaZ5PstawNIhSOU9nV73Sr5M+ForGe619iowU3DKLaQV07qOa7a8EIsNClfVckhD/EMWjagt
BR2hrkUQbIv1B6imlggedDiS0GSgdS1VqoGqmeTvqnmqhFOpFiLHu5Em//9g5d3uAVldFvz4mDMr
GrqjR2JN2I1RjM6IoNb5Bdk9uQccdp7EWG2nsYl/qoN1idT9SKrqApN0OJtPJ/P8lDNJuuns+Gx2
HkGzV2+HPnxX0LAoFBwpfOJS7G596E0PJuQXs+njJynsjYopGPuoNBVCEafJO7WQujLIdoIuX0ip
bJj0qkqUqj8+zekb8hk9UnYJMCLr2pgRewCI7fkRu891sI+uKnXg6Jz/LbHeefRIkcGG0bmpLeBn
AIaqGiL39geSemoiS6Fbd+mS59Eynqyh3NPFI/QT4Z28qYn9W+HDg0AaARoWGutwT4s20BYcBomz
CvD3Z+fRnjqTtJy1NFIF97+2AhVn5oelnj2fnJzEGUybk9OzKW3wrWb9VmO3zRXQxU3oAXEyidE+
mIOoEZoXmv5VjEoqYSXFLrgMeNhchX7U6f2QarVKZjR3ToRb++RkBI88x+567l4EuqEFAzXvHRzG
TyzedWJvGz0trLYBdJ3a9JXX4QZoZlMrDe9LfBTe7pPV6yu4/AMAAP//AwBQSwMEFAAGAAgAAAAh
AB4cjWXdAAAACgEAAA8AAABkcnMvZG93bnJldi54bWxMj81Ow0AMhO9IvMPKSNzaTRp+QzYVqsQF
iUMLD+BmTTZ0f6LspkneHvcEJ9vyaOabajs7K840xC54Bfk6A0G+CbrzrYKvz7fVE4iY0Gu0wZOC
hSJs6+urCksdJr+n8yG1gk18LFGBSakvpYyNIYdxHXry/PsOg8PE59BKPeDE5s7KTZY9SIed5wSD
Pe0MNafD6DgEab/kj9Pu9GHm947s8kPjotTtzfz6AiLRnP7EcMFndKiZ6RhGr6OwClZ3OXdJvDzz
vAiyoihAHBUUm3uQdSX/V6h/AQAA//8DAFBLAQItABQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAA
AAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAhADj9If/WAAAA
lAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAhAB5Corl/AgAA
TQUAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1sUEsBAi0AFAAGAAgAAAAhAB4cjWXd
AAAACgEAAA8AAAAAAAAAAAAAAAAA2QQAAGRycy9kb3ducmV2LnhtbFBLBQYAAAAABAAEAPMAAADj
BQAAAAA=
" fillcolor="#5b9bd5 [3204]" strokecolor="#1f4d78 [1604]" strokeweight="1pt">
 <v:textbox>
  <![if !mso]>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td><![endif]>
    <div>
    <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:
    .0001pt;text-align:center'><b style='mso-bidi-font-weight:normal'><span
    lang=IN style='font-size:12.0pt;line-height:107%'>INFORMASI TAMBAHAN <o:p></o:p></span></b></p>
    </div>
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]></v:textbox>
</v:rect><b style='mso-bidi-font-weight:normal'><span lang=IN><o:p>&nbsp;</o:p></span></b></p>

<br style='mso-ignore:vglayout' clear=ALL>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=718
 style='width:538.7pt;margin-left:-21.55pt;border-collapse:collapse;border:
 none;mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=38 rowspan=2 style='width:28.15pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>NO<o:p></o:p></span></b></p>
  </td>
  <td width=237 rowspan=2 style='width:177.4pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>PERTANYAAN<o:p></o:p></span></b></p>
  </td>
  <td width=94 colspan=2 style='width:70.85pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>PERNYATAAN<o:p></o:p></span></b></p>
  </td>
  <td width=350 colspan=3 style='width:262.3pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>Catatan<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=47 style='width:35.45pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>YES<o:p></o:p></span></b></p>
  </td>
  <td width=47 style='width:35.4pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>NO<o:p></o:p></span></b></p>
  </td>
  <td width=350 colspan=3 valign=top style='width:262.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b style='mso-bidi-font-weight:normal'><span lang=IN
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:40.3pt'>
  <td width=38 valign=top style='width:28.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:40.3pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN>9<o:p></o:p></span></b></p>
  </td>
  <td width=237 valign=top style='width:177.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:40.3pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Apakah Organisasi bagian dari organisasi lain?</span></p>
  </td>
  <td width=47 valign=top style='width:35.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:40.3pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=47 valign=top style='width:35.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:40.3pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=350 colspan=3 valign=top style='width:262.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:40.3pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Jika ya Sebutkan :</span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:9.75pt'>
  <td width=718 colspan=7 valign=top style='width:538.7pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:9.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b style='mso-bidi-font-weight:normal'><span lang=IN><o:p>&nbsp;</o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:17.0pt'>
  <td width=38 rowspan=4 valign=top style='width:28.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN>10<o:p></o:p></span></b></p>
  </td>
  <td width=237 rowspan=4 valign=top style='width:177.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Apakah organisasi sudah pernah <span
  style='mso-spacerun:yes'> </span>mendapatkan sertifikasi Sistim Manajemen ?</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=47 rowspan=4 valign=top style='width:35.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=47 rowspan=4 valign=top style='width:35.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=350 colspan=3 style='width:262.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Jika ya Sebutkan </span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Nama LS </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Alamat LS </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Berlaku Sertifikat </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:17.0pt'>
  <td width=501 colspan=5 style='width:375.7pt;border-top:none;border-left:
  solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
  solid white 1.0pt;mso-border-right-themecolor:background1;mso-border-top-alt:
  solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;mso-border-right-alt:
  solid white .5pt;mso-border-right-themecolor:background1;padding:0in 5.4pt 0in 5.4pt;
  height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b style='mso-bidi-font-weight:normal'><span lang=IN><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:17.0pt'>
  <td width=38 rowspan=6 valign=top style='width:28.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN>11<o:p></o:p></span></b></p>
  </td>
  <td width=237 rowspan=6 valign=top style='width:177.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Dalam penerapan sistem manjemen di perusahaan anda
  apakah menggunakan jasa dari pihak konsultan<span style='mso-spacerun:yes'> 
  </span>(lembaga Konsultan - LK)?</span></p>
  </td>
  <td width=47 rowspan=6 valign=top style='width:35.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=47 rowspan=6 valign=top style='width:35.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Jika ya Sebutkan </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Nama LK </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Alamat LK </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>No.Tlp</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Nama Konsultan</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>No. Tlp Konsultan</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15;height:17.0pt'>
  <td width=501 colspan=5 style='width:375.7pt;border-top:none;border-left:
  solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
  solid white 1.0pt;mso-border-right-themecolor:background1;mso-border-top-alt:
  solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;mso-border-right-alt:
  solid white .5pt;mso-border-right-themecolor:background1;padding:0in 5.4pt 0in 5.4pt;
  height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b style='mso-bidi-font-weight:normal'><span lang=IN><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16;height:17.0pt'>
  <td width=38 rowspan=2 valign=top style='width:28.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN>12<o:p></o:p></span></b></p>
  </td>
  <td width=237 rowspan=2 valign=top style='width:177.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Apakah Klien Telah Melaksanakan Internal Audit ? </span></p>
  </td>
  <td width=47 rowspan=2 valign=top style='width:35.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=47 rowspan=2 valign=top style='width:35.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>JiKA Ya </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:17;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Waktu Pelaksanaan</span></p>
  </td>
  <td width=19 style='width:14.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  mso-border-top-alt:windowtext;mso-border-left-alt:white;mso-border-left-themecolor:
  background1;mso-border-bottom-alt:windowtext;mso-border-right-alt:white;
  mso-border-right-themecolor:background1;mso-border-style-alt:solid;
  mso-border-width-alt:.5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:18;height:17.0pt'>
  <td width=501 colspan=5 valign=top style='width:375.7pt;border-top:none;
  border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
  border-right:solid white 1.0pt;mso-border-right-themecolor:background1;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:19;height:17.0pt'>
  <td width=38 rowspan=6 valign=top style='width:28.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN>13<o:p></o:p></span></b></p>
  </td>
  <td width=237 rowspan=6 valign=top style='width:177.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Apakah Audit Internal menggunakan Pihak Luar?</span></p>
  </td>
  <td width=47 rowspan=6 valign=top style='width:35.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=47 rowspan=6 valign=top style='width:35.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Jika ya Sebutkan</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:20;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Nama Lembaga</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:21;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Alamat Lembaga </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:22;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Nama Auditor</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:23;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Alamat Auditor</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:24;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>No . Tlp auditor</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:25;height:17.0pt'>
  <td width=501 colspan=5 style='width:375.7pt;border-top:none;border-left:
  solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
  solid white 1.0pt;mso-border-right-themecolor:background1;mso-border-top-alt:
  solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;mso-border-right-alt:
  solid white .5pt;mso-border-right-themecolor:background1;padding:0in 5.4pt 0in 5.4pt;
  height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b style='mso-bidi-font-weight:normal'><span lang=IN><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:26;height:17.0pt'>
  <td width=38 rowspan=3 valign=top style='width:28.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  lang=IN>14<o:p></o:p></span></b></p>
  </td>
  <td width=237 rowspan=3 valign=top style='width:177.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Apakah Organisasi Pernah Melaksanakan Rapat Tinjauan Manajemen
  ?</span></p>
  </td>
  <td width=47 rowspan=3 valign=top style='width:35.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=47 rowspan=3 valign=top style='width:35.4pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Jika Ya </span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:27;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>Waktu Pelaksanaan</span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN>:</span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:28;mso-yfti-lastrow:yes;height:17.0pt'>
  <td width=132 style='width:99.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid white 1.0pt;
  mso-border-right-themecolor:background1;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid white .5pt;mso-border-right-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=19 style='width:14.2pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid white 1.0pt;mso-border-right-themecolor:
  background1;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid white .5pt;mso-border-left-themecolor:background1;mso-border-top-alt:
  windowtext;mso-border-left-alt:white;mso-border-left-themecolor:background1;
  mso-border-bottom-alt:windowtext;mso-border-right-alt:white;mso-border-right-themecolor:
  background1;mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=198 valign=top style='width:148.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid white .5pt;
  mso-border-left-themecolor:background1;mso-border-alt:solid windowtext .5pt;
  mso-border-left-alt:solid white .5pt;mso-border-left-themecolor:background1;
  padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=IN><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoListParagraphCxSpFirst><span lang=IN style='font-size:1.0pt;
mso-bidi-font-size:11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;line-height:115%'><span lang=IN style='font-size:1.0pt;mso-bidi-font-size:
11.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:0in;mso-add-space:auto;
line-height:115%'><span lang=IN>Dengan ini kami nyatakan bahwa data yang kami
isikan diatas adalah data yang sebenarnya tanpa ada unsur paksaan dari pihak
manapun.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;line-height:115%'><v:group id="Group_x0020_8" o:spid="_x0000_s1026"
 style='position:absolute;left:0;text-align:left;margin-left:-20.3pt;
 margin-top:8.05pt;width:200.25pt;height:103.35pt;z-index:251689984;
 mso-width-relative:margin;mso-height-relative:margin' coordsize="23050,14363"
 o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQBXgjM5pAMAABcTAAAOAAAAZHJzL2Uyb0RvYy54bWzsWG1P2zAQ/j5p/8Hyd0jdpLSNCAjBQJMQ
IGDis3GcNlpie7ZLy379zk6cllKkjW3sRVUl147vxXe+5+6S/cNFXaEHrk0pRYbJbg8jLpjMSzHJ
8Kfb050RRsZSkdNKCp7hR27w4cH7d/tzlfK+nMoq5xqBEGHSucrw1FqVRpFhU15TsysVF7BZSF1T
C0s9iXJN5yC9rqJ+r7cXzaXOlZaMGwNPT5pNfODlFwVn9rIoDLeoyjCczfpR+/HejdHBPk0nmqpp
ydpj0FecoqalAKWdqBNqKZrp8pmoumRaGlnYXSbrSBZFybi3AawhvTVrzrScKW/LJJ1PVOcmcO2a
n14tll08XGlU5hmGixK0hivyWtHIuWauJilQnGl1o650+2DSrJy1i0LX7h/sQAvv1MfOqXxhEYOH
/UESk+EAIwZ7JCawHjRuZ1O4m2d8bPohcMa9AfxaziTei4f+wqKgOHLn647TLbpzt5Yl/aemwfpX
2fbCCWm6ZtsOIWRvRILdwUJ4miT91jfDeDxKPMmLBgJGzDIMzM+Fwc2UKu6jy7hLDs6Kg7OuATxU
TCqOkrhxmKfrIsGkBoLixTBIxqNhkjQWh1h4Yi/pjxOy5wg6e2mqtLFnXNbITTKs4QweV/Th3NiG
NJA4zZVwo5FVmZ+WVeUXLnXw40qjBwqgt4vg0hUqUOg4IX6CEX5mHyveSL3mBYDCRa/X7tPRUiZl
jAvro9hLAmrHVsAJOkayibGy4TAtrWPjPk11jL1NjE81dhxeqxS2Y65LIfUmAfnnTnNDH6xvbHbm
28X9wmeCcbjte5k/Qlho2aRNo9hpCddyTo29ohryJGATcr+9hKGo5DzDsp1hNJX666bnjh7iFnYx
mkPezbD5MqOaY1R9FBDRY0CES9R+kQyGfVjo1Z371R0xq48l3DKBKqOYnzp6W4VpoWV9ByXiyGmF
LSoY6M4wszosjm1TD6DIMH505MkgOStqz8WNYk6487MLu9vFHdWqjU0LUX0hA4pouhaiDa3jFPJo
ZmVR+vh1nm782t4AINolrLeAdrIB2h6hTj2kgO+F9pNsthHbg8E4Hmyx/fdhm/gKugzCLbh/P7iX
bcpbAR06iqaXW6nhvlz9INBJj4xd7waFF+rUpras3xsC1LdFvCvJTdvw54s48fV+C/T/ropv6M9f
0563r75bUDevCf9GZ07aN9fQQm6r9xtVb/j64l9T2y9F7vPO6tq38svvWQffAAAA//8DAFBLAwQU
AAYACAAAACEAomtdsOEAAAAKAQAADwAAAGRycy9kb3ducmV2LnhtbEyPQUvDQBCF74L/YRnBW7tJ
akMbsymlqKci2AribZudJqHZ2ZDdJum/dzzpcXgf732TbybbigF73zhSEM8jEEilMw1VCj6Pr7MV
CB80Gd06QgU39LAp7u9ynRk30gcOh1AJLiGfaQV1CF0mpS9rtNrPXYfE2dn1Vgc++0qaXo9cbluZ
RFEqrW6IF2rd4a7G8nK4WgVvox63i/hl2F/Ou9v3cfn+tY9RqceHafsMIuAU/mD41Wd1KNjp5K5k
vGgVzJ6ilFEO0hgEA4vleg3ipCBJkhXIIpf/Xyh+AAAA//8DAFBLAQItABQABgAIAAAAIQC2gziS
/gAAAOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgA
AAAhADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgA
AAAhAFeCMzmkAwAAFxMAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1sUEsBAi0AFAAG
AAgAAAAhAKJrXbDhAAAACgEAAA8AAAAAAAAAAAAAAAAA/gUAAGRycy9kb3ducmV2LnhtbFBLBQYA
AAAABAAEAPMAAAAMBwAAAAA=
">
 <v:group id="Group_x0020_42" o:spid="_x0000_s1027" style='position:absolute;
  width:23050;height:14363' coordorigin=",-1116" coordsize="11144,7398"
  o:gfxdata="UEsDBBQABgAIAAAAIQCi+E9TBAEAAOwBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRy07DMBBF
90j8g+UtShxYIISadEGAHSAoHzCyJ4lFYlseN7R/z7iPDaJIXdpzz5wre7HcTKOYMZL1rpbXZSUF
Ou2NdX0tP1dPxZ0UlMAZGL3DWm6R5LK5vFistgFJMO2olkNK4V4p0gNOQKUP6HjS+ThB4mPsVQD9
BT2qm6q6Vdq7hC4VKe+QzaLFDtZjEo8bvt43iTiSFA/7YHbVEkIYrYbETdXszC9LcTCUTO4yNNhA
V1xDqj8NeXJacOBe+WmiNSjeIKYXmLiGMpFUH/060AABOVj+vyp3najwXWc1lm2k5wx/ZPhY75TG
+G8XcT5X0DL2jvNxu9r9VfMDAAD//wMAUEsDBBQABgAIAAAAIQBsBtX+2AAAAJkBAAALAAAAX3Jl
bHMvLnJlbHOkkMFKAzEQhu+C7xDm7mbbg4g025vQa63gNSSz2eAmE2biat/eWBBc6c3jzM9838/s
9p9pVguyRMoGNl0PCrMjH3Mw8HJ6unsAJdVmb2fKaOCMAvvh9mZ3xNnWdiRTLKIaJYuBqdbyqLW4
CZOVjgrmlozEydY2ctDFujcbUG/7/l7zbwYMK6Y6eAN88FtQp3Np5j/sFB2T0Fg7R0nTOEZ3jao9
feQjLo1iOWA14FkuS8ala+VAX/du/ukNTO/lebIFX5tkZb9E8h01/08HvXro8AUAAP//AwBQSwME
FAAGAAgAAAAhADMvBZ5BAAAAOQAAABUAAABkcnMvZ3JvdXBzaGFwZXhtbC54bWyysa/IzVEoSy0q
zszPs1Uy1DNQUkjNS85PycxLt1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBL
AwQUAAYACAAAACEAYRDW68QAAADbAAAADwAAAGRycy9kb3ducmV2LnhtbESPQYvCMBSE78L+h/AW
vGlaVxepRhHZFQ8iqAvi7dE822LzUppsW/+9EQSPw8x8w8yXnSlFQ7UrLCuIhxEI4tTqgjMFf6ff
wRSE88gaS8uk4E4OlouP3hwTbVs+UHP0mQgQdgkqyL2vEildmpNBN7QVcfCutjbog6wzqWtsA9yU
chRF39JgwWEhx4rWOaW3479RsGmxXX3FP83udl3fL6fJ/ryLSan+Z7eagfDU+Xf41d5qBeMRPL+E
HyAXDwAAAP//AwBQSwECLQAUAAYACAAAACEAovhPUwQBAADsAQAAEwAAAAAAAAAAAAAAAAAAAAAA
W0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAAIQBsBtX+2AAAAJkBAAALAAAAAAAAAAAA
AAAAADUBAABfcmVscy8ucmVsc1BLAQItABQABgAIAAAAIQAzLwWeQQAAADkAAAAVAAAAAAAAAAAA
AAAAADYCAABkcnMvZ3JvdXBzaGFwZXhtbC54bWxQSwECLQAUAAYACAAAACEAYRDW68QAAADbAAAA
DwAAAAAAAAAAAAAAAACqAgAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA+gAAAJsDAAAAAA==
">
  <v:rect id="Rectangle_x0020_43" o:spid="_x0000_s1028" style='position:absolute;
   top:4987;width:11144;height:1294;visibility:visible;mso-wrap-style:square;
   v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEAn58sXsIA
AADbAAAADwAAAGRycy9kb3ducmV2LnhtbESP0WoCMRRE3wv+Q7iCbzVrLaWsRhGpIH2odO0HXDbX
zeLmJiZR179vBMHHYWbOMPNlbztxoRBbxwom4wIEce10y42Cv/3m9RNETMgaO8ek4EYRlovByxxL
7a78S5cqNSJDOJaowKTkSyljbchiHDtPnL2DCxZTlqGROuA1w20n34riQ1psOS8Y9LQ2VB+rs1Xg
w8rvzJfZb/qfsP1uzlVrTjelRsN+NQORqE/P8KO91Qrep3D/kn+AXPwDAAD//wMAUEsBAi0AFAAG
AAgAAAAhAPD3irv9AAAA4gEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQ
SwECLQAUAAYACAAAACEAMd1fYdIAAACPAQAACwAAAAAAAAAAAAAAAAAuAQAAX3JlbHMvLnJlbHNQ
SwECLQAUAAYACAAAACEAMy8FnkEAAAA5AAAAEAAAAAAAAAAAAAAAAAApAgAAZHJzL3NoYXBleG1s
LnhtbFBLAQItABQABgAIAAAAIQCfnyxewgAAANsAAAAPAAAAAAAAAAAAAAAAAJgCAABkcnMvZG93
bnJldi54bWxQSwUGAAAAAAQABAD1AAAAhwMAAAAA
" fillcolor="white [3201]" strokecolor="black [3213]" strokeweight="1pt">
   <v:textbox>
    <![if !mso]>
    <table cellpadding=0 cellspacing=0 width="100%">
     <tr>
      <td><![endif]>
      <div>
      <p class=MsoNormal><span lang=IN style='font-size:7.0pt;mso-bidi-font-size:
      11.0pt;line-height:107%'>JABATAN :<o:p></o:p></span></p>
      </div>
      <![if !mso]></td>
     </tr>
    </table>
    <![endif]></v:textbox>
  </v:rect><v:rect id="Rectangle_x0020_44" o:spid="_x0000_s1029" style='position:absolute;
   top:-1116;width:11144;height:5592;visibility:visible;mso-wrap-style:square;
   v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEAEHa0KsIA
AADbAAAADwAAAGRycy9kb3ducmV2LnhtbESP0WoCMRRE34X+Q7gF3zRrkVK2RhFRkD5YutsPuGxu
N0s3NzGJuv69EQQfh5k5wyxWg+3FmULsHCuYTQsQxI3THbcKfuvd5ANETMgae8ek4EoRVsuX0QJL
7S78Q+cqtSJDOJaowKTkSyljY8hinDpPnL0/FyymLEMrdcBLhttevhXFu7TYcV4w6GljqPmvTlaB
D2v/bbam3g2HsP9qT1Vnjlelxq/D+hNEoiE9w4/2XiuYz+H+Jf8AubwBAAD//wMAUEsBAi0AFAAG
AAgAAAAhAPD3irv9AAAA4gEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQ
SwECLQAUAAYACAAAACEAMd1fYdIAAACPAQAACwAAAAAAAAAAAAAAAAAuAQAAX3JlbHMvLnJlbHNQ
SwECLQAUAAYACAAAACEAMy8FnkEAAAA5AAAAEAAAAAAAAAAAAAAAAAApAgAAZHJzL3NoYXBleG1s
LnhtbFBLAQItABQABgAIAAAAIQAQdrQqwgAAANsAAAAPAAAAAAAAAAAAAAAAAJgCAABkcnMvZG93
bnJldi54bWxQSwUGAAAAAAQABAD1AAAAhwMAAAAA
" fillcolor="white [3201]" strokecolor="black [3213]" strokeweight="1pt">
   <v:textbox>
    <![if !mso]>
    <table cellpadding=0 cellspacing=0 width="100%">
     <tr>
      <td><![endif]>
      <div>
      <p class=MsoNormal align=center style='text-align:center'><span lang=IN
      style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></p>
      </div>
      <![if !mso]></td>
     </tr>
    </table>
    <![endif]></v:textbox>
  </v:rect></v:group><v:rect id="Rectangle_x0020_45" o:spid="_x0000_s1030"
  style='position:absolute;top:10191;width:23050;height:2076;visibility:visible;
  mso-wrap-style:square;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEAfzoRscIA
AADbAAAADwAAAGRycy9kb3ducmV2LnhtbESP0WoCMRRE3wv+Q7iCbzVrsaWsRhGpIH2odO0HXDbX
zeLmJiZR179vBMHHYWbOMPNlbztxoRBbxwom4wIEce10y42Cv/3m9RNETMgaO8ek4EYRlovByxxL
7a78S5cqNSJDOJaowKTkSyljbchiHDtPnL2DCxZTlqGROuA1w20n34riQ1psOS8Y9LQ2VB+rs1Xg
w8rvzJfZb/qfsP1uzlVrTjelRsN+NQORqE/P8KO91Qqm73D/kn+AXPwDAAD//wMAUEsBAi0AFAAG
AAgAAAAhAPD3irv9AAAA4gEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQ
SwECLQAUAAYACAAAACEAMd1fYdIAAACPAQAACwAAAAAAAAAAAAAAAAAuAQAAX3JlbHMvLnJlbHNQ
SwECLQAUAAYACAAAACEAMy8FnkEAAAA5AAAAEAAAAAAAAAAAAAAAAAApAgAAZHJzL3NoYXBleG1s
LnhtbFBLAQItABQABgAIAAAAIQB/OhGxwgAAANsAAAAPAAAAAAAAAAAAAAAAAJgCAABkcnMvZG93
bnJldi54bWxQSwUGAAAAAAQABAD1AAAAhwMAAAAA
" fillcolor="white [3201]" strokecolor="black [3213]" strokeweight="1pt">
  <v:textbox>
   <![if !mso]>
   <table cellpadding=0 cellspacing=0 width="100%">
    <tr>
     <td><![endif]>
     <div>
     <p class=MsoNormal><span lang=IN style='font-size:7.0pt;mso-bidi-font-size:
     11.0pt;line-height:107%'>NAMA : <o:p></o:p></span></p>
     </div>
     <![if !mso]></td>
    </tr>
   </table>
   <![endif]></v:textbox>
 </v:rect><v:rect id="Rectangle_x0020_3" o:spid="_x0000_s1031" style='position:absolute;
  width:23050;height:2075;visibility:visible;mso-wrap-style:square;
  v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEAMy8F
nkEAAAA5AAAAEAAAAGRycy9zaGFwZXhtbC54bWyysa/IzVEoSy0qzszPs1Uy1DNQUkjNS85PycxL
t1UKDXHTtVBSKC5JzEtJzMnPS7VVqkwtVrK34+UCAAAA//8DAFBLAwQUAAYACAAAACEAoGxwLMEA
AADaAAAADwAAAGRycy9kb3ducmV2LnhtbESP0WoCMRRE3wv+Q7iCbzVrCyKrUUQqSB8qrn7AZXPd
LG5uYhJ1/XtTKPRxmJkzzGLV207cKcTWsYLJuABBXDvdcqPgdNy+z0DEhKyxc0wKnhRhtRy8LbDU
7sEHulepERnCsUQFJiVfShlrQxbj2Hni7J1dsJiyDI3UAR8Zbjv5URRTabHlvGDQ08ZQfaluVoEP
a783X+a47X/C7ru5Va25PpUaDfv1HESiPv2H/9o7reATfq/kGyCXLwAAAP//AwBQSwECLQAUAAYA
CAAAACEA8PeKu/0AAADiAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBL
AQItABQABgAIAAAAIQAx3V9h0gAAAI8BAAALAAAAAAAAAAAAAAAAAC4BAABfcmVscy8ucmVsc1BL
AQItABQABgAIAAAAIQAzLwWeQQAAADkAAAAQAAAAAAAAAAAAAAAAACkCAABkcnMvc2hhcGV4bWwu
eG1sUEsBAi0AFAAGAAgAAAAhAKBscCzBAAAA2gAAAA8AAAAAAAAAAAAAAAAAmAIAAGRycy9kb3du
cmV2LnhtbFBLBQYAAAAABAAEAPUAAACGAwAAAAA=
" fillcolor="white [3201]" strokecolor="black [3213]" strokeweight="1pt">
  <v:textbox>
   <![if !mso]>
   <table cellpadding=0 cellspacing=0 width="100%">
    <tr>
     <td><![endif]>
     <div>
     <p class=MsoNormal><span lang=IN style='font-size:7.0pt;mso-bidi-font-size:
     11.0pt;line-height:107%'>TANGGAL :<span style='mso-spacerun:yes'>  </span><o:p></o:p></span></p>
     </div>
     <![if !mso]></td>
    </tr>
   </table>
   <![endif]></v:textbox>
 </v:rect></v:group><span lang=IN><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:35.45pt;mso-add-space:
auto;line-height:115%'><span lang=IN><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:35.45pt;mso-add-space:
auto;line-height:115%'><span lang=IN><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>
