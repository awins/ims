<?php
function isoType($num_type,$result = 'full'){
	if ($num_type == 1 && $result == 'full') return '9001:2008';
	if ($num_type == 1 && $result == 'number') return '9001';
	if ($num_type == 1 && $result == 'acronym') return 'Q';
	if ($num_type == 1 && $result == 'detail') return 'Sistem Manajemen Mutu ISO 9001:2008';
	if ($num_type == 1 && $result == 'name') return 'ISO 9001:2008';
	if ($num_type == 1 && $result == 'text') return 'Quality Management System';

	if ($num_type == 2 && $result == 'full') return '14001:2004';
	if ($num_type == 2 && $result == 'number') return '14001';
	if ($num_type == 2 && $result == 'acronym') return 'E';
	if ($num_type == 2 && $result == 'detail') return 'Sistem Manajemen Lingkungan ISO 14001:2004';
	if ($num_type == 2 && $result == 'name') return 'ISO 14001:2004';
	if ($num_type == 2 && $result == 'text') return 'Environmental Management System';

	if ($num_type == 3 && $result == 'full') return '18001:2007';
	if ($num_type == 3 && $result == 'number') return '18001';
	if ($num_type == 3 && $result == 'acronym') return 'O';
	if ($num_type == 3 && $result == 'detail') return 'Sistem Manajemen K3 OHSAS 18001:2007';
	if ($num_type == 3 && $result == 'name') return 'OHSAS 18001:2007';
	if ($num_type == 3 && $result == 'text') return 'Occupational Health & Safety Management System';

}

function provinceList(){
	$arr = array();
	$arr[] ='Nanggroe Aceh Darussalam';
	$arr[] ='Sumatra Utara';
	$arr[] ='Sumatra Barat';
	$arr[] ='Riau';
	$arr[] ='Kepulauan Riau';
	$arr[] ='Jambi';
	$arr[] ='Sumatra Selatan';
	$arr[] ='Bengkulu';
	$arr[] ='Lampung';
	$arr[] ='Bangka Belitung';
	$arr[] ='DKI Jakarta';
	$arr[] ='Jawa Barat';
	$arr[] ='Banten';
	$arr[] ='Jawa Tengah';
	$arr[] ='Daerah Istimewa Yogyakarta (DIY)';
	$arr[] ='Jawa Timur';
	$arr[] ='Bali';
	$arr[] ='Nusa Tenggara Barat';
	$arr[] ='Nusa Tenggara Timur';
	$arr[] ='Kalimantan Barat';
	$arr[] ='Kalimantan Tengah';
	$arr[] ='Kalimantan Selatan';
	$arr[] ='Kalimantan Timur';
	$arr[] ='Kalimantan Utara';
	$arr[] ='Sulawesi Utara';
	$arr[] ='Gorontalo';
	$arr[] ='Sulawesi Tengah';
	$arr[] ='Sulawesi Selatan';
	$arr[] ='Sulawesi Barat';
	$arr[] ='Sulawesi Tenggara';
	$arr[] ='Maluku';
	$arr[] ='Maluku utara';
	$arr[] ='Papua';
	$arr[] ='Papua Barat';
	return $arr;
}

function dateSuperScript($time,$show_year = true){
	$day = date('d',$time);
	$month = date('F',$time);
	$year = date('Y',$time);
	
	$ssc = null;
	$mod = ((int) $day) % 10;
	switch ($mod) {
		case 1:
			$ssc = 'st';
			break;
		case 2:
			$ssc = 'nd';
			break;
		case 3:
			$ssc = 'rd';
			break;
		default:
			$ssc = 'th';
			break;
	}

	$return= $month . ' ' . $day . '<sup>'.$ssc.'</sup>' ;
	if ($show_year){
		$return .= ', ' . $year ;
	} 
	return $return;

}

function indMonth($month){
	switch ($month) {
		case 1:		return 'Januari'; break;
		case 2:		return 'Pebruari'; break;
		case 3:		return 'Maret'; break;
		case 4:		return 'April'; break;
		case 5:		return 'Mei'; break;
		case 6:		return 'Juni'; break;
		case 7:		return 'Juli'; break;
		case 8:		return 'Agustus'; break;
		case 9:		return 'September'; break;
		case 10:	return 'Oktober'; break;
		case 11:	return 'Nopember'; break;
		case 12:	return 'Desember'; break;
		
		default: return null; break;
	}
}

function romawi($number){
	$number = (int) $number;
	switch ($number) {
		case 1 :
			return 'I';
			break;
		case 2 :
			return 'II';
			break;
		case 3 :
			return 'III';
			break;
		case 4 :
			return 'IV';
			break;
		case 5 :
			return 'V';
			break;
		case 6 :
			return 'VI';
			break;
		case 7 :
			return 'VII';
			break;
		case 8 :
			return 'VIII';
			break;
		case 9 :
			return 'IX';
			break;
		case 10 :
			return 'X';
			break;
		case 11 :
			return 'XI';
			break;
		case 12 :
			return 'XII';
			break;
		
		
		default:
			# code...
			break;
	}
}

function indDate($time){
	if (!is_numeric($time)) $time = strtotime($time);
	return date('d',$time). ' ' . (indMonth((int)date('m',$time))) . ' ' . date('Y',$time);
}

function getFileExtension($str)
{
	$arrSegments = explode('.', $str); // may contain multiple dots
    $strExtension = $arrSegments[count($arrSegments) - 1];
    $strExtension = strtolower($strExtension);
    return $strExtension;
}

function terbilang($bilangan){
	 $bilangan = abs($bilangan);

	$angka = array("","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas");
	 $temp = "";

	if($bilangan < 12){
	 $temp = " ".$angka[$bilangan];
	 }else if($bilangan < 20){
	 $temp = terbilang($bilangan - 10)." Belas";
	 }else if($bilangan < 100){
	 $temp = terbilang($bilangan/10)." Puluh".terbilang($bilangan%10);
	 }else if ($bilangan < 200) {
	 $temp = " seratus".terbilang($bilangan - 100);
	 }else if ($bilangan < 1000) {
	 $temp = terbilang($bilangan/100). " Ratus". terbilang($bilangan % 100);
	 }else if ($bilangan < 2000) {
	 $temp = " seribu". terbilang($bilangan - 1000);
	 }else if ($bilangan < 1000000) {
	 $temp = terbilang($bilangan/1000)." Ribu". terbilang($bilangan % 1000);
	 }else if ($bilangan < 1000000000) {
	 $temp = terbilang($bilangan/1000000)." Juta". terbilang($bilangan % 1000000);
	 }

	return $temp;
}

function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        //throw new InvalidArgumentException("$dirPath must be a directory");
        return;
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            self::deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

function cityAddress($company) {
	if (strlen($company->company_city) > 2) {
		$city = $company->company_city . ', ';
	}else {
		$city = null;
	}

	if (strlen($company->company_zip) > 2) {
		$zip = ' ' .$company->company_zip;
	}else {
		$zip = null;
	}


	return $city . $company->company_province . $zip . ' - Indonesia';
}

?>