<?php 

class Holidays extends My_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'holiday';
         $this->scheme = array(
            'id',
            'holiday_date',
            'holiday_note'
        );
        
    }

    function get_list($option = null){
        if (!isset($option['order_by']))  $option['order_by'] = "holiday_date desc" ;
        return parent::get_list($option);
    }
}
?>