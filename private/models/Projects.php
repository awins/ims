<?php 

class Projects extends My_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'project';
        $this->scheme = array(
            'id',
        	'company_id',
        	'project_number',
        	'request_date',
        	'proposal_number',
            'certificate_amount',
        	'surveilance_amount',
            'proposal_index'
        );
    }

    function new_project_number(){
        $option['select'] = 'max(project_number) `number`';
        $res = parent::get_list($option);
        return (int) $res[0]->number + 1; 
    }

    function new_proposal_index(){
        $option['select'] = 'max(proposal_index) `number`';
        $res = parent::get_list($option);
        return (int) $res[0]->number + 1; 
    }


}
?>