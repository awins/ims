<?php 

class Employees extends My_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'employee';
        
    }

    function login($user_name,$password){
        $opt['select'] = "*";
        $opt['from'] = "employee e";
        $opt['where']['user_name'] = $user_name;
        $opt['where']['password'] = $password;
        return parent::get_list($opt); 
    }

}
?>