<?php 

class Companies extends My_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'company';
        $this->scheme = array(
            'id',
            'agent_id',
            'company_initial',
            'company_name',
            'company_address_1',
            'company_address_2',
            'company_city',
            'company_province',
            'company_zip',
            'eac',
            'eac_detail',
            'nace',
            'nace_detail',
            'website_user',
            'website_password',
            'director_name',
            'company_phone',
            'company_scope',
            'company_document'
        );
    }

    function get_list($option = null){
        $option['select'] = "c.*, a.*,c.id company_id";
        $option['from'] = "company c";
        $option['order_by'] = "c.id desc";
        $option['join']['agent a'] = array('c.agent_id = a.id ','left');
        return parent::get_list($option);
    }

    function get_by_id($company_id){
        $option['where']["c.id"] = $company_id;
        $ret = $this->get_list($option);
        if (count($ret)>0){
            return $ret[0];
        }else{
            return false;
        }
    }


}
?>