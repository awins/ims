<?php 

class Certificates extends My_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'certificate';
        $this->scheme = array(
            'id',
            'project_id',
            'iso_type',
            'certificate_number',
            'certificate_number_full',
            'certificate_date',
            'main_assessment_date',
            'surveillance_1_date',
            'surveillance_2_date',
            'renewal_assessment_date'
        );
        
    }

    function get_list($option = null){
    	 $option['select'] = "c.id certificate_id,p.id project_id,com.id company_id, a.id agent_id, c.*,p.*,com.*,a.* ";
    	 $option['from'] = "certificate c";
    	 $option['join']['project p'] = array('c.project_id = p.id','left');
    	 $option['join']['company com'] = array('p.company_id = com.id','left');
    	 $option['join']['agent a'] = array('com.agent_id = a.id','left');
    	 return parent::get_list($option);

    }

    function get_by_project_id($project_id){
        $option['where']['p.id'] = $project_id;
        $data =  $this->get_list($option);        
        if (count($data) > 0){
            return $data;
        }else{
            return false;
        }
    }

    function get_by_id($id){
        $option['where']['c.id'] = $id;
        $data =  $this->get_list($option);        
        if (count($data) > 0){
            return $data[0];
        }else{
            return false;
        }
    }

    function last_certificate_number(){
        $option['select'] = 'max(certificate_number) `number`';
        $res = parent::get_list($option);
        return (int) $res[0]->number ; 
    }
}
?>