<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Holiday extends MY_Controller {

	public function index()
	{
		$this->load->model('holidays');
        $data['holidays'] = $this->holidays->get_list();
		$this->load->view('holiday/index',$data);
	}

	public function save(){
		if ($this->input->post('form_holiday_input')){
			if ($this->input->post('id')) $this->form_validation->set_rules('id', 'Holiday ID', 'xss_clean');
			$this->form_validation->set_rules('holiday_date', 'Date', 'required');
			$this->form_validation->set_rules('holiday_note', 'Note', 'required');
				
			if($this->form_validation->run()) {
				$post = $this->input->post();
				$date_arr = explode('/', $post['holiday_date'] );
				$post['holiday_date'] = $date_arr[2] .'-'.$date_arr[1].'-'.$date_arr[0];

		 		$this->load->model('holidays');
		 		if ($this->input->post('holiday_id')){
		 			$arr_id = array('id' => $this->input->post('holiday_id') );
		 		} else {
		 			$arr_id = array();
		 		}
				$this->holidays->save(array_merge($arr_id,$post) );
				redirect('/holiday');
		 		return true;
	 		} else{
	 			return false;
	 		}
	 	}
	}

	public function  input(){
	 	$data = null;
		$this->setFormConf();

		if ($this->input->post('form_holiday_input')){
			if ($this->save() ){
				return;
			}
	 	}

		$this->load->view('holiday/new');
	}

	public function  update($holiday_id){
	 	$data = null;
 		$this->load->model('holidays');
		$this->setFormConf();

		$view = null;
		if ($this->input->post('form_holiday_input')){
			if ($this->save()) {
				return;
			}
	 	}else{
		 	$view['holiday'] = $this->holidays->get_by_id($holiday_id);
	 	}
	 	
		$this->load->view('holiday/update',$view);
	}

	public function delete(){
		$holiday_id = $this->input->post('holiday_id');
		$this->load->model('holidays');
		$this->holidays->delete($holiday_id);
		
	}

}
