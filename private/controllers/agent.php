<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agent extends MY_Controller {

	public function index()
	{
		$this->load->model('agents');
        $data['agents'] = $this->agents->get_list();
		$this->load->view('agent/index',$data);

	}

	public function save(){
		if ($this->input->post('form_agent_input')){
			if ($this->input->post('id')) $this->form_validation->set_rules('id', 'Agent ID', 'xss_clean');
			$this->form_validation->set_rules('agent_name', 'Name', 'required');
			
			$this->form_validation->set_rules('agent_address', 'Name', 'xss_clean');
			$this->form_validation->set_rules('agent_company_name', 'Name', 'xss_clean');
			$this->form_validation->set_rules('agent_company_address', 'Name', 'xss_clean');
			$this->form_validation->set_rules('agent_email', 'Name', 'xss_clean');
			
			$this->form_validation->set_rules('agent_city', 'City', 'xss_clean');
			$this->form_validation->set_rules('agent_telp', 'Telp ', 'xss_clean');
			
			if($this->form_validation->run()) {
		 		$this->load->model('agents');
		 		if ($this->input->post('agent_id')){
		 			$arr_id = array('id' => $this->input->post('agent_id') );
		 		} else {
		 			$arr_id = array();
		 		}
				$this->agents->save(array_merge($arr_id,$this->input->post()) );
				redirect('/agent');
		 		return true;
	 		} else{
	 			return false;
	 		}
	 	}
	}
	
	public function  input(){
	 	$data = null;
		$this->setFormConf();

		if ($this->input->post('form_agent_input')){
			if ($this->save() ){
				return;
			}
	 	}

		$this->load->view('agent/new');
	}

	public function  update($agent_id){
	 	$data = null;
 		$this->load->model('agents');
		$this->setFormConf();

		$view = null;
		if ($this->input->post('form_agent_input')){
			if ($this->save()) {
				return;
			}
	 	}else{
		 	$view['agent'] = $this->agents->get_by_id($agent_id);
	 	}
	 	
		$this->load->view('agent/update',$view);
	}

	public function delete(){
		$agent_id = $this->input->post('agent_id');
		$this->load->model('agents');
		$this->agents->delete($agent_id);
		
	}

}
