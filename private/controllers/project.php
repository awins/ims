<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends MY_Controller {

	public function index()
	{
		$this->load->model('certificates');
        $data['companies'] = $this->certificates->get_list();
		$this->load->view('project/index',$data);

	}

	public function detail(){
		$project_id = $this->input->post('project_id');
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$this->load->view('project/detail',$data);
		return;
	}

	public function populate_date($certificate_date){
		if (!is_numeric($certificate_date)) $certificate_date = strtotime($certificate_date);
		$date = date('Y-m-d');

		$arr = array();
		$main_assessment = date('d-m-Y' ,strtotime($date . ' -14 day' ) );
		$surveillance_1 = date('d-m-Y' ,strtotime($main_assessment . ' +1 year' ) );
		$surveillance_2 = date('d-m-Y' ,strtotime($surveillance_1 . ' +1 year' ) );
		$renewal_assessment = date('d-m-Y' ,strtotime($surveillance_2 . ' +1 year' ) );

		$arr['main_assessment'] = $main_assessment;
		$arr['surveillance_1'] = $surveillance_1;
		$arr['surveillance_2'] = $surveillance_2;
		$arr['renewal_assessment'] = $renewal_assessment;

		echo json_encode($arr);
	}

	 public function  input(){
	 	$data = null;
		
		
	 	$this->load->model('projects');
	 	$this->load->model('companies');
	 	$data['company'] = $this->companies->get_list();

	 	$view['box_company'] = $this->load->view('modal/company',$data,true);
	 	$view['new_project_number'] = $this->projects->new_project_number();
	 	$view['new_proposal_index'] = $this->projects->new_proposal_index();

		$post_1 = $this->session->userdata('post_1');
	 	if (strlen($post_1['company_id']) > 0 ){
			$view['sel_company'] = $this->companies->get_by_id($post_1['company_id'])  ;
		}

		$this->load->view('project/new',$view);

	 }

	 public function new_step_1(){
		$post_1 = $this->session->userdata('post_1');
		$view = null;
	 	if (strlen($post_1['company_id']) > 0 ){
			$this->load->model('companies');
			$view['sel_company'] = $this->companies->get_by_id($post_1['company_id'])  ;
		}
		$this->load->view('project/new_step_1',$view);
	 }

	public function new_finish(){
		$result = array();

		$this->setFormConf();
		$post = $this->input->post();
		foreach ($post['iso_type'] as $key => $value) {
			$this->form_validation->set_rules('certificate_date['. $key .']', 'Certificate Date', 'required');
			$this->form_validation->set_rules('main_assessment_date['. $key .']', 'Main Ass. Date', 'required');
			$this->form_validation->set_rules('surveillance_1_date['. $key .']', 'Surv #1 Date', 'required');
			$this->form_validation->set_rules('surveillance_2_date['. $key .']', 'Surv #2 Date', 'required');
			$this->form_validation->set_rules('renewal_assessment_date['. $key .']', 'Surv #3 Date', 'required');
		}
		$view = null;

		if($this->form_validation->run()) {
			$this->load->model('certificates');
			foreach ($post['iso_type'] as $key => $value) {
				$post_1 = $this->session->userdata('post_1');

				$date_arr = explode('/', $post['certificate_date'][$key]);
				$post_2['certificate_date'] = $date_arr[2] .'-'.$date_arr[1].'-'.$date_arr[0];

				$date_arr = explode('/', $post['main_assessment_date'][$key]);
				$post_2['main_assessment_date'] = $date_arr[2] .'-'.$date_arr[1].'-'.$date_arr[0];

				$date_arr = explode('/', $post['surveillance_1_date'][$key]);
				$post_2['surveillance_1_date'] = $date_arr[2] .'-'.$date_arr[1].'-'.$date_arr[0];

				$date_arr = explode('/', $post['surveillance_2_date'][$key]);
				$post_2['surveillance_2_date'] = $date_arr[2] .'-'.$date_arr[1].'-'.$date_arr[0];

				$date_arr = explode('/', $post['renewal_assessment_date'][$key]);
				$post_2['renewal_assessment_date'] = $date_arr[2] .'-'.$date_arr[1].'-'.$date_arr[0];
				

				$post_2['iso_type'] = $value;
				$post_2['project_id'] = $post_1['project_id'];
				$post_2['certificate_number'] = $post['certificate_number'][$key];
				$post_2['certificate_number_full'] = $post['certificate_number_full'][$key];


				$this->certificates->save($post_2);


			}

			$project_id = $post_1['project_id'];
			$data['project'] = $this->certificates->get_by_project_id($project_id);
			$view['box_detail']	=$this->load->view('project/detail',$data,true);

			$result['status'] = '200';
			$html =	'project/new_finish';
			$this->session->unset_userdata('post_1');
			$this->session->unset_userdata('post_2');
		} else{
			$result['status'] = '100';
			$html =	'project/new_step_2';
		}

		$this->session->set_userdata(array('post_2' => $post));
		$result['html'] =	$this->load->view($html,$view,true);
		
		echo json_encode($result);

	}

	public function new_step_2(){
		$result = array();

		$this->setFormConf();
		$this->form_validation->set_rules('proposal_number', 'Proposal Number', 'required');
		$this->form_validation->set_rules('proposal_index', 'proposal_index', 'required');
		$this->form_validation->set_rules('request_date', 'Request Date', 'required');
		$this->form_validation->set_rules('certificate_amount', 'Cert. Amount', 'required|numeric');
		$this->form_validation->set_rules('surveilance_amount', 'Surv. Amount', 'required|numeric');
		$this->form_validation->set_rules('iso_type', 'Standard', 'required');
		$this->form_validation->set_rules('company_id', 'Company', 'required');

		$post = $this->input->post();
		$view = null;
		if($this->form_validation->run()) {
			$this->load->model('projects');
			$this->load->model('certificates');

			$date_arr = explode('/', $post['request_date']);
			$request_date = $date_arr[2] .'-'.$date_arr[1].'-'.$date_arr[0];

			if (isset($post['project_id'])){ 
				$post['project_id'] = $this->projects->save(array_merge($post, array('id' => $post['project_id'] , 'request_date' => $request_date )));
			}else{
				$post['project_id'] = $this->projects->save(array_merge($post, array('request_date' => $request_date )));
			}

			$post['last_certificate_number'] = $this->certificates->last_certificate_number();


			$result['status'] = '200';
			$html =	'project/new_step_2';

		}else{
			$result['status'] = '100';
			$html =	'project/new_step_1';

			if (strlen($post['company_id']) > 0 ){
				$this->load->model('companies');
				$view['sel_company'] = $this->companies->get_by_id($post['company_id'])  ;
			}
		}

		$this->session->set_userdata(array('post_1' => $post));
		$result['html'] =	$this->load->view($html,$view,true);
		
		echo json_encode($result);
	}

	public function new_step_3(){

	}


}
