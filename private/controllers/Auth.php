<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$this->setFormConf();

		$this->load->view('auth/login');
	}

	function login(){

		if (!$this->input->post('user_login')){
			$this->index();
			return;
		}

		$this->setFormConf();
		
		$this->form_validation->set_error_delimiters('<span class="help-block" style="color: red; font-size: 11px;margin-bottom: 0">* ', '</span>');
		$this->form_validation->set_rules('user_name', 'User Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run()) {
			$this->load->model('employees');
			$user = $this->employees->login($this->input->post('user_name'),$this->input->post('password'));

			if (count($user) > 0 ){
				
				$this->session->set_userdata(array('user' => $user[0] ));
				$success = true;
			} 
			else {
				$success = false;
			}
		} 
		else {
			$success = false;

		}
		$data['error']['login'] = '<span class="help-block" style="color: red">*User Name or and password not match </span>';

		if ($success){
			redirect (base_url('home'),'refresh');
		}
		else {
			$this->load->view('auth/login',@$data);
		}

	}

	function logout() {
		$this->session->unset_userdata('user');
		redirect (base_url('auth'),'refresh');
	}
}
