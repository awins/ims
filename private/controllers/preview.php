 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preview extends MY_Controller {

	public function save_all($project_id)
	{
		ini_set('memory_limit', '128M'); 
		$this->load->model('certificates');
		$project = $this->certificates->get_by_project_id($project_id);

		$name = $project[0]->company_name;
		$name = str_replace(' ', '_', $name);
		$name = str_replace('.', '_', $name);

		$folder = $_SERVER['DOCUMENT_ROOT'] . '/application/public/user_dir/' . $name . '/' ;

		deleteDir($folder);

		if (!mkdir($folder, 0777, true)) {
		    die('Failed to create folder...');
		}

		$destination = $folder . $name .'.zip';

		$zip = new ZipArchive();
		
		if($zip->open($destination,ZIPARCHIVE::CREATE ) !== true) {
		    die('Failed to create zip file...');
		}

		$file_arr = array();

		$file_arr[] =	$this->bantex($project_id, $folder) ;
		$file_arr[] =	$this->cd_cover($project[0]->company_id, $folder) ;
		$file_arr[] =	$this->envelope($project[0]->company_id, $folder) ;
		$file_arr[] =	$this->map_label($project[0]->company_id, $folder) ;
		$file_arr[] =	$this->transmittal($project_id, $folder) ;
		$file_arr[] =	$this->proposal($project_id, $folder) ;
		$file_arr[] =	$this->form_issued($project_id, $folder) ;
		$file_arr[] =	$this->invoice($project_id, $folder) ;

		foreach ($project as $key => $value) {
			$file_arr[] =	$this->certificate($value->certificate_id, $folder) ;
			$file_arr[] =	$this->manual_mutu(isoType($value->iso_type,'number'),$project_id, $folder) ;
			$file_arr[] =	$this->audit(isoType($value->iso_type,'number'),$project_id, $folder) ;
		}

		foreach ($file_arr as $file) {
			if (!is_null($file)) $zip->addFile($folder. $file,$file);
		}
		$zip->close();

		foreach ($file_arr as $file) {
			unlink($folder. $file);
		}
		header('Location: ' .'/application/public/user_dir/'. $name . '/' . $name . '.zip');
	}


	function bantex($project_id,$path = null){
		
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);

		$body = $this->load->view('template/others/bantex',$data,true);
		//echo $body;
		
		$this->load->library('mpdf60/mpdf');
		$mpdf=new mPDF('utf-8','A4','','',10,10,10,10,0,0);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('BANTEX') ;

		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		
		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'bantex.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	function cd_cover($company_id,$path = null){
		$this->load->model('companies');
		$this->load->library('mpdf60/mpdf');
		
		$data['company'] = $this->companies->get_by_id($company_id);
		$body = $this->load->view('template/others/cd_cover',$data,true);
		//echo $body;
		
		$mpdf=new mPDF('utf-8','A4','','',0,0,0,0,0,0);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('CD COVER') ;

		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		
		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'cd_cover.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	function envelope($company_id,$path = null){
		$this->load->model('companies');
		$this->load->library('mpdf60/mpdf');
		
		$data['company'] = $this->companies->get_by_id($company_id);
		$body = $this->load->view('template/others/amplop',$data,true);
		//echo $body;
		
		$mpdf=new mPDF('utf-8','A4','','',0,0,0,0,0,0);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('ENVELOPE LABEL') ;

		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		
		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'envelope.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	function map_label($company_id,$path = null){
		$this->load->model('companies');
		$this->load->library('mpdf60/mpdf');
		
		$data['company'] = $this->companies->get_by_id($company_id);
		$body = $this->load->view('template/others/map_label',$data,true);
		//echo $body;
		
		$mpdf=new mPDF('utf-8','A4','','',0,0,0,0,0,0);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('MAP LABEL') ;

		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		
		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'map_label.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	public function certificate($certificate_id,$path = null){
		$this->load->model('certificates');
		$data['data'] = $this->certificates->get_by_id($certificate_id);
		
		/*$info = 'Certificate NO. ' . $data['data']->certificate_number;
		$info .= '\n';
		$info .= isoType($data['data']->isoType,'text');
		$info .= '\n';
		$info .= $data['data']->company_name;*/



		/*QRcode::png($info, 'application/public/img/qrcode/'. $certificate_id . '.png');*/

		$body = $this->load->view('template/certificate/iso',$data,true);

		$footer = $this->load->view('template/certificate/footer',$data,true);
		$this->load->library('mpdf60/mpdf');


		//$mpdf = new mPDF('', 'A4', '', '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer); 
		$mpdf=new mPDF('utf-8','A4','','',10,10,20,30,20,20);	
		$mpdf->SetDisplayMode(90);		
		$mpdf->title = 'CERTIFICATE' ;
		$mpdf->SetHTMLFooter($footer,'');
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		
		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'certificate_' . isoType($data['data']->iso_type,'number') . '.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}

	}

	public function transmittal($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);

		$body_1 = $this->load->view('template/transmittal/transmittal_1',$data,true);
		$body_2 = $this->load->view('template/transmittal/transmittal_2',null,true);
		$body_3 = $this->load->view('template/transmittal/transmittal_3',null,true);
//		echo $body_1;

		$this->load->library('mpdf60/mpdf');

		$mpdf=new mPDF('utf-8','A4','','',0,0,0,0,0,0);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('TRANSMITTAL') ;
		
		$mpdf->AddPage();
		$mpdf->WriteHTML($body_1);
		
		$mpdf->AddPage();
		$mpdf->WriteHTML($body_2);

		$mpdf->AddPage();
		$mpdf->WriteHTML($body_3);

		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'transmittal.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	public function proposal($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);

		$header = $this->load->view('template/proposal/header',null,true);
		$footer = $this->load->view('template/proposal/footer',$data,true);

		$body_1 = $this->load->view('template/proposal/proposal_1',$data,true);
		$body_2 = $this->load->view('template/proposal/proposal_2',$data,true);
		$body_3 = $this->load->view('template/proposal/proposal_3',$data,true);
		$body_4 = $this->load->view('template/proposal/proposal_4',$data,true);
		/*$body_5 = $this->load->view('template/proposal/proposal_5',$data,true);*/

		//echo $body_3;
		$this->load->library('mpdf60/mpdf');

		$mpdf=new mPDF('utf-8','A4','','',5,5,25,5,0,30);	
		
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('PROPOSAL') ;
		
		$mpdf->AddPage();
		$mpdf->SetHTMLFooter($footer,'');
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML($body_1);
		
		$mpdf->SetHTMLHeader($header);
		
		$mpdf->AddPage();
		$mpdf->SetHTMLFooter('');
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML($body_2);

		$mpdf->AddPage();
		$mpdf->SetHTMLFooter('');
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML($body_3);

		$mpdf->AddPage();
		$mpdf->SetHTMLFooter('');
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML($body_4);

		/*$mpdf->AddPage();
		$mpdf->SetHTMLFooter('');
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML($body_5);*/

		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'proposal.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	public function form_issued($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$body = $this->load->view('template/others/form_issued',$data,true);

//		echo $body;

		$this->load->library('mpdf60/mpdf');

		$mpdf=new mPDF('utf-8','A4-L','','',5,5,5,5,0,30);	
		
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('FORM ISSUED') ;
		
		$mpdf->AddPage();
		
		$mpdf->WriteHTML($body);
		if (is_null($path)) {
			$mpdf->Output();
		}else{
		$file = 'form_issued.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
		

	}

	public function audit($type,$project_id,$path = null){
		switch ($type) {
			case '9001':
				return $this->audit_qms($project_id,$path);
				break;
			case '14001':
				return $this->audit_ems($project_id,$path );
				break;
			case '18001':
				return $this->audit_ohs($project_id,$path );
				break;

			default:
				# code...
				break;
		}
	}

	private function audit_qms($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$data['header'] = "FORM – AUDIT STAGE 1 REPORT";

		$header = $this->load->view('template/audit/9001/header',$data,true);

		$this->load->library('mpdf60/mpdf');
		
		$mpdf=new mPDF('utf-8','A4','','',12,5,40,15,5,60);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('AUDIT 9001') ;
		
		$body =$this->load->view('template/audit/9001/audit_cover',$data,true);
		$mpdf->AddPage();

		$mpdf->WriteHTML($body);
		$mpdf->SetHTMLHeader($header);
		$mpdf->simpleTables = true;


		$body =$this->load->view('template/audit/9001/audit_stage_1_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);
		
		$body =$this->load->view('template/audit/9001/audit_stage_1_2',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		

		$data['header'] = "FORM – AUDIT STAGE 2 REPORT";
		$header = $this->load->view('template/audit/9001/header',$data,true);
		$mpdf->SetHTMLHeader($header);

		$body =$this->load->view('template/audit/9001/audit_stage_2_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);
		
		$body =$this->load->view('template/audit/9001/audit_stage_2_2',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);

		$header = $this->load->view('template/audit/9001/audit_car_header',null,true);
		$mpdf->SetHTMLHeader($header);
		
		$body =$this->load->view('template/audit/9001/audit_car_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);
		
		for ($i=2; $i <= 6 ; $i++) { 
			$body =$this->load->view('template/audit/9001/audit_car_' . $i ,$data,true);
			$mpdf->AddPage();
			$mpdf->WriteHTML($body);
		}

		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'audit_9001.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	private function audit_ems($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$data['header'] = "FORM – AUDIT STAGE 1 REPORT";

		$header = $this->load->view('template/audit/14001/header',$data,true);

		$this->load->library('mpdf60/mpdf');
		
		$mpdf=new mPDF('utf-8','A4','','',12,5,40,15,5,60);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('AUDIT 14001') ;
		
		$body =$this->load->view('template/audit/14001/audit_cover',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);

		$mpdf->SetHTMLHeader($header);
		$mpdf->simpleTables = true;
		
		
		$body =$this->load->view('template/audit/14001/audit_stage_1_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);
		
		$body =$this->load->view('template/audit/14001/audit_stage_1_2',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		

		$data['header'] = "FORM – AUDIT STAGE 2 REPORT";
		$header = $this->load->view('template/audit/14001/header',$data,true);
		$mpdf->SetHTMLHeader($header);

		$body =$this->load->view('template/audit/14001/audit_stage_2_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);
		
		$body =$this->load->view('template/audit/14001/audit_stage_2_2',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);

		$header = $this->load->view('template/audit/14001/audit_car_header',null,true);
		$mpdf->SetHTMLHeader($header);
		
		$body =$this->load->view('template/audit/14001/audit_car_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);
		
		$body =$this->load->view('template/audit/14001/audit_car_2',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);


		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'audit_14001.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	private function audit_ohs($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$data['header'] = "FORM – AUDIT STAGE 1 REPORT";

		$header = $this->load->view('template/audit/18001/header',$data,true);

		$this->load->library('mpdf60/mpdf');
		
		$mpdf=new mPDF('utf-8','A4','','',12,5,40,15,5,60);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('AUDIT 18001') ;
		
		$body =$this->load->view('template/audit/18001/audit_cover',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);

		$mpdf->SetHTMLHeader($header);
		
		$mpdf->simpleTables = true;
		
		
		$body =$this->load->view('template/audit/18001/audit_stage_1_1',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		
		$body =$this->load->view('template/audit/18001/audit_stage_1_2',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);
		

		$data['header'] = "FORM – AUDIT STAGE 2 REPORT";
		$header = $this->load->view('template/audit/18001/header',$data,true);
		$mpdf->SetHTMLHeader($header);

		$body =$this->load->view('template/audit/18001/audit_stage_2_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);
		
		$body =$this->load->view('template/audit/18001/audit_stage_2_2',$data,true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($body);


		$header = $this->load->view('template/audit/18001/audit_car_header',null,true);
		$mpdf->SetHTMLHeader($header);
		
		$body =$this->load->view('template/audit/18001/audit_car_1',$data,true);
		$mpdf->AddPage('','','1','','off');
		$mpdf->WriteHTML($body);

		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'audit_18001.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}




	private function manual_mutu_qms($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$header = $this->load->view('template/manual/9001/header',$data,true);

		$this->load->library('mpdf60/mpdf');
		$mpdf=new mPDF('utf-8','A4','','',20,5,35,10,5,60);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('MANUAL MUTU 9001') ;
		
		$mpdf->SetHTMLHeader($header);
		
		$mpdf->simpleTables = true;
		
		$body =$this->load->view('template/manual/9001/manual_9k_1',$data,true);
		$mpdf->AddPage('','','2','','off');
		$mpdf->WriteHTML($body);

		for ($page=1; $page <= 3 ; $page++) { 
			$body =$this->load->view('template/manual/9001/manual_9k_'. $page,$data,true);
			$mpdf->AddPage();
			$mpdf->WriteHTML($body);
		}

		for ($page=1; $page <= 8 ; $page++) { 
			$body =$this->load->view('template/manual/9001/manual_9k_bab'. $page,$data,true);
			$mpdf->AddPage();
			$mpdf->WriteHTML($body);
		}		

		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'manual_mutu_9001.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	private function manual_mutu_ems($project_id,$path = null){
		
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$header = $this->load->view('template/manual/14001/header',$data,true);
		$footer = $this->load->view('template/manual/14001/footer',$data,true);
			
		
		$this->load->library('mpdf60/mpdf');
		
		$mpdf=new mPDF('utf-8','A4','','',10,5,42,10,5,13);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('MANUAL MUTU 14001') ;

		$mpdf->SetHTMLHeader($header);
		$mpdf->SetHTMLFooter($footer);
		$mpdf->simpleTables = true;

		for ($page=1; $page <= 4 ; $page++) { 
			$body =$this->load->view('template/manual/14001/manual_14k_'. $page,$data,true);
			$mpdf->AddPage();
			$mpdf->WriteHTML($body);
		}

		for ($page=1; $page <= 4 ; $page++) { 
			$body =$this->load->view('template/manual/14001/manual_14k_bab'. $page,$data,true);
			$mpdf->AddPage();
			$mpdf->WriteHTML($body);
		}	

		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'manual_mutu_14001.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}

	private function manual_mutu_ohs($project_id,$path = null){
		
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$header = $this->load->view('template/manual/18001/header',$data,true);
		$footer = $this->load->view('template/manual/18001/footer',$data,true);
		
		$this->load->library('mpdf60/mpdf');
		
		$mpdf=new mPDF('utf-8','A4','','',10,5,42,15,5,10);	
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('MANUAL MUTU 18001') ;

		$mpdf->SetHTMLHeader($header);
		$mpdf->SetHTMLFooter($footer);
		$mpdf->simpleTables = true;

		for ($page=1; $page <= 4 ; $page++) { 
			$body =$this->load->view('template/manual/18001/manual_18k_'. $page,$data,true);
			$mpdf->AddPage();
			$mpdf->WriteHTML($body);
		}

		for ($page=1; $page <=6 ; $page++) { 
			$body =$this->load->view('template/manual/18001/manual_18k_bab'. $page,$data,true);
			$mpdf->AddPage();
			$mpdf->WriteHTML($body);
		}	

		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'manual_mutu_18001.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}


	public function manual_mutu($type, $project_id,$path = null){
		switch ($type) {
			case '9001':
				return $this->manual_mutu_qms($project_id,$path);
				break;
			case '14001':
				return $this->manual_mutu_ems($project_id,$path );
				break;
			case '18001':
				return $this->manual_mutu_ohs($project_id,$path );
				break;

			default:
				# code...
				break;
		}
	}

	public function invoice($project_id,$path = null){
		$this->load->model('certificates');
		$data['project'] = $this->certificates->get_by_project_id($project_id);
		$body = $this->load->view('template/others/invoice',$data,true);

		//echo $body;
		$this->load->library('mpdf60/mpdf');

		$mpdf=new mPDF('utf-8','A4','','',10,10,10,10,0,30);	
		
		$mpdf->SetDisplayMode(100);		
		$mpdf->setTitle('INVOICE') ;
		
		$mpdf->AddPage();
		
		$mpdf->WriteHTML($body);
		if (is_null($path)) {
			$mpdf->Output();
		}else{
			$file = 'invoice.pdf';
			$mpdf->Output($path . $file ,'F');
			return $file;
		}
	}
}