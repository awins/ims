<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends MY_Controller {

	public function index()
	{
		$this->load->model('companies');
        $data['companies'] = $this->companies->get_list();
		$this->load->view('company/index',$data);

	}

	public function save(){
		if ($this->input->post('form_company_input')){
			if ($this->input->post('company_id')) $this->form_validation->set_rules('company_id', 'Company ID', 'xss_clean');
			$this->form_validation->set_rules('company_name', 'Company Name', 'required');
			$this->form_validation->set_rules('company_initial', 'Initial Name', 'required');
			$this->form_validation->set_rules('company_address_1', 'Company Address', 'required');
			$this->form_validation->set_rules('company_address_2', 'Company Address', 'xss_clean');
			$this->form_validation->set_rules('company_city', 'Company City', 'required');
			$this->form_validation->set_rules('company_province', 'Province', 'required');
			$this->form_validation->set_rules('company_zip', 'Zip ', 'required');

			$this->form_validation->set_rules('eac', 'EAC', 'required');
			$this->form_validation->set_rules('eac_detail', 'EAC Detail', 'required');

			$this->form_validation->set_rules('nace', 'NACE', 'required');
			$this->form_validation->set_rules('nace_detail', 'NACE Detail', 'required');

			$this->form_validation->set_rules('agent_id', 'Agent ID', 'required');
			$this->form_validation->set_rules('agent_name', 'Agent', 'required');

			$this->form_validation->set_rules('website_user', 'Website User', 'required');
			$this->form_validation->set_rules('website_password', 'Website Password', 'required');
			$this->form_validation->set_rules('director_name', 'Director Name', 'required');
			$this->form_validation->set_rules('company_phone', 'Phone Number', 'required');
			$this->form_validation->set_rules('company_scope', 'Scope', 'required');
			$this->form_validation->set_rules('company_document', 'Documents', 'xss_clean');
			
			
			if($this->form_validation->run()) {
				
		 		$this->load->model('companies');
		 		if ($this->input->post('company_id')){
		 			$arr_id = array('id' => $this->input->post('company_id') );
		 		} else {
		 			$arr_id = array();
		 		}
				$this->companies->save(array_merge($arr_id,$this->input->post()) );
				redirect('/company');
		 		return true;
	 		} else{
	 			return false;
	 		}
	 	}
	}

	public function  input(){
	 	$data = null;
	 	$this->load->model('eacs');
	 	$this->load->model('naces');
	 	$this->load->model('agents');
		$this->setFormConf();

		if ($this->input->post('form_company_input')){
			if ($this->save() ){
				return;
			}
	 	}

	 	$data['eac'] = $this->eacs->get_list();
	 	$data['nace'] = $this->naces->get_list();
	 	$data['agent'] = $this->agents->get_list();
	 	$view['box_eac'] = $this->load->view('modal/eac',$data,true);
	 	$view['box_nace'] = $this->load->view('modal/nace',$data,true);
	 	$view['box_agent'] = $this->load->view('modal/agent',$data,true);

		$this->load->view('company/new',$view);
	 	
	 	
	}

	public function  update($company_id){
	 	$data = null;
	 	$this->load->model('eacs');
	 	$this->load->model('naces');
	 	$this->load->model('agents');
 		$this->load->model('companies');
		$this->setFormConf();

		if ($this->input->post('form_company_input')){
			if ($this->save()) {
				return;
			}
	 	} else{
		 	$view['company'] = $this->companies->get_by_id($company_id);
	 	}
	 	$data['eac'] = $this->eacs->get_list();
	 	$data['nace'] = $this->naces->get_list();
	 	$data['agent'] = $this->agents->get_list();
	 	
	 	$view['box_eac'] = $this->load->view('modal/eac',$data,true);
	 	$view['box_nace'] = $this->load->view('modal/nace',$data,true);
	 	$view['box_agent'] = $this->load->view('modal/agent',$data,true);

		$this->load->view('company/update',$view);
	 	
	 	
	}

	public function delete(){
		$company_id = $this->input->post('company_id');
		$this->load->model('companies');
		$this->companies->delete($company_id);
		
	}

	public function file_upload(){
		$tmp_file_name = $_FILES['structure_file']['tmp_name'];
		//$ext = getFileExtension($_FILES['structure_file']['name']);
		$ext = 'png';
		$id = $_POST['company_id'];
		
		$file_name = $id .  '.' . $ext;
		
		$file = $_SERVER['DOCUMENT_ROOT'] . '/application/public/img/organitation_structure/' . $file_name;

		$ok = move_uploaded_file($tmp_file_name,  $file);
		if ($ok) {
			$response = '/application/public/img/organitation_structure/' . $file_name;
		} else {
			$response = 100;
		}
		
		echo $response;
		exit;
	}

}
