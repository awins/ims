<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->checkAuth();
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
	}

	public function setFormConf() {
		$this->load->helper('form');
		$this->load->library(array('form_validation'));
		$this->form_validation->set_error_delimiters('<span class="help-block" style="color: red">* ', '</span>');
		$this->form_validation->set_message('required', '%s must be filled');
		$this->form_validation->set_message('numeric', '%s hanya diisi dengan angka');
		$this->form_validation->set_message('matches', 'Password baru tidak cocok');
	}

	private function checkAuth(){
		$this->load->library('session');
		if ($this->uri->segment(1) =='auth') return;
		
		$user = $this->session->userdata('user');		
		if ($user){
			$this->user = $user;
		}
		else {
			redirect (base_url('auth'),'refresh');
		}
	}

	public function checkAccess($access_name){
		$access_list = explode(',', $this->session->user->access_list);
		return in_array($access_name, $access_list) ;

	}

	
	
}