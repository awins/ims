<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {


	var $table = NULL;
    var $scheme = array();

    public function __construct(){
        parent::__construct();
    }


	public function getScheme($data) {
		$arr = array();
		foreach ($data as $key => $value) {
            if (in_array($key, $this->scheme)) {
            	$arr[$key] = $value;
            }
        }
        return $arr;
	}

    
    function getCount($option = array()) {
        $this->db->select('*');
        $this->db->from($this->table);
        
        if (isset($option['where'])) {
            foreach ($option['where'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }   

         $query = $this->db->get();
         return $query->num_rows();

    }

    function set($id, $field, $value){
        $this->db->set($field, $field . '+ ' . $value , FALSE );
        $this->db->where('id', $id);
        $this->db->update($this->table); 
    }

	function save($data) {
        $arr = $this->getScheme($data);
        if (isset($arr['id'])) {
            $this->db->update($this->table, $arr, array('id' => $arr['id']));
            return $arr['id'];
        } else {
            $this->db->insert($this->table,$arr);
            return $this->db->insert_id();
        }
    }

    function get_list($option = array()) {
        
        if (isset($option['distinct'])) {
            $this->db->distinct();
        }

        if (isset($option['select'])) {
            $this->db->select($option['select']);
        } else {
            $this->db->select('*');
        }

        if (isset($option['from'])) {
            $this->db->from($option['from']);
        } else {
            $this->db->from($this->table);
        }

        
        if (isset($option['join'])) {
            foreach ($option['join'] as $key => $value) {
                if (is_array($value)) {
                    $this->db->join($key,$value[0],$value[1]);
                } else {
                    $this->db->join($key,$value);
                }
            }
        }   
		
		if (isset($option['where'])) {
        	foreach ($option['where'] as $key => $value) {
                if (is_array($value)){
                    $this->db->where_in($key,$value);
                } else {
        		  $this->db->where($key,$value);
                }
        	}
        }	

        if (isset($option['group_by'])) {
            $this->db->group_by($option['group_by']);
        }

        if (isset($option['having'])) {
            $this->db->having($option['having']);
        }

        if (isset($option['order_by'])) {
        	$this->db->order_by($option['order_by']);
        }

        if (isset($option['limit'])) {
            $limit = $option['limit'];
            $page = 0;

            if (isset($option['page'])) {
                $page = ($option['page'] - 1) * $limit;
            } 
            $this->db->limit($limit,$page);

        }
		
        $query = $this->db->get();
        return $query->result();
        
    }

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id',$id);
        
        $query = $this->db->get();
        $query = $query->result();

        if (count($query) > 0)
        {
            return $query[0];
        } else {
            return false;
        }
        
    }

    function delete($id){
        $this->db->delete($this->table, array('id' => $id)); 
    }

}