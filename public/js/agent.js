$(function () {
    
    if ($('#table-agent').length > 0){
        var oTableAgent = $('#table-agent').dataTable();
    }

	
    $('.btn-remove').click(function(){
        var conf = confirm('Do you really want to remove this record');
        if (!conf) return;
        var agent_id = $(this).attr('rel');
         $.post('/agent/delete',{agent_id: agent_id}).done(function(data){
            location.reload();
        });
    });

});
