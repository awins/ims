$(function () {
    if ($('#table-project').length > 0){
        $('#table-project').dataTable();
        $('#table-project tr').click(function(){
            var project_id = $(this).attr('rel');
            $.post('/project/detail',{project_id: project_id}).done(function(data){
                $('#box-detail').html(data);
            });

            location.href = "#box-detail";
        });
    }

    var bind_page_1 = function(){
        $(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        
        $('#step-2, #btn-step-1').click(function(){
            $('#form_project_input_1').ajaxSubmit({
                url: '/project/new_step_2',
                type: 'post',
                dataType: 'json',
                success: function(data){
                    $('#box-step').html(data.html);
                    if (data.status == 200){
                        bind_page_2();     
                    }else{
                        bind_page_1();     
                    }
                }
            });
            
        });

        $('#table-company').dataTable();
        $('#select-company').click(function(){
            var company_id = $('input[name="radio_company"]:checked').val();
            
            if (company_id){
                $('#company_id').val(company_id);
                $('#company_name').val($('#company_name_'+ company_id).val());
                $('#company_address_1').val($('#company_address_1_'+ company_id).val());
                $('#company_address_2').val($('#company_address_2_'+ company_id).val());
                $('#company_city').val($('#company_city_'+ company_id).val());
                $('#company_zip').val($('#company_zip_'+ company_id).val());
                $('#website_user').val($('#website_user_'+ company_id).val());
                $('#website_password').val($('#website_password_'+ company_id).val());
                
                $('#eac').val($('#eac_'+ company_id).val());
                $('#nace').val($('#nace_'+ company_id).val());
                $('#director_name').val($('#director_name_'+ company_id).val());
                $('#company_phone').val($('#company_phone_'+ company_id).val());
                $('#agent_name').val($('#agent_name_'+ company_id).val());

                $(this).attr('data-dismiss','modal');

            }else{
                $(this).attr('data-dismiss','');
                alert("no company choosen !!");
            }
        });


    }

    var bind_page_2 = function(){
        $(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        $('#step-1, #btn-back-2').click(function(){
           $.post('/project/new_step_1',{}).done(function(data){
                $('#box-step').html(data);
                bind_page_1();
            }); 
        });

        $('#btn-step-2').click(function(){
            $('#form_project_input_2').ajaxSubmit({
                url: '/project/new_finish',
                type: 'post',
                dataType: 'json',
                success: function(data){
                    $('#box-step').html(data.html);
                    if (data.status == 200){
                        bind_page_3();     
                    }else{
                        bind_page_2();     
                    }
                }
            });
        });

        $('.certificate_date').on('change' ,function(){
            var id = $(this).attr('id').replace('certificate_date_','');
            
            $.ajax({
                url: '/project/populate_date/' + $(this).val(),
                dataType: 'json'
            }).done(function(data){
                console.log(data.main_assessment);
                $('#main_assessment_date_' + id).val(data.main_assessment);
                $('#surveillance_1_date_' + id).val(data.surveillance_1);
                $('#surveillance_2_date_' + id).val(data.surveillance_2);
                $('#renewal_assessment_date_' + id).val(data.renewal_assessment);

            })
        });

    }

    if ($('#form_project_input_1').length > 0){
        bind_page_1();
    }
});