$(function () {
    var eac_reload = function(){
        var sel_eac = $('#text-eac').html();
        var arr = sel_eac.split(',');
        for (var i = 0; i < arr.length; i++) {
            arr[i] = arr[i].replace(' ','');
        }

        $('#table-eac .check_eac').each(function() {
            var text = $(this).attr('rel');

            if ($.inArray(text,arr) > -1){
                $(this).prop('checked',true);
            } else {
                $(this).prop('checked',false);
            }
        });
    }

    var nace_reload = function(){
        var sel_nace = $('#form_company_input #nace').val();
        var arr = sel_nace.split(',');
        for (var i = 0; i < arr.length; i++) {
            arr[i] = arr[i].replace(' ','');
        }
        $('#table-nace .check_nace').each(function() {
            var text = $(this).val();

            if ($.inArray(text,arr) > -1){
                $(this).prop('checked',true);
            } else {
                $(this).prop('checked',false);
            }

        });
    }

    if ($('#table-company').length > 0){
        var oTableCompany = $('#table-company').dataTable();
    } else{
        var oTableNace = $('#table-nace').dataTable({
            "bSort": false,
            "fnDrawCallback": function() {
                nace_reload();
                $('.check_nace').off("click").on("click", function(){
                    var rel = $(this).attr('rel');
                    var arr_nomor = [];
                    var arr_desc = [];

                    if ($('#text-nace-desc').html().length>0) arr_desc =  $('#text-nace-desc').html().split(", ");
                    if ($('#text-nace').html().length>0) arr_nomor = $('#text-nace').html().split(", ") ;

                    if ($(this).prop('checked')){
                        arr_nomor.push($(this).val());
                        arr_desc.push($('#class_' + rel + ' td.description').html());

                    }else{
                        var i = arr_nomor.indexOf($(this).val()) ;
                        arr_nomor.splice(i,1);

                        i = arr_desc.indexOf($('#class_' + rel + ' td.description').html()) ;
                        arr_desc.splice(i,1);

                    }
                    $('#text-nace').html(arr_nomor.join(", "));  
                    $('#text-nace-desc').html(arr_desc.join(", "));
                });
            },
        });

        var oTableEac = $('#table-eac').dataTable({
            "bSort": false,
            "fnDrawCallback": function() {
                eac_reload();
                $('.check_eac').off("click").on("click", function(){
                    var rel = $(this).attr('rel');
                    var arr_nomor = [];
                    var arr_desc = [];
                    if ($('#text-eac-desc').html().length>0) arr_desc =  $('#text-eac-desc').html().split(", ");
                    if ($('#text-eac').html().length>0) arr_nomor = $('#text-eac').html().split(", ") ;

                    if ($(this).prop('checked')){
                        arr_nomor.push(rel);
                        arr_desc.push($('#eac_desc_' + rel).html());
                      
                    }else{
                        var i = arr_nomor.indexOf(rel) ;
                        arr_nomor.splice(i,1);

                        i = arr_desc.indexOf($('#eac_desc_' + rel).html()) ;
                        arr_desc.splice(i,1);
                    }
                    $('#text-eac').html(arr_nomor.join(", "));  
                    $('#text-eac-desc').html(arr_desc.join(", "));
                });
            },
        });

         var oTableAgent = $('#table-agent').dataTable();
    }

	$('#select-agent').click(function(){
        var agent_id = $('input[name="radio_agent"]:checked').val();
        $('#form_company_input #agent_id').val(agent_id);
        $('#form_company_input #agent_name').val($('#agent_name_' + agent_id).html());
    });

    $('#select-nace').click(function(){
        $('#form_company_input #nace').val($('#text-nace').html());
        $('#form_company_input #nace_detail').val($('#text-nace-desc').html());
    });

    $('#select-eac').click(function(){
        $('#form_company_input #eac').val($('#text-eac').html());
        $('#form_company_input #eac_detail').val($('#text-eac-desc').html());
    });

    $('#btn-browse-nace').click(function(){
        $('#text-nace').html($('#form_company_input #nace').val());
        $('#text-nace-desc').html($('#form_company_input #nace_detail').val());
        oTableNace.fnFilter('');
    });

    $('#btn-browse-eac').click(function(){
        $('#text-eac').html($('#form_company_input #eac').val());
        $('#text-eac-desc').html($('#form_company_input #eac_detail').val());
        oTableEac.fnFilter('');

    });

    $('.btn-remove').click(function(){
        var conf = confirm('Do you really want to remove this record');
        if (!conf) return;
        var company_id = $(this).attr('rel');
         $.post('/company/delete',{company_id: company_id}).done(function(data){
            location.reload();
        });
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        $('#img_structure').attr('src','/application/public/img/element/loader.gif');
        $('#form_company_input').ajaxSubmit({
            url: '/company/file_upload',
            type: 'post',
            success: function(data) {
                console.log(data);
                if (Number(data) == 100) 
                {
                    $('#img_structure').attr('src','/application/public/img/organitation_structure/default.png');
                    
                } else {
                    var img = data + '?'+Math.random()
                    $('#img_structure').attr('src',img);
                    

                    /*var rel = document.getElementById('doc_rel').value;
                    if (rel == 'S_') {
                        $('#divnoImage').remove();
                        var div  = '<div style=" float:left;" class="image-thumbnail"  ><img  src="'+  data +  '"/></div><div style=" float: left;"  id="divnoImage" ><img  src="" id ="noImage" /></div>' ,
                        divimg = document.getElementById('div_suggestion');
                        divimg.innerHTML += div;

                    } else {
                        var img  = document.getElementById(rel);
                        img.src = data;// "http://pnc.local/application/style/images/loader.gif"
                        $(img).attr('src', $(img).attr('src')+'?'+Math.random());
                    }*/



                }
            }
        });
    });

    $('.btn-file :file').on('change',function(){
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
    });

});
