$(function () {
    
    
        
    if ($('#table-holiday').length > 0){
        var oTableAgent = $('#table-holiday').dataTable();
    }else{
        $(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    }

	
    $('.btn-remove').click(function(){
        var conf = confirm('Do you really want to remove this record');
        if (!conf) return;
        var holiday_id = $(this).attr('rel');
         $.post('/holiday/delete',{holiday_id: holiday_id}).done(function(data){
            location.reload();
        });
    });

});
