$(function () {
    
    if ($('#table-stock').length > 0){
        $('#table-stock').dataTable();
    }

    if ($('#table-incoming').length > 0){
        $('#table-incoming').dataTable();
        $('#table-incoming tr').click(function(){
            var incoming_id = $(this).attr('rel');
            $.post('/stock/incoming_detail',{incoming_id: incoming_id}).done(function(data){
                $('#box-detail').html(data);
            });

            location.href = "#box-detail";
        });

    }

    if ($('#incoming_po').length > 0){
        $('#browse-po').dataTable({
            "iDisplayLength": 10
        });
        $(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        

        $('#browse-po tr').click(function(){

            var purchase_id = $(this).attr('rel');
            var po_number = $(this).find('td.po_number').html();
            var po_date = $(this).find('td.po_date').html();
            var supplier_name = $(this).find('td.supplier_name').html();

            purchase_id = $.trim(purchase_id);
            if (purchase_id.length == 0 ){
                $('#box-po').modal('hide');
                return;
            }
            po_number = $.trim(po_number);
            supplier_name = $.trim(supplier_name);


            $('#purchase_id').val(purchase_id);
            $('#po_number').val(po_number);
            $('#po_date').val(po_date);
            $('#supplier_name').val(supplier_name);

            $.post('/stock/product_incoming_list/' + purchase_id ,{}).done(function(data){
                $('#box-product').html(data);
                $(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                
                $('.qty').on('input', function(){
                    $(this).val(accounting.formatNumber($(this).val()));
                    input_change($(this));
                });


                $('.btn-delete').click(function(){
                    var rel = $(this).attr('rel');
                    $('#row_item_' + rel).remove();
                });
            });


            $('#box-po').modal('hide');
        });

        input_change = function(elm){
            var rel = elm.attr('rel');

            var order_qty = $('#order_qty_' + rel ).val();
            var qty =  $('#qty_' + rel ).val();
            var diff;
            
            if (qty == ''){
                qty =0;
            }else{
                qty = parseFloat(qty.replace(/,/g,'') );
            }

            diff = qty - order_qty;
            $('#diff_' + rel).val(diff);
        }
        

        $('#btn-save').click(function(){
            $('.alert').hide();
            if (!checkValidData()){
                $('.alert-danger').show();
                return;
            }


            $(this.form).ajaxSubmit({
                url: '/stock/save_incoming',
                type: 'post',
                dataType: 'json',
                success: function(data){
                   location.href = '/stock/incoming_list';
                },
                error: function(data){
                   
                    
                }
            });


        });

        function checkValidData(){
            var valid = true;
            
            if ($('#incoming_date').val().length == 0 ){
                return false;
            }

           
            if ($('.row_item').length == 0){
                return false;
            }

            $('.row_item').each(function(){
                var rel = $(this).attr('rel');
                var qty = $('#qty_' + rel).val();
                var load_number = $('#load_number_' + rel).val();
                var expiry_date = $('#expiry_date_' + rel).val();

                if (qty == ''){
                    qty =0;
                }else{
                    qty = parseFloat(qty.replace(/,/g,'') );
                }

                if (qty <= 0 ){
                    valid= false;
                }

                if (load_number.length <= 0){
                    valid = false;
                }

                if ((expiry_date).length == 0){
                    valid =false;
                } 
            });

            return valid;
        }
    }

});