$(function () {
    if ($('#table-sales-order').length > 0){
        $('#table-sales-order').dataTable();
        $('#table-sales-order tr').click(function(){
            var so_id = $(this).attr('rel');
            $.post('/sales_order/detail',{so_id: so_id}).done(function(data){
                $('#box-detail').html(data);
            });

            location.href = "#box-detail";
        });
    }

    if ($('#sale_create').length > 0){
        $('#browse-stock').dataTable({
            "iDisplayLength": 10
        });
        $(".datemask").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        

        $('#browse-customer tr').click(function(){
            console.log('supp click');
            var customer_id = $(this).attr('rel');
            var customer_name = $(this).find('td.customer_name').html();
            customer_name = $.trim(customer_name);
            $('#customer_id').val(customer_id);
            $('#customer_name').val(customer_name);

            $('#box-customer').modal('hide');
        });

        $('#browse-stock tr').click(function(){
            console.log('stock click');
            var stock_id = $(this).attr('rel');
            var product_number = $(this).find('td.product_number').html();
            var catalog_number = $(this).find('td.catalog_number').html();
            var description = $(this).find('td.description').html();
            var currency = $(this).find('td.price').find('span.currency').html();
            var price = $(this).find('td.price').find('span.price').html();
            var expiry_date = $(this).find('input.expiry_date').val();

            product_number = $.trim(product_number);
            catalog_number = $.trim(catalog_number);
            description = $.trim(description);
            price = $.trim(price);

            $('#stock_id_' + selected_row_id).val(stock_id);
            $('#currency_id_' + selected_row_id).val(currency);
            $('#product_no_' + selected_row_id).val(product_number);
            $('#catalog_number_' + selected_row_id).val(catalog_number);
            $('#description_' + selected_row_id).val(description);
            $('#unit_price_' + selected_row_id).val(price);
            $('#expiry_date_' + selected_row_id).val(expiry_date);
            
            input_change(selected_row_id);

            $('#box-stock').modal('hide');
        });

        $('#btn-add-row').click(function(){
            $.post('/sale/newRow',{}).done(function(data){
                $('#table_detail tr#tr-add-row').before(data);
                $(".datemask").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                
                $('.btn-browse-stock').click(function(){
                    selected_row_id = $(this).attr('rel');
                });

                $('.qty').on('input', function(){
                    var rel = $(this).attr('rel');
                    input_change(rel);
                });

                $('.btn-delete').click(function(){
                    var rel = $(this).attr('rel');
                    $('#row_item_' + rel).remove();
                });

            });
        });

        var input_change = function(elm_rel){
            var qty = $('#qty_' + elm_rel).val();
            var price = $('#unit_price_' + elm_rel).val();
            var grand_total;


            if (!$.isNumeric(qty)){
                qty =0;
            }

            price = parseFloat(price.replace(/,/g,'') );
            /*if (!$.isNumeric(price)){
                price =0;
            }*/

            grand_total = qty * price;

            $('#grand_total_' + elm_rel).val(grand_total); 
            calculateTotal();
        }

        $('.header_input').change(function(){
            calculateTotal();
        })

        function calculateTotal(){

            var rate;
            var netto;
            var discount;
            var shipping_cost;
            var dp;
            var tax,tax_rate,total_tax_rate;

            netto = 0;
            var currency;
            var total_item;
            $('.grand_total').each(function(){
                var elm = $(this).attr('id').replace("grand_total_","");
                currency = $('#currency_id_' + elm).val();

                total_item = parseFloat($(this).val());
                console.log('elm:' + elm );

                if ($.isNumeric($(this).val()) ){
                    if ($( "#currency option:selected" ).text() == "USD" ){
                        if (currency == "USD"){
                            rate = 1;
                        } else {
                            rate = parseFloat($('#rate').val().replace(/,/g,'') );
                        }
                        total_item = total_item / rate;
                    } else {
                        if (currency == "IDR"){
                            rate = 1;
                        } else {
                            rate = parseFloat($('#rate').val().replace(/,/g,'') );
                        }
                        total_item = total_item * rate;
                        
                    }
                }
                netto += total_item;
            });
            $('#sub_total').val(netto);
            
            discount = parseFloat($('#discount').val().replace(/,/g,'') );
            dp = parseFloat($('#dp').val().replace(/,/g,'') );
            shipping_cost = parseFloat($('#shipping_cost').val().replace(/,/g,'') );
            netto = netto - discount - shipping_cost - dp;
            tax = parseFloat($('#tax').val().replace(/,/g,'') );
            tax = (tax/100)*netto; 
            tax_rate = parseFloat($('#tax_rate').val().replace(/,/g,'') );

            total_tax_rate = tax * tax_rate
            netto = netto - total_tax_rate;
        
            $('#total').val(netto);
        }

        $('#btn-save').click(function(){
            $('.alert').hide();
            if (!checkValidData()){
                $('.alert-danger').show();
                return;
            }

            $(this.form).ajaxSubmit({
                url: '/sale/save',
                type: 'post',
                dataType: 'json',
                success: function(data){
                    console.log(data);
                },
                error: function(data){
                    console.log(data);
                    
                }
            });


        });

        function checkValidData(){
            var valid = true;
            if ($('#po_number').val().length == 0 ){
                return false;
            }

            if ($('#po_date').val().length == 0 ){
                return false;
            }

            if ($('#customer_name').val().length == 0 ){
                return false;
            }

            if ($('.row_item').length == 0){
                $('#btn-add-row').click();
                return false;
            }

            $('.row_item').each(function(){
                var rel = $(this).attr('rel');
                var catalog_number = $('#catalog_number_' + rel).val();
                var product_no = $('#product_no_' + rel).val();
                var qty = $('#qty_' + rel).val();
                var price = $('#unit_price_' + rel).val();
                var expiry_date = $('#expiry_date_' + rel).val();

                if ((catalog_number).length == 0){
                    valid =false;
                } 

                if ((product_no).length == 0){
                    valid =false;
                } 

                if (!$.isNumeric(qty)){
                    valid= false;
                }
                if (!$.isNumeric(price)){
                    valid = false;
                }

                if ((expiry_date).length == 0){
                    valid =false;
                } 

                
            });



            if (!valid){
                return false;
            }



            return true;
        }


        $('#btn-add-row').click();
    }

});