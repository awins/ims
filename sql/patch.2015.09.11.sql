ALTER TABLE agent ADD COLUMN agent_email VARCHAR(255);
ALTER TABLE agent ADD COLUMN agent_address VARCHAR(255);
ALTER TABLE agent ADD COLUMN agent_company_name VARCHAR(255);
ALTER TABLE agent ADD COLUMN agent_company_address VARCHAR(255);